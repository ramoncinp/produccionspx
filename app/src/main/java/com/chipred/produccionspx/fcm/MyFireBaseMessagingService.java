package com.chipred.produccionspx.fcm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.NonNull;

import android.os.IBinder;
import android.util.Log;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.alarms.Alarm;
import com.chipred.produccionspx.cars.Trip;
import com.chipred.produccionspx.location.LocationUtils;
import com.chipred.produccionspx.notifications.NotificationsCreator;
import com.chipred.produccionspx.notifications.NotificationsManager;
import com.chipred.produccionspx.service.AlarmReceiver;
import com.chipred.produccionspx.service.LocationUpdatesService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFireBaseMessagingService extends FirebaseMessagingService
{
    public static final String TAG = "Notificaciones";

    private boolean bundledNotifications = Build.VERSION.SDK_INT >= 24;
    private String username = "";
    private String projectId = "";

    // Objetos del servicio de localizacion
    private LocationUpdatesService locationUpdatesService;
    private ServiceConnection serviceConnection;

    @Override
    public void onNewToken(String s)
    {
        super.onNewToken(s);
        Log.v("NewToken", s);
    }

    /**
     * @param remoteMessage: Objeto que contiene la notificacion recibida
     *                       <p>
     *                       El servidor hace un request con la siguiente estructura:
     *                       <p>
     *                       URL: https://fcm.googleapis.com/fcm/send
     *                       Headers:
     *                       Content-type: application/json
     *                       Authorization: key=<serverKey>
     *                       <p>
     *                       Body:
     *                       {
     *                       "to": "/topics/<idCliente>",
     *                       "data":{
     *                       "title":"<TituloNotificacion>",
     *                       "text":"<Mensaje>"
     *                       }
     *                       }
     */

    @Override
    @SuppressWarnings("ConstantConditions")
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage)
    {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "Mensaje recibido: " + remoteMessage.getData().toString());
        Map<String, String> data = remoteMessage.getData();

        String notificationTitle = data.get("title");
        String notificationDesc = data.get("text");
        String userId = data.get("user_id");

        if (data.containsKey("project_id"))
            projectId = data.get("project_id");
        else if (data.containsKey("trip_id"))
            projectId = data.get("trip_id");

        if (notificationDesc != null && notificationTitle != null)
        {
            // Inicializar sharedPreferences
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES,
                    Context.MODE_PRIVATE);

            //Evaluar título de la alarma
            if (notificationTitle.equals(NotificationsManager.NEW_ALARM_PROD) || notificationTitle.equals(NotificationsManager.NEW_ALARM_ADMIN))
            {
                boolean canReceive = false;

                // Evaluar si puede recibir la alarma correspondiente
                canReceive |= sharedPreferences.getBoolean(Constants.RECEIVE_ALARM_PROD, false) && notificationTitle.equals(NotificationsManager.NEW_ALARM_PROD);
                canReceive |= sharedPreferences.getBoolean(Constants.RECEIVE_ALARM_ADMN, false) && notificationTitle.equals(NotificationsManager.NEW_ALARM_ADMIN);

                if (canReceive)
                {
                    // Crear objeto Alarma
                    Alarm alarm = new Alarm();
                    alarm.setId(projectId);
                    alarm.setNombre(notificationDesc);
                    alarm.setEpoch(data.containsKey("epoch") ?
                            Long.parseLong(data.get("epoch")) : 0);

                    // Evaluar si se debe crear o eliminar
                    boolean add = Boolean.parseBoolean(data.get("add"));
                    if (add)
                    {
                        // Crear alarma
                        setAlarm(alarm);
                    }
                    else
                    {
                        // Desprogramar alarma
                        removeAlarm(alarm);
                    }
                }
                else
                {
                    Log.d(TAG, "No puede recibir " + notificationTitle);
                }
            }
            else
            {
                // Evaluar si se modificó el viaje
                /*if (notificationTitle.contains(NotificationsManager.CAR_REQUEST_MODIFIED))
                {
                    // Evaluar el estado del viaje
                    if (notificationDesc.contains(Trip.State.ACTIVA.name().toLowerCase()))
                    {
                        LocationUtils.setCurrentTripId(this, projectId);
                        startLocationService();
                    }
                    else if (notificationDesc.contains(Trip.State.TERMINADA.name().toLowerCase()))
                    {
                        finishLocationService();
                        Log.d(TAG, "Terminando servicio de localizacion");
                    }
                }*/

                // Evaluar usuario
                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                if (firebaseUser != null)
                {
                    if (userId != null)
                    {
                        if (userId.equals(firebaseUser.getUid())) return;
                    }
                    username = firebaseUser.getEmail();
                }

                // Crear NotificationCreator
                NotificationsCreator notificationsCreator = new NotificationsCreator(this);
                notificationsCreator.setElemetId(projectId);
                notificationsCreator.setUsername(username);
                notificationsCreator.setSharedPreferences(sharedPreferences);

                // Evaluar si se puede crear BundledNotifications
                if (bundledNotifications)
                {
                    notificationsCreator.showBundledNotification(notificationTitle,
                            notificationDesc);
                }
                else
                {
                    notificationsCreator.showSimpleNotification(notificationTitle,
                            notificationDesc);
                }
            }
        }

        Log.d("Notificaciones", notificationTitle);
        Log.d("Notificaciones", notificationDesc);
    }

    public void setAlarm(Alarm alarm)
    {
        // Evaluar epoch
        if (alarm.getEpoch() == 0) return;

        // Si el proyecto no esta pagado ni entregado, agregar una alarma de 1 semana
        // Crear intent
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("projectId", alarm.getId());
        intent.putExtra("projectName", alarm.getNombre());

        // Crear Penging intent
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                Integer.parseInt(alarm.getId()), intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Crear AlarmManager
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getEpoch(),
                pendingIntent);
    }

    public void removeAlarm(Alarm alarm)
    {
        // Crear intent
        Intent intent = new Intent(this, AlarmReceiver.class);

        // Crear Penging intent
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                Integer.parseInt(alarm.getId()), intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Crear AlarmManager y cancelar alarma
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        Log.d("Alarma", "Alarma con id " + alarm.getId() + " fue cancelada");
    }

    private void startLocationService()
    {
        serviceConnection = new ServiceConnection()
        {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder)
            {
                Log.d("LocationUpdates", "Conectado a servicio");
                LocationUpdatesService.LocalBinder binder =
                        (LocationUpdatesService.LocalBinder) iBinder;
                locationUpdatesService = binder.getService();
                locationUpdatesService.requestLocationUpdates();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName)
            {
                Log.d("LocationUpdates",
                        "Service disconnected -> " + componentName.getClassName());
                locationUpdatesService = null;
            }
        };

        bindService(new Intent(this, LocationUpdatesService.class), serviceConnection,
                Context.BIND_AUTO_CREATE);
    }

    private void finishLocationService()
    {
        serviceConnection = new ServiceConnection()
        {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder)
            {
                Log.d("LocationUpdates", "Conectado a servicio");
                LocationUpdatesService.LocalBinder binder =
                        (LocationUpdatesService.LocalBinder) iBinder;
                locationUpdatesService = binder.getService();
                locationUpdatesService.removeLocationUpdates();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName)
            {
                Log.d("LocationUpdates",
                        "Service disconnected -> " + componentName.getClassName());
                locationUpdatesService = null;
            }
        };

        bindService(new Intent(this, LocationUpdatesService.class), serviceConnection,
                Context.BIND_AUTO_CREATE);
    }
}
