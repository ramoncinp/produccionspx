package com.chipred.produccionspx.pdf;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.reports.CalendarProject;
import com.chipred.produccionspx.reports.EventType;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class CalendarProjectsGenerator extends Thread {
    private static final String TAG = CalendarProjectsGenerator.class.getSimpleName();

    // Objetos
    private Context context;
    private final Handler handler;
    private JSONArray monthProjects;
    private String monthTitle;

    // Listas
    private final ArrayList<CalendarProject> requestedProjects = new ArrayList<>();
    private final ArrayList<CalendarProject> finishedProjects = new ArrayList<>();
    private final ArrayList<CalendarProject> shippedProjects = new ArrayList<>();
    private final ArrayList<CalendarProject> startedProjects = new ArrayList<>();

    // Crear formato de encabezados
    private final Font tableTitleFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    private final Font normalFont = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);

    public CalendarProjectsGenerator(Handler handler) {
        this.handler = handler;
    }

    public void generateMonthProjectsReport(Context context, String monthTitle,
                                            JSONArray monthProjects) {
        this.context = context;
        this.monthTitle = monthTitle;
        this.monthProjects = monthProjects;
        this.start();
    }

    @Override
    public void run() {
        orderProjects();

        String filePath = "";

        // Obtener directorio
        File directory =
                new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "ReportesProduccion");

        if (!directory.exists()) {
            if (!directory.mkdir()) {
                Log.e(TAG, "Error al crear directorio para almacenar reporte");
            }
        }

        // Adaptar mes
        String nowStr =
                new SimpleDateFormat("_ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());

        String fileName = monthTitle
                .toUpperCase()
                .replace(".", "")
                .replace("/", "_")
                .replace(" ", "");

        fileName += nowStr;

        // Crear archivo
        File pdfFile = new File(directory.getAbsolutePath(),
                "CalendarioProyectos" + fileName + ".pdf");

        try {
            // Crear archivo PDF
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(pdfFile.getAbsolutePath()));
            document.open();

            // Obtener dimensiones de la página
            Rectangle pageSize = document.getPageSize();
            float width = pageSize.getRight();
            float height = pageSize.getTop();

            // Add metadata
            document.setPageSize(PageSize.A4);
            document.addCreationDate();

            // Agregar marca de agua
            Image backgroundWaterMark = getImageFromAsset("img/binarium_fondo.png");
            BackGroundImage backGroundImageEvent = new BackGroundImage(backgroundWaterMark);
            writer.setPageEvent(backGroundImageEvent);
            if (backgroundWaterMark != null) {
                backgroundWaterMark.scalePercent(50);
                backgroundWaterMark.setAbsolutePosition(
                        width / 2 - backgroundWaterMark.getScaledWidth() / 2,
                        height / 2 - backgroundWaterMark.getScaledHeight() / 2
                );
                document.add(backgroundWaterMark);
            }

            // Agregar imagen de título
            Image supraImage = getImageFromAsset("img/supramax_banner.png");
            if (supraImage != null) {
                supraImage.scalePercent(30);
                supraImage.setAlignment(Element.ALIGN_RIGHT);
                document.add(supraImage);
            }

            // Motrar capítulo
            Chunk documentTitleChunk = new Chunk(
                    "PROYECTOS - " + monthTitle.toUpperCase(),
                    new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD)
            );
            Paragraph titleParagraph = new Paragraph(documentTitleChunk);
            titleParagraph.setAlignment(Element.ALIGN_CENTER);
            titleParagraph.setSpacingBefore(10);
            document.add(titleParagraph);

            // Crear tabla
            PdfPTable table = new PdfPTable(3);
            table.setTotalWidth(width - 100);
            table.setLockedWidth(true);
            table.setSpacingBefore(10);

            // Crear contenido
            if (!requestedProjects.isEmpty()) {
                createCategoryTable(table, getTypeDescription(EventType.REQUEST_DATE), requestedProjects);
            }
            if (!finishedProjects.isEmpty()) {
                createCategoryTable(table, getTypeDescription(EventType.FINISH_DATE), finishedProjects);
            }
            if (!shippedProjects.isEmpty()) {
                createCategoryTable(table, getTypeDescription(EventType.SHIPMENT_DATE), shippedProjects);
            }
            if (!startedProjects.isEmpty()) {
                createCategoryTable(table, getTypeDescription(EventType.START_DATE), startedProjects);
            }

            // Agregar tabla
            document.add(table);

            // Agregar fecha
            String nowFooterStr =
                    new SimpleDateFormat("dd/MMMM/yyyy HH:mm:ss", Locale.getDefault()).format(new Date()).toUpperCase();

            Chunk dateTimeChunk = new Chunk(
                    "Fecha de generación: " + nowFooterStr,
                    new Font(Font.FontFamily.HELVETICA, 8)
            );
            Paragraph dateParagraph = new Paragraph(dateTimeChunk);
            dateParagraph.setSpacingBefore(10);
            document.add(dateParagraph);

            // Cerrar documento
            document.close();

            // Definir path de archivo generado
            filePath = pdfFile.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error al crear pdf");
        }

        Message message = new Message();
        message.obj = filePath;
        handler.sendMessage(message);
    }

    private void createCategoryTable(PdfPTable table, String typeDescription, ArrayList<CalendarProject> projects) {
        // Agregar titulos
        Chunk titleChunk = new Chunk(typeDescription);
        titleChunk.setFont(tableTitleFont);
        Phrase titlePhrase = new Phrase(titleChunk);

        PdfPCell titleCell = new PdfPCell();
        titleCell.setColspan(3);
        titleCell.setPhrase(titlePhrase);
        titleCell.setPaddingLeft(8);
        titleCell.setPaddingTop(8);
        titleCell.setPaddingBottom(8);
        titleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        titleCell.disableBorderSide(Rectangle.BOTTOM);
        table.addCell(titleCell);

        // Definir columnas
        String[] columns = new String[]{"ESTACION", "CLIENTE", "FECHA"};
        Font columnsFont = new Font();
        columnsFont.setStyle(Font.BOLD);
        columnsFont.setSize(10);
        columnsFont.setColor(BaseColor.DARK_GRAY);
        for (String column : columns) {
            Chunk columnChunk = new Chunk(column);
            Font subtitleFont = new Font(Font.FontFamily.HELVETICA);
            subtitleFont.setColor(BaseColor.DARK_GRAY);
            columnChunk.setFont(columnsFont);
            Phrase subtitlePhrase = new Phrase(columnChunk);
            PdfPCell subtitleCell = new PdfPCell();
            subtitleCell.setPhrase(subtitlePhrase);
            subtitleCell.setPadding(6);
            subtitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            subtitleCell.setBackgroundColor(BaseColor.WHITE);
            table.addCell(subtitleCell);
        }

        // Escribir eventos del mes
        for (CalendarProject calendarProject : projects) {
            PdfPCell content = new PdfPCell();
            content.setPadding(6);

            Chunk normalChunk = new Chunk(calendarProject.getEstacion());
            normalChunk.setFont(normalFont);
            content.setPhrase(new Phrase(normalChunk));
            table.addCell(content);

            normalChunk = new Chunk(calendarProject.getCliente());
            normalChunk.setFont(normalFont);
            content.setPhrase(new Phrase(normalChunk));
            table.addCell(content);

            normalChunk = new Chunk(
                    Objects.requireNonNull(Constants.dateObjectToString(new Date(calendarProject.getFecha())))
            );
            normalChunk.setFont(normalFont);
            content.setPhrase(new Phrase(normalChunk));
            table.addCell(content);
        }
    }

    private Image getImageFromAsset(String assetPath) {
        try {
            InputStream ims = context.getAssets().open(assetPath);
            Bitmap bm = BitmapFactory.decodeStream(ims);
            return getImageFromBitmap(bm);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Image getImageFromBitmap(Bitmap bm) {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return Image.getInstance(stream.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static class BackGroundImage extends PdfPageEventHelper {
        private final Image image;

        public BackGroundImage(Image image) {
            this.image = image;
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            Rectangle pageSize = document.getPageSize();
            float width = pageSize.getRight();
            float height = pageSize.getTop();

            if (image != null) {
                try {
                    image.scalePercent(50);
                    image.setAbsolutePosition(width / 2 - image.getScaledWidth() / 2,
                            height / 2 - image.getScaledHeight() / 2);
                    document.add(image);
                } catch (DocumentException e) {
                    e.printStackTrace();
                    Log.d(TAG, e.getMessage());
                }
            }
        }
    }

    private void orderProjects() {
        for (int idx = 0; idx < monthProjects.length(); idx++) {
            try {
                JSONObject event = monthProjects.getJSONObject(idx);

                // Crear objeto "projecto"
                String station = event.getString("estacion");
                String client = event.getString("cliente");
                int type = event.getInt("tipo");
                Long date = event.getLong("fecha");

                CalendarProject calendarProject = new CalendarProject(
                        station,
                        client,
                        date,
                        type);

                if (calendarProject.getTipo() == EventType.REQUEST_DATE)
                    requestedProjects.add(calendarProject);
                if (calendarProject.getTipo() == EventType.FINISH_DATE)
                    finishedProjects.add(calendarProject);
                if (calendarProject.getTipo() == EventType.SHIPMENT_DATE)
                    shippedProjects.add(calendarProject);
                if (calendarProject.getTipo() == EventType.START_DATE)
                    startedProjects.add(calendarProject);

            } catch (JSONException e) {
                Message message = new Message();
                message.obj = "";
                handler.sendMessage(message);
            }
        }

        Comparator<CalendarProject> comparator =
                (calendarProject1, t1) -> calendarProject1.getFecha().compareTo(t1.getFecha());

        Collections.sort(requestedProjects, comparator);
        Collections.sort(finishedProjects, comparator);
        Collections.sort(shippedProjects, comparator);
        Collections.sort(startedProjects, comparator);
    }

    private String getTypeDescription(int type) {
        String description = "";

        // Definir nombre
        switch (type) {
            case EventType.REQUEST_DATE:
                description = context.getResources().getString(R.string.event_type_request);
                break;

            case EventType.FINISH_DATE:
                description = context.getResources().getString(R.string.event_type_finish);
                break;

            case EventType.SHIPMENT_DATE:
                description = context.getResources().getString(R.string.event_type_shipment);
                break;

            case EventType.START_DATE:
                description = context.getResources().getString(R.string.event_type_start);
                break;
        }

        return description;
    }
}
