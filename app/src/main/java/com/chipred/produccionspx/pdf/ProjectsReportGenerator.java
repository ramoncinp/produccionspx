package com.chipred.produccionspx.pdf;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.chipred.produccionspx.DispModel;
import com.chipred.produccionspx.projects.ProjectDispensers;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ProjectsReportGenerator extends Thread
{
    private static final String TAG = ProjectsReportGenerator.class.getSimpleName();

    // Objetos
    private Context context;
    private Handler handler;
    private String monthTitle;

    // Listas
    private ArrayList<ProjectDispensers> projectDispensers;

    public ProjectsReportGenerator(Handler handler)
    {
        this.handler = handler;
    }

    public void generateMonthDispensersReport(Context context, String monthTitle,
                                              ArrayList<ProjectDispensers> projectDispensers)
    {
        this.context = context;
        this.monthTitle = monthTitle;
        this.projectDispensers = projectDispensers;
        this.start();
    }

    @Override
    public void run()
    {
        String filePath = "";

        // Obtener directorio
        File directory =
                new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "ReportesProduccion");

        if (!directory.exists())
        {
            if (!directory.mkdir())
            {
                Log.e(TAG, "Error al crear directorio para almacenar reporte");
            }
        }

        // Adaptar mes
        String nowStr =
                new SimpleDateFormat("_ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());

        String fileName = monthTitle
                .toUpperCase()
                .replace(".", "")
                .replace(" ", "");

        fileName += nowStr;

        // Crear archivo
        File pdfFile = new File(directory.getAbsolutePath(), fileName + ".pdf");

        try
        {
            // Crear archivo PDF
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(pdfFile.getAbsolutePath()));
            document.open();

            // Obtener dimensiones de la página
            Rectangle pageSize = document.getPageSize();
            float width = pageSize.getRight();
            float height = pageSize.getTop();

            // Add metadata
            document.setPageSize(PageSize.A4);
            document.addCreationDate();

            // Agregar marca de agua
            Image backgroundWaterMark = getImageFromAsset("img/binarium_fondo.png");
            BackGroundImage backGroundImageEvent = new BackGroundImage(backgroundWaterMark);
            writer.setPageEvent(backGroundImageEvent);
            if (backgroundWaterMark != null)
            {
                backgroundWaterMark.scalePercent(50);
                backgroundWaterMark.setAbsolutePosition(
                        width / 2 - backgroundWaterMark.getScaledWidth() / 2,
                        height / 2 - backgroundWaterMark.getScaledHeight() / 2
                );
                document.add(backgroundWaterMark);
            }

            // Agregar imagen de título
            Image supraImage = getImageFromAsset("img/supramax_banner.png");
            if (supraImage != null)
            {
                supraImage.scalePercent(30);
                supraImage.setAlignment(Element.ALIGN_RIGHT);
                document.add(supraImage);
            }

            // Motrar capítulo
            Chunk documentTitleChunk = new Chunk(
                    "DISPENSARIOS - " + monthTitle.toUpperCase(),
                    new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD)
            );
            Paragraph titleParagraph = new Paragraph(documentTitleChunk);
            titleParagraph.setAlignment(Element.ALIGN_CENTER);
            titleParagraph.setSpacingBefore(10);
            document.add(titleParagraph);

            // Crear tabla
            PdfPTable table = new PdfPTable(4);
            table.setTotalWidth(width - 100);
            table.setLockedWidth(true);
            table.setSpacingBefore(10);

            // Crear formato de encabezados
            Font tableTitleFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
                    BaseColor.BLACK);
            Font tableSubtitleFont = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL,
                    BaseColor.BLACK);
            Font normalFont = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL,
                    BaseColor.BLACK);

            // Definir variable para total de máquinas
            int totalDispensers = 0;

            for (ProjectDispensers project : projectDispensers)
            {
                // Acumular total
                totalDispensers += project.getDispModels().size();

                // Agregar titulos
                Chunk titleChunk =
                        new Chunk(project.getStation() + " - " + project.getDispModels().size());
                titleChunk.setFont(tableTitleFont);
                Phrase titlePhrase = new Phrase(titleChunk);
                Chunk subTitleChunk = new Chunk(project.getClient());
                subTitleChunk.setFont(tableSubtitleFont);
                Phrase subTitlePhrase = new Phrase(subTitleChunk);

                PdfPCell titleCell = new PdfPCell();
                titleCell.setColspan(4);
                titleCell.setPhrase(titlePhrase);
                titleCell.setPaddingLeft(8);
                titleCell.setPaddingTop(8);
                titleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                titleCell.disableBorderSide(Rectangle.BOTTOM);
                table.addCell(titleCell);

                PdfPCell subTitleCell = new PdfPCell();
                subTitleCell.setColspan(4);
                subTitleCell.setPhrase(subTitlePhrase);
                subTitleCell.setPaddingLeft(8);
                subTitleCell.setPaddingBottom(8);
                subTitleCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                subTitleCell.disableBorderSide(Rectangle.TOP);
                table.addCell(subTitleCell);

                // Definir columnas
                String[] columns = new String[]{"SAE", "N.SERIE", "MODELO", "FECHA FAB."};
                Font columnsFont = new Font();
                columnsFont.setStyle(Font.BOLD);
                columnsFont.setSize(10);
                columnsFont.setColor(BaseColor.DARK_GRAY);
                for (String column : columns)
                {
                    Chunk columnChunk = new Chunk(column);
                    Font subtitleFont = new Font(Font.FontFamily.HELVETICA);
                    subtitleFont.setColor(BaseColor.DARK_GRAY);
                    columnChunk.setFont(columnsFont);
                    Phrase subtitlePhrase = new Phrase(columnChunk);
                    PdfPCell subtitleCell = new PdfPCell();
                    subtitleCell.setPhrase(subtitlePhrase);
                    subtitleCell.setPadding(6);
                    subtitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    subtitleCell.setBackgroundColor(BaseColor.WHITE);
                    table.addCell(subtitleCell);
                }

                // Escribir dispensarios del proyecto
                for (DispModel dispModel : project.getDispModels())
                {
                    PdfPCell content = new PdfPCell();
                    content.setPadding(6);

                    Chunk saeChunk = new Chunk(dispModel.getSae());
                    Font saeFont = new Font();
                    saeFont.setColor(dispModel.isSaeOk() ? BaseColor.GREEN : BaseColor.RED);
                    saeFont.setSize(8);
                    saeChunk.setFont(saeFont);
                    content.setPhrase(new Phrase(saeChunk));
                    table.addCell(content);

                    Chunk normalChunk = new Chunk(dispModel.getSerialNumber());
                    normalChunk.setFont(normalFont);
                    content.setPhrase(new Phrase(normalChunk));
                    table.addCell(content);

                    normalChunk = new Chunk(dispModel.getModel());
                    normalChunk.setFont(normalFont);
                    content.setPhrase(new Phrase(normalChunk));
                    table.addCell(content);

                    normalChunk = new Chunk(dispModel.getManufactureDate());
                    normalChunk.setFont(normalFont);
                    content.setPhrase(new Phrase(normalChunk));
                    table.addCell(content);
                }
            }

            // Agregar total
            Font totalsFont = new Font();
            totalsFont.setStyle(Font.BOLD);
            totalsFont.setColor(BaseColor.BLACK);
            totalsFont.setSize(12);

            PdfPCell totalCell = new PdfPCell();
            totalCell.setBackgroundColor(BaseColor.YELLOW);
            totalCell.setPadding(6);

            Chunk totalChunk1 = new Chunk("TOTAL: " + totalDispensers);
            totalChunk1.setFont(totalsFont);
            totalCell.setPhrase(new Phrase(totalChunk1));
            totalCell.setColspan(4);
            table.addCell(totalCell);

            // Agregar tabla
            document.add(table);

            // Agregar fecha
            String nowFooterStr =
                    new SimpleDateFormat("dd/MMMM/yyyy HH:mm:ss", Locale.getDefault()).format(new Date()).toUpperCase();

            Chunk dateTimeChunk = new Chunk(
                    "Fecha de generación: " + nowFooterStr,
                    new Font(Font.FontFamily.HELVETICA, 8)
            );
            Paragraph dateParagraph = new Paragraph(dateTimeChunk);
            dateParagraph.setSpacingBefore(10);
            document.add(dateParagraph);

            // Cerrar documento
            document.close();

            // Definir path de archivo generado
            filePath = pdfFile.getAbsolutePath();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, "Error al crear pdf");
        }

        Message message = new Message();
        message.obj = filePath;
        handler.sendMessage(message);
    }

    private Image getImageFromAsset(String assetPath)
    {
        try
        {
            InputStream ims = context.getAssets().open(assetPath);
            Bitmap bm = BitmapFactory.decodeStream(ims);
            return getImageFromBitmap(bm);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private Image getImageFromBitmap(Bitmap bm)
    {
        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return Image.getInstance(stream.toByteArray());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private static class BackGroundImage extends PdfPageEventHelper
    {
        private Image image;

        public BackGroundImage(Image image)
        {
            this.image = image;
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document)
        {
            Rectangle pageSize = document.getPageSize();
            float width = pageSize.getRight();
            float height = pageSize.getTop();

            if (image != null)
            {
                try
                {
                    image.scalePercent(50);
                    image.setAbsolutePosition(width / 2 - image.getScaledWidth() / 2,
                            height / 2 - image.getScaledHeight() / 2);
                    document.add(image);
                }
                catch (DocumentException e)
                {
                    e.printStackTrace();
                    Log.d(TAG, e.getMessage());
                }
            }
        }
    }
}
