package com.chipred.produccionspx.pcn;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.chipred.produccionspx.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class PcnDocumentActivity extends AppCompatActivity
{
    public static final String URL = "url";

    private String urlString;
    private ProgressBar progressBar;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pcn_document);

        // Esconder Action Bar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().hide();
        }

        progressBar = findViewById(R.id.progress_bar);
        webView = findViewById(R.id.webView);

        urlString = getIntent().getStringExtra(URL);

        initWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView()
    {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageFinished(WebView view, String url)
            {
                webView.loadUrl("javascript:(function() { " +
                        "document.querySelector('[role=\"toolbar\"]').remove();})()");
                progressBar.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
            }
        });

        String url = urlString;
        String decodedUrl = "";
        try
        {
            decodedUrl = URLEncoder.encode(url, "UTF-8"); //Url Convert to UTF-8 It important.
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + decodedUrl);
    }
}