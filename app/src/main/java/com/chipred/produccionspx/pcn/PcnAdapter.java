package com.chipred.produccionspx.pcn;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Date;

public class PcnAdapter extends RecyclerView.Adapter<PcnAdapter.PcnViewHolder> implements View.OnClickListener
{
    // Objetos
    private ArrayList<Pcn> pcns;
    private View.OnClickListener listener;

    PcnAdapter(ArrayList<Pcn> pcns)
    {
        this.pcns = pcns;
    }

    @Override
    @NonNull
    public PcnViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.version_layout,
                parent, false);
        v.setOnClickListener(listener);

        return new PcnViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PcnViewHolder holder, int position)
    {
        Pcn pcn = pcns.get(position);

        String date = Constants.dateObjectToString(new Date(pcn.getDate()));

        holder.title.setVisibility(View.VISIBLE);
        holder.title.setText(pcn.getTitle());
        holder.date.setText(date);
        holder.folio.setText(pcn.getFolio());
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount()
    {
        return pcns.size();
    }

    static class PcnViewHolder extends RecyclerView.ViewHolder
    {
        private TextView folio;
        private TextView date;
        private TextView title;

        PcnViewHolder(View itemView)
        {
            super(itemView);

            folio = itemView.findViewById(R.id.version_name);
            date = itemView.findViewById(R.id.version_date);
            title = itemView.findViewById(R.id.version_md5);
        }
    }
}
