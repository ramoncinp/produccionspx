package com.chipred.produccionspx.pcn;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Pcn
{
    private long date;
    private String id;
    private String fileUrl;
    private String folio;
    private String title;

    public Pcn()
    {
    }

    public Pcn(JSONObject jsonObject)
    {
        try
        {
            if (jsonObject.has("id")) id = jsonObject.getString("id");
            if (jsonObject.has("title")) title = jsonObject.getString("title");
            if (jsonObject.has("date")) date = jsonObject.getLong("date");
            if (jsonObject.has("fileUrl")) fileUrl = jsonObject.getString("fileUrl");
            if (jsonObject.has("folio")) folio = jsonObject.getString("folio");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public long getDate()
    {
        return date;
    }

    public void setDate(long date)
    {
        this.date = date;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getFileUrl()
    {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl)
    {
        this.fileUrl = fileUrl;
    }

    public String getFolio()
    {
        return folio;
    }

    public void setFolio(String folio)
    {
        this.folio = folio;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    @NonNull
    @Override
    public String toString()
    {
        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put("id", id);
            jsonObject.put("title", title);
            jsonObject.put("date", date);
            jsonObject.put("fileUrl", fileUrl);
            jsonObject.put("folio", folio);
        }
        catch (JSONException e)
        {
            return super.toString();
        }

        return jsonObject.toString();
    }

    public Map<String, Object> toMap()
    {
        Map<String, Object> mMap = new HashMap<>();
        mMap.put("id", id);
        mMap.put("title", title);
        mMap.put("date", date);
        mMap.put("fileUrl", fileUrl);
        mMap.put("folio", folio);

        return mMap;
    }
}
