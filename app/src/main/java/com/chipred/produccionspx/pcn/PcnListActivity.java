package com.chipred.produccionspx.pcn;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class PcnListActivity extends AppCompatActivity
{
    private static final String TAG = PcnListActivity.class.getSimpleName();
    public static final String CAN_WRITE = "canWrite";

    // Variables
    private boolean canWrite = false;

    // Objetos
    private ArrayList<Pcn> pcns = new ArrayList<>();
    private CollectionReference pcnRef = FirebaseFirestore.getInstance().collection("pcn");
    private ProgressDialog progressDialog;

    // Views
    private FloatingActionButton addPcnButton;
    private ProgressBar progressBar;
    private RecyclerView pcnList;
    private TextView noPcns;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pcn_list);
        setTitle("PCN");

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        // Obtener parámetros
        canWrite = getIntent().getBooleanExtra(CAN_WRITE, false);

        // Inicializar vistas
        initViews();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        getPcn();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (progressDialog != null && progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }

    private void initViews()
    {
        addPcnButton = findViewById(R.id.add_pcn_button);
        pcnList = findViewById(R.id.pcn_list);
        progressBar = findViewById(R.id.progress_bar);
        noPcns = findViewById(R.id.no_pcn);

        addPcnButton.setVisibility(canWrite ? View.VISIBLE : View.GONE);
        addPcnButton.setOnClickListener(view ->
        {
            Intent newPcnFile = new Intent(PcnListActivity.this, PcnFileActivity.class);
            newPcnFile.putExtra(CAN_WRITE, canWrite);
            startActivity(newPcnFile);
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getPcn()
    {
        pcns.clear();
        pcnRef.get().addOnCompleteListener(this, task ->
        {
            if (task.isSuccessful())
            {
                if (task.getResult() != null)
                {
                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments())
                    {
                        Pcn pcn = documentSnapshot.toObject(Pcn.class);
                        if (pcn != null)
                        {
                            pcn.setId(documentSnapshot.getId());
                            pcns.add(pcn);
                        }
                    }

                    if (pcns.isEmpty())
                    {
                        showNoPcns();
                    }
                    else
                    {
                        showPcns(pcns);
                    }
                }
                else
                {
                    showNoPcns();
                }
            }
            else
            {
                Toast.makeText(this, "Error al obtener registros de PCN", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void showPcns(ArrayList<Pcn> pcns)
    {
        addPcnButton.setVisibility(View.VISIBLE);
        noPcns.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        pcnList.setVisibility(View.VISIBLE);

        // Crear adaptador
        PcnAdapter pcnAdapter = new PcnAdapter(pcns);
        pcnAdapter.setOnClickListener(view ->
        {
            progressDialog = ProgressDialog.show(PcnListActivity.this, "", "Cargando...");

            Intent newPcnFile = new Intent(PcnListActivity.this, PcnFileActivity.class);
            newPcnFile.putExtra(CAN_WRITE, canWrite);
            newPcnFile.putExtra(PcnFileActivity.PCN_DATA,
                    pcns.get(pcnList.getChildAdapterPosition(view)).toString());
            startActivity(newPcnFile);
        });

        // Asignar adaptador
        pcnList.setAdapter(pcnAdapter);
        pcnList.setLayoutManager(new LinearLayoutManager(this));
    }

    private void showNoPcns()
    {
        addPcnButton.setVisibility(View.VISIBLE);
        noPcns.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        pcnList.setVisibility(View.GONE);
    }
}