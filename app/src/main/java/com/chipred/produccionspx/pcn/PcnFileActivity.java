package com.chipred.produccionspx.pcn;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.signs.SignDialogFragment;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static com.chipred.produccionspx.Constants.emptyValidator;

public class PcnFileActivity extends AppCompatActivity
{
    // Constantes
    private static final String TAG = PcnFileActivity.class.getSimpleName();
    public static final String PCN_DATA = "pcnData";
    private static final int PICK_PDF_CODE = 11;

    // Variables
    private boolean canWrite;

    // Objetos
    private CollectionReference pcnRef = FirebaseFirestore.getInstance().collection("pcn");
    private FirebaseStorage firebaseStorage;
    private Pcn pcn;

    // Views
    private CardView pcnDataCv;
    private CardView pdfCv;
    private CardView fullScreenCv;
    private ProgressBar progressBar;
    private ProgressBar pdfProgressBar;
    private ImageView cloudImageView;
    private WebView webView;
    private MaterialEditText pcnTitle;
    private MaterialEditText pcnDay;
    private MaterialBetterSpinner pcnMonth;
    private MaterialEditText pcnYear;
    private MaterialEditText pcnFolio;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pcn_file);
        setTitle("PCN");

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Obtener permiso para editar
        canWrite = getIntent().getBooleanExtra(PcnListActivity.CAN_WRITE, false);

        // Inicializar views
        initViews();

        // Obtener datos de PCN
        String pcnData = getIntent().getStringExtra(PCN_DATA);
        initPcn(pcnData);
    }

    private void initViews()
    {
        pcnDataCv = findViewById(R.id.pcn_data_card_view);
        pdfCv = findViewById(R.id.pdf_card_view);
        webView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progress_bar);
        pdfProgressBar = findViewById(R.id.pdf_progress_bar);
        cloudImageView = findViewById(R.id.upload_icon);
        cloudImageView.setOnClickListener(view -> pickPdfDocument());

        pcnTitle = findViewById(R.id.pcn_title_et);
        pcnFolio = findViewById(R.id.pcn_folio_et);
        pcnDay = findViewById(R.id.pcn_day_et);
        pcnMonth = findViewById(R.id.pcn_month_spinner);
        pcnYear = findViewById(R.id.pcn_year_et);
        fullScreenCv = findViewById(R.id.full_screen_cv);
        fullScreenCv.setOnClickListener(view ->
        {
            if (webView.getVisibility() == View.VISIBLE)
            {
                showFullScreenDocument();
            }
        });

        // Inicializar spinner
        pcnMonth.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1,
                Constants.getStringMonths()));
    }

    private void initPcn(String pcnData)
    {
        try
        {
            pcn = new Pcn(new JSONObject(pcnData));
            setPcnData();
        }
        catch (JSONException | NullPointerException e)
        {
            e.printStackTrace();
        }

        if (pcn == null)
        {
            showUploadIcon();
        }
    }

    private void setPcnData()
    {
        // Escribir datos en views
        pcnTitle.setText(pcn.getTitle());
        pcnFolio.setText(pcn.getFolio());

        // Obtener fecha y convertirla a String
        String dateStr = Constants.dateObjectToString(new Date(pcn.getDate()));
        if (dateStr != null)
        {
            // Separarla la cadena por "/"
            String[] dateElements = dateStr.split("/");

            // Setear datos
            pcnDay.setText(dateElements[0]);
            pcnMonth.setText(dateElements[1]);
            pcnYear.setText(dateElements[2]);

            // Redefinir adaptador
            pcnMonth.setAdapter(new ArrayAdapter<>(this,
                    android.R.layout.simple_expandable_list_item_1,
                    Constants.getStringMonths()));
        }

        if (pcn.getFileUrl() == null || pcn.getFileUrl().isEmpty())
        {
            showUploadIcon();
        }
        else
        {
            initWebView();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == PICK_PDF_CODE && data != null)
            {
                // Mostrar progressBar
                showProgress();

                // Obtener path del archivo seleccionado
                Uri fileUri = data.getData();
                uploadPcnFile(fileUri);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.version_form_menu, menu);

        // Obtener elementos del menú
        MenuItem saveMenuItem = menu.findItem(R.id.save);
        saveMenuItem.setVisible(canWrite);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        else if (item.getItemId() == R.id.save)
        {
            if (savePcn())
                showProgress();
        }
        else if (item.getItemId() == R.id.vobo_sign)
        {
            if (pcn != null)
            {
                showSignDialogFragment();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView()
    {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageFinished(WebView view, String url)
            {
                webView.loadUrl("javascript:(function() { " +
                        "document.querySelector('[role=\"toolbar\"]').remove();})()");
                showWebView();
            }
        });

        String url = pcn.getFileUrl();
        String decodedUrl = "";
        try
        {
            decodedUrl = URLEncoder.encode(url, "UTF-8"); //Url Convert to UTF-8 It important.
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + decodedUrl);
    }

    private void showFullScreenDocument()
    {
        Intent intent = new Intent(this, PcnDocumentActivity.class);
        intent.putExtra(PcnDocumentActivity.URL, pcn.getFileUrl());
        startActivity(intent);
    }

    private boolean savePcn()
    {
        // Validar
        boolean isValid = pcnTitle.validateWith(emptyValidator);
        isValid &= pcnFolio.validateWith(emptyValidator);
        isValid &= pcnDay.validateWith(new METValidator("Día no válido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                if (text.length() == 0 || text.length() > 2)
                {
                    return false;
                }
                else
                {
                    // Obtener versión Int
                    int val = Integer.parseInt(text.toString());
                    return val > 0 && val <= 31;
                }
            }
        });
        isValid &= pcnMonth.validateWith(emptyValidator);
        isValid &= pcnYear.validateWith(new METValidator("Año no válido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text,
                                   boolean isEmpty)
            {
                if (text.length() == 0 || text.length() > 4)
                {
                    return false;
                }
                else
                {
                    // Obtener versión Int
                    int val = Integer.parseInt(text.toString());
                    return val > 2000;
                }
            }
        });

        // Validar
        if (!isValid) return false;

        // Obtener datos generales
        String title = pcnTitle.getText().toString();
        String folio = pcnFolio.getText().toString();

        // Construir fecha
        String day = pcnDay.getText().toString();
        String month = pcnMonth.getText().toString();
        String year = pcnYear.getText().toString();
        String date = String.format(Locale.getDefault(), "%s/%s/%s", day, month, year);
        Date dateObj = Constants.dateStringToObject2(date);

        // True para agregar, false para editar
        final Pcn mPcn = new Pcn();
        mPcn.setTitle(title);
        mPcn.setFolio(folio);
        if (dateObj != null) mPcn.setDate(dateObj.getTime());

        // Evaluar si se tiene que editar o agregar
        if (pcn != null)
        {
            // Editar
            mPcn.setId(pcn.getId());
            mPcn.setFileUrl(pcn.getFileUrl());
            pcnRef.document(mPcn.getId()).update(mPcn.toMap()).addOnCompleteListener(task ->
            {
                // Asignar a objeto global
                onTransactionCompleted(mPcn, "PCN Modificado");
            });
        }
        else
        {
            // Agregar
            pcnRef.add(mPcn).addOnCompleteListener(task ->
            {
                // Asignar a objeto global
                mPcn.setId(task.getResult().getId());
                onTransactionCompleted(mPcn, "PCN Registrado");
            });
        }

        return true;
    }

    private void onTransactionCompleted(Pcn mPcn, String message)
    {
        pcn = mPcn;
        Toast.makeText(PcnFileActivity.this, message, Toast.LENGTH_SHORT).show();
        showContent();
    }

    private void showProgress()
    {
        progressBar.setVisibility(View.VISIBLE);
        pcnDataCv.setVisibility(View.GONE);
        pdfCv.setVisibility(View.GONE);
    }

    private void showContent()
    {
        progressBar.setVisibility(View.GONE);
        pcnDataCv.setVisibility(View.VISIBLE);
        pdfCv.setVisibility(View.VISIBLE);
    }

    private void showWebViewProgress()
    {
        pdfProgressBar.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);
        cloudImageView.setVisibility(View.GONE);
        fullScreenCv.setVisibility(View.GONE);
    }

    private void showWebView()
    {
        pdfProgressBar.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
        cloudImageView.setVisibility(View.GONE);
        fullScreenCv.setVisibility(View.VISIBLE);
    }

    private void showUploadIcon()
    {
        pdfProgressBar.setVisibility(View.GONE);
        webView.setVisibility(View.GONE);
        cloudImageView.setVisibility(View.VISIBLE);
        fullScreenCv.setVisibility(View.GONE);
    }

    private void showFileOptions()
    {
        String[] options = {"Ver documento", "Subir documento"};
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(this);

        // Crear adaptador para mostrar opciones
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1);

        // Agregar opciones
        arrayAdapter.add(options[0]);
        arrayAdapter.add(options[1]);

        // Definir clickListener
        builder.setAdapter(arrayAdapter, (dialog, which) ->
        {
            // Obtener opcion seleccionada
            String selectedOption = arrayAdapter.getItem(which);

            if (selectedOption != null)
            {
                if (selectedOption.equals(options[0]))
                {
                    // Mostrar documento en pantalla completa
                    showFullScreenDocument();
                }
                else if (selectedOption.equals(options[1]))
                {
                    pickPdfDocument();
                }
            }

            dialog.dismiss();
        });

        // Mostrar
        builder.show();
    }

    private void pickPdfDocument()
    {
        if (pcn == null)
        {
            return;
        }

        // Crear intent tipo "Action pick"
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

        // Seleccionar tipo de archivo
        intent.setType("application/pdf");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Launching the Intent
        startActivityForResult(Intent.createChooser(intent, "Seleccione archivo"),
                PICK_PDF_CODE);
    }

    private void uploadPcnFile(Uri uri)
    {
        if (firebaseStorage == null) firebaseStorage = FirebaseStorage.getInstance();

        // Obtener referencia
        StorageReference storageReference =
                firebaseStorage.getReference().child("pcns/" + pcn.getId());

        // Subir archivo
        UploadTask uploadTask = storageReference.putFile(uri);

        // Definir proceso
        uploadTask.continueWithTask(task ->
        {
            // Manejar primer resultado
            if (!task.isSuccessful())
            {
                Toast.makeText(PcnFileActivity.this, "Error al subir archivo",
                        Toast.LENGTH_SHORT).show();
                throw Objects.requireNonNull(task.getException());
            }

            return storageReference.getDownloadUrl();
        }).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                // Obtener Uri del archivo recien subido
                Uri downloadUri = task.getResult();

                // Validar Uri
                if (downloadUri != null)
                {
                    // Asignar al pcn
                    pcn.setFileUrl(downloadUri.toString());

                    // Actualizar documento
                    savePcn();

                    // Mostrar documento
                    showWebViewProgress();
                    initWebView();
                }
            }
            else
            {
                Toast.makeText(PcnFileActivity.this, "Error al subir archivo",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showSignDialogFragment()
    {
        SignDialogFragment signDialogFragment = new SignDialogFragment();
        signDialogFragment.setData(pcn.getId());
        signDialogFragment.show(getSupportFragmentManager(), "pcnSign");
    }
}