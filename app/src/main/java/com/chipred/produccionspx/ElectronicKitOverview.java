package com.chipred.produccionspx;

public class ElectronicKitOverview
{
    private String kitNumber;
    private String client;
    private String station = "";
    private String projectId;
    private String userId;
    private String userInitials;
    private Long epoch;

    public ElectronicKitOverview(String kitNumber)
    {
        this.kitNumber = kitNumber;
    }

    public String getKitNumber()
    {
        return kitNumber;
    }

    public void setKitNumber(String kitNumber)
    {
        this.kitNumber = kitNumber;
    }

    public String getClient()
    {
        return client;
    }

    public void setClient(String client)
    {
        this.client = client;
    }

    public String getStation()
    {
        return station;
    }

    public void setStation(String station)
    {
        this.station = station;
    }

    public Long getEpoch()
    {
        return epoch;
    }

    public void setEpoch(Long epoch)
    {
        this.epoch = epoch;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserInitials()
    {
        return userInitials;
    }

    public void setUserInitials(String userInitials)
    {
        this.userInitials = userInitials;
    }
}
