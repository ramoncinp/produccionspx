package com.chipred.produccionspx.notifications;

import android.util.Log;

import androidx.annotation.NonNull;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.alarms.Alarm;
import com.chipred.produccionspx.cars.Trip;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class NotificationsManager
{
    //Constantes
    private final static String NEW_PROJECT = "Nuevo proyecto";
    private final static String NEW_SIGN = "Nueva firma";
    private final static String MODIFIED_PROJECT = "Proyecto modificado";
    private final static String NEW_MESSAGE = "Nuevo mensaje";
    public final static String NEW_ALARM_PROD = "Nueva alarma produccion";
    public final static String NEW_ALARM_ADMIN = "Nueva alarma administracion";
    public final static String NEW_CAR_REQUEST = "Nueva salida solicitada";
    public final static String CAR_REQUEST_MODIFIED = "Salida modificada";
    private final static String URL = "https://fcm.googleapis.com/fcm/send";

    //Objetos
    private final OnMessageReceived onMessageReceived;

    public NotificationsManager(OnMessageReceived onMessageReceived)
    {
        this.onMessageReceived = onMessageReceived;
    }

    public void onNewProject(String project, String projectId)
    {
        //Crear request
        JSONObject data = new JSONObject();
        try
        {
            data.put("title", NEW_PROJECT);
            data.put("text", project);
            data.put("project_id", projectId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        // Crear notificacion
        Notification notification = new Notification();
        notification.setEpoch(new Date().getTime());
        notification.setTitulo(NEW_PROJECT);
        notification.setMensaje(project);
        notification.setTipo(Notification.NEW_PROJECT_GROUP);
        notification.setProjectId(projectId);

        sendRequest(data, Constants.PROJECTS_TOPIC);
        addNotificationToDb(notification);
    }

    public void onProjectModified(String user, String project, String projectId)
    {
        //Crear request
        JSONObject data = new JSONObject();
        try
        {
            data.put("title", MODIFIED_PROJECT);
            data.put("text", user + " - " + project);
            data.put("project_id", projectId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        // Crear notificacion
        Notification notification = new Notification();
        notification.setEpoch(new Date().getTime());
        notification.setTitulo(MODIFIED_PROJECT);
        notification.setMensaje(user + " - " + project);
        notification.setTipo(Notification.PROJECT_MODIFIED_GROUP);
        notification.setProjectId(projectId);

        sendRequest(data, Constants.PROJECTS_TOPIC);
        addNotificationToDb(notification);
    }

    public void onNewSign(String user, String project, String projectId)
    {
        //Crear request
        final JSONObject data = new JSONObject();
        try
        {
            data.put("title", NEW_SIGN);
            data.put("text", user + " - " + project);
            data.put("project_id", projectId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        // Crear notificacion
        Notification notification = new Notification();
        notification.setEpoch(new Date().getTime());
        notification.setTitulo(NEW_SIGN);
        notification.setMensaje(user + " - " + project);
        notification.setTipo(Notification.SIGN_GROUP);
        notification.setProjectId(projectId);

        sendRequest(data, Constants.PROJECTS_TOPIC);
        addNotificationToDb(notification);
    }

    public void onNewAlarm(Alarm alarm, String type, boolean add)
    {
        //Crear request
        JSONObject data = new JSONObject();
        try
        {
            data.put("title", type);
            data.put("text", alarm.getNombre());
            data.put("project_id", alarm.getId());
            data.put("epoch", alarm.getEpoch());
            data.put("add", add);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        sendRequest(data, Constants.ALARMS_TOPIC);
    }

    public void onNewMessage(String message)
    {
        //Crear request
        JSONObject data = new JSONObject();
        try
        {
            data.put("title", NEW_MESSAGE);
            data.put("text", message);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        // Crear notificacion
        Notification notification = new Notification();
        notification.setEpoch(new Date().getTime());
        notification.setTitulo(NEW_MESSAGE);
        notification.setMensaje(message);
        notification.setTipo(Notification.SPX);
        notification.setProjectId("");

        sendRequest(data, Constants.PROJECTS_TOPIC);
        addNotificationToDb(notification);
    }

    public void onNewCarRequest(Trip trip)
    {
        //Crear request
        JSONObject data = new JSONObject();
        try
        {
            String tripText = trip.getPlaces().get(0) + " - " + trip.getUserName();
            data.put("title", NEW_CAR_REQUEST);
            data.put("text", tripText);
            data.put("trip_id", trip.getId());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        sendRequest(data, Constants.VEHICLE_ADMIN_TOPIC);
    }

    public void onNewGeneralCarRequest(Trip trip)
    {
        //Crear request
        JSONObject data = new JSONObject();
        try
        {
            String tripText = trip.getPlaces().get(0) + " - " + trip.getUserName();
            data.put("title", NEW_CAR_REQUEST);
            data.put("text", tripText);
            data.put("trip_id", trip.getId());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        sendRequest(data, Constants.VEHICLE_USER_TOPIC);
    }

    public void onCarRequestNewState(Trip trip)
    {
        //Crear request
        JSONObject data = new JSONObject();
        try
        {
            String tripText = "Salida a " + trip.getPlaces().get(0);
            tripText += " se encuentra ";
            tripText += trip.getCurrentState().toLowerCase().replace('_', ' ');

            data.put("title", CAR_REQUEST_MODIFIED);
            data.put("text", tripText);
            data.put("trip_id", trip.getId());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        sendRequest(data, Constants.VEHICLE_USER_TOPIC + "_" + trip.getResponsibleUserId());
    }

    private void addNotificationToDb(Notification notification)
    {
        FirebaseFirestore.getInstance().collection("notificaciones").add(notification).addOnCompleteListener(new OnCompleteListener<DocumentReference>()
        {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task)
            {
                if (task.isSuccessful())
                {
                    Log.d("Notificaciones", "Notificacion agregada correctamente");
                }
                else
                {
                    Log.d("Notificaciones", "Error al registrar notificacion a base de datos");
                }
            }
        });
    }

    private void sendRequest(final JSONObject request, String topic)
    {
        String userId = "";

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
        {
            userId = user.getUid();
        }

        //Agregar destinatario
        final JSONObject root = new JSONObject();
        try
        {
            request.put("user_id", userId);
            root.put("to", "/topics/" + topic);
            root.put("data", request);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            onMessageReceived.onError("Error al crear notificacion");
            return;
        }

        Thread task = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody body = RequestBody.create(mediaType, root.toString());
                    com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder()
                            .url(URL)
                            .post(body)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Authorization", "key=AAAAwb05MoE" +
                                    ":APA91bF1WBgXllk0189qUmnkLFoiZ8mAsD8phcgQt_Ep_M0t7kfNfmr33Wv_aU9Owx41aA6Rh5TGQHSVcpH3x1lgVBddLtkL22dX59d6g5Pxfe7aEfKUya1G_PTOh0_cDoVNUEyKLrDs")
                            .addHeader("cache-control", "no-cache")
                            .build();

                    com.squareup.okhttp.Response response = client.newCall(request).execute();
                    onMessageReceived.onSuccess("Notificacion enviada correctamente");
                    Log.d("Notifications", response.body().string());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        //Iniciar actividad
        task.start();
    }

    public interface OnMessageReceived
    {
        void onSuccess(String message);

        void onError(String errorMsg);
    }
}
