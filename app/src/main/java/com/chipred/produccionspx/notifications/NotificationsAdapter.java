package com.chipred.produccionspx.notifications;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Date;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationViewHolder> implements View.OnClickListener
{
    private ArrayList<Notification> notifications;
    private View.OnClickListener listener;

    NotificationsAdapter(ArrayList<Notification> notifications)
    {
        this.notifications = notifications;
    }

    @Override
    @NonNull
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_layout,
                parent, false);
        v.setOnClickListener(listener);

        return new NotificationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position)
    {
        Notification notification = notifications.get(position);

        // Mostrar datos principales
        holder.title.setText(notification.getTitulo());
        holder.message.setText(notification.getMensaje());

        // Obtener fecha
        Date mDate = new Date();
        mDate.setTime(notification.getEpoch());

        // Convertir fecha a String
        String date = Constants.dateObjectToString2(mDate);
        if (date != null)
            date = date.replace(" ", "\n");

        // Definir fecha
        holder.date.setText(date);

        // Elegir icono
        switch (notification.getTipo())
        {
            case Notification.SPX:
                holder.icon.setImageResource(R.drawable.ic_binarium_black);
                break;

            case Notification.NEW_PROJECT_GROUP:
            case Notification.PROJECT_MODIFIED_GROUP:
                holder.icon.setImageResource(R.drawable.ic_gas_black);
                break;

            case Notification.SIGN_GROUP:
                holder.icon.setImageResource(R.drawable.ic_sign_in_black);
                break;
        }
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount()
    {
        return notifications.size();
    }

    static class NotificationViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView icon;
        private TextView title;
        private TextView message;
        private TextView date;

        NotificationViewHolder(View itemView)
        {
            super(itemView);

            icon = itemView.findViewById(R.id.version_element_icon);
            title = itemView.findViewById(R.id.notification_title);
            message = itemView.findViewById(R.id.notification_message);
            date = itemView.findViewById(R.id.notification_date);
        }
    }
}
