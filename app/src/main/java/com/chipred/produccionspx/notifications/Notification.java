package com.chipred.produccionspx.notifications;

public class Notification
{
    public static final int SPX = 0;
    public static final int NEW_PROJECT_GROUP = 1;
    public static final int PROJECT_MODIFIED_GROUP = 2;
    public static final int SIGN_GROUP = 3;
    public static final int PROJECT_ALARM = 4;
    public static final int NOTIFICATIONS_MENU = 5;
    public static final int ALARMS_MENU = 6;
    public static final int VEHICLE_NOTIFICATION = 7;

    private int tipo;
    private long epoch;
    private String mensaje;
    private String projectId;
    private String titulo;

    public Notification()
    {

    }

    public int getTipo()
    {
        return tipo;
    }

    public void setTipo(int tipo)
    {
        this.tipo = tipo;
    }

    public long getEpoch()
    {
        return epoch;
    }

    public void setEpoch(long epoch)
    {
        this.epoch = epoch;
    }

    public String getMensaje()
    {
        return mensaje;
    }

    public void setMensaje(String mensaje)
    {
        this.mensaje = mensaje;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }
}
