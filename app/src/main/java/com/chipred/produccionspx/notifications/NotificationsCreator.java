package com.chipred.produccionspx.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.MainActivity;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.chipred.produccionspx.notifications.Notification.NEW_PROJECT_GROUP;
import static com.chipred.produccionspx.notifications.Notification.PROJECT_MODIFIED_GROUP;
import static com.chipred.produccionspx.notifications.Notification.SIGN_GROUP;
import static com.chipred.produccionspx.notifications.Notification.SPX;
import static com.chipred.produccionspx.notifications.Notification.PROJECT_ALARM;
import static com.chipred.produccionspx.notifications.Notification.VEHICLE_NOTIFICATION;

public class NotificationsCreator
{
    // Variables
    private String elemetId; // Id del proyecto o viaje seleccionado
    private String username;

    // Objetos
    private Context context;
    private NotificationManagerCompat notificationManager;
    private SharedPreferences sharedPreferences;

    public NotificationsCreator(Context context)
    {
        this.context = context;

        // Inicializar notificationManager
        notificationManager = NotificationManagerCompat.from(context);
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setElemetId(String elemetId)
    {
        this.elemetId = elemetId;
    }

    public void setSharedPreferences(SharedPreferences sharedPreferences)
    {
        this.sharedPreferences = sharedPreferences;
    }

    public void showBundledNotification(String title, String body)
    {
        ArrayList<String> notifications;

        if (!sharedPreferences.contains(Constants.NOTIFICATIONS))
        {
            notifications = new ArrayList<>();
        }
        else
        {
            notifications = getArrayListFromSp(sharedPreferences);
        }

        //Agregar notificacion actual a la lista
        notifications.add(title + ": " + body);

        NotificationCompat.Builder summaryBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_binarium_vector).setAutoCancel(false).setShowWhen(true)
                .setWhen(System.currentTimeMillis()).setGroupSummary(true).setSubText(username)
                .setGroup("spx");

        if (notifications.size() == 1)
        {
            summaryBuilder.setContentTitle(title);
            summaryBuilder.setContentText(body);
            summaryBuilder.setContentIntent(setIntentToSummary(title, false));
        }
        else
        {
            summaryBuilder.setContentTitle(notifications.size() + " nuevos " +
                    "eventos");
            summaryBuilder.setContentText("Deslice hacia abajo para ver el detalle");
            summaryBuilder.setContentIntent(setIntentToSummary(title, true));
        }

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle("Eventos: ");
        for (String notification : notifications)
        {
            inboxStyle.addLine(notification);
        }

        summaryBuilder.setStyle(inboxStyle);
        summaryBuilder.setDefaults(android.app.Notification.DEFAULT_SOUND | android.app.Notification.DEFAULT_VIBRATE |
                Notification.DEFAULT_LIGHTS);
        notificationManager.notify(SPX, summaryBuilder.build());

        //Obtener el dato de que tipo de notificacion es y contar las de su mismo tipo
        ArrayList<String> sameNotificationList = new ArrayList<>();
        String notificationType = getNotificationTypeString(title);

        for (String notification : notifications)
        {
            if (getNotificationConcept(title) == getNotificationConcept(notification))
            {
                sameNotificationList.add(notification);
            }
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context).setSmallIcon(R
                .drawable.ic_binarium_vector).setGroup("spx");

        builder.setContentIntent(getPendingIntent(title, sameNotificationList.size()));
        builder.setLargeIcon(drawableToBitmap(getNotificationTypeDrawable(title)));

        NotificationCompat.InboxStyle inInboxStyle = new NotificationCompat.InboxStyle();

        if (sameNotificationList.size() == 1)
        {
            builder.setContentTitle(title);
            builder.setContentText(body);

            inInboxStyle.addLine(body);
        }
        else
        {
            builder.setContentTitle(sameNotificationList.size() + " " +
                    notificationType);
            builder.setContentText("Deslice hacia abajo para ver el detalle");

            inInboxStyle.setBigContentTitle(sameNotificationList.size() + " " +
                    notificationType);

            for (String notification : sameNotificationList)
            {
                inInboxStyle.addLine(notification.substring(notification.indexOf(":") + 2));
            }
        }
        builder.setStyle(inInboxStyle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            builder.setChannelId(Constants.GENERAL_NOTIFICATIONS_ID);

        notificationManager.notify(getNotificationConcept(title), builder.build());
        putStringSet(sharedPreferences, Constants.NOTIFICATIONS, notifications);
    }

    public void showSimpleNotification(String title, String body)
    {
        ArrayList<String> notifications;
        if (!sharedPreferences.contains(Constants.NOTIFICATIONS))
        {
            //Crear una nueva lista de notificaciones pendientes por revisar
            notifications = new ArrayList<>();
        }
        else
        {
            //Agregar notificacion a la lista de pendientes por cancelar
            notifications = getArrayListFromSp(sharedPreferences);
            if (notifications == null) notifications = new ArrayList<>();
        }

        //Obtener el dato de que tipo de notificacion es y contar las de su mismo tipo
        ArrayList<String> sameNotificationList = new ArrayList<>();
        String notificationType = getNotificationTypeString(title);

        // Crear estilo de notificacion
        NotificationCompat.InboxStyle inInboxStyle = new NotificationCompat.InboxStyle();

        // Obtener todas las notificaciones
        for (String notification : notifications)
        {
            if (getNotificationConcept(title) == getNotificationConcept(notification))
            {
                // Agregar notificaciones anteriores
                inInboxStyle.addLine(notification.substring(notification.indexOf(":") + 2));
                sameNotificationList.add(notification);
            }
        }

        // Agregar notificacion actual
        inInboxStyle.setBigContentTitle(notificationType);
        inInboxStyle.addLine(body);
        sameNotificationList.add(body);

        // Definir título
        if (sameNotificationList.size() == 1)
        {
            inInboxStyle.setSummaryText(body);
        }
        else
        {
            inInboxStyle.setSummaryText(sameNotificationList.size() + " - " + notificationType);
        }

        // Agregar notificacion actual a la lista
        notifications.add(title + ": " + body);
        putStringSet(sharedPreferences, Constants.NOTIFICATIONS, notifications);

        // Crear notificacion
        Notification notification = new NotificationCompat.Builder(context, Constants
                .GENERAL_NOTIFICATIONS_ID).setSmallIcon(R.drawable.ic_binarium_vector).setContentTitle
                (title).setContentText(body).setAutoCancel(false).setShowWhen(true).setWhen
                (System.currentTimeMillis()).setLargeIcon(drawableToBitmap
                (getNotificationTypeDrawable(title))).setStyle(inInboxStyle).setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(getPendingIntent(title, sameNotificationList.size())).setDefaults(Notification.DEFAULT_SOUND
                        | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS).build();

        // Notificar
        notificationManager.notify(getNotificationConcept(title), notification);
    }

    private static Bitmap drawableToBitmap(Drawable drawable)
    {
        if (drawable instanceof BitmapDrawable)
        {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable
                .getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private ArrayList<String> getArrayListFromSp(SharedPreferences sharedPreferences)
    {
        Set<String> set = sharedPreferences.getStringSet(Constants.NOTIFICATIONS, null);
        ArrayList<String> list = new ArrayList<>();

        if (set != null)
        {
            for (String str : set)
            {
                list.add(str);
            }
        }
        else
        {
            return null;
        }

        return list;
    }

    private int getNotificationConcept(String keyWord)
    {
        if (keyWord.contains("Nueva firma"))
        {
            return SIGN_GROUP;
        }
        else if (keyWord.contains("Nuevo proyecto"))
        {
            return NEW_PROJECT_GROUP;
        }
        else if (keyWord.contains("Proyecto modificado"))
        {
            return PROJECT_MODIFIED_GROUP;
        }
        else if (keyWord.contains("Proyecto inactivo"))
        {
            return PROJECT_ALARM;
        }
        else if (keyWord.toLowerCase().contains("salida"))
        {
            return VEHICLE_NOTIFICATION;
        }
        else
        {
            return SPX;
        }
    }

    private Drawable getNotificationTypeDrawable(String title)
    {
        if (getNotificationConcept(title) == NEW_PROJECT_GROUP)
        {
            return context.getResources().getDrawable(R.drawable.ic_gas_black);
        }
        else if (getNotificationConcept(title) == PROJECT_MODIFIED_GROUP)
        {
            return context.getResources().getDrawable(R.drawable.ic_gas_black);
        }
        else if (getNotificationConcept(title) == SIGN_GROUP)
        {
            return context.getResources().getDrawable(R.drawable.ic_sign_in_black);
        }
        else if (getNotificationConcept(title) == PROJECT_ALARM)
        {
            return context.getResources().getDrawable(R.drawable.ic_alarm_black);
        }
        else if (getNotificationConcept(title) == VEHICLE_NOTIFICATION)
        {
            return context.getResources().getDrawable(R.drawable.ic_car);
        }
        else
        {
            return context.getResources().getDrawable(R.drawable.ic_binarium_transparent);
        }
    }

    private String getNotificationTypeString(String title)
    {
        if (getNotificationConcept(title) == NEW_PROJECT_GROUP)
        {
            return "Nuevo proyecto";
        }
        else if (getNotificationConcept(title) == PROJECT_MODIFIED_GROUP)
        {
            return "Proyecto modificado";
        }
        else if (getNotificationConcept(title) == SIGN_GROUP)
        {
            return "Nueva firma";
        }
        else if (getNotificationConcept(title) == PROJECT_ALARM)
        {
            return "Proyecto inactivo";
        }
        else if (getNotificationConcept(title) == VEHICLE_NOTIFICATION)
        {
            return title;
        }
        else
        {
            return "Mensajes";
        }
    }

    private PendingIntent getPendingIntent(String title, int groupSize)
    {
        // Evaluar para definir request code
        int requestCode;

        if (groupSize > 1)
        {
            if (title.contains("Proyecto inactivo"))
            {
                requestCode =
                        com.chipred.produccionspx.notifications.Notification.ALARMS_MENU + 1000;
            }
            else if (title.toLowerCase().contains("salida"))
            {
                requestCode = VEHICLE_NOTIFICATION + 1000;
            }
            else
            {
                requestCode =
                        com.chipred.produccionspx.notifications.Notification.NOTIFICATIONS_MENU + 1000;
            }
        }
        else
        {
            requestCode = getNotificationConcept(title) + 1000;
        }

        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra("notification_action", requestCode);
        resultIntent.setAction("notification_action");
        resultIntent.putExtra("project_id", elemetId);

        return PendingIntent.getActivity(context, requestCode, resultIntent, PendingIntent
                .FLAG_UPDATE_CURRENT);
    }

    private PendingIntent setIntentToSummary(String notificationTitle, boolean defaultFragment)
    {
        int requestCode;

        if (defaultFragment)
        {
            requestCode = SPX;
        }
        else
        {
            requestCode = getNotificationConcept(notificationTitle) + 1000;
        }

        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra("notification_action", requestCode);
        resultIntent.putExtra("project_id", elemetId);
        resultIntent.setAction("notification_action");

        return PendingIntent.getActivity(context, requestCode, resultIntent, PendingIntent
                .FLAG_UPDATE_CURRENT);
    }

    private void putStringSet(SharedPreferences sharedPreferences, String mKey,
                              ArrayList<String> list)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Set<String> set = new HashSet<>(list);
        editor.putStringSet(mKey, set).apply();
    }
}
