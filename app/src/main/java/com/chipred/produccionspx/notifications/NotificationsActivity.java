package com.chipred.produccionspx.notifications;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.projects.ProjectsActivity;
import com.chipred.produccionspx.R;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import javax.annotation.Nullable;

import static com.chipred.produccionspx.notifications.Notification.NEW_PROJECT_GROUP;
import static com.chipred.produccionspx.notifications.Notification.PROJECT_MODIFIED_GROUP;
import static com.chipred.produccionspx.notifications.Notification.SIGN_GROUP;

public class NotificationsActivity extends AppCompatActivity
{
    // Objetos
    private ArrayList<Notification> notifications;
    private CollectionReference notificationCollection;
    private Query query;

    // Views
    private Dialog filterSelectorDialog;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private TextView noNotifications;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        // Obtener views
        noNotifications = findViewById(R.id.no_notifications_tv);
        progressBar = findViewById(R.id.progress_bar);
        recyclerView = findViewById(R.id.notifications_list);

        // Mostrar título
        setTitle(getResources().getString(R.string.notifications_title));

        //  Agregar flecha para regresar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Obtener coleccion de base de datos
        notificationCollection = FirebaseFirestore.getInstance().collection("notificaciones");

        // Definir query default
        query = notificationCollection.orderBy("epoch",
                Query.Direction.DESCENDING).limit(20);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
        {
            this.finish();
        }
        else if (id == R.id.notification_filter)
        {
            showNotificatiosFilter();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.notification_list_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        // Obtener notificaciones
        getNotifications();
    }

    private void getNotifications()
    {
        // Crear query
        query.addSnapshotListener(this,
                (queryDocumentSnapshots, e) ->
                {
                    if (e != null)
                    {
                        Log.d("CloudFirestoreException", e.toString());
                    }

                    if (queryDocumentSnapshots != null)
                    {
                        // Inicializar lista
                        if (notifications == null) notifications = new ArrayList<>();
                        else notifications.clear();

                        // Iterar en la lista de notificaciones
                        for (DocumentSnapshot documentSnapshot :
                                queryDocumentSnapshots.getDocuments())
                        {
                            // Obtener notificacion
                            Notification notification =
                                    documentSnapshot.toObject(Notification.class);

                            // Validar objeto
                            if (notification != null)
                            {
                                notifications.add(notification);
                            }
                        }

                        // Mostrar notificaciones
                        if (!notifications.isEmpty())
                        {
                            showNotifications();
                        }
                        else
                        {
                            noNotifications.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void showNotifications()
    {
        // Crear adaptador
        NotificationsAdapter adapter = new NotificationsAdapter(notifications);
        adapter.setListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Obtener notificacion
                Notification notification =
                        notifications.get(recyclerView.getChildAdapterPosition(v));

                // Validar si tiene id de proyecto
                if (notification.getProjectId() != null && !notification.getProjectId().isEmpty())
                {
                    //Preparar intent
                    Intent mIntent = new Intent(NotificationsActivity.this, ProjectsActivity.class);

                    switch (notification.getTipo())
                    {
                        case NEW_PROJECT_GROUP:
                        case PROJECT_MODIFIED_GROUP:
                            mIntent.putExtra(Constants.PROJECT_ORDER_NUMBER,
                                    notification.getProjectId());
                            break;

                        case SIGN_GROUP:
                            mIntent.putExtra(Constants.PROJECT_ORDER_NUMBER,
                                    notification.getProjectId());
                            mIntent.putExtra(Constants.OPEN_SIGNS_DIALOG, true);
                            break;
                    }

                    //Iniciar actividad
                    startActivity(mIntent);
                }
            }
        });

        // Asignar adaptador
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Mostrar lista
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void showNotificatiosFilter()
    {
        if (filterSelectorDialog == null)
        {
            // Crear builder
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // Crear layout
            View content =
                    getLayoutInflater().inflate(R.layout.dialog_notifications_filter_selector,
                            null);

            // Definir layout
            builder.setView(content);

            // Definir boton de regresar
            builder.setNegativeButton("Regresar", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });

            // Crear diálogo
            filterSelectorDialog = builder.create();

            // Crear listener para los elementos
            View.OnClickListener filterSelected = new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // Obtener texto de radio button
                    RadioButton selectedButton = (RadioButton) v;
                    String text = selectedButton.getText().toString();

                    // Obtener item seleccionado
                    if (text.equals(getResources().getString(R.string.fn_none)))
                    {
                        query = notificationCollection.orderBy("epoch",
                                Query.Direction.DESCENDING).limit(20);
                    }
                    else if (text.equals(getResources().getString(R.string.fn_new_project)))
                    {
                        query = notificationCollection.whereEqualTo("tipo", NEW_PROJECT_GROUP).orderBy("epoch", Query.Direction.DESCENDING).limit(20);
                    }
                    else if (text.equals(getResources().getString(R.string.fn_modified_project)))
                    {
                        query = notificationCollection.whereEqualTo("tipo",
                                PROJECT_MODIFIED_GROUP).orderBy("epoch", Query.Direction.DESCENDING).limit(20);
                    }
                    else if (text.equals(getResources().getString(R.string.fn_signs)))
                    {
                        query = notificationCollection.whereEqualTo("tipo",
                                SIGN_GROUP).orderBy("epoch", Query.Direction.DESCENDING).limit(20);
                    }

                    // Cerar diálogo
                    filterSelectorDialog.dismiss();

                    // Ejecutar query
                    getNotifications();

                    // Ocultar lista
                    progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            };

            // Obtener layout y seleccionar el filtro actual
            RadioGroup list = content.findViewById(R.id.filters_radio_group);
            for (int i = 0; i < list.getChildCount(); i++)
            {
                View view = list.getChildAt(i);
                if (view instanceof RadioButton)
                {
                    // Hacer cast a RadioButton del view actual
                    RadioButton mRadioButton = (RadioButton) view;

                    // Definir clickListener
                    mRadioButton.setOnClickListener(filterSelected);
                }
            }
        }

        // Mostrar diálogo
        filterSelectorDialog.show();
    }
}
