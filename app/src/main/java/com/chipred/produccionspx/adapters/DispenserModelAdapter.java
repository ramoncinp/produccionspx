package com.chipred.produccionspx.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chipred.produccionspx.DispModel;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;

public class DispenserModelAdapter extends RecyclerView.Adapter<DispenserModelAdapter.DispenserModelViewHolder> implements View.OnClickListener, View.OnLongClickListener {
    public static final int IN_PROJECT_FORM = 0;
    public static final int IN_CHART = 1;
    private int activity = IN_PROJECT_FORM;

    private final Context context;
    private final ArrayList<DispModel> dispModels;
    private View.OnClickListener onClickListener;
    private View.OnLongClickListener onLongClickListener;

    public DispenserModelAdapter(ArrayList<DispModel> dispModels, Context context) {
        this.dispModels = dispModels;
        this.context = context;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    @NonNull
    @Override
    public DispenserModelViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.disp_model_item_layout, viewGroup, false);

        DispenserModelViewHolder viewHolder = new DispenserModelViewHolder(v);
        v.setOnClickListener(this);
        v.setOnLongClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DispenserModelViewHolder dispenserModelViewHolder, int i) {
        //Obtener dispensario
        DispModel dispModel = dispModels.get(i);

        dispenserModelViewHolder.serialNumber.setText(dispModel.getSerialNumber());
        dispenserModelViewHolder.model.setText(dispModel.getModel());
        dispenserModelViewHolder.manufacDate.setText(dispModel.getManufactureDate());
        dispenserModelViewHolder.products.setText(dispModel.getProducts());
        dispenserModelViewHolder.sae.setText(dispModel.getSae());
        dispenserModelViewHolder.chasis.setText(dispModel.getChasis());

        //Cambiar color
        if (!dispModel.isSaeOk()) {
            dispenserModelViewHolder.sae.setTextColor(ContextCompat.getColor(context,
                    android.R.color.holo_red_dark));
        } else {
            dispenserModelViewHolder.sae.setTextColor(ContextCompat.getColor(context,
                    android.R.color.holo_green_dark));
        }

        //Verificar si esta cambiado
        if (dispModel.isHasChanged() && activity == IN_PROJECT_FORM) {
            dispenserModelViewHolder.itemView.setBackgroundColor(ContextCompat.getColor(context,
                    R.color.yellow));
        } else if (!dispModel.isFinished() && activity == IN_CHART) {
            dispenserModelViewHolder.serialNumber.setAlpha(0.5f);
            dispenserModelViewHolder.model.setAlpha(0.5f);
            dispenserModelViewHolder.manufacDate.setAlpha(0.5f);
            dispenserModelViewHolder.products.setAlpha(0.5f);
            dispenserModelViewHolder.sae.setAlpha(0.5f);
            dispenserModelViewHolder.chasis.setAlpha(0.5f);
        } else {
            dispenserModelViewHolder.itemView.setBackgroundColor(ContextCompat.getColor(context,
                    android.R.color.white));

            dispenserModelViewHolder.serialNumber.setAlpha(1f);
            dispenserModelViewHolder.model.setAlpha(1f);
            dispenserModelViewHolder.manufacDate.setAlpha(1f);
            dispenserModelViewHolder.products.setAlpha(1f);
            dispenserModelViewHolder.sae.setAlpha(1f);
            dispenserModelViewHolder.chasis.setAlpha(1f);
        }

        // Verificar si tiene garantía
        if (dispModel.getInvoiceDate() != null) {
            dispenserModelViewHolder.warrantyCv.setVisibility(View.VISIBLE);

            // Fecha de hoy
            final Long now = new Date().getTime();

            // Mayor a un año
            if (now - dispModel.getInvoiceDate() > DateUtils.YEAR_IN_MILLIS) {
                dispenserModelViewHolder.warrantyCv.setCardBackgroundColor(
                        ContextCompat.getColor(context, android.R.color.holo_red_dark));
                dispenserModelViewHolder.warrantyTv.setText("NG");
            } else {
                dispenserModelViewHolder.warrantyCv.setCardBackgroundColor(
                        ContextCompat.getColor(context, android.R.color.holo_green_dark));
                dispenserModelViewHolder.warrantyTv.setText("G");
            }
        } else {
            dispenserModelViewHolder.warrantyCv.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return dispModels.size();
    }

    @Override
    public void onClick(View v) {
        if (onClickListener != null) {
            onClickListener.onClick(v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (onLongClickListener != null) {
            onLongClickListener.onLongClick(v);
            return true;
        }
        return false;
    }

    static class DispenserModelViewHolder extends RecyclerView.ViewHolder {
        private final CardView warrantyCv;
        private final TextView warrantyTv;
        private final TextView serialNumber;
        private final TextView model;
        private final TextView manufacDate;
        private final TextView products;
        private final TextView sae;
        private final TextView chasis;

        DispenserModelViewHolder(@NonNull View itemView) {
            super(itemView);
            serialNumber = itemView.findViewById(R.id.order_number);
            model = itemView.findViewById(R.id.model);
            manufacDate = itemView.findViewById(R.id.manufac_date);
            products = itemView.findViewById(R.id.product);
            sae = itemView.findViewById(R.id.sae);
            chasis = itemView.findViewById(R.id.chasis);
            warrantyCv = itemView.findViewById(R.id.warranty_status);
            warrantyTv = itemView.findViewById(R.id.warranty_text);
        }
    }
}
