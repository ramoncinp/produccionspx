package com.chipred.produccionspx.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.ElectronicKitOverview;
import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class ElectronicKitOverviewAdapter extends RecyclerView.Adapter<ElectronicKitOverviewAdapter.ElectronicKitOverviewViewHolder> implements View.OnClickListener, Filterable
{
    private ArrayList<ElectronicKitOverview> electronicKitOverviews;
    private ArrayList<ElectronicKitOverview> filteredKits;
    private View.OnClickListener clickListener;

    public ElectronicKitOverviewAdapter(ArrayList<ElectronicKitOverview> electronicKitOverviews)
    {
        this.electronicKitOverviews = electronicKitOverviews;
        this.filteredKits = electronicKitOverviews;
    }

    @NonNull
    @Override
    public ElectronicKitOverviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.electronic_kit_overview, viewGroup, false);

        ElectronicKitOverviewViewHolder viewHolder = new ElectronicKitOverviewViewHolder(v);
        v.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ElectronicKitOverviewViewHolder electronicKitOverviewViewHolder,
                                 int i)
    {
        ElectronicKitOverview electronicKitOverview = filteredKits.get(i);

        electronicKitOverviewViewHolder.kitNumber.setText(electronicKitOverview.getKitNumber());
        electronicKitOverviewViewHolder.client.setText(electronicKitOverview.getClient());
        electronicKitOverviewViewHolder.station.setText(electronicKitOverview.getStation());

        if (electronicKitOverview.getEpoch() != null)
        {
            electronicKitOverviewViewHolder.lastModif.setText(Constants.getLastModifText(electronicKitOverview.getEpoch()));
            electronicKitOverviewViewHolder.lastModif.setVisibility(View.VISIBLE);
        }
        else
        {
            electronicKitOverviewViewHolder.lastModif.setVisibility(View.GONE);
        }

        if (electronicKitOverview.getUserInitials() != null)
        {
            electronicKitOverviewViewHolder.kitUser.setText(electronicKitOverview.getUserInitials());
        }
    }

    @Override
    public int getItemCount()
    {
        return filteredKits.size();
    }

    public ElectronicKitOverview getEkit(int idx)
    {
        return filteredKits.get(idx);
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    @Override
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults results = new FilterResults();
                final ArrayList<ElectronicKitOverview> filteredKits = electronicKitOverviews;

                int count = filteredKits.size();
                final ArrayList<ElectronicKitOverview> nlist = new ArrayList<>(count);

                // Hacer lowerCase la búsqueda
                constraint = constraint.toString().toLowerCase();

                // Obtener los proyectos que coincidan
                for (int i = 0; i < electronicKitOverviews.size(); i++)
                {
                    // Obtener proyectos
                    ElectronicKitOverview electronicKitOverview = filteredKits.get(i);

                    // Obtener nombre de estacion
                    String projectName = electronicKitOverview.getStation().toLowerCase();

                    // Hacer la comparación
                    if (projectName.contains(constraint))
                    {
                        nlist.add(electronicKitOverview);
                    }
                }

                results.count = nlist.size();
                results.values = nlist;
                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                // Asignar lista de proyectos
                filteredKits = (ArrayList<ElectronicKitOverview>) results.values;

                // Actualizar adaptador
                notifyDataSetChanged();
            }
        };
    }

    static class ElectronicKitOverviewViewHolder extends RecyclerView.ViewHolder
    {
        private TextView kitNumber;
        private TextView client;
        private TextView station;
        private TextView lastModif;
        private TextView kitUser;

        ElectronicKitOverviewViewHolder(@NonNull View itemView)
        {
            super(itemView);
            kitNumber = itemView.findViewById(R.id.e_kit_number);
            client = itemView.findViewById(R.id.project_client);
            station = itemView.findViewById(R.id.project_station);
            lastModif = itemView.findViewById(R.id.last_modification);
            kitUser = itemView.findViewById(R.id.kit_user);
        }
    }
}
