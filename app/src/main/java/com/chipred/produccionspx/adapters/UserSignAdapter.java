package com.chipred.produccionspx.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.users.UserData;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class UserSignAdapter extends RecyclerView.Adapter<UserSignAdapter.UserSignViewHolder>
{
    private Context context;
    private ArrayList<UserData> userDataList;
    private ArrayList<Map<String, Object>> registers;

    public UserSignAdapter(ArrayList<UserData> userDataList, Context context)
    {
        this.context = context;
        this.userDataList = userDataList;
    }

    public void setRegisters(ArrayList<Map<String, Object>> registers)
    {
        this.registers = registers;
    }

    public ArrayList<Map<String, Object>> getRegisters()
    {
        return registers;
    }

    @NonNull
    @Override
    public UserSignViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_sign_adapter,
                        viewGroup, false);

        return new UserSignViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserSignViewHolder userSignViewHolder, int i)
    {
        //Obtener dispensario
        UserData userData = userDataList.get(i);
        userSignViewHolder.userName.setText(userData.getName());
        userSignViewHolder.userResp.setText(userData.getCharge());

        //Obtener imagen desde internet
        if (userData.getPhotoUrl() != null && !userData.getPhotoUrl().isEmpty())
            Glide.with(context).load(userData.getPhotoUrl()).dontAnimate().into(userSignViewHolder.userImage);
        else
            Glide.with(context).load(context.getResources().getDrawable(R.drawable.ic_binarium)).dontAnimate().into(userSignViewHolder.userImage);

        //Definir fechas
        Long epoch = (Long) registers.get(i).get("epoch");
        userSignViewHolder.date.setText(Constants.dateObjectToString(new Date(epoch)));
    }

    @Override
    public int getItemCount()
    {
        return userDataList.size();
    }

    static class UserSignViewHolder extends RecyclerView.ViewHolder
    {
        private TextView date;
        private TextView userName;
        private TextView userResp;
        private ImageView userImage;

        UserSignViewHolder(@NonNull View itemView)
        {
            super(itemView);
            date = itemView.findViewById(R.id.date_tv);
            userName = itemView.findViewById(R.id.user_name);
            userResp = itemView.findViewById(R.id.user_resp);
            userImage = itemView.findViewById(R.id.user_image_view);
        }
    }
}
