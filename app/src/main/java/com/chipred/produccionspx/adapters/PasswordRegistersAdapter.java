package com.chipred.produccionspx.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class PasswordRegistersAdapter extends RecyclerView.Adapter<PasswordRegistersAdapter.PasswordRegisterViewHolder>
{
    private ArrayList<Map<String, Object>> registers;

    public PasswordRegistersAdapter(ArrayList<Map<String, Object>> registers)
    {
        this.registers = registers;
    }

    @NonNull
    @Override
    public PasswordRegisterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.password_register_layout, viewGroup, false);

        return new PasswordRegisterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PasswordRegisterViewHolder passwordRegisterViewHolder,
                                 int i)
    {
        Map<String, Object> register = registers.get(i);

        passwordRegisterViewHolder.station.setText((String) register.get("id_estacion"));
        passwordRegisterViewHolder.comments.setText((String) register.get("commentarios"));

        //Convertir epoch a fecha
        Date date = new Date((long) register.get("fecha"));
        String dateString = Constants.dateObjectToString2(date);

        //Setear fecha
        passwordRegisterViewHolder.date.setText(dateString);

        //Obtener nombre de usuario
        if (register.containsKey("iniciales_usuario"))
        {
            passwordRegisterViewHolder.user.setText((String) register.get("iniciales_usuario"));
        }
    }

    @Override
    public int getItemCount()
    {
        return registers.size();
    }

    static class PasswordRegisterViewHolder extends RecyclerView.ViewHolder
    {
        private TextView station;
        private TextView date;
        private TextView user;
        private TextView comments;

        PasswordRegisterViewHolder(@NonNull View itemView)
        {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            user = itemView.findViewById(R.id.user);
            station = itemView.findViewById(R.id.station);
            comments = itemView.findViewById(R.id.comments);
        }
    }
}
