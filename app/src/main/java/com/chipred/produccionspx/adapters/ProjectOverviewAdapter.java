package com.chipred.produccionspx.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.projects.ProjectOverview;
import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class ProjectOverviewAdapter extends RecyclerView.Adapter<ProjectOverviewAdapter.ProjectOverviewViewHolder> implements View.OnClickListener, Filterable
{
    private ArrayList<ProjectOverview> projectOverviews;
    private ArrayList<ProjectOverview> filteredProjects;
    private View.OnClickListener clickListener;

    public ProjectOverviewAdapter(ArrayList<ProjectOverview> projectOverviews)
    {
        this.projectOverviews = projectOverviews;
        this.filteredProjects = projectOverviews;
    }

    @NonNull
    @Override
    public ProjectOverviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.project_overview_layout, viewGroup, false);

        ProjectOverviewViewHolder viewHolder = new ProjectOverviewViewHolder(v);
        v.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectOverviewViewHolder projectOverviewViewHolder,
                                 int i)
    {
        ProjectOverview projectOverview = filteredProjects.get(i);

        projectOverviewViewHolder.name.setText(projectOverview.getName());
        projectOverviewViewHolder.client.setText(projectOverview.getClient());
        projectOverviewViewHolder.station.setText(projectOverview.getStation());
        projectOverviewViewHolder.station.append(" " + projectOverview.getStationNumber());
        projectOverviewViewHolder.orderNumber.setText(projectOverview.getOrderNumber());
        projectOverviewViewHolder.status.setText(projectOverview.getStatus());
        projectOverviewViewHolder.projectStatus.setText(projectOverview.getProjectStatus());

        if (projectOverview.getEpoch() != null)
        {
            projectOverviewViewHolder.lastModif.setText(Constants.getLastModifText(projectOverview.getEpoch()));
            projectOverviewViewHolder.lastModif.setVisibility(View.VISIBLE);
        }
        else
        {
            projectOverviewViewHolder.lastModif.setVisibility(View.GONE);
        }

        if (projectOverview.getAdminStatus() == null || projectOverview.getAdminStatus().isEmpty())
        {
            projectOverviewViewHolder.adminStatus.setVisibility(View.GONE);
        }
        else
        {
            projectOverviewViewHolder.adminStatus.setText(projectOverview.getAdminStatus());
            projectOverviewViewHolder.adminStatus.setVisibility(View.VISIBLE);
        }

        if (projectOverview.getCrmTicket() == null || projectOverview.getCrmTicket().isEmpty())
        {
            projectOverviewViewHolder.crmTicketLayout.setVisibility(View.GONE);
        }
        else
        {
            projectOverviewViewHolder.crmTicket.setText(projectOverview.getCrmTicket());
            projectOverviewViewHolder.crmTicket.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount()
    {
        return filteredProjects.size();
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    public ProjectOverview getProyect(int idx)
    {
        return filteredProjects.get(idx);
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    @Override
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults results = new FilterResults();
                final ArrayList<ProjectOverview> filteredProjects = projectOverviews;

                int count = filteredProjects.size();
                final ArrayList<ProjectOverview> nlist = new ArrayList<>(count);

                // Hacer lowerCase la búsqueda
                constraint = constraint.toString().toLowerCase();

                // Obtener los proyectos que coincidan
                for (int i = 0; i < projectOverviews.size(); i++)
                {
                    // Obtener proyectos
                    ProjectOverview projectOverview = filteredProjects.get(i);

                    // Obtener nombre de estacion
                    String projectName = projectOverview.getStation().toLowerCase();
                    String clientName = projectOverview.getClient().toLowerCase();

                    // Hacer la comparación
                    if (projectName.contains(constraint) || clientName.contains(constraint))
                    {
                        nlist.add(projectOverview);
                    }
                }

                results.count = nlist.size();
                results.values = nlist;
                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                // Asignar lista de proyectos
                filteredProjects = (ArrayList<ProjectOverview>) results.values;

                // Actualizar adaptador
                notifyDataSetChanged();
            }
        };
    }

    static class ProjectOverviewViewHolder extends RecyclerView.ViewHolder
    {
        private TextView orderNumber;
        private TextView crmTicket;
        private TextView client;
        private TextView station;
        private TextView status;
        private TextView adminStatus;
        private TextView projectStatus;
        private TextView name;
        private TextView lastModif;
        private LinearLayout crmTicketLayout;

        ProjectOverviewViewHolder(@NonNull View itemView)
        {
            super(itemView);
            orderNumber = itemView.findViewById(R.id.order_number);
            client = itemView.findViewById(R.id.project_client);
            station = itemView.findViewById(R.id.project_station);
            name = itemView.findViewById(R.id.project_name);
            status = itemView.findViewById(R.id.project_prod_status);
            adminStatus = itemView.findViewById(R.id.project_admin_status);
            lastModif = itemView.findViewById(R.id.last_modification);
            crmTicket = itemView.findViewById(R.id.crm_ticket);
            projectStatus = itemView.findViewById(R.id.project_status);
            crmTicketLayout = itemView.findViewById(R.id.crm_ticket_layout);
        }
    }
}
