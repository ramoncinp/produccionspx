package com.chipred.produccionspx.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class SerialsListAdapter extends RecyclerView.Adapter<SerialsListAdapter.SerialsListViewHolder> implements View.OnClickListener
{
    private ArrayList<String> descriptions;
    private View.OnClickListener clickListener;

    public SerialsListAdapter(ArrayList<String> descriptions)
    {
        this.descriptions = descriptions;
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public SerialsListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.serial_list_adapter_layout, null);
        v.setOnClickListener(clickListener);

        return new SerialsListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SerialsListViewHolder viewHolder, int i)
    {
        viewHolder.desc.setText(descriptions.get(i));
    }

    @Override
    public int getItemCount()
    {
        return descriptions.size();
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    static class SerialsListViewHolder extends RecyclerView.ViewHolder
    {
        private TextView desc;

        SerialsListViewHolder(@NonNull View itemView)
        {
            super(itemView);
            desc = itemView.findViewById(R.id.desc);
        }
    }
}
