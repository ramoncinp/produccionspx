package com.chipred.produccionspx.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.projects.ProjectDispensers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class ProjectDispensersAdapter extends RecyclerView.Adapter<ProjectDispensersAdapter.ProjectDispensersViewHolder> implements View.OnClickListener
{
    private ArrayList<ProjectDispensers> projectDispensersList;
    private Context context;
    private View.OnClickListener clickListener;

    public ProjectDispensersAdapter(Context context,
                                    ArrayList<ProjectDispensers> projectDispensersList)
    {
        this.context = context;
        this.projectDispensersList = projectDispensersList;
        orderProjects();
    }

    @NonNull
    @Override
    public ProjectDispensersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.project_dispensers_layout, viewGroup, false);

        ProjectDispensersViewHolder viewHolder = new ProjectDispensersViewHolder(v);
        v.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectDispensersViewHolder projectOverviewViewHolder,
                                 int i)
    {
        ProjectDispensers projectDispensers = projectDispensersList.get(i);
        projectOverviewViewHolder.client.setText(projectDispensers.getClient());
        projectOverviewViewHolder.station.setText(projectDispensers.getStation());

        String dispCount = String.format(Locale.getDefault(),
                " - %d/%d",
                projectDispensers.getDispModels().size(),
                projectDispensers.getAllDispsCount()
        );
        projectOverviewViewHolder.station.append(dispCount);

        // Ordenar dispensarios
        orderDispensers(projectDispensers);

        final DispenserModelAdapter dispenserModelAdapter =
                new DispenserModelAdapter(projectDispensers.getDispModels(),
                        context);
        dispenserModelAdapter.setActivity(DispenserModelAdapter.IN_CHART);
        projectOverviewViewHolder.dispensers.setAdapter(dispenserModelAdapter);
        projectOverviewViewHolder.dispensers.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    public int getItemCount()
    {
        return projectDispensersList.size();
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    public void setProjectDispensersList(ArrayList<ProjectDispensers> projectDispensersList)
    {
        this.projectDispensersList = projectDispensersList;
        orderProjects();
    }

    private void orderProjects()
    {
        Collections.sort(projectDispensersList,
                (projectDispensers, t1) -> projectDispensers.getStation().compareTo(t1.getStation()));
    }

    private void orderDispensers(ProjectDispensers projectDispensers)
    {
        Collections.sort(projectDispensers.getDispModels(), Constants.dispModelComparator);
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    static class ProjectDispensersViewHolder extends RecyclerView.ViewHolder
    {
        private RecyclerView dispensers;
        private TextView client;
        private TextView station;

        ProjectDispensersViewHolder(@NonNull View itemView)
        {
            super(itemView);
            dispensers = itemView.findViewById(R.id.dispensers_list);
            client = itemView.findViewById(R.id.project_client);
            station = itemView.findViewById(R.id.project_station);
        }
    }
}
