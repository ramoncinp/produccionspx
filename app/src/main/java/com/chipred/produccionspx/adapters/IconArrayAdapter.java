package com.chipred.produccionspx.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class IconArrayAdapter extends ArrayAdapter<Drawable>
{
    public IconArrayAdapter(Context context, ArrayList<Drawable> resources)
    {
        super(context, 0, resources);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }

    @Nullable
    @Override
    public Drawable getItem(int position)
    {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.icon_spinner_item,
                    parent, false);
        }

        ImageView iconIv = convertView.findViewById(R.id.icon_item_iv);

        Drawable resource = getItem(position);
        if (resource != null)
            iconIv.setImageDrawable(resource);

        return convertView;
    }
}
