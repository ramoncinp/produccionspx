package com.chipred.produccionspx.alarms;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.projects.ProjectForm;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.notifications.NotificationsManager;
import com.chipred.produccionspx.service.AlarmReceiver;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;

public class AlarmsActivity extends AppCompatActivity
{
    // Variables
    private boolean canReceiveProd;
    private boolean canReceiveAdmin;

    // Objetos
    private ArrayList<Alarm> alarms = new ArrayList<>();
    private CollectionReference alarmsCollection;

    // Views
    private ProgressDialog progressDialog;
    private ProgressBar progressBar;
    private RecyclerView alarmsList;
    private TextView noAlarmsTv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarms);
        setTitle("Alarmas");

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Obtener views
        alarmsList = findViewById(R.id.alarms_list);
        progressBar = findViewById(R.id.progress_bar);
        noAlarmsTv = findViewById(R.id.no_alarms);

        // Inicializar sharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES,
                Context.MODE_PRIVATE);

        // Obtener variables
        canReceiveProd = sharedPreferences.getBoolean(Constants.RECEIVE_ALARM_PROD, false);
        canReceiveAdmin = sharedPreferences.getBoolean(Constants.RECEIVE_ALARM_ADMN, false);

        // Inicializar base de datos
        initDataBase();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        getAlarms();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (progressDialog != null && progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initDataBase()
    {
        // Obtener referencia
        alarmsCollection = FirebaseFirestore.getInstance().collection("alarmas");
    }

    private boolean alarmIsActive(int id)
    {
        Intent intent = new Intent(this, AlarmReceiver.class);
        return (PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_NO_CREATE) != null);
    }

    private void getAlarms()
    {
        alarmsCollection.addSnapshotListener(this, new EventListener<QuerySnapshot>()
        {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e)
            {
                // Validar datos
                if (queryDocumentSnapshots != null)
                {
                    // Limpiar lista
                    alarms.clear();

                    // Obtener fecha actual
                    long currentEpoch = new Date().getTime();

                    // Obtener documentos
                    for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments())
                    {
                        // Obtener alarma
                        Alarm alarm = snapshot.toObject(Alarm.class);

                        // Validar
                        if (alarm != null)
                        {
                            boolean addAlarm;
                            addAlarm =
                                    canReceiveProd && alarm.getTipo().contains(NotificationsManager.NEW_ALARM_PROD);
                            addAlarm |= canReceiveAdmin && alarm.getTipo().contains(NotificationsManager.NEW_ALARM_ADMIN);

                            // Evaluar si se debe agregar la alarma a la lista
                            if (addAlarm)
                            {
                                // Agregar a la lista
                                alarms.add(alarm);
                            }

                            // Validar que la alarma no este activa y que no sea pasada
                            if (!alarmIsActive(Integer.parseInt(alarm.getId())) && (alarm.getEpoch() > currentEpoch))
                            {
                                Log.d("Alarmas", "Alarma inactiva... creando alarma");
                                setInactiveAlarm(alarm);
                            }
                            else
                            {
                                Log.d("Alarmas", "Alarma activa para proyecto " + alarm.getId());
                            }
                        }
                    }

                    if (alarms.isEmpty())
                    {
                        noAlarmsTv.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        alarmsList.setVisibility(View.GONE);
                    }
                    else
                    {
                        renderAlarmsList();
                    }
                }
            }
        });
    }

    private void renderAlarmsList()
    {
        // Crear adaptador
        AlarmAdapter alarmAdapter = new AlarmAdapter(this);
        alarmAdapter.setAlarms(alarms);
        alarmAdapter.setClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Mostrar progressDialog
                progressDialog = ProgressDialog.show(AlarmsActivity.this, "", "Cargando...");

                // Obtener numero de orden
                String orderNumber = alarms.get(alarmsList.getChildAdapterPosition(v)).getId();

                // Abrir proyecto correspondiente
                Intent openForm = new Intent(AlarmsActivity.this, ProjectForm.class);
                openForm.putExtra(Constants.ADD_OR_MODIFY_PROJECT, false);
                openForm.putExtra(Constants.CAN_MODIFY, true);
                openForm.putExtra(Constants.PROJECT_ORDER_NUMBER,
                        orderNumber);

                startActivity(openForm);
            }
        });

        // Asignar a RecyclerView
        alarmsList.setLayoutManager(new LinearLayoutManager(this));
        alarmsList.setAdapter(alarmAdapter);

        // Mostrar lista
        alarmsList.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        noAlarmsTv.setVisibility(View.GONE);
    }

    private void setInactiveAlarm(Alarm alarm)
    {
        // Evaluar epoch
        if (alarm.getEpoch() == 0) return;

        // Si el proyecto no esta pagado ni entregado, agregar una alarma de 1 semana
        // Crear intent
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("projectId", alarm.getId());
        intent.putExtra("projectName", alarm.getNombre());

        // Crear Penging intent
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                Integer.parseInt(alarm.getId()), intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Crear AlarmManager
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getEpoch(),
                pendingIntent);
    }
}
