package com.chipred.produccionspx.alarms;

import java.util.ArrayList;

public class Alarm
{
    private ArrayList<String> tipo;
    private String nombre;
    private String id;
    private long epoch;

    public Alarm()
    {
    }

    public ArrayList<String> getTipo()
    {
        return tipo;
    }

    public void setTipo(ArrayList<String> tipo)
    {
        this.tipo = tipo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public long getEpoch()
    {
        return epoch;
    }

    public void setEpoch(long epoch)
    {
        this.epoch = epoch;
    }
}
