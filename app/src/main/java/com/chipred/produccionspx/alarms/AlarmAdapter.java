package com.chipred.produccionspx.alarms;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Date;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder> implements View.OnClickListener
{
    private ArrayList<Alarm> alarms;
    private Context context;
    private View.OnClickListener clickListener;

    AlarmAdapter(Context context)
    {
        this.context = context;
    }

    void setAlarms(ArrayList<Alarm> alarms)
    {
        this.alarms = alarms;
    }

    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.alarm_layout, parent,
                false);

        AlarmViewHolder alarmViewHolder = new AlarmViewHolder(v);
        v.setOnClickListener(clickListener);

        return alarmViewHolder;
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder holder, int position)
    {
        // Obtener alarma
        Alarm alarm = alarms.get(position);

        // Obtener hora actual
        long currentDate = new Date().getTime();

        // Evaluar si no ha expirado
        if (currentDate > alarm.getEpoch())
        {
            // Ya expiró!
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.red_light));
        }
        else
        {
            // No ha expirado
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context,
                    android.R.color.white));
        }

        // Escribir valores
        holder.desc.setText(alarm.getNombre());
        holder.leftTime.setText(Constants.dateObjectToString2(new Date(alarm.getEpoch())));
    }

    @Override
    public int getItemCount()
    {
        return alarms.size();
    }

    class AlarmViewHolder extends RecyclerView.ViewHolder
    {
        private TextView desc;
        private TextView leftTime;

        AlarmViewHolder(@NonNull View itemView)
        {
            super(itemView);
            desc = itemView.findViewById(R.id.alarm_desc);
            leftTime = itemView.findViewById(R.id.alarm_time);
        }
    }
}
