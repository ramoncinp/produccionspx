package com.chipred.produccionspx;

import androidx.annotation.NonNull;

public class SaeDispModel
{
    private String key;
    private String model;
    private String product;
    private String type;

    public SaeDispModel()
    {
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getProduct()
    {
        return product;
    }

    public void setProduct(String product)
    {
        this.product = product;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    @NonNull
    public String toString()
    {
        return key;
    }
}
