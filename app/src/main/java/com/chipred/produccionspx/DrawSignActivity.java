package com.chipred.produccionspx;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.File;
import java.io.FileOutputStream;

public class DrawSignActivity extends AppCompatActivity
{
    private ProgressDialog progressDialog;
    private SignaturePad signaturePad;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_sign_activity);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().hide();
        }

        ImageView returnButton = findViewById(R.id.return_button);
        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        ImageView saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                progressDialog = ProgressDialog.show(DrawSignActivity.this, "", "Cargando...",
                        true, true);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        // Guardar firma
                        String signImagePath = saveImageBitmap(signaturePad.getSignatureBitmap());

                        // Crear intent
                        Intent intent = new Intent();

                        // Evaluar imagen
                        if (signImagePath == null)
                        {
                            setResult(RESULT_CANCELED, intent);
                            Toast.makeText(DrawSignActivity.this, "Error al guardar imagen",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            intent.putExtra("imagePath", signImagePath);
                            setResult(RESULT_OK, intent);
                        }

                        finish();
                    }
                }, 500);
            }
        });

        signaturePad = findViewById(R.id.signature_pad);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }

    private String saveImageBitmap(Bitmap bitmap)
    {
        String imagePath = null;

        File dir = new File(getFilesDir(), "firmas");
        if (!dir.exists())
        {
            dir.mkdir();
        }

        FileOutputStream fos = null;
        try
        {
            File image = File.createTempFile("firma", ".png", dir);
            fos = new FileOutputStream(image);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            imagePath = image.getAbsolutePath();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                fos.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return imagePath;
    }
}
