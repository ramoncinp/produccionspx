package com.chipred.produccionspx.projects;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.DispModel;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.adapters.ProjectOverviewAdapter;
import com.chipred.produccionspx.printer.PrinterManager;
import com.chipred.produccionspx.printer.PrintersDialogFragment;
import com.chipred.produccionspx.users.UserData;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.annotation.Nullable;

public class ProjectsActivity extends AppCompatActivity
{
    // Constantes
    private static final int PROJECT_FORM_REQUEST_CODE = 1;
    public static final String CAN_WRITE_PROJECTS = "canWrite";

    // Variables
    private boolean canWrite = false;
    private boolean fromNotification = false;
    private boolean openSignsDialog = false;
    private int counter = 0;
    private int requestedDispModels = 0;
    private int gottenDispModels = 0;
    private String currentFilter;
    private String projectId;

    // Colecciones
    private ArrayList<ProjectOverview> projectOverviews = new ArrayList<>();
    private ArrayList<UserData> projectsUsers = new ArrayList<>();
    private HashMap<String, HashSet<DispModel>> dispModels;

    // Objetos
    private CollectionReference projectsCollection;
    private CollectionReference dispModelsCollections;
    private Query query;
    private PrintersDialogFragment printerFragment;
    private PrinterManager printerManager;
    private ProjectOverviewAdapter projectOverviewAdapter;
    private SharedPreferences sharedPreferences;

    // Views
    private Dialog filterSelectorDialog;
    private FloatingActionButton fabAddProject;
    private FloatingActionButton fabPrintProjects;
    private FloatingActionMenu fam;
    private ProgressDialog progressDialog;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView noProjects;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        // Dar título a la actividad
        setTitle(getResources().getString(R.string.projects_header));

        //Agregar flecha para regresar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Obtener permisos
        canWrite = getIntent().getBooleanExtra(CAN_WRITE_PROJECTS, false);

        // Referenciar views
        initViews();

        // Iniciar sharedPreferences
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES,
                Context.MODE_PRIVATE);

        //Evaluar si viene de una notificacion
        Intent intent = getIntent();
        if (intent.hasExtra(Constants.PROJECT_ORDER_NUMBER))
        {
            fromNotification = true;
            projectId = intent.getStringExtra(Constants.PROJECT_ORDER_NUMBER);

            if (intent.hasExtra(Constants.OPEN_SIGNS_DIALOG))
            {
                openSignsDialog = true;
            }
        }

        //Crear instancia para obtener el query
        projectsCollection =
                FirebaseFirestore.getInstance().collection(Constants.PROJECTS_COLLECTION);

        //Definir filtro actual
        currentFilter = sharedPreferences.getString(Constants.PROJECTS_LIST_FILTER,
                getResources().getString(R.string.fp_fecha_modificacion));

        //Crear query default
        queryWithCurrentFilter();

        //Ejecutar query
        getProjectsFromDb();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (progressDialog != null)
        {
            if (progressDialog.isShowing()) progressDialog.dismiss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @androidx.annotation.Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PROJECT_FORM_REQUEST_CODE)
        {
            getProjectsFromDb();
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if (fromNotification)
        {
            //Abrir formulario
            openProject(projectId);

            //Limpiar variables
            fromNotification = false;
            projectId = null;
            openSignsDialog = false;
        }
    }

    private void initViews()
    {
        progressBar = findViewById(R.id.progress_bar);
        noProjects = findViewById(R.id.no_projects_tv);
        recyclerView = findViewById(R.id.projects_overview_list);
        swipeRefreshLayout = findViewById(R.id.swipe_layout);
        fabAddProject = findViewById(R.id.add_project_fab);
        fam = findViewById(R.id.fab_projects_menu);
        fabPrintProjects = findViewById(R.id.print_projects_fab);
        fabAddProject.setOnClickListener(v ->
        {
            //Iniciar progress dialog
            progressDialog = ProgressDialog.show(ProjectsActivity.this, "", "Cargando...");

            Intent openForm = new Intent(ProjectsActivity.this, ProjectForm.class);
            openForm.putExtra(Constants.ADD_OR_MODIFY_PROJECT, true);
            openForm.putExtra(Constants.CAN_MODIFY, true);
            startActivityForResult(openForm, PROJECT_FORM_REQUEST_CODE);
        });
        fabPrintProjects.setOnClickListener(v -> selectPrinter());
        swipeRefreshLayout.setOnRefreshListener(this::getProjectsFromDb);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.filter_list_menu, menu);

        // Obtener searchView
        SearchView searchView = (SearchView) menu.findItem(R.id.search_menu).getActionView();
        searchView.setQueryHint(getResources().getString(R.string.search_projects));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                Log.d("SearchProject", "BuscarFinalmente -> " + query);
                projectOverviewAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                Log.d("SearchProject", "Buscar -> " + newText);
                projectOverviewAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
        {
            this.finish();
        }
        else if (id == R.id.project_filter)
        {
            showFilterSelectorDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings({"unchecked", "null"})
    private void getProjectsFromDb()
    {
        query.addSnapshotListener(this, (queryDocumentSnapshots, e) ->
        {
            projectOverviews.clear();
            projectsUsers.clear();
            counter = 0;

            if (queryDocumentSnapshots != null)
            {
                //Crear adaptador para mostrar las vistas previas
                projectOverviewAdapter =
                        new ProjectOverviewAdapter(projectOverviews);

                for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments())
                {
                    //Crear vista previa de proyecto
                    ProjectOverview projectOverview = new ProjectOverview(snapshot.getId());

                    //Obtener datos
                    projectOverview.setName(snapshot.getString("nombre"));
                    projectOverview.setProjectStatus(snapshot.getString("estatus_proyecto"));
                    projectOverview.setStatus(snapshot.getString("estatus"));
                    projectOverview.setAdminStatus(snapshot.getString("estatu" +
                            "s_administrativo"
                    ));
                    projectOverview.setCrmTicket(snapshot.getString("ticket_crm"));

                    Log.d("Proyectos", projectOverview.getName());

                    //Obtener datos generales
                    Map<String, Object> generalData = (Map<String, Object>) snapshot.get(
                            "datos_generales");

                    projectOverview.setClient(generalData.get("cliente").toString());
                    projectOverview.setStation(generalData.get("estacion").toString());
                    projectOverview.setStationNumber(generalData.get("numero_estacion").toString());
                    projectOverview.setRequestDate(generalData.get(
                            "fecha_solicitud_cliente").toString());
                    projectOverview.setDispensersIds((ArrayList<String>) snapshot.get(
                            "dispensarios"));

                    //Obtener timestamp
                    projectOverview.setEpoch(snapshot.getLong("epoch"));

                    //Agregar a lista
                    projectOverviews.add(projectOverview);

                    //Posteriormente agregar id_usuario
                    String userId;
                    if ((userId = snapshot.getString("id_usuario")) != null)
                    {
                        projectOverview.setUserId(userId);
                        searchUser(userId);
                    }
                }

                //Validar si hubo proyectos
                if (projectOverviews.isEmpty())
                {
                    noProjects.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
                else
                {
                    //Definir clickListener
                    projectOverviewAdapter.setClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            //Iniciar progress dialog
                            progressDialog = ProgressDialog.show(ProjectsActivity.this, "",
                                    "Cargando...");

                            //Obtener objeto del proyecto
                            ProjectOverview project =
                                    projectOverviewAdapter.getProyect(recyclerView.getChildAdapterPosition(v));
                            openProject(project.getOrderNumber());
                        }
                    });

                    //Asignar a lista
                    recyclerView.setAdapter(projectOverviewAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(ProjectsActivity.this));
                    noProjects.setVisibility(View.GONE);
                }
            }
            else
            {
                noProjects.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setVisibility(View.GONE);
            }
        });
    }

    private void openProject(String orderNumber)
    {
        Intent openForm = new Intent(ProjectsActivity.this, ProjectForm.class);
        openForm.putExtra(Constants.ADD_OR_MODIFY_PROJECT, false);
        openForm.putExtra(Constants.CAN_MODIFY, canWrite);
        openForm.putExtra(Constants.PROJECT_ORDER_NUMBER,
                orderNumber);

        if (openSignsDialog)
        {
            openForm.putExtra(Constants.OPEN_SIGNS_DIALOG,
                    true);
        }

        startActivityForResult(openForm, PROJECT_FORM_REQUEST_CODE);
    }

    private void searchUser(String userId)
    {
        //Obtener usuario correspondiente al proyecto
        UserData projectUser = getUserFromList(userId);

        //Si no esta el usuario en la lista... buscarlo en la bd
        if (projectUser == null)
        {
            CollectionReference users = FirebaseFirestore.getInstance().collection("usuarios");
            users.document(userId).addSnapshotListener(this, new EventListener<DocumentSnapshot>()
            {
                @Override
                public void onEvent(@Nullable DocumentSnapshot snapshot,
                                    @Nullable FirebaseFirestoreException e)
                {
                    if (e != null || snapshot == null)
                    {
                        return;
                    }

                    //Obtener objeto
                    UserData userData = snapshot.toObject(UserData.class);

                    //Actualizar lista
                    completeUsersInfo(userData);

                    //Agregar a la lista de informacion de usuarios
                    projectsUsers.add(userData);
                }
            });
        }
        else
        {
            completeUsersInfo(projectUser);
        }
    }

    private UserData getUserFromList(String userId)
    {
        for (UserData user : projectsUsers)
        {
            if (user.getId().equals(userId)) return user;
        }

        return null;
    }

    private void completeUsersInfo(UserData userData)
    {
        for (ProjectOverview projectOverview : projectOverviews)
        {
            //Si no se ha asignado las iniciales al elemento
            if (projectOverview.getUserInitials().isEmpty())
            {
                //Asignar informacion de usuario
                if (projectOverview.getUserId().equals(userData.getId()))
                {
                    projectOverview.setUserInitials(userData.getInitials());
                    counter++;

                    //Si ya se agregaron los nombres a los proyectos... mostrar contenido
                    if (counter == projectOverviews.size())
                    {
                        showContent();
                    }
                }
            }
        }

        //Actualizar elementos
        projectOverviewAdapter.notifyDataSetChanged();
    }

    private void showContent()
    {
        //Evaluar permisos
        if (canWrite)
        {
            fam.showMenuButton(true);
        }
        else
        {
            fam.showMenuButton(false);
        }

        if (noProjects.getVisibility() == View.GONE)
        {
            swipeRefreshLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            swipeRefreshLayout.setVisibility(View.GONE);
            Toast.makeText(ProjectsActivity.this, "No se tienen permisos para ver " +
                    "proyectos", Toast.LENGTH_SHORT).show();
        }

        progressBar.setVisibility(View.GONE);
        if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
    }

    private void hideContent()
    {
        progressBar.setVisibility(View.VISIBLE);
        fabAddProject.hideButtonInMenu(false);
        swipeRefreshLayout.setVisibility(View.GONE);
    }

    private void selectPrinter()
    {
        printerFragment =
                PrintersDialogFragment.newInstance(new PrintersDialogFragment.PrinterDialogInterface()
                {
                    @Override
                    public void onIpSelected(String ip)
                    {
                        printProjectInfo(ip);
                    }
                });

        printerFragment.show(getSupportFragmentManager(), "print_dialog");
    }

    private void printProjectInfo(String printerIp)
    {
        //Crear instancia de impresora
        printerManager = new PrinterManager(this, printerIp,
                new PrinterManager.PrinterListener()
                {
                    @Override
                    public void onSuccesfulPrint(final String msg)
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                printerFragment.dismiss();
                                Toast.makeText(ProjectsActivity.this, msg, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void onPrinterError(final String msg)
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                printerFragment.dismiss();
                                Toast.makeText(ProjectsActivity.this, msg, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void onWarning(final String msg)
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                printerFragment.dismiss();
                                Toast.makeText(ProjectsActivity.this, msg, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });

        //Obtener información de los dispensarios de cada proyecto, // Realizar impresión
        // porteriormente
        getProjectDispModels();

        //Realizar impresión
        //printerManager.printProyectsList(currentFilter, projectOverviews);
    }

    private void showFilterSelectorDialog()
    {
        if (filterSelectorDialog == null)
        {
            // Crear builder
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // Crear layout
            View content = getLayoutInflater().inflate(R.layout.dialog_project_filter_selector,
                    null);

            // Definir layout
            builder.setView(content);

            // Definir boton de regresar
            builder.setNegativeButton("Regresar", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });

            // Crear diálogo
            filterSelectorDialog = builder.create();

            // Checkear filtro actual
            RadioGroup list = content.findViewById(R.id.filters_radio_group);
            for (int i = 0; i < list.getChildCount(); i++)
            {
                View view = list.getChildAt(i);
                if (view instanceof RadioButton)
                {
                    // Hacer cast a RadioButton del view actual
                    RadioButton mRadioButton = (RadioButton) view;

                    // Checkear el filtro correspondiente
                    if (mRadioButton.getText().toString().equals(currentFilter))
                    {
                        mRadioButton.setChecked(true);
                    }
                }
            }

            // Crear listener para los elementos
            View.OnClickListener filterSelected = new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // Obtener texto de radio button
                    RadioButton selectedButton = (RadioButton) v;

                    // Definir filtro actual
                    currentFilter = selectedButton.getText().toString();

                    // Guardar como filtro predeterminado
                    sharedPreferences.edit().putString(Constants.PROJECTS_LIST_FILTER,
                            currentFilter).apply();

                    // Cerar diálogo
                    filterSelectorDialog.dismiss();

                    // Obtener query a ejecutar
                    queryWithCurrentFilter();

                    // Ejecutar query
                    getProjectsFromDb();

                    //Mostrar progress
                    hideContent();
                }
            };

            // Obtener layout y seleccionar el filtro actual
            for (int i = 0; i < list.getChildCount(); i++)
            {
                View view = list.getChildAt(i);
                if (view instanceof RadioButton)
                {
                    // Hacer cast a RadioButton del view actual
                    RadioButton mRadioButton = (RadioButton) view;

                    // Definir clickListener
                    mRadioButton.setOnClickListener(filterSelected);
                }
            }
        }

        // Mostrar diálogo
        filterSelectorDialog.show();
    }

    private void queryWithCurrentFilter()
    {
        // Obtener item seleccionado
        if (currentFilter.equals(getResources().getString(R.string.fp_arrendamiento)))
        {
            query = projectsCollection.whereEqualTo("tipo_venta",
                    Constants.ARRENDAMIENTO);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_venta)))
        {
            query = projectsCollection.whereEqualTo("tipo_venta", Constants.VENTA);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_en_proceso)))
        {
            query = projectsCollection.whereEqualTo("estatus", Constants.EN_PROCESO);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_entregados)))
        {
            query = projectsCollection.whereEqualTo("estatus", Constants.ENTREGADO);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_terminados)))
        {
            query = projectsCollection.whereEqualTo("estatus", Constants.TERMINADO);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_cotizados)))
        {
            query = projectsCollection.whereEqualTo("estatus_administrativo",
                    Constants.COTIZADO);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_facturados)))
        {
            query = projectsCollection.whereEqualTo("estatus_administrativo",
                    Constants.FACTURA);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_pagados)))
        {
            query = projectsCollection.whereEqualTo("estatus_administrativo",
                    Constants.PAGADO);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_parcialidad)))
        {
            query = projectsCollection.whereEqualTo("estatus_administrativo",
                    Constants.PARCIALIDAD);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_prefacturados)))
        {
            query = projectsCollection.whereEqualTo("estatus_administrativo",
                    Constants.PREFACTURA);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_estatus)))
        {
            query = projectsCollection.orderBy("estatus", Query.Direction.DESCENDING);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_estatus_administrativo)))
        {
            query = projectsCollection.orderBy("estatus_administrativo",
                    Query.Direction.DESCENDING);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_fecha_modificacion)))
        {
            query = projectsCollection.orderBy("epoch", Query.Direction.DESCENDING);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_numero_orden)))
        {
            query = projectsCollection.orderBy("numero_orden",
                    Query.Direction.DESCENDING);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_ticket_crm)))
        {
            query = projectsCollection.orderBy("ticket_crm",
                    Query.Direction.DESCENDING);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_en_espera)))
        {
            query = projectsCollection.whereEqualTo("estatus_proyecto",
                    Constants.EN_ESPERA);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_proyecto_en_proceso)))
        {
            query = projectsCollection.whereEqualTo("estatus_proyecto",
                    Constants.EN_PROCESO);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_concluido)))
        {
            query = projectsCollection.whereEqualTo("estatus_proyecto",
                    Constants.CONCLUIDO);
        }
        else if (currentFilter.equals(getResources().getString(R.string.fp_cancelado)))
        {
            query = projectsCollection.whereEqualTo("estatus_proyecto",
                    Constants.CANCELADO);
        }
    }

    private void getProjectDispModels()
    {
        // Obtener referencia de dispensarios
        if (dispModelsCollections == null)
        {
            dispModelsCollections =
                    FirebaseFirestore.getInstance().collection(Constants.DISPENSERS_COLLECTION);
        }

        // Reiniciar contadores
        gottenDispModels = 0;
        requestedDispModels = 0;

        // Inicializar HashMap
        dispModels = new HashMap<>();

        // Pedir la información de todos los dispensarios
        for (ProjectOverview project : projectOverviews)
        {
            if (project.getDispensersIds() != null)
            {
                for (String dispRef : project.getDispensersIds())
                {
                    // Aumentar contador de dispensarios pedidos
                    requestedDispModels++;

                    // Pedir información de dispensario
                    getDispData(dispRef);
                }
            }
        }
    }

    private void getDispData(String dispModelId)
    {
        dispModelsCollections.document(dispModelId).get().addOnCompleteListener(task ->
        {
            // Aumentar contador de dispensarios obtenidos
            gottenDispModels++;

            // Evaluar
            if (task.isSuccessful() && task.getResult() != null)
            {
                // Obtener dato
                DispModel dispModel = task.getResult().toObject(DispModel.class);

                // Agregar modelo a lista de resultados
                if (dispModel != null)
                {
                    HashSet<DispModel> set;
                    if (dispModels.containsKey(dispModel.getOrdenInterna()))
                    {
                        set = dispModels.get(dispModel.getOrdenInterna());
                    }
                    else
                    {
                        set = new HashSet<>();
                    }
                    if (set != null) set.add(dispModel);
                    dispModels.put(dispModel.getOrdenInterna(), set);
                }
            }
            else
            {
                Log.e("Dispensarios", "Error al obtener dispensario " + dispModelId);
            }

            if (requestedDispModels == gottenDispModels)
            {
                printerManager.printProyectsList(currentFilter, projectOverviews, dispModels);
                Log.e("Dispensarios", "Dispensarios obtenidos!");
            }
        });
    }
}