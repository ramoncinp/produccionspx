package com.chipred.produccionspx.projects;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.DispModel;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class DispensersProvider
{
    public static void getDisModels(ArrayList<String> dispReferences, DispProviderInterface dispInterface)
    {
        final CollectionReference dispsCollection =
                FirebaseFirestore.getInstance().collection(Constants.DISPENSERS_COLLECTION);

        List<Task<DocumentSnapshot>> tasks = new ArrayList<>();

        for (String dispId : dispReferences)
        {
            tasks.add(dispsCollection.document(dispId).get());
        }

        Tasks.whenAllComplete(tasks).addOnCompleteListener(task ->
        {
            ArrayList<DispModel> dispModels = null;

            if (task.isSuccessful() && task.getResult() != null)
            {
                dispModels = new ArrayList<>();

                // Obtener lista de resultados de dispensarios
                for (Task<?> mTask : task.getResult())
                {
                    // Obtener resultado
                    DocumentSnapshot dispDocument =
                            ((Task<DocumentSnapshot>) mTask).getResult();

                    // Convertirlo a objeto
                    DispModel dispModel = dispDocument.toObject(DispModel.class);
                    if (dispModel != null)
                    {
                        dispModel.setId(dispDocument.getId());
                        dispModels.add(dispModel);
                    }
                }
            }

            dispInterface.onComplete(dispModels);
        });
    }

    public interface DispProviderInterface
    {
        void onComplete(ArrayList<DispModel> dispModels);
    }
}
