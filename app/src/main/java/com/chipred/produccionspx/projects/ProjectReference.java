package com.chipred.produccionspx.projects;

import java.util.List;

public class ProjectReference {

    private Long numero_orden;
    private Long epoch;
    private String cliente;
    private String estacion;
    private String estatus;
    private String estatus_administrativo;
    private String estauts_proyecto;
    private String ticket_crm;
    private List<String> cliente_query;
    private List<String> estacion_query;

    public ProjectReference() {
    }

    public Long getNumero_orden() {
        return numero_orden;
    }

    public void setNumero_orden(Long numero_orden) {
        this.numero_orden = numero_orden;
    }

    public Long getEpoch() {
        return epoch;
    }

    public void setEpoch(Long epoch) {
        this.epoch = epoch;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus_administrativo() {
        return estatus_administrativo;
    }

    public void setEstatus_administrativo(String estatus_administrativo) {
        this.estatus_administrativo = estatus_administrativo;
    }

    public String getEstauts_proyecto() {
        return estauts_proyecto;
    }

    public void setEstauts_proyecto(String estauts_proyecto) {
        this.estauts_proyecto = estauts_proyecto;
    }

    public List<String> getCliente_query() {
        return cliente_query;
    }

    public void setCliente_query(List<String> cliente_query) {
        this.cliente_query = cliente_query;
    }

    public List<String> getEstacion_query() {
        return estacion_query;
    }

    public void setEstacion_query(List<String> estacion_query) {
        this.estacion_query = estacion_query;
    }

    public String getTicket_crm() {
        return ticket_crm;
    }

    public void setTicket_crm(String ticket_crm) {
        this.ticket_crm = ticket_crm;
    }
}
