package com.chipred.produccionspx.projects;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.chipred.produccionspx.Color;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.utils.DatePickerFragment;
import com.chipred.produccionspx.DispModel;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.SaeDispModel;
import com.chipred.produccionspx.Vehicle;
import com.chipred.produccionspx.alarms.Alarm;
import com.chipred.produccionspx.signs.SignDialogFragment;
import com.chipred.produccionspx.utils.DateUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.adapters.DispenserModelAdapter;
import com.chipred.produccionspx.notifications.NotificationsManager;
import com.chipred.produccionspx.printer.PrinterManager;
import com.chipred.produccionspx.printer.PrintersDialogFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectForm extends AppCompatActivity {
    //Constantes
    private static final String TAG = ProjectForm.class.getSimpleName();
    private static final String[] PROJECT_STATUS = {Constants.EN_ESPERA, Constants.EN_PROCESO,
            Constants.CONCLUIDO, Constants.CANCELADO};
    private static final String[] PROD_STATUS = {Constants.EN_PROCESO, Constants.TERMINADO,
            Constants.ENTREGADO};
    private static final String[] PROJECT_ADMN_STATUS = {Constants.COTIZADO, Constants.PREFACTURA,
            Constants.FACTURA, Constants.PARCIALIDAD, Constants.PAGADO};

    //Variables
    private boolean addOrModify = true; //Verdadero para agregar, falso para modificar
    private boolean canModify = false;
    private boolean removeProdAlarm = false;
    private boolean removeAdminAlarm = false;
    private String currentProjectStatus = Constants.EN_PROCESO;
    private String currentProdStatus = Constants.EN_PROCESO;
    private String currentAdminStatus = Constants.COTIZADO;
    private String orderNumber;
    private String projectSimpleName;

    //Listas
    private ArrayList<String> dispReferences = new ArrayList<>();
    private final ArrayList<Color> canopysList = new ArrayList<>();
    private final ArrayList<Color> fibrasList = new ArrayList<>();
    private final ArrayList<DispModel> dispModels = new ArrayList<>();
    private final ArrayList<SaeDispModel> saeDispModels = new ArrayList<>();
    private final ArrayList<Vehicle> vehicleList = new ArrayList<>();
    private final List<Contact> commonContacts = new ArrayList<>();

    //Objetos
    private Contact mainContact, techContact;
    private Color selectedCanopy;
    private Color selectedFibra;
    private DispenserModelAdapter dispenserModelAdapter;
    private Map<String, Object> existingProjectRoot = new HashMap<>();
    private Map<String, Object> projectDiffs = new HashMap<>();
    private final Map<String, Object> projectRoot = new HashMap<>();
    private final Map<String, Object> dispsDiffs = new HashMap<>();
    private PrintersDialogFragment printerFragment;

    //Objetos Firebase
    private FirebaseFirestore db;
    private CollectionReference projectsRef;
    private CollectionReference projectsReferencesCollection;
    private CollectionReference dispensersRef;
    private CollectionReference projectsDifferencesRef;

    //Views
    private CardView shipmentAccesoriesCv;
    private Dialog contactDetailDialog;
    private Dialog setStatusDialog;
    private MaterialEditText clientRequestDateEt;
    private MaterialEditText finishedProjectDateEt;
    private MaterialEditText internOrder;
    private MaterialEditText startProjectDateEt;
    private MaterialEditText shipmentDateEt;
    private NestedScrollView content;
    private ProgressBar progressBar;
    private TextView noDispModels;
    private RecyclerView dispModelsList;

    //Layouts de contenido
    private LinearLayout generalDataContent;
    private LinearLayout dispensersContent;
    private LinearLayout evo6ComponentsContent;
    private LinearLayout shipmentTypeContent;
    private LinearLayout shipmentAccesoriesContent;
    private LinearLayout vehicleContent;
    private LinearLayout softwareContent;
    private LinearLayout imagesContent;
    private LinearLayout specialInstructionsContent;

    //Progress
    private ProgressBar generalDataProgress;
    private ProgressBar dispensersProgress;
    private ProgressBar evo6ComponentsProgress;
    private ProgressBar shipmentTypeProgress;
    private ProgressBar shipmentAccesoriesProgress;
    private ProgressBar vehicleProgress;
    private ProgressBar softwareProgress;
    private ProgressBar imagesProgress;
    private ProgressBar specialInstructionsProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_form);

        //Agregar flecha para regresar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Obtener variables de la clase
        addOrModify = getIntent().getBooleanExtra(Constants.ADD_OR_MODIFY_PROJECT, false);
        canModify = getIntent().getBooleanExtra(Constants.CAN_MODIFY, false);

        //Mostrar titulo en base a variable
        if (addOrModify) {
            setTitle(getResources().getString(R.string.add_project_form_header));
        } else {
            if (canModify) {
                setTitle(getResources().getString(R.string.modify_project_form_header));
            } else {
                setTitle(getResources().getString(R.string.read_project_form_header));
            }
        }

        //Inicializar vistas
        initViews();

        //Inicializar componentes de base de datos
        initCollectionReferences();
        getSaeDispModelsFromDb();
        getVehiclesFromDb();
        getImagesFromDb();

        //Si se eligió modificar
        if (!addOrModify) {
            //Obtener numero de orden
            orderNumber = getIntent().getStringExtra(Constants.PROJECT_ORDER_NUMBER);

            //Obtener datos de proyecto existente
            queryProjectsChanges(orderNumber);
        } else {
            //Obtener último número de orden
            getLastOrderNumber();

            //Mostrar formulario
            showAllContents();

            //Obtener sugerencias de contactos
            getContacts();
        }

        if (!canModify) disableViews();

        if (getIntent().hasExtra(Constants.OPEN_SIGNS_DIALOG)) {
            showSignsDialog();
        }
    }

    private void showAllContents() {
        generalDataContent.setVisibility(View.VISIBLE);
        generalDataProgress.setVisibility(View.GONE);

        dispensersContent.setVisibility(View.VISIBLE);
        dispensersProgress.setVisibility(View.GONE);

        evo6ComponentsContent.setVisibility(View.VISIBLE);
        evo6ComponentsProgress.setVisibility(View.GONE);

        shipmentTypeContent.setVisibility(View.VISIBLE);
        shipmentTypeProgress.setVisibility(View.GONE);

        vehicleContent.setVisibility(View.VISIBLE);
        vehicleProgress.setVisibility(View.GONE);

        softwareContent.setVisibility(View.VISIBLE);
        softwareProgress.setVisibility(View.GONE);

        imagesContent.setVisibility(View.VISIBLE);
        imagesProgress.setVisibility(View.GONE);

        specialInstructionsContent.setVisibility(View.VISIBLE);
        specialInstructionsProgress.setVisibility(View.GONE);

        shipmentAccesoriesContent.setVisibility(View.VISIBLE);
        shipmentAccesoriesProgress.setVisibility(View.GONE);
    }

    private void queryProjectsChanges(final String orderNumber) {
        projectsDifferencesRef.document(orderNumber).get().addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult() != null) {
                if (task.getResult().getData() != null)
                    projectDiffs = task.getResult().getData();
            } else {
                Log.v(TAG, "No se obtuvieron los cambios del proyecto -> " + orderNumber);
            }

            // Obtener información del proyecto
            queryExistingProject(orderNumber);
        });
    }

    @SuppressWarnings({"unchecked", "null"})
    private void queryExistingProject(String orderNumber) {
        projectsRef.document(orderNumber).get().addOnCompleteListener(this,
                task ->
                {
                    if (task.isSuccessful()) {
                        //Obtener documento
                        DocumentSnapshot project = task.getResult();

                        //Validar documento
                        if (project == null) return;

                        //////// Asignar todos los datos a objeto principal ////////
                        existingProjectRoot = project.getData();

                        try {
                            //Obtener estatus actual
                            currentProjectStatus = (String) existingProjectRoot.get(
                                    "estatus_proyecto");
                            currentProdStatus = (String) existingProjectRoot.get("estatus");
                            currentAdminStatus = (String) existingProjectRoot.get(
                                    "estatus_administrativo");

                            //Obtener datos generales
                            setGeneralData((Map<String, Object>) existingProjectRoot.get(
                                    "datos_generales"));

                            //setShipmentAccesories((Map<String, Object>) project.get(
                            //      "accesorios_embarque"));

                            //Obtener lista de referencia de dispensarios
                            setDispensers((ArrayList<String>) existingProjectRoot.get(
                                    "dispensarios"));

                            setEvo6Components((Map<String, Object>) existingProjectRoot.get(
                                    "componentes_evo6"));

                            setShipment((Map<String, Object>) existingProjectRoot.get(
                                    "tipo_embarque"));

                            setVehicles((ArrayList<String>) existingProjectRoot.get(
                                    "vehiculos"));

                            setSoftware((Map<String, Object>) existingProjectRoot.get(
                                    "software"));

                            setImages((Map<String, Object>) existingProjectRoot.get(
                                    "imagenes"));

                            setSpecialInstructions((String) existingProjectRoot.get(
                                    "instrucciones_especiales"));
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            Toast.makeText(ProjectForm.this, "Error al obtener datos del " +
                                    "proyecto", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ProjectForm.this, "Error al buscar proyecto " +
                                        "seleccionado",
                                Toast.LENGTH_LONG).show();

                        //Terminar actividad
                        closeProjectForm(RESULT_CANCELED);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.project_form_menu, menu);
        if (!addOrModify && !canModify) {
            menu.findItem(R.id.save).setVisible(false);
        } else if (addOrModify) {
            menu.findItem(R.id.vobo_sign).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                closeProjectForm(RESULT_CANCELED);
                break;

            case R.id.save:
                addProjectToDatabase();
                break;

            case R.id.vobo_sign:
                showSignsDialog();
                break;

            case R.id.set_status:
                showSetStatusDialog();
                break;

            case R.id.print:
                selectPrinter();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        //Referenciar views
        internOrder = findViewById(R.id.intern_order_number);
        clientRequestDateEt = findViewById(R.id.client_request_date_et);
        shipmentDateEt = findViewById(R.id.shipment_date_et);
        finishedProjectDateEt = findViewById(R.id.finished_project_date_et);
        startProjectDateEt = findViewById(R.id.start_project_date_et);
        FloatingActionButton addDispenserModel = findViewById(R.id.add_dispenser_model);
        noDispModels = findViewById(R.id.no_disp_models);
        dispModelsList = findViewById(R.id.dispenser_model_list);
        progressBar = findViewById(R.id.progress_bar);
        content = findViewById(R.id.project_form_content);
        MaterialEditText stationNumber = findViewById(R.id.station_number);

        //Contenidos
        generalDataContent = findViewById(R.id.general_data_content);
        dispensersContent = findViewById(R.id.dispensers_content);
        evo6ComponentsContent = findViewById(R.id.evo6_components_content);
        shipmentAccesoriesContent = findViewById(R.id.shipment_accesories_content);
        shipmentTypeContent = findViewById(R.id.shipment_type_content);
        vehicleContent = findViewById(R.id.vehicle_content);
        softwareContent = findViewById(R.id.software_content);
        imagesContent = findViewById(R.id.images_content);
        specialInstructionsContent = findViewById(R.id.special_instructions_content);

        //Progress
        generalDataProgress = findViewById(R.id.general_data_progress);
        dispensersProgress = findViewById(R.id.dispensers_progress);
        evo6ComponentsProgress = findViewById(R.id.evo6_components_progress);
        shipmentTypeProgress = findViewById(R.id.shipment_type_progress);
        shipmentAccesoriesProgress = findViewById(R.id.shipment_accesories_progress);
        vehicleProgress = findViewById(R.id.vehicle_progress);
        softwareProgress = findViewById(R.id.software_progress);
        imagesProgress = findViewById(R.id.images_progress);
        specialInstructionsProgress = findViewById(R.id.special_instructions_progress);

        //CardViews
        shipmentAccesoriesCv = findViewById(R.id.shipment_accesories_cv);

        //Definir listener
        View.OnClickListener datePickerListener = v -> {
            //Obtener id de la vista seleccionada
            int selectedId = v.getId();

//                // Fecha de embarque
//                if (selectedId == R.id.shipment_date_et || selectedId == R.id
//                .start_project_date_et)
//                {
//                    // Validar si existe fecha de terminado
//                    if (finishedProjectDateEt.getText().toString().isEmpty())
//                    {
//                        ((MaterialEditText) v).setError("Definir primero fecha de terminado");
//                        return;
//                    }
//                }

            DatePickerFragment fragment = DatePickerFragment.newInstance(new DatePickerDialog
                    .OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    String gottenDate =
                            Constants.twoDigits(i2) + "/" + Constants.monthNumberToString
                                    (i1 + 1) + "/" + i;

                    MaterialEditText dateEt = (MaterialEditText) v;
                    dateEt.setText(gottenDate);
                }
            });

            fragment.show(ProjectForm.this.getSupportFragmentManager(), "datePicker");
        };

        //Definir listener para borrar fecha seleccionada
        View.OnLongClickListener removeDate = v -> {
            //Instanciar EditText
            MaterialEditText editText = (MaterialEditText) v;
            editText.setText("");

            return false;
        };

        //Inicializar views para detalles de contacto
        initContactsViews();

        //Asignar listeners
        clientRequestDateEt.setOnClickListener(datePickerListener);
        shipmentDateEt.setOnClickListener(datePickerListener);
        finishedProjectDateEt.setOnClickListener(datePickerListener);
        startProjectDateEt.setOnClickListener(datePickerListener);
        clientRequestDateEt.setOnLongClickListener(removeDate);
        shipmentDateEt.setOnLongClickListener(removeDate);
        finishedProjectDateEt.setOnLongClickListener(removeDate);
        startProjectDateEt.setOnLongClickListener(removeDate);

        addDispenserModel.setOnClickListener(v -> {
            MaterialEditText internOrder = findViewById(R.id.intern_order_number);

            //Validar que ya exista una orden interna
            boolean valid = internOrder.validateWith(new METValidator("Defina orden interna") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return !isEmpty;
                }
            });

            if (valid) {
                showDispenserDialog(null);
            }
        });

        //Definir adaptador de modelos de dispensarios
        dispenserModelAdapter = new DispenserModelAdapter(dispModels, this);
        dispenserModelAdapter.setOnClickListener(v -> {
            final DispModel dispModel = dispModels.get(dispModelsList.getChildAdapterPosition(v));
            showDispenserDialog(dispModel);
        });
        dispenserModelAdapter.setOnLongClickListener(v -> {
            deleteDispenserDialog(v);
            return true;
        });
        dispModelsList.setAdapter(dispenserModelAdapter);
        dispModelsList.setLayoutManager(new LinearLayoutManager(this));

        //Spinners
        //Obtener referencia de spinner
        MaterialBetterSpinner imagesSpinner = findViewById(R.id.images_spinner);
        MaterialBetterSpinner canopySpinner = findViewById(R.id.canopy_spinner);
        MaterialBetterSpinner fibrasSpinner = findViewById(R.id.fibras_spinner);

        //Definir adaptador
        ArrayAdapter<String> modelsArrayAdapter = new ArrayAdapter<>(ProjectForm.this
                , android.R
                .layout.simple_dropdown_item_1line, new ArrayList<>());

        imagesSpinner.setAdapter(modelsArrayAdapter);
        canopySpinner.setAdapter(modelsArrayAdapter);
        fibrasSpinner.setAdapter(modelsArrayAdapter);

        //Definir filtros
        stationNumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        //Inicializar componentes evo6
        initEvo6Components();

        //Inicializar accesorios de embarque
        initShipmentAccesories();

        //Inicializar spinners de vehículos
        initVehiclesSpinner();

        //Inicializar views de software
        initSoftwareViews();
    }

    private void initCollectionReferences() {
        //Inicializar instancia
        db = FirebaseFirestore.getInstance();

        //Crear referencia para base de datos
        projectsRef = db.collection(Constants.PROJECTS_COLLECTION);
        projectsReferencesCollection = db.collection(Constants.PROJECTS_REFERENCES_COLLECTION);

        //Crear referencia para dispensarios
        dispensersRef = db.collection(Constants.DISPENSERS_COLLECTION);

        //Crear referencia para colección cambios de proyectos
        projectsDifferencesRef = db.collection(Constants.PROJECTS_CHANGES_COLLECTION);
    }

    private void initContactsViews() {
        final MaterialEditText projectContact = findViewById(R.id.project_contact);
        final MaterialEditText projectTechContact = findViewById(R.id.project_tech_contact);

        projectContact.setFocusable(false);
        projectContact.setOnClickListener(v -> showContactDetailDialog(true, projectContact.getText().toString()));

        projectTechContact.setFocusable(false);
        projectTechContact.setOnClickListener(v -> showContactDetailDialog(false, projectTechContact.getText().toString()));
    }

    private void showContactDetailDialog(final boolean isMainContact, final String name) {
        //Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //Definir view
        final View content = getLayoutInflater().inflate(R.layout.contact_detail_layout, null);
        builder.setView(content);

        //Obtener editText's
        final MaterialEditText mainNameEt = findViewById(R.id.project_contact);
        final MaterialEditText mainNameTechEt = findViewById(R.id.project_tech_contact);
        final MaterialAutoCompleteTextView nameEt = content.findViewById(R.id.name_et);
        final MaterialEditText email = content.findViewById(R.id.email_et);
        final MaterialEditText phone = content.findViewById(R.id.phone_et);

        //Definir adaptador para sugerencias
        if (commonContacts != null) {
            // Definir adaptador
            ContactsAdapter adapter = new ContactsAdapter(this, new ArrayList<>(commonContacts));
            nameEt.setDropDownBackgroundResource(android.R.color.white);
            nameEt.setThreshold(1);
            nameEt.setAdapter(adapter);
            nameEt.setOnItemClickListener((adapterView, view, i, l) -> {
                // Obtener contacto seleccionado
                Contact mContact = adapter.getFilteredList().get(i);

                // Escribir los demás textos
                if (mContact.getEmail() != null && !mContact.getEmail().isEmpty()) {
                    email.setText(mContact.getEmail());
                }
                if (mContact.getPhone() != null && !mContact.getPhone().isEmpty()) {
                    phone.setText(mContact.getPhone());
                }
            });
        }

        //Deshabilitar editText's cuando no hay permisos
        if (!addOrModify && !canModify) {
            mainNameEt.setFocusable(false);
            mainNameTechEt.setFocusable(false);
            nameEt.setFocusable(false);
            email.setFocusable(false);
            phone.setFocusable(false);
        }

        //Obtener referencia del contacto elegido
        Contact contact;
        if (isMainContact) {
            contact = mainContact;
            builder.setTitle("Contacto");

            // Buscar si hay diferencias de contacto
            if (projectDiffs.containsKey("datos_generales")) {
                // Obtener datos de contacto
                Map<String, Object> generalData = (Map<String, Object>) projectDiffs.get(
                        "datos_generales");

                // Validar si existo contacto técnico
                if (generalData.containsKey("contacto")) {
                    // Obtener color que se usará para remarcar
                    int highlightColor = ContextCompat.getColor(this, R.color.yellow);

                    // Obtener cambios de contacto tecnico
                    Map<String, Object> contactMap = (Map<String, Object>) generalData.get(
                            "contacto");

                    // Buscar cambios
                    if (contactMap.containsKey("name")) nameEt.setBackgroundColor(highlightColor);
                    if (contactMap.containsKey("phone")) phone.setBackgroundColor(highlightColor);
                    if (contactMap.containsKey("email")) email.setBackgroundColor(highlightColor);
                }
            }
        } else {
            contact = techContact;
            builder.setTitle("Contacto técnico");

            // Buscar si hay diferencias de contacto tecnico
            if (projectDiffs.containsKey("datos_generales")) {
                // Obtener datos de contacto
                Map<String, Object> generalData = (Map<String, Object>) projectDiffs.get(
                        "datos_generales");

                // Validar si existo contacto técnico
                if (generalData.containsKey("contacto_tecnico")) {
                    // Obtener color que se usará para remarcar
                    int highlightColor = ContextCompat.getColor(this, R.color.yellow);

                    // Obtener cambios de contacto tecnico
                    Map<String, Object> contactMap = (Map<String, Object>) generalData.get(
                            "contacto_tecnico");

                    // Buscar cambios
                    if (contactMap.containsKey("name")) nameEt.setBackgroundColor(highlightColor);
                    if (contactMap.containsKey("phone")) phone.setBackgroundColor(highlightColor);
                    if (contactMap.containsKey("email")) email.setBackgroundColor(highlightColor);
                }
            }
        }

        //Evaluar objeto
        if (contact != null) {
            email.setText(contact.getEmail());
            phone.setText(contact.getPhone());
        }
        nameEt.setText(name);

        //Obtener botones
        Button returnButton = content.findViewById(R.id.return_contact_button);
        Button saveButton = content.findViewById(R.id.save_contact_button);

        //Definir listeners para los botones
        returnButton.setOnClickListener(v -> {
            //Cerrar teclado
            Constants.hideKeyboardFrom(ProjectForm.this, content.findFocus());

            //Cerrar diálogo
            contactDetailDialog.dismiss();
        });

        saveButton.setOnClickListener(v -> {
            //Obtener referencia del contacto elegido
            Contact contact1 = new Contact();

            //Guardar contacto
            contact1.setNombre(nameEt.getText().toString());
            contact1.setEmail(email.getText().toString());
            contact1.setPhone(phone.getText().toString());

            //Asignar modificaciones
            if (isMainContact) {
                mainContact = contact1;
                mainNameEt.setText(contact1.getNombre());
            } else {
                techContact = contact1;
                mainNameTechEt.setText(contact1.getNombre());
            }

            //Cerrar teclado
            Constants.hideKeyboardFrom(ProjectForm.this, content.findFocus());

            //Cerrar diálogo
            contactDetailDialog.dismiss();
        });

        //Crear diálogo
        contactDetailDialog = builder.create();
        contactDetailDialog.show();
    }

    private void getSaeDispModelsFromDb() {
        CollectionReference saeModels = db.collection("modelos");
        saeModels.addSnapshotListener(this, (queryDocumentSnapshots, e) -> {
            if (queryDocumentSnapshots != null) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    SaeDispModel saeDispModel = new SaeDispModel();
                    saeDispModel.setKey(documentSnapshot.getId());
                    saeDispModel.setModel(documentSnapshot.getString("modelo"));
                    saeDispModel.setProduct(documentSnapshot.getString("producto"));
                    saeDispModel.setType(documentSnapshot.getString("tipo"));

                    saeDispModels.add(saeDispModel);
                }
            }
        });
    }

    private void getVehiclesFromDb() {
        CollectionReference vehiculos = db.collection("vehiculos");
        vehiculos.addSnapshotListener(this, (queryDocumentSnapshots, e) -> {
            if (queryDocumentSnapshots != null) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    String id = documentSnapshot.getId();
                    String desc = documentSnapshot.getString("descripcion");

                    Vehicle vehicle = new Vehicle(id, desc);
                    vehicleList.add(vehicle);
                }
            }
        });
    }

    private void getImagesFromDb() {
        db.collection("imagenes").orderBy("distribuidor").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                //Validar documentos
                if (task.getResult() == null) return;

                //Crear lista
                ArrayList<String> images = new ArrayList<>();

                //Obtener imagenes
                for (DocumentSnapshot snapshot : task.getResult().getDocuments()) {
                    images.add(snapshot.getString("distribuidor"));
                }

                //Si es de solo lectura... no definir adaptador
                if (!addOrModify && !canModify) return;

                //Obtener referencia de spinner
                MaterialBetterSpinner imagesSpinner = findViewById(R.id.images_spinner);

                //Definir adaptador
                ArrayAdapter<String> modelsArrayAdapter = new ArrayAdapter<>(ProjectForm.this
                        , android.R
                        .layout.simple_dropdown_item_1line, images);
                imagesSpinner.setAdapter(modelsArrayAdapter);
            } else {
                Log.e("DB", task.getException().toString());
            }
        });

        db.collection("canopy").orderBy("descripcion").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                //Validar documentos
                if (task.getResult() == null) return;

                //Preparar lista
                canopysList.clear();

                //Obtener imagenes
                for (DocumentSnapshot snapshot : task.getResult().getDocuments()) {
                    Color mColor = snapshot.toObject(Color.class);
                    mColor.setId(snapshot.getId());
                    canopysList.add(mColor);
                }

                //Revisar si hay hay un id definido
                if (selectedCanopy != null) setCanopy();

                //Si es de solo lectura... no definir adaptador
                if (!addOrModify && !canModify) return;

                //Obtener referencia de spinner
                MaterialBetterSpinner canopySpinner = findViewById(R.id.canopy_spinner);

                //Definir adaptador
                ArrayAdapter<Color> modelsArrayAdapter = new ArrayAdapter<>(ProjectForm.this
                        , android.R
                        .layout.simple_dropdown_item_1line, canopysList);
                canopySpinner.setAdapter(modelsArrayAdapter);

                //Definir listener
                canopySpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        selectedCanopy = (Color) parent.getItemAtPosition(position);
                    }
                });
            } else {
                Log.e("DB", task.getException().toString());
            }
        });

        db.collection("fibras").orderBy("descripcion").get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                //Validar documentos
                if (task.getResult() == null) return;

                //Preparar lista
                fibrasList.clear();

                //Obtener imagenes
                for (DocumentSnapshot snapshot : task.getResult().getDocuments()) {
                    Color mColor = snapshot.toObject(Color.class);
                    mColor.setId(snapshot.getId());
                    fibrasList.add(mColor);
                }

                //Revisar si hay hay un id definido
                if (selectedFibra != null) setFibras();

                //Si es de solo lectura... no definir adaptador
                if (!addOrModify && !canModify) return;

                //Obtener referencia de spinner
                MaterialBetterSpinner fibrasSpinner = findViewById(R.id.fibras_spinner);

                //Definir adaptador
                ArrayAdapter<Color> modelsArrayAdapter = new ArrayAdapter<>(ProjectForm.this
                        , android.R
                        .layout.simple_dropdown_item_1line, fibrasList);
                fibrasSpinner.setAdapter(modelsArrayAdapter);

                //Definir listener
                fibrasSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        selectedFibra = (Color) parent.getItemAtPosition(position);
                    }
                });
            } else {
                Log.e("DB", task.getException().toString());
            }
        });
    }

    private void showDispenserDialog(final DispModel selectedDispModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View content = getLayoutInflater().inflate(R.layout.dialog_dispenser, null);
        builder.setView(content);

        //Obtener views
        Button saveButton = content.findViewById(R.id.save_button);
        Button canelButton = content.findViewById(R.id.cancel_button);
        final ImageView infoButton = content.findViewById(R.id.info_button);
        final MaterialEditText models = content.findViewById(R.id.model_tv);
        final MaterialEditText products = content.findViewById(R.id.products_tv);
        final MaterialEditText chasisTypes = content.findViewById(R.id.chasis_tv);
        final MaterialEditText serialNumber = content.findViewById(R.id.serial_number_et);
        final MaterialBetterSpinner sae = content.findViewById(R.id.sae_spinner);
        final MaterialBetterSpinner manufacDateMonth =
                content.findViewById(R.id.manufac_date_month_spinner);
        final MaterialBetterSpinner manufacDateYear =
                content.findViewById(R.id.manufac_date_year_spinner);
        final MaterialBetterSpinner invoiceDayView = content.findViewById(R.id.invoice_date_day_spinner);
        final MaterialBetterSpinner invoiceMonthView = content.findViewById(R.id.invoice_date_month_spinner);
        final MaterialBetterSpinner invoiceYearView = content.findViewById(R.id.invoice_date_year_spinner);
        final MaterialEditText invoiceNumberEt = content.findViewById(R.id.invoice_number_tv);
        final LinearLayout invoiceDateLayout = content.findViewById(R.id.invoice_date_layout);
        final CheckBox generalInvoiceData = content.findViewById(R.id.general_invoice_cb);
        final CheckBox saeOk = content.findViewById(R.id.sae_ok_cb);
        final CheckBox dispFinished = content.findViewById(R.id.finished_cb);

        // Inicializar views
        // Si es solo lectura
        if (!addOrModify && !canModify) {
            saveButton.setEnabled(false);
            saveButton.setAlpha(0.5f);
            sae.setFocusable(false);
            serialNumber.setFocusable(false);
            manufacDateMonth.setFocusable(false);
            manufacDateYear.setFocusable(false);
            saeOk.setEnabled(false);
            dispFinished.setEnabled(false);
            generalInvoiceData.setEnabled(false);
            invoiceDayView.setFocusable(false);
            invoiceMonthView.setFocusable(false);
            invoiceYearView.setFocusable(false);
        } else {
            manufacDateMonth.setAdapter(new ArrayAdapter<>(this, android.R
                    .layout.simple_dropdown_item_1line, Constants.getMonths()));

            manufacDateYear.setAdapter(new ArrayAdapter<>(this, android.R
                    .layout.simple_dropdown_item_1line, Constants.getYears()));

            invoiceDayView.setAdapter(new ArrayAdapter<>(this, android.R
                    .layout.simple_dropdown_item_1line, Constants.getDays()));

            invoiceMonthView.setAdapter(new ArrayAdapter<>(this, android.R
                    .layout.simple_dropdown_item_1line, Constants.getStringMonths()));

            invoiceYearView.setAdapter(new ArrayAdapter<>(this, android.R
                    .layout.simple_dropdown_item_1line, Constants.getYears()));

            ArrayAdapter<SaeDispModel> saeElementsAdapter = new ArrayAdapter<>(this, android.R
                    .layout.simple_dropdown_item_1line, saeDispModels);
            sae.setAdapter(saeElementsAdapter);
            sae.setOnItemClickListener((parent, view, position, id) ->
            {
                SaeDispModel saeDispModel = (SaeDispModel) parent.getItemAtPosition(position);
                models.setText(saeDispModel.getModel());
                products.setText(saeDispModel.getProduct());
                chasisTypes.setText(saeDispModel.getType());
            });

            generalInvoiceData.setOnCheckedChangeListener((compoundButton, b) -> {
                invoiceNumberEt.setVisibility(b ? View.GONE : View.VISIBLE);
                invoiceDateLayout.setVisibility(b ? View.GONE : View.VISIBLE);
            });
        }

        infoButton.setOnClickListener(v -> showModelsDialog());

        if (selectedDispModel == null) {
            // Dialogo para agregar dispensario
            dispFinished.setVisibility(View.GONE);
        } else {
            //Setear textos
            models.setText(selectedDispModel.getModel());
            products.setText(selectedDispModel.getProducts());
            chasisTypes.setText(selectedDispModel.getChasis());
            serialNumber.setText(selectedDispModel.getSerialNumber());
            sae.setText(selectedDispModel.getSae());
            saeOk.setChecked(selectedDispModel.isSaeOk());
            dispFinished.setChecked(selectedDispModel.isFinished());
            generalInvoiceData.setChecked(selectedDispModel.isUsesGeneralInvoiceData());

            try {
                manufacDateMonth.setText(selectedDispModel.getManufactureDate().substring(0, 2));
                manufacDateYear.setText(selectedDispModel.getManufactureDate().substring(2, 4));
            } catch (NullPointerException | IndexOutOfBoundsException e) {
                Log.e(TAG, e.getMessage());
            }

            invoiceNumberEt.setText(selectedDispModel.getInvoiceNumber());
            if (selectedDispModel.getInvoiceDate() != null) {
                final String[] dateNumbers = DateUtils.dateToNumbers(new Date(selectedDispModel.getInvoiceDate()));
                invoiceDayView.setText(dateNumbers[0]);
                invoiceMonthView.setText(dateNumbers[1]);
                invoiceYearView.setText(dateNumbers[2]);
            }
        }

        final Dialog dialog = builder.create();
        dialog.show();

        saveButton.setOnClickListener(v ->
        {
            if (selectedDispModel == null) {
                // Crear nuevo dispensario
                if (sae.getText().toString().isEmpty()) {
                    sae.setError("Campo obligatorio");
                } else {
                    DispModel dispModel = new DispModel();
                    dispModel.setModel(models.getText().toString());
                    dispModel.setSerialNumber(serialNumber.getText().toString().trim());
                    dispModel.setProducts(products.getText().toString());
                    dispModel.setSae(sae.getText().toString());
                    dispModel.setChasis(chasisTypes.getText().toString());
                    dispModel.setSaeOk(saeOk.isChecked());

                    //Obtener fecha de fabricacion
                    String month = manufacDateMonth.getText().toString();
                    String year = manufacDateYear.getText().toString();
                    dispModel.setManufactureDate(month + year);

                    validateDispModelToAdd(dispModel);

                    dialog.dismiss();
                }
            } else {
                boolean validateSerialNumber;

                //Obtener objeto desde la lista
                selectedDispModel.setModel(models.getText().toString());
                selectedDispModel.setProducts(products.getText().toString());
                selectedDispModel.setSae(sae.getText().toString());
                selectedDispModel.setChasis(chasisTypes.getText().toString());
                selectedDispModel.setSaeOk(saeOk.isChecked());
                selectedDispModel.setId(selectedDispModel.getId());
                selectedDispModel.setKitElectronico(selectedDispModel.getKitElectronico());
                selectedDispModel.setOrdenInterna(selectedDispModel.getOrdenInterna());

                String oldSerialNumber = selectedDispModel.getSerialNumber();
                String newSerialNumber = serialNumber.getText().toString().trim();
                validateSerialNumber = selectedDispModel.getSerialNumber() == null || !selectedDispModel.getSerialNumber().equals(newSerialNumber);
                selectedDispModel.setSerialNumber(serialNumber.getText().toString().trim());

                //Evaluar si se acaba de cambiar estado de dispensario
                if (!selectedDispModel.isFinished() && dispFinished.isChecked()) {
                    //Obtener fecha de terminado para dispensario
                    Long today = new Date().getTime();

                    //Definir fecha de terminado
                    selectedDispModel.setFinishedDate(today);
                }

                selectedDispModel.setFinished(dispFinished.isChecked());

                //Obtener fecha de fabricacion
                final String month = manufacDateMonth.getText().toString();
                final String year = manufacDateYear.getText().toString();
                selectedDispModel.setManufactureDate(month + year);

                //Obtener datos de facturación
                selectedDispModel.setUsesGeneralInvoiceData(generalInvoiceData.isChecked());
                if (!selectedDispModel.isUsesGeneralInvoiceData()) {
                    // Obtener datos ingresados
                    final String invoiceNumber = invoiceNumberEt.getText().toString();
                    final String invoiceDay = invoiceDayView.getText().toString();
                    final String invoiceYear = invoiceYearView.getText().toString();
                    String invoiceMonth = invoiceMonthView.getText().toString();

                    if (!invoiceMonth.isEmpty()) {
                        invoiceMonth = Constants.monthStringToNumber(invoiceMonth);
                    }

                    // Obtener fecha
                    final Date invoiceDate = DateUtils.dateNumbersToDate(invoiceDay, invoiceMonth, invoiceYear);
                    Long invoiceEpoch = null;
                    if (invoiceDate != null) {
                        invoiceEpoch = invoiceDate.getTime();
                    }

                    selectedDispModel.setInvoiceDate(invoiceEpoch);
                    selectedDispModel.setInvoiceNumber(invoiceNumber);
                }

                Log.d(TAG, "Validate serial number is " + validateSerialNumber);
                if (validateSerialNumber) {
                    validateDispModelToEdit(selectedDispModel, oldSerialNumber);
                } else {
                    //Actualizar base de datos
                    editDispenserFromDatabase(selectedDispModel);
                    //Actualizar lista
                    dispenserModelAdapter.notifyDataSetChanged();
                }

                //Cerrar diálogo
                dialog.dismiss();
            }
        });

        canelButton.setOnClickListener(v -> dialog.dismiss());
    }

    private void showModelsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Modelos dispensarios");

        //Instanciar layout
        View content = getLayoutInflater().inflate(R.layout.dialog_image_view, null);
        ImageView imageView = content.findViewById(R.id.image_view);

        //Definir imagen
        Glide.with(this).load(R.drawable.modelos_dispensarios).dontAnimate().into(imageView);

        //Definir imageView en dialogo
        builder.setView(content);

        //Definir boton de regreso
        builder.setNegativeButton("Regresar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        //Mostrar diálogo
        Dialog dialog = builder.create();
        dialog.show();
    }

    private void addDispenserModel(DispModel dispModel) {
        noDispModels.setVisibility(View.GONE);
        dispModelsList.setVisibility(View.VISIBLE);

        dispModels.add(dispModel);
        dispenserModelAdapter.notifyDataSetChanged();
    }

    @SuppressWarnings("unchecked")
    private void setGeneralData(Map<String, Object> generalData) {
        //Referenciar views
        MaterialEditText internOrder = findViewById(R.id.intern_order_number);
        MaterialEditText crmTicket = findViewById(R.id.crm_ticket);
        MaterialEditText projectClient = findViewById(R.id.project_client);
        MaterialEditText projectStation = findViewById(R.id.project_station);
        MaterialEditText projectStationNumber = findViewById(R.id.station_number);
        MaterialEditText projectContact = findViewById(R.id.project_contact);
        MaterialEditText projectTechContact = findViewById(R.id.project_tech_contact);
        RadioButton newProjectRb = findViewById(R.id.new_project_rb);
        RadioButton changeDispRb = findViewById(R.id.disp_change_rb);
        RadioButton saleTypeRb = findViewById(R.id.sale_type_rb);
        RadioButton leaseTypeRb = findViewById(R.id.lease_type_rb);
        RadioGroup projectTypeRg = findViewById(R.id.project_type_rg);
        RadioGroup saleTypeRg = findViewById(R.id.sale_type_rg);

        // Obtener color
        int highlightColor = ContextCompat.getColor(this, R.color.yellow);

        // Validar cambios
        if (projectDiffs.containsKey("datos_generales")) {
            // Obtener mapa
            Map<String, Object> generalDataChanges = (Map<String, Object>) projectDiffs.get(
                    "datos_generales");

            // Buscar cambios
            if (projectDiffs.containsKey("ticket_crm"))
                crmTicket.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("fecha_solicitud_cliente"))
                clientRequestDateEt.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("fecha_terminado"))
                finishedProjectDateEt.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("fecha_embarque"))
                shipmentDateEt.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("fecha_arranque"))
                startProjectDateEt.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("cliente"))
                projectClient.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("numero_estacion"))
                projectStationNumber.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("estacion"))
                projectStation.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("contacto"))
                projectContact.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("contacto_tecnico"))
                projectTechContact.setBackgroundColor(highlightColor);
            if (generalDataChanges.containsKey("tipo_proyecto")) {
                projectTypeRg.setBackgroundColor(highlightColor);
            }
        }

        if (projectDiffs.containsKey("tipo_venta")) {
            saleTypeRg.setBackgroundColor(highlightColor);
        }

        //Obtener datos para nombre de proyecto
        String mProjectClient = (String) generalData.get("cliente");
        String mProjectStation = (String) generalData.get("estacion");
        if (!mProjectClient.isEmpty() && !mProjectStation.isEmpty()) {
            projectSimpleName = mProjectClient + " " + mProjectStation;
        }

        if (generalData.get("numero_estacion") != null) {
            projectStationNumber.setText((String) generalData.get("numero_estacion"));
        }

        //Escribir datos
        internOrder.setText((String) generalData.get("orden_interna"));
        clientRequestDateEt.setText((String) generalData.get("fecha_solicitud_cliente"));
        shipmentDateEt.setText((String) generalData.get("fecha_embarque"));

        if (generalData.containsKey("ticket_crm"))
            crmTicket.setText((String) generalData.get("ticket_crm"));

        if (generalData.containsKey("fecha_terminado"))
            finishedProjectDateEt.setText((String) generalData.get("fecha_terminado"));

        if (generalData.containsKey("fecha_arranque"))
            startProjectDateEt.setText((String) generalData.get("fecha_arranque"));

        projectClient.setText(mProjectClient);
        projectStation.setText(mProjectStation);

        //Obtener objetos
        try {
            //Crear objetos
            mainContact = new Contact();
            techContact = new Contact();

            //Obtener datos de cada uno
            mainContact.setNombre((String) ((Map) generalData.get("contacto")).get("nombre"));
            mainContact.setPhone((String) ((Map) generalData.get("contacto")).get("phone"));
            mainContact.setEmail((String) ((Map) generalData.get("contacto")).get("email"));
            techContact.setNombre((String) ((Map) generalData.get("contacto_tecnico")).get(
                    "nombre"));
            techContact.setPhone((String) ((Map) generalData.get("contacto_tecnico")).get("phone"));
            techContact.setEmail((String) ((Map) generalData.get("contacto_tecnico")).get("email"));

            projectContact.setText(mainContact.getNombre());
            projectTechContact.setText(techContact.getNombre());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error al obtener contactos", Toast.LENGTH_SHORT).show();
        }

        if (generalData.containsKey("tipo_proyecto")) {
            String projectType = (String) generalData.get("tipo_proyecto");
            if (projectType != null && projectType.equals("proyecto_nuevo")) {
                newProjectRb.setChecked(true);
            } else {
                changeDispRb.setChecked(true);
            }
        }

        if (existingProjectRoot.containsKey("ticket_crm"))
            crmTicket.setText((String) existingProjectRoot.get("ticket_crm"));

        if (existingProjectRoot.containsKey("tipo_venta")) {
            String saleType = (String) existingProjectRoot.get("tipo_venta");
            if (saleType != null && saleType.equals("venta")) {
                saleTypeRb.setChecked(true);
            } else {
                leaseTypeRb.setChecked(true);
            }
        }

        //Deshabilitar editText de orden interna
        internOrder.setEnabled(false);

        generalDataContent.setVisibility(View.VISIBLE);
        generalDataProgress.setVisibility(View.GONE);
    }

    private void setShipmentAccesories(Map<String, Object> accesories) {
        if (accesories != null) {
            existingProjectRoot.put("accesorios_embarque", accesories);
        }
    }

    private Map<String, Object> getGeneralData() {
        //Referenciar views
        MaterialEditText internOrder = findViewById(R.id.intern_order_number);
        MaterialEditText projectClient = findViewById(R.id.project_client);
        MaterialEditText projectStation = findViewById(R.id.project_station);
        MaterialEditText projectStationNumber = findViewById(R.id.station_number);
        MaterialEditText crmTicket = findViewById(R.id.crm_ticket);
        RadioButton newProjectCb = findViewById(R.id.new_project_rb);
        RadioButton saleTypeRb = findViewById(R.id.sale_type_rb);

        //Crear mapa principal
        Map<String, Object> generalData = new HashMap<>();

        //Obtener textos
        if (internOrder.getText() != null)
            generalData.put("orden_interna", internOrder.getText().toString());

        if (clientRequestDateEt.getText() != null) {
            generalData.put("fecha_solicitud_cliente", clientRequestDateEt.getText().toString());

            Date date = Constants.dateStringToObject2(clientRequestDateEt.getText().toString());
            projectRoot.put("fecha_solicitud", date == null ? null : date.getTime());
        } else
            generalData.put("fecha_solicitud_cliente", "");

        if (shipmentDateEt.getText() != null) {
            generalData.put("fecha_embarque", shipmentDateEt.getText().toString());

            Date date = Constants.dateStringToObject2(shipmentDateEt.getText().toString());
            projectRoot.put("fecha_embarque", date == null ? null : date.getTime());
        } else
            generalData.put("fecha_embarque", "");

        if (finishedProjectDateEt.getText() != null) {
            generalData.put("fecha_terminado", finishedProjectDateEt.getText().toString());

            Date date = Constants.dateStringToObject2(finishedProjectDateEt.getText().toString());
            projectRoot.put("fecha_terminado", date == null ? null : date.getTime());
        }

        if (startProjectDateEt.getText() != null) {
            generalData.put("fecha_arranque", startProjectDateEt.getText().toString());

            Date date = Constants.dateStringToObject2(startProjectDateEt.getText().toString());
            projectRoot.put("fecha_arranque", date == null ? null : date.getTime());
        }

        if (projectClient.getText() != null) {
            String client = projectClient.getText().toString();
            generalData.put("cliente", client);
            projectSimpleName = client;
        }

        if (projectStation.getText() != null) {
            String station = projectStation.getText().toString();
            generalData.put("estacion", station);
            projectSimpleName += " ";
            projectSimpleName += station;
        }

        if (projectStationNumber.getText() != null)
            generalData.put("numero_estacion", projectStationNumber.getText().toString());

        //Agregar objetos de contactos
        generalData.put("contacto", mainContact == null ? new Contact().toMap() :
                mainContact.toMap());
        generalData.put("contacto_tecnico", techContact == null ? new Contact().toMap() :
                techContact.toMap());

        //Validar si existe ya el contacto, si no, guardarlo
        saveContacts(mainContact);
        saveContacts(techContact);

        //Agregar tipo de proyecto
        if (newProjectCb.isChecked()) {
            generalData.put("tipo_proyecto", "proyecto_nuevo");
        } else {
            generalData.put("tipo_proyecto", "cambio_dispensarios");
        }

        //Agregar ticket crm
        projectRoot.put("ticket_crm", crmTicket.getText().toString());

        if (saleTypeRb.isChecked()) {
            //Agregar tipo de venta
            projectRoot.put("tipo_venta", "venta");
        } else {
            //Agregar tipo de venta
            projectRoot.put("tipo_venta", "arrendamiento");
        }

        return generalData;
    }

    private void setDispensers(final ArrayList<String> dispensersList) {
        // Obtener views para obtener datos de facturación
        final LinearLayout invoiceDateLayout = findViewById(R.id.invoice_general_date_layout);
        final MaterialEditText invoiceGeneralNumber = findViewById(R.id.invoice_general_number_tv);
        final MaterialBetterSpinner invoiceDayView = findViewById(R.id.invoice_general_date_day_spinner);
        final MaterialBetterSpinner invoiceMonthView = findViewById(R.id.invoice_general_date_month_spinner);
        final MaterialBetterSpinner invoiceYearView = findViewById(R.id.invoice_general_date_year_spinner);

        // Validar que existan dispensarios registrados
        if (dispensersList.isEmpty()) {
            noDispModels.setVisibility(View.VISIBLE);
            dispensersContent.setVisibility(View.VISIBLE);
            dispensersProgress.setVisibility(View.GONE);
            return;
        }

        // Definir lista de id's de dispensarios
        dispReferences = dispensersList;

        // Obtener objetos
        DispensersProvider.getDisModels(dispensersList, fetchedDispModels ->
        {
            if (fetchedDispModels == null) {
                Toast.makeText(this, "Error al obtener dispensarios", Toast.LENGTH_SHORT).show();
            } else {
                // Ordenar dispensarios por número de serie
                Collections.sort(fetchedDispModels, Constants.dispModelComparator);

                // Asignar datos de facturación
                for (DispModel fetchedDisp : fetchedDispModels) {
                    if (fetchedDisp.isUsesGeneralInvoiceData()) {
                        if (existingProjectRoot.get("numero_factura_dispensarios") != null) {
                            fetchedDisp.setInvoiceNumber((String) existingProjectRoot.get("numero_factura_dispensarios"));
                        }
                        if (existingProjectRoot.get("fecha_factura_dispensarios") != null) {
                            fetchedDisp.setInvoiceDate((Long) existingProjectRoot.get("fecha_factura_dispensarios"));
                        }
                    }
                }

                // Definir lista de objetos
                dispModels.addAll(fetchedDispModels);
                dispenserModelAdapter.notifyDataSetChanged();

                // Mostrar accesorios
                calculateAccesoriosEmbarque();

                // Mostrar lista
                noDispModels.setVisibility(View.GONE);
                dispModelsList.setVisibility(View.VISIBLE);

                // Mostrar layout para datos de facturación
                invoiceGeneralNumber.setVisibility(View.VISIBLE);
                invoiceDateLayout.setVisibility(View.VISIBLE);

                // Definir adaptadores
                invoiceDayView.setAdapter(new ArrayAdapter<>(this, android.R
                        .layout.simple_dropdown_item_1line, Constants.getDays()));

                invoiceMonthView.setAdapter(new ArrayAdapter<>(this, android.R
                        .layout.simple_dropdown_item_1line, Constants.getStringMonths()));

                invoiceYearView.setAdapter(new ArrayAdapter<>(this, android.R
                        .layout.simple_dropdown_item_1line, Constants.getYears()));

                // Obtener datos de facturación
                if (existingProjectRoot.get("numero_factura_dispensarios") != null) {
                    final String invoiceNumber = existingProjectRoot.get("numero_factura_dispensarios").toString();
                    invoiceGeneralNumber.setText(invoiceNumber);
                }

                if (existingProjectRoot.get("fecha_factura_dispensarios") != null) {
                    final Long invoiceDate = (Long) existingProjectRoot.get("fecha_factura_dispensarios");
                    if (invoiceDate != null) {
                        final String[] dateNumbers = DateUtils.dateToNumbers(new Date(invoiceDate));
                        invoiceDayView.setText(dateNumbers[0]);
                        invoiceMonthView.setText(dateNumbers[1]);
                        invoiceYearView.setText(dateNumbers[2]);
                    }
                }
            }

            //Ocultar progress
            shipmentAccesoriesProgress.setVisibility(View.GONE);
            shipmentAccesoriesContent.setVisibility(View.VISIBLE);
            dispensersContent.setVisibility(View.VISIBLE);
            dispensersProgress.setVisibility(View.GONE);
        });
    }

    private boolean isDispChanged(String dispId) {
        // Buscar el mapa de cambios en dispensarios
        if (projectDiffs.containsKey("dispensarios")) {
            // Obtener mapa
            Map<String, Object> dispChanges = (Map<String, Object>) projectDiffs.get(
                    "dispensarios");

            return dispChanges.containsKey(dispId);
        }

        return false;
    }

    private Map<String, Object> getShipment() {
        CheckBox spxCb = findViewById(R.id.spx_cb);
        CheckBox clientCb = findViewById(R.id.client_cb);
        CheckBox fleteraCb = findViewById(R.id.fletera_cb);
        CheckBox exportacionCb = findViewById(R.id.exportacion_cb);

        Map<String, Object> shipmentTypes = new HashMap<>();
        shipmentTypes.put("supramax", spxCb.isChecked());
        shipmentTypes.put("cliente", clientCb.isChecked());
        shipmentTypes.put("fletera", fleteraCb.isChecked());
        shipmentTypes.put("exportacion", exportacionCb.isChecked());

        return shipmentTypes;
    }

    @SuppressWarnings("unchecked")
    private void setShipment(Map<String, Object> shipmentTypes) {
        CheckBox spxCb = findViewById(R.id.spx_cb);
        CheckBox clientCb = findViewById(R.id.client_cb);
        CheckBox fleteraCb = findViewById(R.id.fletera_cb);
        CheckBox exportacionCb = findViewById(R.id.exportacion_cb);

        spxCb.setChecked((boolean) shipmentTypes.get("supramax"));
        clientCb.setChecked((boolean) shipmentTypes.get("cliente"));
        fleteraCb.setChecked((boolean) shipmentTypes.get("fletera"));
        exportacionCb.setChecked((boolean) shipmentTypes.get("exportacion"));

        //Validar cambios
        if (projectDiffs.containsKey("tipo_embarque")) {
            Map<String, Object> tipoEmbarqueDiffs = (Map<String, Object>) projectDiffs.get(
                    "tipo_embarque");

            int highligthColor = ContextCompat.getColor(this, R.color.yellow);

            if (tipoEmbarqueDiffs != null) {
                if (tipoEmbarqueDiffs.containsKey("cliente"))
                    clientCb.setBackgroundColor(highligthColor);
                if (tipoEmbarqueDiffs.containsKey("exportacion"))
                    exportacionCb.setBackgroundColor(highligthColor);
                if (tipoEmbarqueDiffs.containsKey("fletera"))
                    fleteraCb.setBackgroundColor(highligthColor);
                if (tipoEmbarqueDiffs.containsKey("supramax"))
                    spxCb.setBackgroundColor(highligthColor);
            }
        }

        shipmentTypeContent.setVisibility(View.VISIBLE);
        shipmentTypeProgress.setVisibility(View.GONE);
    }

    private void initEvo6Components() {
        //Referenciar Edit Text's
        MaterialBetterSpinner impresora = findViewById(R.id.impresora_et);
        MaterialBetterSpinner lector = findViewById(R.id.lector_et);
        MaterialBetterSpinner cajaRetornos = findViewById(R.id.caja_retornos_et);
        MaterialBetterSpinner brick = findViewById(R.id.brick_et);
        MaterialBetterSpinner hammerTag = findViewById(R.id.hammer_tag_et);
        MaterialBetterSpinner tablerElec = findViewById(R.id.tablero_electrico_et);

        //Definir lista
        ArrayList<String> numbers = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            numbers.add(String.valueOf(i));
        }

        //Definir adaptador
        ArrayAdapter<String> numbersAdapter = new ArrayAdapter<>(this, android.R
                .layout.simple_dropdown_item_1line, numbers);

        //Asignar adaptador a todos los MaterialEditText
        impresora.setAdapter(numbersAdapter);
        lector.setAdapter(numbersAdapter);
        cajaRetornos.setAdapter(numbersAdapter);
        brick.setAdapter(numbersAdapter);
        hammerTag.setAdapter(numbersAdapter);
        tablerElec.setAdapter(numbersAdapter);
    }

    private void initShipmentAccesories() {
        //Referenciar Edit Text's
        MaterialBetterSpinner greenTagEt = findViewById(R.id.green_tag_et);
        MaterialBetterSpinner blueTagEt = findViewById(R.id.blue_tag_et);
        MaterialBetterSpinner redTagEt = findViewById(R.id.red_tag_et);
        MaterialBetterSpinner tapasNegrasEt = findViewById(R.id.tapas_negras_et);
        MaterialBetterSpinner nomCopiesEt = findViewById(R.id.nom_copies_et);

        //Definir lista
        ArrayList<String> numbers = new ArrayList<>();
        for (int i = 0; i <= 30; i++) {
            numbers.add(String.valueOf(i));
        }

        //Definir adaptador
        ArrayAdapter<String> numbersAdapter = new ArrayAdapter<>(this, android.R
                .layout.simple_dropdown_item_1line, numbers);

        //Asignar adaptador a todos los MaterialEditText
        greenTagEt.setAdapter(numbersAdapter);
        blueTagEt.setAdapter(numbersAdapter);
        redTagEt.setAdapter(numbersAdapter);
        tapasNegrasEt.setAdapter(numbersAdapter);
        nomCopiesEt.setAdapter(numbersAdapter);
    }

    private void initVehiclesSpinner() {
        //Obtener referencias
        MaterialBetterSpinner[] spinners = new MaterialBetterSpinner[3];
        spinners[0] = findViewById(R.id.vehicle_spinner_1);
        spinners[1] = findViewById(R.id.vehicle_spinner_2);
        spinners[2] = findViewById(R.id.vehicle_spinner_3);

        //Crear adaptador
        ArrayAdapter<Vehicle> vehicleArrayAdapter = new ArrayAdapter<>(this, android.R
                .layout.simple_dropdown_item_1line, vehicleList);

        //Long clickListener
        View.OnLongClickListener deleteListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ((MaterialBetterSpinner) v).setText("");
                return false;
            }
        };

        //Definir adaptadores a spinners
        for (MaterialBetterSpinner spinner : spinners) {
            spinner.setAdapter(vehicleArrayAdapter);
            spinner.setOnLongClickListener(deleteListener);
        }
    }

    private Map<String, Object> getEvo6Components() {
        //Referenciar views
        MaterialBetterSpinner impresora = findViewById(R.id.impresora_et);
        MaterialBetterSpinner lector = findViewById(R.id.lector_et);
        MaterialBetterSpinner cajaRetornos = findViewById(R.id.caja_retornos_et);
        CheckBox dona34 = findViewById(R.id.dona34_et);
        CheckBox dona1 = findViewById(R.id.dona1_et);
        MaterialBetterSpinner brick = findViewById(R.id.brick_et);
        MaterialBetterSpinner hammerTag = findViewById(R.id.hammer_tag_et);
        MaterialBetterSpinner tablerElec = findViewById(R.id.tablero_electrico_et);
        CheckBox codona34 = findViewById(R.id.codona34_et);
        CheckBox rv = findViewById(R.id.rv_et);

        //Crear mapa principal
        Map<String, Object> evo6Components = new HashMap<>();

        //Obtener textos
        evo6Components.put("impresora", impresora.getText().toString());
        evo6Components.put("lector", lector.getText().toString());
        evo6Components.put("caja_retornos", cajaRetornos.getText().toString());
        evo6Components.put("dona_34", dona34.isChecked());
        evo6Components.put("dona_1", dona1.isChecked());
        evo6Components.put("brick", brick.getText().toString());
        evo6Components.put("hammer_tag", hammerTag.getText().toString());
        evo6Components.put("tablero_electrico", tablerElec.getText().toString());
        evo6Components.put("codona_34", codona34.isChecked());
        evo6Components.put("rv", rv.isChecked());

        return evo6Components;
    }

    @SuppressWarnings("unchecked")
    private void setEvo6Components(Map<String, Object> evo6Components) {
        //Referenciar views
        MaterialBetterSpinner impresora = findViewById(R.id.impresora_et);
        MaterialBetterSpinner lector = findViewById(R.id.lector_et);
        MaterialBetterSpinner cajaRetornos = findViewById(R.id.caja_retornos_et);
        CheckBox dona34 = findViewById(R.id.dona34_et);
        CheckBox dona1 = findViewById(R.id.dona1_et);
        MaterialBetterSpinner brick = findViewById(R.id.brick_et);
        MaterialBetterSpinner hammerTag = findViewById(R.id.hammer_tag_et);
        MaterialBetterSpinner tablerElec = findViewById(R.id.tablero_electrico_et);
        CheckBox codona34 = findViewById(R.id.codona34_et);
        CheckBox rv = findViewById(R.id.rv_et);

        // Validar cambios
        if (projectDiffs.containsKey("componentes_evo6")) {
            Map<String, Object> evo6compChanges = (Map<String, Object>) projectDiffs.get(
                    "componentes_evo6");
            if (evo6compChanges != null) {
                int highlightColor = ContextCompat.getColor(this, R.color.yellow);
                if (evo6compChanges.containsKey("brick")) brick.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("caja_retornos"))
                    cajaRetornos.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("codona_34"))
                    codona34.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("dona_1")) dona1.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("dona_34"))
                    dona34.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("hammer_tag"))
                    hammerTag.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("impresora"))
                    impresora.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("lector"))
                    lector.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("rv")) rv.setBackgroundColor(highlightColor);
                if (evo6compChanges.containsKey("tablero_electrico"))
                    tablerElec.setBackgroundColor(highlightColor);
            }
        }

        //Obtener valores
        impresora.setText(evo6Components.get("impresora").toString());
        lector.setText(evo6Components.get("lector").toString());
        cajaRetornos.setText(evo6Components.get("caja_retornos").toString());
        dona34.setChecked((boolean) evo6Components.get("dona_34"));
        dona1.setChecked((boolean) evo6Components.get("dona_1"));
        brick.setText(evo6Components.get("brick").toString());
        hammerTag.setText(evo6Components.get("hammer_tag").toString());
        tablerElec.setText(evo6Components.get("tablero_electrico").toString());
        codona34.setChecked((boolean) evo6Components.get("codona_34"));
        rv.setChecked((boolean) evo6Components.get("rv"));

        evo6ComponentsContent.setVisibility(View.VISIBLE);
        evo6ComponentsProgress.setVisibility(View.GONE);
    }

    private Map<String, Object> getSoftware() {
        //Referenciar views
        CheckBox servidor = findViewById(R.id.servidor_cb);
        CheckBox emax_admin = findViewById(R.id.emax_admin_cb);
        CheckBox cvmaxContrVol = findViewById(R.id.cvmax_control_vol_cb);
        CheckBox polizaCvmax = findViewById(R.id.poliza_cvmax_cb);
        CheckBox flotillas = findViewById(R.id.flotillas_credito_prepago_cb);
        CheckBox polizaIadmin = findViewById(R.id.poliza_iadmin_cb);
        CheckBox cfdi_fact = findViewById(R.id.cfdi_cb);
        MaterialEditText serverNumberEt = findViewById(R.id.server_number_et);

        //Crear mapa principal
        Map<String, Object> softwareMap = new HashMap<>();

        //Obtener textos
        softwareMap.put("numero_servidor", serverNumberEt.getText().toString());
        softwareMap.put("servidor", servidor.isChecked());
        softwareMap.put("emax_administrativo", emax_admin.isChecked());
        softwareMap.put("cvmax_control_vol", cvmaxContrVol.isChecked());
        softwareMap.put("poliza_cvmax", polizaCvmax.isChecked());
        softwareMap.put("flotillas", flotillas.isChecked());
        softwareMap.put("poliza_iadmin", polizaIadmin.isChecked());
        softwareMap.put("cfdi_facturacion", cfdi_fact.isChecked());

        return softwareMap;
    }

    @SuppressWarnings("unchecked")
    private void setSoftware(Map<String, Object> softwareMap) {
        //Referenciar views
        CheckBox servidor = findViewById(R.id.servidor_cb);
        CheckBox emax_admin = findViewById(R.id.emax_admin_cb);
        CheckBox cvmaxContrVol = findViewById(R.id.cvmax_control_vol_cb);
        CheckBox polizaCvmax = findViewById(R.id.poliza_cvmax_cb);
        CheckBox flotillas = findViewById(R.id.flotillas_credito_prepago_cb);
        CheckBox polizaIadmin = findViewById(R.id.poliza_iadmin_cb);
        CheckBox cfdi_fact = findViewById(R.id.cfdi_cb);
        MaterialEditText serverNumberEt = findViewById(R.id.server_number_et);

        servidor.setChecked((boolean) softwareMap.get("servidor"));
        emax_admin.setChecked((boolean) softwareMap.get("emax_administrativo"));
        cvmaxContrVol.setChecked((boolean) softwareMap.get("cvmax_control_vol"));
        polizaCvmax.setChecked((boolean) softwareMap.get("poliza_cvmax"));
        flotillas.setChecked((boolean) softwareMap.get("flotillas"));
        polizaIadmin.setChecked((boolean) softwareMap.get("poliza_iadmin"));
        cfdi_fact.setChecked((boolean) softwareMap.get("cfdi_facturacion"));
        if (softwareMap.containsKey("numero_servidor"))
            serverNumberEt.setText((String) softwareMap.get("numero_servidor"));

        //Evaluar numero de servidor
        serverNumberEt.setVisibility(servidor.isChecked() ? View.VISIBLE : View.GONE);

        if (projectDiffs.containsKey("software")) {
            Map<String, Object> softwareDiffs = (Map<String, Object>) projectDiffs.get("software");
            if (softwareDiffs != null) {
                int highLigthColor = ContextCompat.getColor(this, R.color.yellow);
                if (softwareDiffs.containsKey("cfdi_facturacion"))
                    cfdi_fact.setBackgroundColor(highLigthColor);
                if (softwareDiffs.containsKey("cvmax_control_vol"))
                    cvmaxContrVol.setBackgroundColor(highLigthColor);
                if (softwareDiffs.containsKey("emax_administrativo"))
                    emax_admin.setBackgroundColor(highLigthColor);
                if (softwareDiffs.containsKey("flotillas"))
                    flotillas.setBackgroundColor(highLigthColor);
                if (softwareDiffs.containsKey("numero_servidor"))
                    serverNumberEt.setBackgroundColor(highLigthColor);
                if (softwareDiffs.containsKey("poliza_cvmax"))
                    polizaCvmax.setBackgroundColor(highLigthColor);
                if (softwareDiffs.containsKey("servidor"))
                    servidor.setBackgroundColor(highLigthColor);
            }
        }

        softwareContent.setVisibility(View.VISIBLE);
        softwareProgress.setVisibility(View.GONE);
    }

    private void initSoftwareViews() {
        //Referenciar views
        CheckBox serverCb = findViewById(R.id.servidor_cb);

        //Definir listener de checked
        serverCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MaterialEditText serverNumberEt = findViewById(R.id.server_number_et);
                serverNumberEt.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
    }

    private ArrayList<String> getVehicles() {
        //Obtener referencias
        MaterialBetterSpinner[] spinners = new MaterialBetterSpinner[3];
        spinners[0] = findViewById(R.id.vehicle_spinner_1);
        spinners[1] = findViewById(R.id.vehicle_spinner_2);
        spinners[2] = findViewById(R.id.vehicle_spinner_3);

        ArrayList<String> selectedVehicles = new ArrayList<>();

        //Definir adaptadores a spinners
        for (MaterialBetterSpinner spinner : spinners) {
            if (!spinner.getText().toString().isEmpty()) {
                selectedVehicles.add(spinner.getText().toString());
            }
        }

        return selectedVehicles;
    }

    private void setVehicles(ArrayList<String> selectedVehicles) {
        //Obtener referencias
        MaterialBetterSpinner[] spinners = new MaterialBetterSpinner[3];
        spinners[0] = findViewById(R.id.vehicle_spinner_1);
        spinners[1] = findViewById(R.id.vehicle_spinner_2);
        spinners[2] = findViewById(R.id.vehicle_spinner_3);

        //Definir adaptadores a spinners
        for (int i = 0; i < selectedVehicles.size(); i++) {
            spinners[i].setText(selectedVehicles.get(i));
        }

        if (projectDiffs.containsKey("vehiculos")) {
            spinners[0].setBackgroundColor(ContextCompat.getColor(this, R.color.yellow));
        }

        vehicleContent.setVisibility(View.VISIBLE);
        vehicleProgress.setVisibility(View.GONE);
    }

    private Map<String, Object> getImagesInfo() {
        //Spinner
        MaterialBetterSpinner images = findViewById(R.id.images_spinner);

        //Obtener datos
        String image = images.getText().toString();

        Map<String, Object> imagesMap = new HashMap<>();
        imagesMap.put("imagen", image);

        if (selectedCanopy != null) {
            imagesMap.put("id_canopy", selectedCanopy.getId());
        } else {
            imagesMap.put("id_canopy", "");
        }

        if (selectedFibra != null) {
            imagesMap.put("id_fibra", selectedFibra.getId());
        } else {
            imagesMap.put("id_fibra", "");
        }

        return imagesMap;
    }

    @SuppressWarnings("unchecked")
    private void setImages(Map<String, Object> imagesMap) {
        if (imagesMap != null) {
            //Spinner
            MaterialBetterSpinner images = findViewById(R.id.images_spinner);
            MaterialBetterSpinner canopy = findViewById(R.id.canopy_spinner);
            MaterialBetterSpinner fibras = findViewById(R.id.fibras_spinner);
            images.setText((String) imagesMap.get("imagen"));
            String canopyId = (String) imagesMap.get("id_canopy");
            String fibrasId = (String) imagesMap.get("id_fibra");

            selectedCanopy = new Color();
            selectedFibra = new Color();
            selectedCanopy.setId(canopyId);
            selectedFibra.setId(fibrasId);

            //Validar cambios
            if (projectDiffs.containsKey("imagenes")) {
                Map<String, Object> imagenesDiffs = (Map<String, Object>) projectDiffs.get(
                        "imagenes");
                if (imagenesDiffs != null) {
                    int highligthColor = ContextCompat.getColor(this, R.color.yellow);
                    if (imagenesDiffs.containsKey("id_canopy"))
                        canopy.setBackgroundColor(highligthColor);
                    if (imagenesDiffs.containsKey("id_fibra"))
                        fibras.setBackgroundColor(highligthColor);
                    if (imagenesDiffs.containsKey("imagen"))
                        images.setBackgroundColor(highligthColor);
                }
            }

            setCanopy();
            setFibras();
        }

        imagesContent.setVisibility(View.VISIBLE);
        imagesProgress.setVisibility(View.GONE);
    }

    private void setCanopy() {
        MaterialBetterSpinner canopy = findViewById(R.id.canopy_spinner);

        //Relacionar canopy
        for (Color color : canopysList) {
            if (color.getId().equals(selectedCanopy.getId())) {
                canopy.setText(color.getDescripcion());
                break;
            }
        }
    }

    private void setFibras() {
        MaterialBetterSpinner fibras = findViewById(R.id.fibras_spinner);

        //Relacionar fibra
        for (Color color : fibrasList) {
            if (color.getId().equals(selectedFibra.getId())) {
                fibras.setText(color.getDescripcion());
                break;
            }
        }
    }

    private String getSpecialInstructions() {
        //Obtener instrucciones especiales
        EditText instructions = findViewById(R.id.special_instructions);
        return instructions.getText().toString();
    }

    private void setSpecialInstructions(String instructionsText) {
        //Obtener instrucciones especiales
        EditText instructions = findViewById(R.id.special_instructions);
        instructions.setText(instructionsText);

        //Validar cambios
        if (projectDiffs.containsKey("instrucciones_especiales")) {
            instructions.setBackgroundColor(ContextCompat.getColor(this, R.color.yellow));
        }

        specialInstructionsContent.setVisibility(View.VISIBLE);
        specialInstructionsProgress.setVisibility(View.GONE);
    }

    private void addProjectToDatabase() {
        //Validar
        //Referenciar views
        MaterialEditText internOrder = findViewById(R.id.intern_order_number);

        //Validar orden interna
        boolean valid = internOrder.validateWith(new METValidator("Campo obligatorio") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty;
            }
        });

        if (valid) {
            //Preparar Views
            progressBar.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);

            //Definir numero de orden interna
            orderNumber = internOrder.getText().toString();

            //Convertir a numero y agregarlo
            long orderNumberValue = Long.parseLong(orderNumber);
            projectRoot.put("numero_orden", orderNumberValue);

            //Agregar primeramente Dispensarios
            //addDispensersToDatabase(internOrder.getText().toString());

            //Agregar nombre
            projectRoot.put("nombre", getProjectName());

            //Agregar datos generales
            projectRoot.put("datos_generales", getGeneralData());

            //Agregar componentes EVO6
            projectRoot.put("componentes_evo6", getEvo6Components());

            //Agregar tipo de mebarque
            projectRoot.put("tipo_embarque", getShipment());

            //Agregar software
            projectRoot.put("software", getSoftware());

            //Agregar vehículos
            projectRoot.put("vehiculos", getVehicles());

            //Agergar status inicial
            projectRoot.put("estatus", currentProdStatus);
            projectRoot.put("estatus_administrativo", currentAdminStatus);
            projectRoot.put("estatus_proyecto", currentProjectStatus);

            //Agergar info de imagenes
            projectRoot.put("imagenes", getImagesInfo());

            //Obtener intrucciones especiales
            projectRoot.put("instrucciones_especiales", getSpecialInstructions());

            //Agregar timestamp
            projectRoot.put("epoch", new Date().getTime());

            //Agregar referencias de dispensarios
            projectRoot.put("dispensarios", dispReferences);
            getInvoiceDispensersData();

            //Definir mensaje de resupuesta
            final String wordSuccess, wordFail;
            if (addOrModify) {
                wordSuccess = "agregado";
                wordFail = "agregar";
            } else {
                wordSuccess = "modificado";
                wordFail = "modificar";
            }

            //Definir listeners
            OnSuccessListener<Void> successListener = aVoid ->
            {
                Toast.makeText(ProjectForm.this, "Proyecto " + wordSuccess + " correctamente",
                        Toast.LENGTH_SHORT).show();

                progressBar.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);

                // Actualizar referencias de proyectos
                setProjectReference();

                // Terminar la actividad
                closeProjectForm(RESULT_OK);
            };

            OnFailureListener failureListener = e ->
            {
                Toast.makeText(ProjectForm.this, "Error al " + wordFail + " proyecto",
                        Toast.LENGTH_SHORT).show();

                progressBar.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            };

            //Obtener usuario
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String uid = user.getUid();

            // Para nuevo proyecto
            if (addOrModify) {
                // Validar si existe el proyecto, si no exsite, se registra
                doesProjectExist(orderNumber, () -> {
                    //Definir usuario
                    projectRoot.put("id_usuario", uid);

                    //Agregar proyecto
                    projectsRef.document(orderNumber).set(projectRoot).addOnSuccessListener(successListener).addOnFailureListener(failureListener);

                    //Notificar
                    sendNotification(user.getDisplayName());
                });
            } else {
                //Obtener diferencias
                Map<String, Object> diffs = checkChanges(projectRoot, existingProjectRoot);

                //Subir diferencias, incluso si no hubo
                uploadChanges(diffs);
                Log.d("Diffs", diffs.toString());

                //Modificar proyecto
                projectsRef.document(orderNumber).update(projectRoot).addOnSuccessListener(successListener).addOnFailureListener(failureListener);
                sendNotification(user.getDisplayName());
            }

            // Crear alarma
            setProjectAlarm();
        }
    }


    private void getInvoiceDispensersData() {
        // Obtener views
        final MaterialEditText invoiceNumberTv = findViewById(R.id.invoice_general_number_tv);
        final MaterialBetterSpinner invoiceDaySpinner = findViewById(R.id.invoice_general_date_day_spinner);
        final MaterialBetterSpinner invoiceMonthSpinner = findViewById(R.id.invoice_general_date_month_spinner);
        final MaterialBetterSpinner invoiceYearSpinner = findViewById(R.id.invoice_general_date_year_spinner);

        // Obtener textos
        final String invoiceNumber = invoiceNumberTv.getText().toString();
        final String invoiceDay = invoiceDaySpinner.getText().toString();
        final String invoiceYear = invoiceYearSpinner.getText().toString();
        String invoiceMonth = invoiceMonthSpinner.getText().toString();

        if (!invoiceMonth.isEmpty()) {
            invoiceMonth = Constants.monthStringToNumber(invoiceMonth);
        }

        // Obtener fecha
        final Date invoiceDate = DateUtils.dateNumbersToDate(invoiceDay, invoiceMonth, invoiceYear);
        Long invoiceEpoch = null;
        if (invoiceDate != null) {
            invoiceEpoch = invoiceDate.getTime();
        }

        // Guardar en datos de proyecto
        projectRoot.put("numero_factura_dispensarios", invoiceNumber);
        projectRoot.put("fecha_factura_dispensarios", invoiceEpoch);
    }

    private void addDispenserToDatabase(final DispModel dispModel) {
        //Obtener valor de orden interna
        MaterialEditText internOrder = findViewById(R.id.intern_order_number);
        orderNumber = internOrder.getText().toString();

        //Definirla al objeto "Dispensario"
        dispModel.setOrdenInterna(orderNumber);

        //Escribir en base de datos
        dispensersRef.add(dispModel).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                //Agregar referencia obtenida
                dispReferences.add(documentReference.getId());

                //Agregar Id a objeto
                dispModel.setId(documentReference.getId());

                //Ejecturar funcion disparada por nuevo dispensario
                calculateAccesoriosEmbarque();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("DB", "Error al agregar dispensario");
            }
        });
    }

    private void editDispenserFromDatabase(DispModel dispModel) {
        //Escribir en base de datos
        dispensersRef.document(dispModel.getId()).set(dispModel).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("DB", "Dispensario actualizado correctamente");
            } else {
                Log.e("DB", "Error al actualizar dispensario");
            }
        });
    }

    private void deleteDispenserDialog(final View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Eliminar dispensario");
        builder.setMessage("¿Seguro que desea remover este dispensario?");

        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeDispenserFromDatabase(v);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void removeDispenserFromDatabase(View v) {
        //Obtener dispensario
        final int elementPosition = dispModelsList.getChildAdapterPosition(v);
        final DispModel dispModel = dispModels.get(elementPosition);

        //Eliminar dispensario de base de datos
        dispensersRef.document(dispModel.getId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    int idxToRemove = -1;

                    //Remover de lista de referencias
                    for (int i = 0; i < dispReferences.size(); i++) {
                        if (dispReferences.get(i).equals(dispModel.getId())) {
                            //Remover
                            idxToRemove = i;
                        }
                    }

                    if (idxToRemove != -1) dispReferences.remove(idxToRemove);

                    //Actualizar lista
                    dispModels.remove(elementPosition);
                    dispenserModelAdapter.notifyDataSetChanged();

                    //Actualizar documento de proyecto
                    projectsRef.document(orderNumber).update("dispensarios", dispReferences);
                }
            }
        });
    }

    @SuppressWarnings("unchekced")
    private void calculateAccesoriosEmbarque() {
        //Map para almacenar los accesorios
        Map<String, Object> accesories = null;

        //Validar si ya existe el key de accesorios de embarque en objeto principal
        if (projectRoot.containsKey("accesorios_embarque")) {
            accesories = (HashMap) projectRoot.get("accesorios_embarque");
        }

        //Validar que el Map no sea nulo
        if (accesories == null) {
            //Crear nuevo MAP para almacenar los datos
            accesories = new HashMap<>();

            //Agregar TAGS VERDES (3 x dispensario)
            accesories.put("tags_verdes", new Long(dispModels.size() * 3));

            //Agregar TAGS AZULES (3 x dispensario)
            accesories.put("tags_azules", new Long(dispModels.size()));

            //Agregar TAGS ROJOS (2 x estacion)
            accesories.put("tags_rojos", new Long(2));

            //Agregar TAPAS NEGRAS (2 x dispensario)
            accesories.put("tapas_negras", new Long(dispModels.size() * 2));

            //Copias NOM
            if (!accesories.containsKey("nom_copies")) {
                accesories.put("nom_copies", new Long(1));
            }
        }

        //Actualizar Spinners de cada elemento
        MaterialBetterSpinner greenTagEt = findViewById(R.id.green_tag_et);
        MaterialBetterSpinner blueTagEt = findViewById(R.id.blue_tag_et);
        MaterialBetterSpinner redTagEt = findViewById(R.id.red_tag_et);
        MaterialBetterSpinner tapasNegrasEt = findViewById(R.id.tapas_negras_et);
        MaterialBetterSpinner nomCopiesEt = findViewById(R.id.nom_copies_et);

        greenTagEt.setText(String.valueOf(accesories.get("tags_verdes")));
        blueTagEt.setText(String.valueOf(accesories.get("tags_azules")));
        redTagEt.setText(String.valueOf(accesories.get("tags_rojos")));
        tapasNegrasEt.setText(String.valueOf(accesories.get("tapas_negras")));
        nomCopiesEt.setText(String.valueOf(accesories.get("nom_copies")));

        if (projectDiffs.containsKey("accesorios_embarque")) {
            Map<String, Object> shipmentAccDiff = (Map<String, Object>) projectDiffs.get(
                    "accesorios_embarque");

            int highlightColor = ContextCompat.getColor(this, R.color.yellow);
            if (shipmentAccDiff != null) {
                if (shipmentAccDiff.containsKey("nom_copies"))
                    nomCopiesEt.setBackgroundColor(highlightColor);
                if (shipmentAccDiff.containsKey("tags_azules"))
                    blueTagEt.setBackgroundColor(highlightColor);
                if (shipmentAccDiff.containsKey("tags_rojos"))
                    redTagEt.setBackgroundColor(highlightColor);
                if (shipmentAccDiff.containsKey("tags_verdes"))
                    greenTagEt.setBackgroundColor(highlightColor);
                if (shipmentAccDiff.containsKey("tapas_negras"))
                    tapasNegrasEt.setBackgroundColor(highlightColor);
            }
        }

        //Actualizar elemento en objeto principal
        projectRoot.put("accesorios_embarque", accesories);

        //Mostrar contenido si no era visible antes
        if (shipmentAccesoriesCv.getVisibility() == View.GONE) {
            shipmentAccesoriesCv.setVisibility(View.VISIBLE);
        }
    }

    private String getProjectName() {
        HashMap<String, Integer> selectedModels = new HashMap<>();

        try {
            for (DispModel dispModel : dispModels) {
                String dispProducts = dispModel.getProducts();
                if (selectedModels.containsKey(dispProducts)) {
                    int currentVal = selectedModels.get(dispProducts);
                    currentVal++;

                    selectedModels.put(dispProducts, currentVal);
                } else {
                    selectedModels.put(dispProducts, 1);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return "";
        }

        StringBuilder name = new StringBuilder();
        int setSize = selectedModels.keySet().size();
        int counter = 0;

        for (String key : selectedModels.keySet()) {
            name.append(selectedModels.get(key));
            name.append(" ");
            name.append(key);

            if (counter++ < (setSize - 1)) name.append(" + ");
        }

        return name.toString();
    }

    //Deshabilitar CardViews
    private void disableViews() {
        //Datos generales
        Constants.disableLinearLayoutChilds(generalDataContent, this);

        //Boton para agregar dispensarios
        FloatingActionButton addDispenserModel = findViewById(R.id.add_dispenser_model);
        addDispenserModel.hide();

        //Lista de dispensarios
        dispenserModelAdapter.setOnLongClickListener(null);

        //Componentes evo6
        Constants.disableLinearLayoutChilds(evo6ComponentsContent, this);

        //Accesorios para embarque
        Constants.disableLinearLayoutChilds(shipmentAccesoriesContent, this);

        //Tipo de embarque
        Constants.disableLinearLayoutChilds(shipmentTypeContent, this);

        //Vehiculos
        Constants.disableLinearLayoutChilds(vehicleContent, this);

        //Software
        Constants.disableLinearLayoutChilds(softwareContent, this);

        //Imagenes
        Constants.disableLinearLayoutChilds(imagesContent, this);

        //Instrucciones especiales
        Constants.disableLinearLayoutChilds(specialInstructionsContent, this);
    }

    private void showSignsDialog() {
        String projectName;
        if (projectSimpleName != null) {
            projectName = projectSimpleName;
        } else {
            projectName = "Proyecto " + orderNumber;
        }

        SignDialogFragment signDialogFragment = new SignDialogFragment();
        signDialogFragment.setData(orderNumber, projectName);
        signDialogFragment.show(getSupportFragmentManager(), "signs_dialog");
    }

    private void showSetStatusDialog() {
        //Crear builder para diálogo
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //Obtener layout
        View content = getLayoutInflater().inflate(R.layout.dialog_set_status, null);

        //Definir contenido
        builder.setView(content);

        //Obtener views del layout
        final MaterialBetterSpinner projectStatusOptions =
                content.findViewById(R.id.project_status_spinner);
        final MaterialBetterSpinner prodStatusOptions =
                content.findViewById(R.id.prod_status_spinner);
        final MaterialBetterSpinner admStatusOptions =
                content.findViewById(R.id.admin_status_spinner);

        // Checar diferencias
        if (projectDiffs != null) {
            // Obtener color que se usará para remarcar
            int highlightColor = ContextCompat.getColor(this, R.color.yellow);
            if (projectDiffs.containsKey("estatus")) {
                prodStatusOptions.setBackgroundColor(highlightColor);
            }

            if (projectDiffs.containsKey("estatus_administrativo")) {
                admStatusOptions.setBackgroundColor(highlightColor);
            }

            if (projectDiffs.containsKey("estatus_proyecto")) {
                projectStatusOptions.setBackgroundColor(highlightColor);
            }
        }

        //Crear adaptador
        ArrayAdapter<String> arrayAdapter1;
        ArrayAdapter<String> arrayAdapter2;
        ArrayAdapter<String> arrayAdapter3;

        if (canModify) {
            //Mostrar opciones
            arrayAdapter1 = new ArrayAdapter<>(this,
                    android.R.layout.simple_expandable_list_item_1, PROD_STATUS);

            //Mostrar opciones
            arrayAdapter2 = new ArrayAdapter<>(this,
                    android.R.layout.simple_expandable_list_item_1, PROJECT_ADMN_STATUS);

            arrayAdapter3 = new ArrayAdapter<>(this,
                    android.R.layout.simple_expandable_list_item_1, PROJECT_STATUS);
        } else {
            //No puede modificar, no se da opcion de mostrar las opciones
            arrayAdapter1 = new ArrayAdapter<>(this,
                    android.R.layout.simple_expandable_list_item_1, new ArrayList<>());

            arrayAdapter2 = arrayAdapter1;
            arrayAdapter3 = arrayAdapter1;
        }

        //Definir adaptador a spinner
        prodStatusOptions.setAdapter(arrayAdapter1);
        admStatusOptions.setAdapter(arrayAdapter2);
        projectStatusOptions.setAdapter(arrayAdapter3);

        //Inicializar texto con estatus actual
        prodStatusOptions.setText(currentProdStatus);
        admStatusOptions.setText(currentAdminStatus);
        projectStatusOptions.setText(currentProjectStatus);

        //Obtener botones
        Button saveButton = content.findViewById(R.id.save_button);
        Button cancelButton = content.findViewById(R.id.cancel_button);

        //Evaluar si puede guardar
        if (!canModify) {
            saveButton.setEnabled(false);
        }

        //Definir listeners
        saveButton.setOnClickListener(v -> {
            //Obtener texto y Asignar texto seleccionado a variable global
            String selectedProjectStatus = projectStatusOptions.getText().toString();
            String selectedProdStatus = prodStatusOptions.getText().toString();
            String selectedAdminStatus = admStatusOptions.getText().toString();

            //Evaluar cambio
            if (!currentProdStatus.equals(Constants.TERMINADO) && selectedProdStatus.equals(Constants.TERMINADO)) {
                //Definir fecha de proyecto terminado
                projectRoot.put("fecha_terminado", new Date().getTime());
            } else if (!currentProdStatus.equals(Constants.ENTREGADO) && selectedProdStatus.equals(Constants.ENTREGADO)) {
                //Definir fecha de proyecto terminado
                projectRoot.put("fecha_entregado", new Date().getTime());
            }

            //Evaluar si se tendrá que cancelar alguna alarma
            removeProdAlarm =
                    !currentProdStatus.equals(Constants.ENTREGADO) && selectedProdStatus.equals(Constants.ENTREGADO);
            removeAdminAlarm =
                    !currentAdminStatus.equals(Constants.PAGADO) && selectedAdminStatus.equals(Constants.PAGADO);

            //Actualizar estado
            currentProdStatus = selectedProdStatus;
            currentAdminStatus = selectedAdminStatus;
            currentProjectStatus = selectedProjectStatus;

            //Cerrar diálogo
            setStatusDialog.dismiss();
        });

        cancelButton.setOnClickListener(v -> {
            //Cerrar diálogo
            setStatusDialog.dismiss();
        });

        //Crear y mostrar diálogo
        setStatusDialog = builder.create();
        setStatusDialog.setCancelable(false);
        setStatusDialog.show();
    }

    private void sendNotification(String user) {
        NotificationsManager notificationsManager = new NotificationsManager(
                new NotificationsManager.OnMessageReceived() {
                    @Override
                    public void onSuccess(String message) {
                        Log.d(TAG, message);
                    }

                    @Override
                    public void onError(String errorMsg) {
                        Toast.makeText(ProjectForm.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                });

        String projectName;
        if (projectSimpleName != null) {
            projectName = projectSimpleName;
        } else {
            projectName = "Proyecto " + orderNumber;
        }

        if (addOrModify) {
            notificationsManager.onNewProject(projectName, orderNumber);
        } else {
            notificationsManager.onProjectModified(user, projectName, orderNumber);
        }
    }

    private void sendAlarmNotification(Alarm alarm, boolean add) {
        NotificationsManager notificationsManager = new NotificationsManager(
                new NotificationsManager.OnMessageReceived() {
                    @Override
                    public void onSuccess(String message) {
                        Toast.makeText(ProjectForm.this, message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(String errorMsg) {
                        Toast.makeText(ProjectForm.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                });

        for (String alarmType : alarm.getTipo()) {
            // Enviar nueva alarma
            notificationsManager.onNewAlarm(alarm, alarmType, add);
        }
    }

    private void selectPrinter() {
        printerFragment = PrintersDialogFragment.newInstance(this::printProjectInfo);
        printerFragment.show(getSupportFragmentManager(), "print_dialog");
    }

    private void printProjectInfo(String printerIp) {
        //Crear instancia de impresora
        PrinterManager printerManager = new PrinterManager(this, printerIp,
                new PrinterManager.PrinterListener() {
                    @Override
                    public void onSuccesfulPrint(final String msg) {
                        runOnUiThread(() -> {
                            printerFragment.dismiss();
                            Toast.makeText(ProjectForm.this, msg, Toast.LENGTH_LONG).show();
                        });
                    }

                    @Override
                    public void onPrinterError(final String msg) {
                        runOnUiThread(() -> {
                            printerFragment.dismiss();
                            Toast.makeText(ProjectForm.this, msg, Toast.LENGTH_LONG).show();
                        });
                    }

                    @Override
                    public void onWarning(final String msg) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                printerFragment.dismiss();
                                Toast.makeText(ProjectForm.this, msg, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });

        //Realizar impresión
        printerManager.printProyectInfo(existingProjectRoot, dispModels);
    }

    private void getLastOrderNumber() {
        projectsRef.orderBy("numero_orden",
                Query.Direction.DESCENDING).limit(1).addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Toast.makeText(ProjectForm.this, "Error al obtener siguiente número de orden"
                        , Toast.LENGTH_SHORT).show();
            } else {
                //Validar resultado
                if (queryDocumentSnapshots != null) {
                    Long orderNumber = null;
                    for (DocumentSnapshot documentSnapshot :
                            queryDocumentSnapshots.getDocuments()) {
                        if (documentSnapshot.getData() != null) {
                            orderNumber = (Long) documentSnapshot.getData().get("numero_orden");
                        }
                    }

                    if (orderNumber != null) {
                        //Definir número de orden
                        internOrder.setText(String.valueOf(orderNumber + 1));
                    }
                }
            }
        });
    }

    private void doesProjectExist(String orderNumber, Runnable onValidated) {
        projectsRef.document(orderNumber).get().addOnCompleteListener(task -> {
            DocumentSnapshot snapshot = task.getResult();
            if (snapshot != null && snapshot.exists()) {
                Toast.makeText(ProjectForm.this, "El proyecto " + orderNumber + " ya existe",
                        Toast.LENGTH_LONG).show();

                progressBar.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            } else {
                onValidated.run();
            }
        });
    }

    private Map<String, Object> checkChanges(Map<String, Object> newMap, Map<String,
            Object> oldMap) {
        // Mapa donde se almacenarán las diferencias
        Map<String, Object> diffs = new HashMap<>();

        // Iterar en todos los valores del documento
        for (Map.Entry<String, Object> entry : newMap.entrySet()) {
            // No busar diferencias de: epoch ni fechas
            if (!entry.getKey().equals("epoch") && !entry.getKey().contains("fecha")) {
                // Ordenar listas antes de compararlas
                if (entry.getValue() instanceof ArrayList && entry.getKey().equals(
                        "dispensarios")) {
                    // Si es array list de dispensarios ordenar
                    Collections.sort((ArrayList<String>) entry.getValue(),
                            Constants.stringComparator);
                    if (oldMap.containsKey(entry.getKey())) {
                        Collections.sort((ArrayList<String>) oldMap.get(entry.getKey()),
                                Constants.stringComparator);
                    }
                }

                if (entry.getValue() != null) {
                    if (!oldMap.containsKey(entry.getKey()) || !entry.getValue().equals(oldMap.get(entry.getKey()))) {
                        // Obtener tipo del objeto actual
                        if (entry.getValue() instanceof Map) {
                            //Obtener mapa de diferencias
                            Map<String, Object> mapDiffs =
                                    checkChanges((Map<String, Object>) entry.getValue(),
                                            (Map<String,
                                                    Object>) oldMap.get(entry.getKey()));

                            // Agregar diferencias al objeto principal
                            diffs.put(entry.getKey(), mapDiffs);
                        } else {
                            //Agregar valor
                            diffs.put(entry.getKey(), entry.getValue());
                        }
                    }
                }
            }
        }

        return diffs;
    }

    public void uploadChanges(Map<String, Object> differences) {
        //Agregar número de orden al mapa
        differences.put("numero_orden", Integer.valueOf(orderNumber));

        //Agregar diferencias de dispensarios
        differences.put("dispensarios", dispsDiffs);

        //Subir cambios
        projectsDifferencesRef.document(orderNumber).set(differences).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.v(TAG, "Cambios de proyectos guardados correctamente");
                } else {
                    Log.e(TAG, "Error al guardar cambios de proyectos");
                }
            }
        });
    }

    private void registerAlarm(Alarm alarm) {
        // Crear referencia a colección en base de datos
        CollectionReference alarmsRef = db.collection(Constants.ALARMS_COLLECTION);

        // Subir registro
        alarmsRef.document(orderNumber).set(alarm).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d(TAG, "Alarma registrada correctamente");
            }
        });
    }

    private void removeAlarm(String id) {
        // Crear referencia a colección en base de datos
        CollectionReference alarmsRef = db.collection(Constants.ALARMS_COLLECTION);

        // Subir registro
        alarmsRef.document(id).delete().addOnCompleteListener(task -> Log.d(TAG, "Alarma " +
                "eliminada correctamente"));
    }

    private void setProjectAlarm() {
        // Evaluar si se debe programa o actualizar una alarma
        if (!currentProdStatus.equals(Constants.ENTREGADO) || !currentAdminStatus.equals(Constants.PAGADO)) {
            // Crear lista para agregar tipos de alarmas
            ArrayList<String> alarmTypes = new ArrayList<>();

            // Evaluar status
            if (!currentProdStatus.equals(Constants.ENTREGADO)) {
                alarmTypes.add(NotificationsManager.NEW_ALARM_PROD);
            }
            if (!currentAdminStatus.equals(Constants.PAGADO)) {
                alarmTypes.add(NotificationsManager.NEW_ALARM_ADMIN);
            }

            // Crear alarma para registrar
            Alarm alarm = new Alarm();
            alarm.setTipo(alarmTypes);
            alarm.setEpoch(System.currentTimeMillis() + Constants.PROJECT_ALARM_TIME_MS);
            alarm.setId(orderNumber);
            alarm.setNombre(projectSimpleName);

            // Subir registro
            registerAlarm(alarm);

            // Crear notificacion
            sendAlarmNotification(alarm, true);
        }
        // Evaluar si se tiene que eliminar el registro en base de datos
        else if (removeProdAlarm || removeAdminAlarm) {
            removeAlarm(orderNumber);
        }

        // Evaluar si se tiene que desprogramar alguna alarma
        if (removeProdAlarm || removeAdminAlarm) {
            ArrayList<String> alarmType = new ArrayList<>();
            if (removeProdAlarm) alarmType.add(NotificationsManager.NEW_ALARM_PROD);
            if (removeAdminAlarm) alarmType.add(NotificationsManager.NEW_ALARM_ADMIN);

            // Crear alarma para remover
            Alarm alarm = new Alarm();
            alarm.setTipo(alarmType);
            alarm.setEpoch(-1);
            alarm.setId(orderNumber);
            alarm.setNombre(projectSimpleName);
            sendAlarmNotification(alarm, false);
        }
    }

    private void getContacts() {
        db.collection("proyectos_contactos").get().addOnCompleteListener(placesTask ->
        {
            if (placesTask.isSuccessful() && placesTask.getResult() != null) {
                QuerySnapshot querySnapshot = placesTask.getResult();

                for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                    if (documentSnapshot.exists()) {
                        Contact contact = documentSnapshot.toObject(Contact.class);
                        commonContacts.add(contact);
                    }
                }

                if (!commonContacts.isEmpty()) {
                    Collections.sort(commonContacts,
                            (contact, t1) -> contact.getNombre().compareTo(t1.getNombre()));
                }
            }
        });
    }

    private void saveContacts(Contact contact) {
        // Obtener referencia de contactos
        if (contact != null && contact.getNombre() != null) {
            db.collection("proyectos_contactos").whereEqualTo("nombre", contact.getNombre()).limit(1).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    // Obtener coincidencias
                    if (task.getResult() != null && task.getResult().getDocuments().isEmpty()) {
                        // Si la lista esta vacía, no existe el contacto, se debe entonces almacenar
                        db.collection("proyectos_contactos").add(contact).addOnCompleteListener(task1 ->
                        {
                            if (task1.isSuccessful()) {
                                Log.d(TAG, "Nuevo contacto guardado en base de datos");
                            } else {
                                Log.e(TAG, "Error al guardar contacto en base de datos");
                            }
                        });
                    } else {
                        Log.d(TAG, "El contacto ya existe");
                    }
                }
            });
        }
    }

    private void closeProjectForm(int result) {
        Intent resultIntent = new Intent();
        setResult(result, resultIntent);
        finish();
    }

    @SuppressWarnings({"unchecked", "null"})
    private void setProjectReference() {
        final Map<String, String> datosGenerales = (Map<String, String>) projectRoot.get("datos_generales");
        final String cliente = datosGenerales.get("cliente");
        final String estacion = datosGenerales.get("estacion");

        final ProjectReference projectReference = new ProjectReference();
        projectReference.setNumero_orden((Long) projectRoot.get("numero_orden"));
        projectReference.setEpoch((Long) new Date().getTime());
        projectReference.setCliente(cliente);
        projectReference.setEstacion(estacion);
        projectReference.setEstatus((String) projectRoot.get("estatus"));
        projectReference.setEstatus_administrativo((String) projectRoot.get("estatus_administrativo"));
        projectReference.setEstauts_proyecto((String) projectRoot.get("estatus_proyecto"));
        projectReference.setCliente_query(Constants.toQueryData(cliente));
        projectReference.setTicket_crm((String) projectRoot.get("ticket_crm"));
        projectReference.setEstacion_query(Constants.toQueryData(estacion));

        projectsReferencesCollection.document(
                projectReference.getNumero_orden().toString())
                .set(projectReference)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.v(TAG, "Referencia de proyecto registrada");
                    } else {
                        Log.v(TAG, "Error al registrar referencia de proyecto");
                    }
                });
    }

    private void validateDispModelToAdd(DispModel dispModel) {
        if (dispModel.getSerialNumber() == null || dispModel.getSerialNumber().isEmpty()) {
            addDispenserModel(dispModel);
            addDispenserToDatabase(dispModel);
        } else {
            dispensersRef
                    .whereEqualTo("serialNumber", dispModel.getSerialNumber()).get().addOnCompleteListener(task -> {
                if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {
                    // Si se encontró un dispensario con ese número de serie... no agregar
                    Constants.showDialog(
                            this,
                            "Error",
                            "El número de serie " + dispModel.getSerialNumber() + " ya esta registrado",
                            (dialogInterface, i) -> {

                            });
                    return;
                } else {
                    Log.e(TAG, "Error al buscar dispensario");
                }

                addDispenserModel(dispModel);
                addDispenserToDatabase(dispModel);
            });
        }
    }

    private void validateDispModelToEdit(DispModel dispModel, String oldSerialNumber) {
        if (dispModel.getSerialNumber() == null || dispModel.getSerialNumber().isEmpty()) {
            //Actualizar base de datos
            editDispenserFromDatabase(dispModel);

            //Actualizar lista
            dispenserModelAdapter.notifyDataSetChanged();
        } else {
            dispensersRef
                    .whereEqualTo("serialNumber", dispModel.getSerialNumber()).get().addOnCompleteListener(task -> {
                if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {
                    // Si se encontró un dispensario con ese número de serie... no agregar
                    Constants.showDialog(
                            this,
                            "Error",
                            "El número de serie " + dispModel.getSerialNumber() + " ya esta registrado",
                            (dialogInterface, i) -> {

                            });

                    dispModel.setSerialNumber(oldSerialNumber);

                    return;
                } else {
                    Log.e(TAG, "Error al buscar dispensario");
                }

                //Actualizar base de datos
                editDispenserFromDatabase(dispModel);

                //Actualizar lista
                dispenserModelAdapter.notifyDataSetChanged();
            });
        }
    }
}