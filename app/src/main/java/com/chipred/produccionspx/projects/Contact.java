package com.chipred.produccionspx.projects;

import java.util.HashMap;
import java.util.Map;

public class Contact
{
    private String nombre = "";
    private String email = "";
    private String phone = "";

    public Contact()
    {
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public Map<String, Object> toMap()
    {
        Map<String, Object> mMap = new HashMap<>();
        mMap.put("email", email);
        mMap.put("phone", phone);
        mMap.put("nombre", nombre);

        return mMap;
    }
}
