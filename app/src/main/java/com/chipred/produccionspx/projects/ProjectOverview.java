package com.chipred.produccionspx.projects;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class ProjectOverview
{
    private int allDispsCount;
    private String orderNumber;
    private String crmTicket;
    private String client = "";
    private String station = "";
    private String stationNumber = "";
    private String requestDate = "";
    private String name = "";
    private String status = "";
    private String adminStatus = "";
    private String projectStatus = "";
    private String userInitials = "";
    private String userId = "";
    private ArrayList<String> dispensersIds = new ArrayList<>();
    private Long epoch;

    public ProjectOverview(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public String getClient()
    {
        return client;
    }

    public void setClient(String client)
    {
        this.client = client;
    }

    public String getStation()
    {
        return station;
    }

    public void setStation(String station)
    {
        this.station = station;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getRequestDate()
    {
        return requestDate;
    }

    public void setRequestDate(String requestDate)
    {
        this.requestDate = requestDate;
    }

    public String getUserInitials()
    {
        return userInitials;
    }

    public void setUserInitials(String userInitials)
    {
        this.userInitials = userInitials;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getStationNumber()
    {
        return stationNumber;
    }

    public void setStationNumber(String stationNumber)
    {
        this.stationNumber = stationNumber;
    }

    public String getAdminStatus()
    {
        return adminStatus;
    }

    public void setAdminStatus(String adminStatus)
    {
        this.adminStatus = adminStatus;
    }

    public String getCrmTicket()
    {
        return crmTicket;
    }

    public void setCrmTicket(String crmTicket)
    {
        this.crmTicket = crmTicket;
    }

    public Long getEpoch()
    {
        return epoch;
    }

    public void setEpoch(Long epoch)
    {
        this.epoch = epoch;
    }

    public String getProjectStatus()
    {
        return projectStatus == null ? "" : projectStatus;
    }

    public void setProjectStatus(String projectStatus)
    {
        this.projectStatus = projectStatus;
    }

    public ArrayList<String> getDispensersIds()
    {
        return dispensersIds;
    }

    public void setDispensersIds(ArrayList<String> dispensersIds)
    {
        this.dispensersIds = dispensersIds;
    }

    public int getAllDispsCount()
    {
        return allDispsCount;
    }

    public void setAllDispsCount(int allDispsCount)
    {
        this.allDispsCount = allDispsCount;
    }

    @NonNull
    @Override
    public String toString()
    {
        return orderNumber + " - " + client + " " + station;
    }
}
