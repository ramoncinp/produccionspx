package com.chipred.produccionspx.projects;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class ContactsAdapter extends ArrayAdapter<Contact>
{
    private List<Contact> contactsListFull;
    private List<Contact> filteredList;

    ContactsAdapter(@NonNull Context context, @NonNull List<Contact> contactsList)
    {
        super(context, 0, contactsList);
        contactsListFull = new ArrayList<>(contactsList);
    }

    public List<Contact> getFilteredList()
    {
        return filteredList;
    }

    @NonNull
    @Override
    public Filter getFilter()
    {
        return contactNameFilter;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        Contact contact = getItem(position);

        String contactName = contact == null ? "" : contact.getNombre();
        if (convertView == null)
        {
            convertView =
                    LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_expandable_list_item_1,
                            parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        if (contactName != null)
        {
            viewHolder.bindBeer(contactName);
        }

        return convertView;
    }

    private Filter contactNameFilter = new Filter()
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            FilterResults results = new FilterResults();
            List<Contact> suggestionList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0)
            {
                suggestionList.addAll(contactsListFull);
            }
            else
            {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Contact contact : contactsListFull)
                {
                    if (contact.getNombre().toLowerCase().contains(filterPattern))
                    {
                        suggestionList.add(contact);
                    }
                }
            }
            results.values = suggestionList;
            results.count = suggestionList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            filteredList = (List<Contact>) results.values;
            if (filteredList != null)
            {
                clear();
                addAll(filteredList);
            }
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue)
        {
            return ((Contact) resultValue).getNombre();
        }
    };

    private static class ViewHolder
    {
        private TextView contactNameTextView;

        ViewHolder(View itemView)
        {
            contactNameTextView = itemView.findViewById(android.R.id.text1);
        }

        void bindBeer(String contactName)
        {
            contactNameTextView.setText(contactName);
        }
    }
}