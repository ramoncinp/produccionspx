package com.chipred.produccionspx.projects;

import com.chipred.produccionspx.DispModel;

import java.util.ArrayList;

public class ProjectDispensers
{
    private int allDispsCount;
    private String orderNumber;
    private String station;
    private String client;
    private ArrayList<DispModel> dispModels = new ArrayList<>();

    public ProjectDispensers()
    {
    }

    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public void setStation(String station)
    {
        this.station = station;
    }

    public void setClient(String client)
    {
        this.client = client;
    }

    public int getAllDispsCount()
    {
        return allDispsCount;
    }

    public void setAllDispsCount(int allDispsCount)
    {
        this.allDispsCount = allDispsCount;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public String getStation()
    {
        return station;
    }

    public String getClient()
    {
        return client;
    }

    public ArrayList<DispModel> getDispModels()
    {
        return dispModels;
    }

    public void addDispenser(DispModel dispModel)
    {
        dispModels.add(dispModel);
    }

    public void updateProjectData(ProjectOverview projectOverview)
    {
        station = projectOverview.getStation();
        client = projectOverview.getClient();
        allDispsCount = projectOverview.getAllDispsCount();
    }
}
