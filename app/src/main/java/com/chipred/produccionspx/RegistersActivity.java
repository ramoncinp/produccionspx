package com.chipred.produccionspx;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.adapters.PasswordRegistersAdapter;
import com.chipred.produccionspx.users.UserData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Nullable;

public class RegistersActivity extends AppCompatActivity
{
    //Objetos
    private ArrayList<Map<String, Object>> registers;
    private ArrayList<UserData> savedUsers = new ArrayList<>();

    //Views
    private PasswordRegistersAdapter adapter;
    private ProgressBar progressBar;
    private RecyclerView registersList;
    private TextView noRegisters;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registers);

        //Definir titulo
        setTitle(getResources().getString(R.string.registers_title));

        //Agregar flecha para regresar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Obtener views
        noRegisters = findViewById(R.id.no_registers_tv);
        progressBar = findViewById(R.id.progress_bar);
        registersList = findViewById(R.id.registers_list);

        //Inicializar adaptador
        registers = new ArrayList<>();
        adapter = new PasswordRegistersAdapter(registers);

        //Asignar adaptador
        registersList.setAdapter(adapter);
        registersList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
        {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        //Obtener registros
        getRegisters();
    }

    private void getRegisters()
    {
        FirebaseFirestore.getInstance().collection("generacion_password").orderBy("fecha",
                Query.Direction.DESCENDING).limit(15).addSnapshotListener(this, new EventListener<QuerySnapshot>()
        {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e)
            {
                if (e != null)
                {
                    Toast.makeText(RegistersActivity.this, "Error al obtener registros",
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if (queryDocumentSnapshots != null)
                    {
                        //Limpiar lista de registros
                        registers.clear();

                        //Obtener registros
                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments())
                        {
                            //Obtener MAP
                            Map<String, Object> mData = snapshot.getData();

                            //Obtener usuario
                            String userId = snapshot.getString("usuario");
                            String userInitials = getUser(userId);

                            if (!userInitials.isEmpty())
                            {
                                mData.put("iniciales_usuario", userInitials);
                            }


                            //Agregar a la lista de registros
                            registers.add(mData);
                        }

                        //Actualizar adaptador
                        adapter.notifyDataSetChanged();

                        //Mostrar registros
                        showRegisters();
                    }
                    else
                    {
                        //Indicar que no hay registros
                        showNoRegisters();
                    }
                }
            }
        });
    }

    private void showRegisters()
    {
        noRegisters.setVisibility(View.GONE);
        registersList.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void showNoRegisters()
    {
        noRegisters.setVisibility(View.VISIBLE);
        registersList.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void updateElectronicKitsOverview(UserData userData)
    {
        //Actualizar datos a kits electrónicos con el número de orden del proyecto que entra al
        // método
        for (Map<String, Object> register : registers)
        {
            if (!register.containsKey("iniciales_usuario") && (register.get("usuario")).equals(userData.getId()))
            {
                register.put("iniciales_usuario", userData.getInitials());
            }
        }

        //Actualizar adaptador
        adapter.notifyDataSetChanged();
    }

    private String getUser(final String userId)
    {
        UserData userData;

        //Si ya fue consultado el usuario
        if ((userData = isUserContained(userId)) != null)
        {
            //Actualizar elementos
            return userData.getInitials();
        }
        else
        {
            //Obtenerlo de la base de datos
            FirebaseFirestore.getInstance().collection("usuarios").document(userId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
            {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task)
                {
                    if (task.isSuccessful() && task.getResult() != null)
                    {
                        //Obtener documento
                        DocumentSnapshot snapshot = task.getResult();

                        if (snapshot == null)
                        {
                            Toast.makeText(RegistersActivity.this, "El usuario no existe",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        //Crear objeto de usuario
                        UserData mUser = snapshot.toObject(UserData.class);

                        //Agregar a la lista de usuarios
                        savedUsers.add(mUser);

                        //Actualizar kits
                        updateElectronicKitsOverview(mUser);
                    }
                    else
                    {
                        Toast.makeText(RegistersActivity.this, "Error al obtener usuario" +
                                " " + userId, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        return "";
    }

    private UserData isUserContained(String userId)
    {
        for (UserData userData : savedUsers)
        {
            if (userData.getId().equals(userId))
            {
                return userData;
            }
        }

        return null;
    }
}
