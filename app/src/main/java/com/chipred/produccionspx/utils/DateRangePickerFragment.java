package com.chipred.produccionspx.utils;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class DateRangePickerFragment extends DialogFragment
{
    // Views
    private Button submitButton;
    private Button cancelButton;
    private MaterialEditText startDateEt;
    private MaterialEditText endDateEt;

    // Objetos
    private Date startDate;
    private Date endDate;

    // Interfaz
    private DateRangePickerInterface dateRangePickerInterface;

    public DateRangePickerFragment()
    {
        // Required empty public constructor
    }

    public void setInitialDateRange(Date startDate, Date endDate)
    {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Obtener referencias de views
        View content = inflater.inflate(R.layout.dialog_date_range_picker, container, false);
        submitButton = content.findViewById(R.id.submit_button);
        cancelButton = content.findViewById(R.id.cancel_button);
        startDateEt = content.findViewById(R.id.start_date_et);
        endDateEt = content.findViewById(R.id.end_date_et);

        // Init dates editTexts values
        setEditTextValues();

        // Definir listeners a editText's
        setViewsListeners();

        // Inflate the layout for this fragment
        return content;
    }

    private void setEditTextValues()
    {
        if (startDate != null)
        {
            String startDateStr = Constants.dateObjectToString(startDate);
            startDateEt.setText(startDateStr);
        }

        if (endDate != null)
        {
            String endDateStr = Constants.dateObjectToString(endDate);
            endDateEt.setText(endDateStr);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    private void setViewsListeners()
    {
        View.OnClickListener datePickerListener = v ->
        {
            DatePickerFragment fragment = DatePickerFragment.newInstance((datePicker, i, i1, i2) ->
            {
                String gottenDate =
                        Constants.twoDigits(i2) + "/" + Constants.monthNumberToString
                                (i1 + 1) + "/" + i;

                final MaterialEditText dateEt = (MaterialEditText) v;
                dateEt.setText(gottenDate);
            });

            fragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(),
                    "datePicker");
        };

        startDateEt.setOnClickListener(datePickerListener);
        endDateEt.setOnClickListener(datePickerListener);

        submitButton.setOnClickListener(view ->
        {
            onSubmit();
        });

        cancelButton.setOnClickListener(view ->
        {
            if (getDialog() != null) getDialog().dismiss();
        });
    }

    private void onSubmit()
    {
        // Validar las longitudes de las fechas ingresadas
        // Fecha válida: 07/SEP/20

        METValidator lengthValidator = new METValidator("Fecha no válida")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return text.length() == "dd/MMM/yyyy".length();
            }
        };

        boolean isValid = startDateEt.validateWith(lengthValidator);
        isValid &= endDateEt.validateWith(lengthValidator);

        if (!isValid) return;

        // Obtener fechas
        Date startDate = Constants.dateStringToObject2(startDateEt.getText().toString());
        Date endDate = Constants.dateStringToObject2(endDateEt.getText().toString());

        if (startDate == null || endDate == null)
        {
            Toast.makeText(getActivity(), "Error al obtener fechas", Toast.LENGTH_SHORT).show();
        }
        else
        {
            // Definir horas de inicio y fin
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(startDate);
            startCalendar.set(Calendar.HOUR_OF_DAY, 0);
            startCalendar.set(Calendar.MINUTE, 0);
            startCalendar.set(Calendar.SECOND, 0);
            startDate = startCalendar.getTime();

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(endDate);
            endCalendar.set(Calendar.HOUR_OF_DAY, 23);
            endCalendar.set(Calendar.MINUTE, 59);
            endCalendar.set(Calendar.SECOND, 59);
            endDate = endCalendar.getTime();

            // Retornar fechas
            dateRangePickerInterface.onSelectedDates(startDate, endDate);
        }

        // Cerrar diálogo
        if (getDialog() != null) getDialog().dismiss();
    }

    public void setDateRangePickerInterface(DateRangePickerInterface dateRangePickerInterface)
    {
        this.dateRangePickerInterface = dateRangePickerInterface;
    }

    public interface DateRangePickerInterface
    {
        void onSelectedDates(Date startDate, Date endDate);
    }
}
