package com.chipred.produccionspx.utils;

import android.util.Log;

import com.chipred.produccionspx.Constants;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static final long YEAR_IN_MILLIS = 31556900000L;

    public static Date getFirstDateOfTheWeek() {
        Calendar calendar = Calendar.getInstance();
        int currentYearWeek = calendar.get(Calendar.WEEK_OF_YEAR);
        int firstDay = calendar.getActualMinimum(Calendar.DAY_OF_WEEK);

        // Obtener fechas de inicio y final
        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.set(Calendar.WEEK_OF_YEAR, currentYearWeek);
        startDateCalendar.set(Calendar.DAY_OF_WEEK, firstDay);
        startDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startDateCalendar.set(Calendar.MINUTE, 0);
        startDateCalendar.set(Calendar.SECOND, 0);

        return startDateCalendar.getTime();
    }

    public static Date getLastDateOfTheWeek() {
        Calendar calendar = Calendar.getInstance();
        int currentYearWeek = calendar.get(Calendar.WEEK_OF_YEAR);
        int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_WEEK);

        // Obtener fechas de inicio y final
        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.set(Calendar.WEEK_OF_YEAR, currentYearWeek);
        endDateCalendar.set(Calendar.DAY_OF_WEEK, lastDay);
        endDateCalendar.set(Calendar.HOUR_OF_DAY, 23);
        endDateCalendar.set(Calendar.MINUTE, 59);
        endDateCalendar.set(Calendar.SECOND, 59);

        return endDateCalendar.getTime();
    }

    public static Date getFirstDateOfTheMonth() {
        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        int firstDay = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);

        // Obtener fechas de inicio y final
        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.set(Calendar.MONTH, currentMonth);
        startDateCalendar.set(Calendar.DAY_OF_MONTH, firstDay);
        startDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startDateCalendar.set(Calendar.MINUTE, 0);
        startDateCalendar.set(Calendar.SECOND, 0);

        return startDateCalendar.getTime();
    }

    public static Date getLastDateOfTheMonth() {
        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        // Obtener fechas de inicio y final
        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.set(Calendar.MONTH, currentMonth);
        endDateCalendar.set(Calendar.DAY_OF_MONTH, lastDay);
        endDateCalendar.set(Calendar.HOUR_OF_DAY, 23);
        endDateCalendar.set(Calendar.MINUTE, 59);
        endDateCalendar.set(Calendar.SECOND, 59);

        return endDateCalendar.getTime();
    }

    public static Date substractMonths(Date date, int amount) {
        // Convertir a calendario
        Calendar referenceCal = Calendar.getInstance();
        referenceCal.setTime(date);

        // Restar cantidad de meses
        referenceCal.add(Calendar.MONTH, amount * -1);

        // Retornar fecha
        return referenceCal.getTime();
    }

    /**
     * @param day   - número de día (1 - 31)
     * @param month - número de mes (1 - 12)
     * @param year  - número de año (1 - 30)
     * @return objeto de fecha
     */
    public static Date dateNumbersToDate(String day, String month, String year) {
        Date convertedDate = null;

        if (!day.isEmpty() && !month.isEmpty() && !year.isEmpty()) {
            try {
                final int realYear = Integer.parseInt(year) + 2000;

                // Obtener fechas de inicio y final
                final Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MONTH, Integer.parseInt(month) - 1);
                calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
                calendar.set(Calendar.YEAR, realYear);
                calendar.set(Calendar.HOUR_OF_DAY, 12);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                convertedDate = calendar.getTime();

            } catch (Exception e) {
                Log.e("ParseDate", "Error al convertir a fecha");
            }
        }

        return convertedDate;
    }

    public static String[] dateToNumbers(Date date) {
        String[] numbers = new String[3];

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        final String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        final String year = String.valueOf(calendar.get(Calendar.YEAR) - 2000);
        int monthVal = calendar.get(Calendar.MONTH) + 1;
        final String month = Constants.monthNumberToString(monthVal);

        numbers[0] = day;
        numbers[1] = month;
        numbers[2] = year;

        return numbers;
    }
}
