package com.chipred.produccionspx.signs;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.R;
import com.chipred.produccionspx.adapters.UserSignAdapter;

import java.text.SimpleDateFormat;

public class SignDialogFragment extends DialogFragment
{
    //Constantes
    private static final String TAG = SimpleDateFormat.class.getSimpleName();
    private static final String SIGN_TYPE = "tipoFirma";
    public static final String PROJECT_SIGN = "firmaProyecto";
    public static final String VERSION_SIGN = "firmaVersion";

    private String orderNumber;
    private String projectName;
    private String signType;
    private String versionId;

    //Views
    private Button signButton;
    private Button returnButton;
    private ConstraintLayout content;
    private ProgressBar progressBar;
    private RecyclerView signsList;
    private TextView noSigns;

    //Objetos
    private Activity parentActivity;
    private SignsManager signsManager;


    public SignDialogFragment()
    {
    }

    public void setData(String orderNumber, String projectName)
    {
        this.orderNumber = orderNumber;
        this.projectName = projectName;
        this.signType = PROJECT_SIGN;
    }

    public void setData(String versionId)
    {
        this.versionId = versionId;
        this.signType = VERSION_SIGN;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.sign_dialog_fragment, null);
        content = view.findViewById(R.id.content_layout);
        progressBar = view.findViewById(R.id.progress_bar);
        noSigns = view.findViewById(R.id.no_signs_text);

        signsList = view.findViewById(R.id.user_signs_list);
        signButton = view.findViewById(R.id.sign_button);
        returnButton = view.findViewById(R.id.return_button);

        returnButton.setOnClickListener(v -> getDialog().dismiss());
        signButton.setOnClickListener(v -> signsManager.sign());

        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        parentActivity = getActivity();

        // Crear manejador de firmas
        initSignManager();
    }

    private void initSignManager()
    {
        // Crear listener
        final SignsManager.SignsManagerInterface listener = new SignsManager.SignsManagerInterface()
        {
            @Override
            public void updateAdapter(UserSignAdapter userSignAdapter)
            {
                signsList.setAdapter(userSignAdapter);
                signsList.setLayoutManager(new LinearLayoutManager(parentActivity));
            }

            @Override
            public void letSignListener(boolean letSign)
            {
                changeSignButtonState(letSign);
            }

            @Override
            public void onSignsFetched(boolean emptyList)
            {
                setUserDataList(emptyList);
            }

            @Override
            public void onSignedResult(boolean success, String message)
            {
                Toast.makeText(parentActivity, message, Toast.LENGTH_SHORT).show();
            }
        };

        // Inicializar instancia
        if (signType.equals(PROJECT_SIGN))
        {
            signsManager = new SignsManager(parentActivity, orderNumber, projectName, listener);
        }
        else
        {
            signsManager = new SignsManager(parentActivity, versionId, listener);
        }

        // Obtener firmas
        signsManager.fetchSignsList();
    }

    private void setUserDataList(boolean listEmpty)
    {
        if (listEmpty)
        {
            signsList.setVisibility(View.GONE);
            noSigns.setVisibility(View.VISIBLE);
        }
        else
        {
            noSigns.setVisibility(View.GONE);
            signsList.setVisibility(View.VISIBLE);
        }

        content.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void changeSignButtonState(boolean toSign)
    {
        signButton.setText(toSign ?
                parentActivity.getResources().getString(R.string.sign_text) :
                parentActivity.getResources().getString(R.string.signed_text));
        signButton.setAlpha(toSign ? 1f : 0.5f);
        signButton.setEnabled(toSign);
    }
}
