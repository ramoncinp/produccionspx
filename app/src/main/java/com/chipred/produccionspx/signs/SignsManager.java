package com.chipred.produccionspx.signs;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.chipred.produccionspx.adapters.UserSignAdapter;
import com.chipred.produccionspx.notifications.NotificationsManager;
import com.chipred.produccionspx.users.UserData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Map;

public class SignsManager
{
    private static final String TAG = SignsManager.class.getSimpleName();
    private static final String SIGN_TYPE = "tipoFirma";
    public static final String PROJECT_SIGN = "firmaProyecto";
    public static final String VERSION_SIGN = "firmaVersion";

    // Variables
    private String currentUserCharge;
    private String currentUserId;
    private String orderNumber;
    private String projectName;
    private String userName;
    private String signType;
    private String versionId;

    // Objetos
    private Activity activity;
    private ArrayList<String> userIds = new ArrayList<>();
    private ArrayList<UserData> userDataList = new ArrayList<>();
    private SignsSet signsSet;
    private UserSignAdapter userSignAdapter;

    // Interfaz
    private SignsManagerInterface signsManagerInterface;

    // Referencia de base de datos
    private final FirebaseFirestore firestoreInstance = FirebaseFirestore.getInstance();
    private final CollectionReference signsCollection;


    public SignsManager(Activity activity, String orderNumber, String projectName,
                        SignsManagerInterface signsManagerInterface)
    {
        // Obtener atributos
        this.activity = activity;
        this.orderNumber = orderNumber;
        this.projectName = projectName;
        this.signType = PROJECT_SIGN;
        this.signsManagerInterface = signsManagerInterface;

        // Inicializar
        signsCollection = firestoreInstance.collection("firmas");
        getUser();
    }

    public SignsManager(Activity activity, String versionId,
                        SignsManagerInterface signsManagerInterface)
    {
        // Obtener atributos
        this.activity = activity;
        this.versionId = versionId;
        this.signType = VERSION_SIGN;
        this.signsManagerInterface = signsManagerInterface;

        // Inicializar
        signsCollection = firestoreInstance.collection("firmas");
        getUser();
    }

    public void getUser()
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
        {
            currentUserId = user.getUid();
            userName = user.getDisplayName();

            //Obtener cargo actual del usuario
            getUserCharge();
        }
    }

    private void getUserCharge()
    {
        firestoreInstance.collection("usuarios").document(currentUserId).get().addOnCompleteListener(activity, task ->
        {
            if (task.isSuccessful() && task.getResult() != null)
            {
                //Obtener usuario
                UserData userData = task.getResult().toObject(UserData.class);

                //Validar resultado
                if (userData != null)
                {
                    currentUserCharge = userData.getCharge();
                    Log.d(TAG, "Cargo de usuario obtenido -> " + currentUserCharge);
                }
            }
            else
            {
                Log.e(TAG, "Error al obtener el cargo del usuario");
            }
        });
    }

    public void fetchSignsList()
    {
        // Evaluar tipo de firma para obtener set de firmas
        Query query;
        if (signType.equals(PROJECT_SIGN))
        {
            query = signsCollection.whereEqualTo("idProyecto", orderNumber);
        }
        else
        {
            query = signsCollection.whereEqualTo("idVersion", versionId);
        }

        // Ejecutar query
        query.addSnapshotListener((queryDocumentSnapshots, e) ->
        {
            userDataList.clear();

            if (e != null)
            {
                Log.e(TAG, "Error al obtener lista de firmas");
            }
            else
            {
                if (queryDocumentSnapshots != null && !queryDocumentSnapshots.getDocuments().isEmpty())
                {
                    for (DocumentSnapshot document :
                            queryDocumentSnapshots.getDocuments())
                    {
                        //Obtener primer elemento
                        signsSet = document.toObject(SignsSet.class);
                        signsSet.setId(document.getId());

                        //Preparar adaptador
                        userSignAdapter = new UserSignAdapter(userDataList, activity);
                        userSignAdapter.setRegisters(signsSet.getRegistros());
                        signsManagerInterface.updateAdapter(userSignAdapter);

                        //Si no esta el usuario actual en la lista, dejar firmar
                        signsManagerInterface.letSignListener(true);

                        //Buscar registros
                        for (Map<String, Object> map : signsSet.getRegistros())
                        {
                            //Obtener id de usuario
                            String userId = (String) map.get("usuario");

                            //Obtener cargo
                            String charge = (String) map.get("cargo");

                            UserData userData = new UserData();
                            userData.setId(userId);
                            userData.setCharge(charge);
                            userDataList.add(userData);

                            if (userId == null || userId.equals(currentUserId))
                            {
                                signsManagerInterface.letSignListener(false);
                            }

                            //Buscar cliente
                            searchUser(userId);
                        }

                        //Terminar ciclo
                        break;
                    }
                }
                else
                {
                    Log.e(TAG, "No hay firmas");
                }
            }

            // Reportar firmas obtenidas
            signsManagerInterface.onSignsFetched(userDataList.isEmpty());
        });
    }

    private void searchUser(String userId)
    {
        if (!userIds.contains(userId))
        {
            CollectionReference users = FirebaseFirestore.getInstance().collection("usuarios");
            users.document(userId).get().addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    DocumentSnapshot snapshot = task.getResult();
                    if (snapshot == null) return;

                    for (UserData userData : userDataList)
                    {
                        String uid = snapshot.getString("id");
                        if (userData.getId().equals(uid))
                        {
                            userData.setName(snapshot.getString("name"));
                            userData.setPhotoUrl(snapshot.getString("photoUrl"));
                        }
                    }

                    //Actualizar elementos
                    userSignAdapter.notifyDataSetChanged();
                    signsManagerInterface.updateAdapter(userSignAdapter);
                }
                else
                {
                    Log.e(TAG, "Error al obtener usuario");
                }
            });
        }
    }

    public void sign()
    {
        final String successMessage = "Firmado correctamente";
        final String errorMessage = "Error al generar firma";

        // Firmar
        if (signsSet == null)
        {
            //No existe el set aun, crearlo
            signsSet = new SignsSet();
            signsSet.setIdProyecto(orderNumber);
            signsSet.setType(signType);
            signsSet.setIdVersion(versionId);

            //Agregar info de usuario
            signsSet.addUser(currentUserId, currentUserCharge);
            signsCollection.add(signsSet).addOnCompleteListener(task ->
            {
                boolean success = task.isSuccessful();
                signsManagerInterface.onSignedResult(
                        success,
                        success ? successMessage : errorMessage);

                //Enviar notificacion
                if (success && signType.equals(PROJECT_SIGN)) sendNotification();
            });
        }
        else
        {
            //Agregar info de usuario
            signsSet.addUser(currentUserId, currentUserCharge);

            //Si existe el set, modificar solamente
            //Actualizar documento
            signsCollection.document(signsSet.getId()).update(signsSet.toMap()).addOnCompleteListener(task ->
            {
                boolean success = task.isSuccessful();
                signsManagerInterface.onSignedResult(
                        success,
                        success ? successMessage : errorMessage);

                //Enviar notificacion
                if (success && signType.equals(PROJECT_SIGN)) sendNotification();
            });
        }
    }

    private void sendNotification()
    {
        NotificationsManager notificationsManager = new NotificationsManager(
                new NotificationsManager.OnMessageReceived()
                {
                    @Override
                    public void onSuccess(String message)
                    {
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(String errorMsg)
                    {
                        Toast.makeText(activity, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                });

        notificationsManager.onNewSign(userName, projectName, orderNumber);
    }

    public interface SignsManagerInterface
    {
        void updateAdapter(UserSignAdapter userSignAdapter);

        void letSignListener(boolean letSign);

        void onSignsFetched(boolean emptyList);

        void onSignedResult(boolean success, String message);
    }
}
