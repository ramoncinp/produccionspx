package com.chipred.produccionspx.signs;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SignsSet
{
    private String id;
    private String idProyecto;
    private String idVersion;
    private String type;
    private ArrayList<Map<String, Object>> registros;

    public SignsSet()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getIdVersion()
    {
        return idVersion;
    }

    public void setIdVersion(String idVersion)
    {
        this.idVersion = idVersion;
    }

    public String getIdProyecto()
    {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto)
    {
        this.idProyecto = idProyecto;
    }

    public ArrayList<Map<String, Object>> getRegistros()
    {
        return registros;
    }

    public void setRegistros(ArrayList<Map<String, Object>> registros)
    {
        this.registros = registros;
    }

    public void addUser(String userId, String charge)
    {
        if (registros == null)
        {
            registros = new ArrayList<>();
        }

        //Checar que no exista el usuario
        for (Map<String, Object> maps : registros)
        {
            if (maps.get("usuario").equals(userId)) return;
        }

        //Obtener timestamp
        long timeStamp = new Date().getTime();

        //Crear nuevo map
        Map<String, Object> map = new HashMap<>();
        map.put("usuario", userId);
        map.put("cargo", charge);
        map.put("epoch", timeStamp);

        registros.add(map);
    }

    public void removeUser(String userId)
    {
        if (registros == null)
        {
            return;
        }
        else
        {
            //Crear variable de indice
            int idxToRemove = -1;

            //Checar que no exista el usuario
            for (int i = 0; i < registros.size(); i++)
            {
                Map<String, Object> map = registros.get(i);
                if (map.get("usuario").equals(userId))
                {
                    idxToRemove = i;
                    break;
                }
            }

            if (idxToRemove != -1) registros.remove(idxToRemove);
        }
    }

    public Map<String, Object> toMap()
    {
        Map<String, Object> map = new HashMap<>();

        map.put("id", id);
        map.put("idProyecto", idProyecto);
        map.put("registros", registros);

        return map;
    }
}
