package com.chipred.produccionspx;

import androidx.annotation.NonNull;

public class Vehicle
{
    private String id;
    private String desc;

    public Vehicle(String id, String desc)
    {
        this.id = id;
        this.desc = desc;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getDesc()
    {
        return desc;
    }

    public void setDesc(String desc)
    {
        this.desc = desc;
    }

    @NonNull
    @Override
    public String toString()
    {
        return desc;
    }
}
