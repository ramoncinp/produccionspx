package com.chipred.produccionspx.versions;

public class VersionElement
{
    public static final String AK1 = "AK1";
    public static final String ESP32 = "ESP32";
    public static final String ESP8266 = "ESP8266";
    public static final String FLEXIS = "Flexis";
    public static final String KINETIS = "Kinetis";
    public static final String QG8 = "QG8";

    private String key = "";
    private String description = "";
    private String check_sum = "";
    private String version = "";
    private String ip;
    private String mc;

    public VersionElement()
    {
    }

    public VersionElement(String description)
    {
        this.description = description;
    }

    public VersionElement(String description, String mc)
    {
        this.description = description;
        this.mc = mc;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getMc()
    {
        return mc;
    }

    public void setMc(String mc)
    {
        this.mc = mc;
    }

    public String getCheck_sum()
    {
        return check_sum;
    }

    public void setCheck_sum(String check_sum)
    {
        this.check_sum = check_sum;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }
}
