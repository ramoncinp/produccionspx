package com.chipred.produccionspx.versions;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Collections;

public class EnsambleAdapter extends RecyclerView.Adapter<EnsambleAdapter.EnsambleViewHolder>
{
    private ArrayList<VersionAccesoryEnsamble> ensambles;
    private Context context;
    private EnsambleAdapterInterface ensambleAdapterInterface;

    public EnsambleAdapter(Context context, ArrayList<VersionAccesoryEnsamble> ensambles)
    {
        this.context = context;
        this.ensambles = ensambles;
    }

    public void setEnsambleAdapterInterface(EnsambleAdapterInterface ensambleAdapterInterface)
    {
        this.ensambleAdapterInterface = ensambleAdapterInterface;
    }

    @NonNull
    @Override
    public EnsambleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ensamble_type_layout,
                null);

        return new EnsambleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EnsambleViewHolder viewHolder, int i)
    {
        // Obtener lista de versiones
        VersionAccesoryEnsamble ensamble = ensambles.get(i);

        // Ordenar lista
        Collections.sort(ensamble.getAccesories(), (versionAccesory, t1) ->
        {
            // Tratar de convertir nombre de version a número
            try
            {
                double v1 = Double.parseDouble(versionAccesory.getNombre());
                double v2 = Double.parseDouble(t1.getNombre());

                return Double.compare(v2, v1);
            }
            catch (Exception e)
            {
                Log.e("Conversion", "Error al convertir nombre de version a double");
            }

            return 0;
        });

        // Crear adaptador
        ArrayList<Object> versions = new ArrayList<>(ensamble.getAccesories());
        VersionsAdapter adapter = new VersionsAdapter(versions);
        adapter.setVersionType(VersionsAdapter.ACCESORY_VERSION);
        adapter.setListener(view ->
        {
            // Obtener versión
            VersionAccesory versionAccesory =
                    (VersionAccesory) versions.get(viewHolder.versionsList.getChildAdapterPosition(view));

            if (ensambleAdapterInterface != null)
            {
                ensambleAdapterInterface.onChildSelected(versionAccesory);
            }
        });

        // Asignar adaptador
        viewHolder.versionsList.setAdapter(adapter);
        viewHolder.versionsList.setLayoutManager(new LinearLayoutManager(context));

        // Asignar titulo
        viewHolder.name.setText(ensamble.getEnsamble());
    }

    @Override
    public int getItemCount()
    {
        return ensambles.size();
    }

    static class EnsambleViewHolder extends RecyclerView.ViewHolder
    {
        private RecyclerView versionsList;
        private TextView name;

        EnsambleViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.ensamble_type_name);
            versionsList = itemView.findViewById(R.id.versions_list);
        }
    }

    public interface EnsambleAdapterInterface
    {
        void onChildSelected(VersionAccesory versionAccesory);
    }
}
