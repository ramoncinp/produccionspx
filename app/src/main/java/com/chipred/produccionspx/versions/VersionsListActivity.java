package com.chipred.produccionspx.versions;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class VersionsListActivity extends AppCompatActivity
{
    public final static String MODEL_VERSIONS = "modelo";
    public final static String CAN_WRITE = "canWrite";
    public final static String EVO_5_VERSIONS = "Versiones EVO5";
    public final static String EVO_6_VERSIONS = "Versiones EVO6";
    public final static String EVO_5_COLLECTION = "versiones_evo5";
    public final static String EVO_6_COLLECTION = "versiones_evo6";
    public final static String ACCESORIES_VERSIONS_COLLECTION = "versiones_accesorios";

    // Variables
    private boolean canWrite = false;
    private String title;
    private String collectionName;
    private String accesory;

    // Objetos
    private ArrayList<Object> versions = new ArrayList<>();
    private CollectionReference versionsCollection;
    private HashMap<String, ArrayList<VersionAccesory>> ensambles = new HashMap<>();

    // Views
    private FloatingActionButton addVersionFab;
    private ProgressBar progressBar;
    private RecyclerView versionsList;
    private TextView noVersions;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_versions_list);

        // Agregar flecha para regresar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Obtener parámetros
        canWrite = getIntent().hasExtra(CAN_WRITE) && getIntent().getBooleanExtra(CAN_WRITE, false);

        // Obtener título
        title = getIntent().getStringExtra(MODEL_VERSIONS);

        // Obtener título de la actividad
        if (title == null)
        {
            Toast.makeText(this, "Error al obtener modelo", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        // Definir nombre de colección
        if (title.equals(EVO_5_VERSIONS))
        {
            collectionName = EVO_5_COLLECTION;
        }
        else if (title.equals(EVO_6_VERSIONS))
        {
            collectionName = EVO_6_COLLECTION;
        }
        else if (title.equals(VersionAccesory.BRICK_ACCESORY))
        {
            accesory = title;
            title = "Versiones Brick";
            collectionName = ACCESORIES_VERSIONS_COLLECTION;
        }
        else if (title.equals(VersionAccesory.LECTOR32_ACCESORY))
        {
            accesory = title;
            title = "Versiones PCA Lector32";
            collectionName = ACCESORIES_VERSIONS_COLLECTION;
        }
        else if (title.equals(VersionAccesory.RETORNOS_ACCESORY))
        {
            accesory = title;
            title = "Versiones PCA MK Retornos";
            collectionName = ACCESORIES_VERSIONS_COLLECTION;
        }

        // Escribir título
        setTitle(title);

        // Inicializar views
        initViews();

        // Evaluar permiso para editar
        if (canWrite)
        {
            addVersionFab.show();
        }

        // Obtener referencia de collecion correspondiente
        versionsCollection = FirebaseFirestore.getInstance().collection(collectionName);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        getVersions();
    }

    private void initViews()
    {
        progressBar = findViewById(R.id.progress_bar);
        versionsList = findViewById(R.id.versions_list);
        addVersionFab = findViewById(R.id.add_version_button);
        noVersions = findViewById(R.id.no_versions);
        addVersionFab.setOnClickListener(view ->
        {
            if (accesory == null)
            {
                Intent intent = new Intent(VersionsListActivity.this, VersionForm.class);
                intent.putExtra(MODEL_VERSIONS, title);
                intent.putExtra(CAN_WRITE, canWrite);
                startActivity(intent);
            }
            else
            {
                showVersionAccesoryDialog(null);
            }
        });
    }

    private void getVersions()
    {
        final Query query;

        // Definir query
        if (accesory != null)
        {
            query = versionsCollection.whereEqualTo("accesory", accesory);
        }
        else
        {
            query = versionsCollection.orderBy("nombre", Query.Direction.DESCENDING);
        }

        // Ejecutar query
        query.addSnapshotListener(this, (queryDocumentSnapshots, e) ->
        {
            if (e != null || queryDocumentSnapshots == null)
            {
                Toast.makeText(VersionsListActivity.this, "Error al obtener lista de versiones",
                        Toast.LENGTH_LONG).show();

                noVersions.setVisibility(View.VISIBLE);
            }
            else
            {
                // Limpiar lista
                versions.clear();
                ensambles.clear();

                // Obtener todos los documentos
                for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments())
                {
                    try
                    {
                        // Obtener datos de version
                        String id = snapshot.getId();
                        String name = snapshot.getString("nombre");
                        String releaseDate = snapshot.getString("fechaLiberacion");

                        // Crear version
                        final Object versionToAdd;
                        if (accesory == null)
                        {
                            Version version = new Version(name, releaseDate);
                            version.setId(id);
                            versionToAdd = version;
                        }
                        else
                        {
                            VersionAccesory version = new VersionAccesory();
                            version.setNombre(name);
                            version.setFechaLiberacion(releaseDate);
                            version.setAccesory(accesory);
                            version.setId(id);
                            version.setChecksumMd5(snapshot.getString("checksumMd5"));
                            version.setEnsamble(snapshot.contains("ensamble") ?
                                    snapshot.getString("ensamble") :
                                    "");
                            versionToAdd = version;

                            // Agregar a mapa de ensambles
                            addVersionAccesoryToMap(version);
                        }

                        // Agregar version
                        versions.add(versionToAdd);
                    }
                    catch (Exception npe)
                    {
                        Toast.makeText(VersionsListActivity.this, "Error al procesar " +
                                "información de version", Toast.LENGTH_LONG).show();
                        npe.printStackTrace();
                    }
                }

                // Validar lista
                if (!versions.isEmpty())
                {
                    if (accesory == null)
                    {
                        // Ordenar por nombre de version
                        Collections.sort(versions, (o, t1) ->
                        {
                            Version v1 = (Version) o;
                            Version v2 = (Version) t1;

                            // Tratar de convertir nombres de versiones
                            try
                            {
                                double v1Val = Double.parseDouble(v1.getNombre());
                                double v2Val = Double.parseDouble(v2.getNombre());

                                return Double.compare(v2Val, v1Val);
                            }
                            catch (Exception e1)
                            {
                                Log.e("Conversion", "Error al convertir nombre de versiones " +
                                        "EVO");
                            }

                            return 0;
                        });

                        // Crear adaptador
                        VersionsAdapter adapter = new VersionsAdapter(versions);
                        adapter.setVersionType(accesory == null ? VersionsAdapter.EVO_VERSION :
                                VersionsAdapter.ACCESORY_VERSION);
                        adapter.setListener(view ->
                        {
                            // Obtener version
                            Version version =
                                    (Version) versions.get(versionsList.getChildAdapterPosition(view));

                            Intent intent = new Intent(VersionsListActivity.this,
                                    VersionForm.class);
                            intent.putExtra(MODEL_VERSIONS, title);
                            intent.putExtra(VersionForm.ID_VERSION, version.getId());
                            intent.putExtra(CAN_WRITE, canWrite);

                            // Visualizar formulario
                            startActivity(intent);
                        });

                        // Asignar adaptador
                        versionsList.setAdapter(adapter);
                    }
                    else
                    {
                        // Convertir HashMap a lista
                        ArrayList<VersionAccesoryEnsamble> ensamblesList = new ArrayList<>();

                        // Iterar en hashMap
                        for (Map.Entry<String, ArrayList<VersionAccesory>> entry :
                                ensambles.entrySet())
                        {
                            // Crear ensamble
                            VersionAccesoryEnsamble ensamble = new VersionAccesoryEnsamble();
                            ensamble.setEnsamble(entry.getKey());
                            ensamble.setAccesories(entry.getValue());

                            // Agregar ensamble
                            ensamblesList.add(ensamble);
                        }

                        // Ordenar lista
                        Collections.sort(ensamblesList,
                                (versionAccesoryEnsamble, t1) -> versionAccesoryEnsamble.getEnsamble().compareTo(t1.getEnsamble()));

                        // Crear adaptador
                        EnsambleAdapter ensambleAdapter = new EnsambleAdapter(this, ensamblesList);
                        ensambleAdapter.setEnsambleAdapterInterface(this::showVersionAccesoryDialog);
                        versionsList.setAdapter(ensambleAdapter);
                    }

                    versionsList.setLayoutManager(new LinearLayoutManager(VersionsListActivity.this));
                    versionsList.setVisibility(View.VISIBLE);
                }
            }

            progressBar.setVisibility(View.GONE);
        });
    }

    private void addVersionAccesoryToMap(VersionAccesory accesory)
    {
        // Obtener tipo de ensamble
        String ensamble = accesory.getEnsamble();

        // Evaluar si ya existe en el mapa
        if (!ensambles.containsKey(ensamble))
        {
            // Si no existe, crear nueva lista y agregarla al mapa
            ArrayList<VersionAccesory> ensamblesList = new ArrayList<>();
            ensamblesList.add(accesory);
            ensambles.put(ensamble, ensamblesList);
        }
        else
        {
            // Si ya existe la lista correspondiente, agregar elemento
            Objects.requireNonNull(ensambles.get(ensamble)).add(accesory);
        }
    }

    private void showVersionAccesoryDialog(VersionAccesory versionAccesory)
    {
        final VersionAccesoryFragment versionAccesoryFragment = new VersionAccesoryFragment();
        versionAccesoryFragment.setAccesoryType(accesory);
        versionAccesoryFragment.setCanWrite(canWrite);
        versionAccesoryFragment.setCollectionReference(versionsCollection);
        versionAccesoryFragment.setVersionAccesory(versionAccesory);
        versionAccesoryFragment.show(getSupportFragmentManager(), "version_dialog");
    }
}
