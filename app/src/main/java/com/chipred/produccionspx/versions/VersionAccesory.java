package com.chipred.produccionspx.versions;

import java.util.HashMap;
import java.util.Map;

public class VersionAccesory extends Version
{
    // Constantes
    public static final String BRICK_ACCESORY = "Brick";
    public static final String LECTOR32_ACCESORY = "PCA Lector 32";
    public static final String RETORNOS_ACCESORY = "PCA MK Retornos";

    // Variables
    private String accesory;
    private String checksumMd5;
    private String ensamble; // Ensamble

    public VersionAccesory()
    {
    }

    public String getChecksumMd5()
    {
        return checksumMd5;
    }

    public void setChecksumMd5(String checksumMd5)
    {
        this.checksumMd5 = checksumMd5;
    }

    public String getAccesory()
    {
        return accesory;
    }

    public void setAccesory(String accesory)
    {
        this.accesory = accesory;
    }

    public String getEnsamble()
    {
        return ensamble;
    }

    public void setEnsamble(String ensamble)
    {
        this.ensamble = ensamble;
    }

    public Map<String, Object> toMap()
    {
        Map<String, Object> map = new HashMap<>();
        map.put("accesory", accesory);
        map.put("checksumMd5", checksumMd5);
        map.put("fechaLiberacion", getFechaLiberacion());
        map.put("id", getId());
        map.put("nombre", getNombre());
        map.put("ensamble", ensamble);

        return map;
    }
}
