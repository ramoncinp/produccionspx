package com.chipred.produccionspx.versions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class VersionsAdapter extends RecyclerView.Adapter<VersionsAdapter.VersionViewHolder> implements View.OnClickListener
{
    // Constantes
    public static final int EVO_VERSION = 1;
    public static final int ACCESORY_VERSION = 2;

    // Variables
    private int versionType = EVO_VERSION;

    // Objetos
    private ArrayList<Object> versions;
    private View.OnClickListener listener;

    VersionsAdapter(ArrayList<Object> versions)
    {
        this.versions = versions;
    }

    public void setVersionType(int versionType)
    {
        this.versionType = versionType;
    }

    @Override
    @NonNull
    public VersionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.version_layout,
                parent, false);
        v.setOnClickListener(listener);

        return new VersionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VersionViewHolder holder, int position)
    {
        if (versionType == EVO_VERSION)
        {
            // Obtener version
            Version version = (Version) (versions.get(position));

            // Mostrar datos
            holder.name.setText(String.format("V%s", version.getNombre()));
            holder.date.setText(version.getFechaLiberacion());
            holder.icon.setImageResource(R.drawable.ic_binarium_vector);
            holder.md5.setVisibility(View.GONE);
        }
        else
        {
            // Obtener version
            VersionAccesory versionAccesory = (VersionAccesory) (versions.get(position));

            // Mostrar datos
            holder.name.setText(String.format("V%s", versionAccesory.getNombre()));
            holder.date.setText(versionAccesory.getFechaLiberacion());
            holder.icon.setImageResource(R.drawable.ic_binarium_vector);
            holder.md5.setVisibility(View.VISIBLE);
            holder.md5.setText(versionAccesory.getChecksumMd5());
        }

        if (position == 0) holder.lastVersionIcon.setVisibility(View.VISIBLE);
        else holder.lastVersionIcon.setVisibility(View.GONE);
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount()
    {
        return versions.size();
    }

    static class VersionViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView icon;
        private ImageView lastVersionIcon;
        private TextView name;
        private TextView date;
        private TextView md5;

        VersionViewHolder(View itemView)
        {
            super(itemView);

            name = itemView.findViewById(R.id.version_name);
            date = itemView.findViewById(R.id.version_date);
            icon = itemView.findViewById(R.id.version_element_icon);
            md5 = itemView.findViewById(R.id.version_md5);
            lastVersionIcon = itemView.findViewById(R.id.last_version_icon);
        }
    }
}
