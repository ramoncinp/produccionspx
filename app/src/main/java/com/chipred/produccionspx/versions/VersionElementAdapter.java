package com.chipred.produccionspx.versions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class VersionElementAdapter extends RecyclerView.Adapter<VersionElementAdapter.VersionViewHolder> implements View.OnClickListener, View.OnLongClickListener
{
    private ArrayList<VersionElement> versionElements;
    private View.OnClickListener listener;
    private View.OnLongClickListener longClickListener;

    VersionElementAdapter(ArrayList<VersionElement> versionElements)
    {
        this.versionElements = versionElements;
    }

    @Override
    @NonNull
    public VersionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.version_element_layout,
                parent, false);
        v.setOnClickListener(listener);
        v.setOnLongClickListener(longClickListener);

        return new VersionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VersionViewHolder holder, int position)
    {
        // Obtener version
        VersionElement versionElement = versionElements.get(position);

        // Mostrar datos
        holder.desc.setText(versionElement.getDescription());
        holder.mc.setText(versionElement.getMc());
        holder.version.setText(versionElement.getVersion());

        // Obtener checksum
        String checksumVal = versionElement.getCheck_sum();
        if (checksumVal.length() > 8)
        {
            checksumVal =
                    checksumVal.substring(0, 4) + ".." + checksumVal.substring(checksumVal.length() - 4);
            holder.checksum.setText(checksumVal);
        }
        else
        {
            holder.checksum.setText(versionElement.getCheck_sum());
        }

        // Validar si tiene definido microcontrolador
        if (versionElement.getMc() == null)
        {
            holder.separator1.setVisibility(View.GONE);
            holder.mc.setVisibility(View.GONE);

            // Definir nuevo weight para descripcion
            LinearLayout.LayoutParams lp =
                    new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.weight = 5.5f;
            holder.desc.setLayoutParams(lp);
        }
        else
        {
            holder.separator1.setVisibility(View.VISIBLE);
            holder.mc.setVisibility(View.VISIBLE);

            // Definir nuevo weight para descripcion
            LinearLayout.LayoutParams lp =
                    new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.weight = 4.2f;
            holder.desc.setLayoutParams(lp);
        }

        // Eliminar línea de base para el último elemento
        if (position == (versionElements.size() - 1))
        {
            holder.separatorBottom.setVisibility(View.GONE);
        }
        else
        {
            holder.separatorBottom.setVisibility(View.VISIBLE);
        }

        if (versionElement.getIp() != null && !versionElement.getIp().isEmpty())
        {
            holder.desc.append(" " + versionElement.getIp());
        }
        else
        {
            holder.desc.setText(versionElement.getDescription());
        }
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    public void setLongClickListener(View.OnLongClickListener longClickListener)
    {
        this.longClickListener = longClickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if (longClickListener != null)
        {
            longClickListener.onLongClick(view);
        }
        return false;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount()
    {
        return versionElements.size();
    }

    static class VersionViewHolder extends RecyclerView.ViewHolder
    {
        private TextView desc;
        private TextView mc;
        private TextView version;
        private TextView checksum;
        private View separator1;
        private View separatorBottom;

        VersionViewHolder(View itemView)
        {
            super(itemView);

            desc = itemView.findViewById(R.id.desc_tv);
            mc = itemView.findViewById(R.id.mc_tv);
            version = itemView.findViewById(R.id.version_tv);
            checksum = itemView.findViewById(R.id.checksum_tv);
            separator1 = itemView.findViewById(R.id.separator_1);
            separatorBottom = itemView.findViewById(R.id.separator_4);
        }
    }
}
