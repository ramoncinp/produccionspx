package com.chipred.produccionspx.versions;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.adapters.UserSignAdapter;
import com.chipred.produccionspx.signs.SignsManager;
import com.google.firebase.firestore.CollectionReference;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;


public class VersionAccesoryFragment extends DialogFragment
{
    // Constantes
    private static final String TAG = VersionAccesory.class.getSimpleName();

    // Variables
    private boolean canWrite = false;
    private String accesoryType;

    // Views
    private Button save;
    private Button cancel;
    private LinearLayout contentLayout;
    private MaterialEditText dayEt;
    private MaterialEditText monthEt;
    private MaterialEditText yearEt;
    private MaterialEditText accesoryEditText;
    private MaterialEditText name;
    private MaterialEditText checksumEt;
    private MaterialEditText revisionEt;
    private ProgressBar progressBar;
    private RecyclerView signsList;
    private TextView dialogTitle;

    // Objetos
    private Activity activity;
    private CollectionReference collectionReference;
    private SignsManager signsManager;
    private VersionAccesory versionAccesory;
    private View content;


    public VersionAccesoryFragment()
    {
        // Required empty public constructor
    }

    public void setCanWrite(boolean canWrite)
    {
        this.canWrite = canWrite;
    }

    public void setVersionAccesory(VersionAccesory versionAccesory)
    {
        this.versionAccesory = versionAccesory;
    }

    public void setAccesoryType(String accesoryType)
    {
        this.accesoryType = accesoryType;
    }

    public void setCollectionReference(CollectionReference collectionReference)
    {
        this.collectionReference = collectionReference;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        content = inflater.inflate(R.layout.fragment_accesory_version, container, false);
        contentLayout = content.findViewById(R.id.content_layout);
        progressBar = content.findViewById(R.id.progress_bar);
        dialogTitle = content.findViewById(R.id.dialog_title);
        save = content.findViewById(R.id.save_button);
        cancel = content.findViewById(R.id.cancel_button);
        accesoryEditText = content.findViewById(R.id.accesory_type_et);
        dayEt = content.findViewById(R.id.version_day);
        monthEt = content.findViewById(R.id.version_month);
        yearEt = content.findViewById(R.id.version_year);
        name = content.findViewById(R.id.version_name_et);
        checksumEt = content.findViewById(R.id.checksum_et);
        signsList = content.findViewById(R.id.signs_list);
        revisionEt = content.findViewById(R.id.version_revision);

        // Definir tipo de accesorio
        accesoryEditText.setText(accesoryType);

        // Inicializar views
        initViews();

        return content;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        if (context instanceof Activity)
        {
            activity = (Activity) context;
        }
    }

    private void initViews()
    {
        // Evaluar si viene ya un id de versión
        String title = "Versión";
        if (versionAccesory == null)
        {
            contentLayout.setVisibility(View.VISIBLE);
            title = "Nueva versión";
        }
        else
        {
            setVersionData();
            showSignsLayout();
        }

        // Asignar filtros a editText de ensamble
        revisionEt.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        // Escribir título
        dialogTitle.setText(title);

        // Definir listeners de botones
        save.setOnClickListener(view ->
        {
            VersionAccesory versionToAdd = buildVersion();
            if (versionToAdd != null)
            {
                uploadVersion(versionToAdd);
                closeDialog();
            }
        });

        save.setEnabled(canWrite);
        save.setAlpha(canWrite ? 1.0f : 0.5f);

        cancel.setOnClickListener(view -> closeDialog());
    }

    private void setVersionData()
    {
        name.setText(versionAccesory.getNombre());
        checksumEt.setText(versionAccesory.getChecksumMd5());

        // Desarmar fecha
        String completeDate = versionAccesory.getFechaLiberacion();
        String monthText = completeDate.substring(0, 3);
        String day = completeDate.substring(3, 5);
        String year = completeDate.substring(5, 7);

        monthEt.setText(monthText);
        dayEt.setText(day);
        yearEt.setText(year);
        revisionEt.setText(versionAccesory.getEnsamble());
    }

    private VersionAccesory buildVersion()
    {
        VersionAccesory versionAccesory = null;

        boolean isValid = name.validateWith(new METValidator("Nombre no válido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return text.length() == 3 && text.charAt(1) == '.';
            }
        });

        // Validar día
        isValid &= dayEt.validateWith(new METValidator("Dos dígitos obligatorios")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return text.length() == 2 && Constants.isValidDay(text.toString());
            }
        });

        // Validar mes
        isValid &= monthEt.validateWith(new METValidator("Mes no válido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return Constants.isValidMonth(text.toString());
            }
        });

        // Validar views
        METValidator emptyValidator = new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        };

        isValid &= accesoryEditText.validateWith(emptyValidator);
        isValid &= checksumEt.validateWith(emptyValidator);
        isValid &= yearEt.validateWith(emptyValidator);

        if (isValid)
        {
            versionAccesory = (this.versionAccesory == null) ? new VersionAccesory() :
                    this.versionAccesory;
            versionAccesory.setNombre(name.getText().toString());
            versionAccesory.setAccesory(accesoryEditText.getText().toString());
            versionAccesory.setChecksumMd5(checksumEt.getText().toString());
            versionAccesory.setEnsamble(revisionEt.getText().toString());

            String day = dayEt.getText().toString();
            String month = monthEt.getText().toString();
            String year = yearEt.getText().toString();
            String releaseDate = month + day + year;
            versionAccesory.setFechaLiberacion(releaseDate);
        }

        return versionAccesory;
    }

    private void uploadVersion(VersionAccesory versionAccesory)
    {
        String processMessage;
        if (versionAccesory.getId() == null)
        {
            processMessage = "Registrando versión";
            collectionReference.add(versionAccesory).addOnCompleteListener(task ->
            {
                String resultMessage;
                if (task.isSuccessful())
                {
                    resultMessage = "Versión registrada correctamente";
                    Log.v(TAG, resultMessage);
                }
                else
                {
                    resultMessage = "Error al registrar versión";
                    Log.e(TAG, resultMessage);
                }

                Toast.makeText(activity, resultMessage, Toast.LENGTH_SHORT).show();
            });
        }
        else
        {
            processMessage = "Actualizando versión";
            collectionReference.document(versionAccesory.getId()).update(versionAccesory.toMap()).addOnCompleteListener(task ->
            {
                String resultMessage;
                if (task.isSuccessful())
                {
                    resultMessage = "Version actualizada correctamente";
                    Log.v(TAG, resultMessage);
                }
                else
                {
                    resultMessage = "Error al actualizar versión";
                    Log.e(TAG, resultMessage);
                }

                Toast.makeText(activity, resultMessage, Toast.LENGTH_SHORT).show();
            });
        }

        Toast.makeText(activity, processMessage, Toast.LENGTH_SHORT).show();
    }

    private void showSignsLayout()
    {
        // Obtener views
        LinearLayout signsLayout = content.findViewById(R.id.signs_layout);
        ProgressBar progressBar = content.findViewById(R.id.signs_list_progress_bar);
        Button signButton = content.findViewById(R.id.sign_button);
        TextView noSignsText = content.findViewById(R.id.no_signs_text);

        // Obtener manejador de firmas
        signsManager = new SignsManager(activity, versionAccesory.getId(),
                new SignsManager.SignsManagerInterface()
                {
                    @Override
                    public void letSignListener(boolean letSign)
                    {
                        signButton.setEnabled(letSign);
                        signButton.setAlpha(letSign ? 1.0f : 0.5f);
                    }

                    @Override
                    public void onSignsFetched(boolean emptyList)
                    {
                        signsList.setVisibility(emptyList ? View.INVISIBLE : View.VISIBLE);
                        noSignsText.setVisibility(emptyList ? View.VISIBLE : View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSignedResult(boolean success, String message)
                    {
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void updateAdapter(UserSignAdapter userSignAdapter)
                    {
                        signsList.setAdapter(userSignAdapter);
                    }
                });

        // Asignar adaptador
        signsList.setLayoutManager(new LinearLayoutManager(activity));

        // Asignar listener para firmar
        signButton.setOnClickListener(view -> signsManager.sign());

        // Obtener firmas
        signsManager.fetchSignsList();

        // Mostrar views
        signsLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    private void closeDialog()
    {
        if (getDialog() != null)
        {
            getDialog().dismiss();
        }
    }
}
