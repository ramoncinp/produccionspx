package com.chipred.produccionspx.versions;

import java.util.ArrayList;

/**
 * Un accesorio puede ser de un tipo (Lector32 o Brick)
 * Un tipo de accesorio puede pertenecear a un tipo de Ensamble
 * Ej. Para LECTOR 32 el ensamble puede ser:
 * - LECTOR BM
 * - HAMMER TAG
 * - HAMMER TAG con BM
 * <p>
 * Esta clase agrupa los accesorios pertenecientes al mismo ensamble
 **/

public class VersionAccesoryEnsamble
{
    // Variables
    private String ensamble;
    private ArrayList<VersionAccesory> accesories = new ArrayList<>();

    public VersionAccesoryEnsamble()
    {
    }

    public String getEnsamble()
    {
        return ensamble;
    }

    public void setEnsamble(String ensamble)
    {
        this.ensamble = ensamble;
    }

    public ArrayList<VersionAccesory> getAccesories()
    {
        return accesories;
    }

    public void setAccesories(ArrayList<VersionAccesory> accesories)
    {
        this.accesories = accesories;
    }
}
