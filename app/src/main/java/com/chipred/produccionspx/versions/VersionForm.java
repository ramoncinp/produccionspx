package com.chipred.produccionspx.versions;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.signs.SignDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class VersionForm extends AppCompatActivity
{
    public final static String CAN_WRITE = "canWrite";
    public final static String ID_VERSION = "id_version";

    // Variables
    private boolean canWrite = false;

    // Cadena
    private String collectionName;
    private String title;
    private String versionId;
    private String versionName;
    private String releaseDate;

    // Objetos
    private ArrayList<VersionElement> elements = new ArrayList<>();
    private CollectionReference versionsCollection;
    private Map<String, Object> root = new HashMap<>();
    private VersionElementAdapter versionElementAdapter;

    // Views
    private MaterialEditText changesEt;
    private MenuItem signVersionButton;
    private RecyclerView list;
    private TextView numeroEvoTv;
    private TextView versionNameTv;
    private TextView fechaLiberacionTv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_form);
        setTitle("Tabla de versiones de Firmware");

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        title = getIntent().getStringExtra(VersionsListActivity.MODEL_VERSIONS);
        if (title == null)
        {
            Toast.makeText(this, "Error al obtener modelo", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Obtener dato para saber si se puede editar
        canWrite = getIntent().getBooleanExtra(CAN_WRITE, false);

        // Inicializar views
        initViews();

        // Validar título
        if (title.equals(VersionsListActivity.EVO_5_VERSIONS))
        {
            collectionName = VersionsListActivity.EVO_5_COLLECTION;
            numeroEvoTv.setText(getResources().getString(R.string.evo_5));
        }
        else
        {
            collectionName = VersionsListActivity.EVO_6_COLLECTION;
            numeroEvoTv.setText(getResources().getString(R.string.evo_6));
        }

        // Validar si es nueva version o version existente
        versionId = getIntent().getStringExtra(ID_VERSION);

        // Obtener coleccion de la base de datos
        versionsCollection = FirebaseFirestore.getInstance().collection(collectionName);

        // Si es nulo, es nueva versión
        if (versionId == null)
        {
            createVersionRoot();
            setVersionElementsData();
        }
        else
        {
            getVersion();
        }
    }

    private void initViews()
    {
        // Obtener views
        numeroEvoTv = findViewById(R.id.numero_evo_tv);
        fechaLiberacionTv = findViewById(R.id.fecha_liberacion);
        list = findViewById(R.id.versions_list_elements);
        changesEt = findViewById(R.id.version_changes);
        versionNameTv = findViewById(R.id.version_name_tv);

        if (canWrite)
        {
            FloatingActionButton addVersionElementFab = findViewById(R.id.add_version_element_fab);
            addVersionElementFab.setOnClickListener(view -> addEditElement(null));
            addVersionElementFab.show();
        }

        CardView headerCardView = findViewById(R.id.header_card_view);
        headerCardView.setOnClickListener(view -> setVersionData());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Definir menu
        getMenuInflater().inflate(R.menu.version_form_menu, menu);

        // Evaluar si debe mostrar el ícono para guardar
        if (canWrite)
        {
            menu.findItem(R.id.save).setVisible(true);
        }

        // Obtener referencia de elemento del menu para firmar
        signVersionButton = menu.findItem(R.id.vobo_sign);
        signVersionButton.setVisible(versionId != null);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        else if (item.getItemId() == R.id.save)
        {
            if (versionId == null)
            {
                setVersionData();
            }
            else
            {
                updateVersion();
            }
        }
        else if (item.getItemId() == R.id.vobo_sign)
        {
            showSignDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSignDialog()
    {
        SignDialogFragment signDialogFragment = new SignDialogFragment();
        signDialogFragment.setData(versionId);
        signDialogFragment.show(getSupportFragmentManager(), "version_signs_dialog");
    }

    private void saveVersion()
    {
        // Definir datos
        root.put("fechaLiberacion", releaseDate);
        root.put("nombre", versionName);
        root.put("elementos", elements);
        root.put("cambios", Objects.requireNonNull(changesEt.getText()).toString());

        // Guardar nueva version
        if (versionId == null)
        {
            versionsCollection.add(root).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    if (task.getResult() != null)
                    {
                        versionId = task.getResult().getId();
                        fechaLiberacionTv.setText((String) root.get("fechaLiberacion"));

                        // Mostrar elemento del menú para firmar versión
                        signVersionButton.setVisible(true);
                    }
                    Toast.makeText(VersionForm.this, "Versión guardada correctamente",
                            Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(VersionForm.this, "Error al guardar versión",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
        else
        {
            versionsCollection.document(versionId).update(root).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    if (task.getResult() != null)
                    {
                        versionNameTv.setText(versionName);
                        fechaLiberacionTv.setText(releaseDate);
                    }
                    Toast.makeText(VersionForm.this, "Versión guardada correctamente",
                            Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(VersionForm.this, "Error al guardar versión",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void updateVersion()
    {
        // Definir datos
        root.put("fechaLiberacion", releaseDate);
        root.put("nombre", versionName);
        root.put("elementos", elements);
        root.put("cambios", Objects.requireNonNull(changesEt.getText()).toString());

        // Actualizar version
        versionsCollection.document(versionId).update(root).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                Toast.makeText(VersionForm.this, "Versión actualizada " +
                                "correctamente",
                        Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(VersionForm.this, "Error al guardar versión",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showErrorDialog(String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error");
        builder.setMessage(message);
        builder.setNegativeButton("Regresar", (dialogInterface, i) -> dialogInterface.dismiss());

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void getVersion()
    {
        versionsCollection.document(versionId).addSnapshotListener((documentSnapshot, e) ->
        {
            if (e != null || documentSnapshot == null)
            {
                Toast.makeText(VersionForm.this, "Error al obtener versión",
                        Toast.LENGTH_LONG).show();
            }
            else
            {
                root = documentSnapshot.getData();

                if (root != null)
                {
                    try
                    {
                        releaseDate = (String) root.get("fechaLiberacion");
                        versionName = (String) root.get("nombre");
                        setVersionsList(root.get("elementos"));
                        setVersionElementsData();
                        changesEt.setText((String) root.get("cambios"));
                    }
                    catch (Exception exc)
                    {
                        Toast.makeText(this, "Error al obtener datos de la versión",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(this, "Error al obtener datos de la versión",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setVersionsList(final Object versionElements)
    {
        // Borrar datos de lista
        elements.clear();

        ArrayList<Map<String, Object>> list = (ArrayList<Map<String, Object>>) versionElements;

        for (Map<String, Object> versionElementMap : list)
        {
            // Convertir mapa a objeto
            VersionElement versionElement = new VersionElement();

            // Obtener sus datos
            String desc = (String) versionElementMap.get("description");
            String version = (String) versionElementMap.get("version");
            String checkSum = (String) versionElementMap.get("check_sum");
            String mc = (String) versionElementMap.get("mc");

            // Definir datos
            versionElement.setDescription(desc);
            versionElement.setVersion(version);
            versionElement.setCheck_sum(checkSum);
            versionElement.setMc(mc);

            // Agregar a la lista global
            elements.add(versionElement);
        }
    }

    private void setVersionElementsData()
    {
        // Escribir encabezados
        if (versionName != null)
            versionNameTv.setText(String.format(Locale.getDefault(), "v%s",
                    versionName.toUpperCase()));

        if (releaseDate != null)
            fechaLiberacionTv.setText(releaseDate.toUpperCase());


        // Ordenar
        Collections.sort(elements, (versionElement, t1) ->
                versionElement.getDescription().toLowerCase().compareTo(t1.getDescription().toLowerCase()));

        // Crear adaptador
        versionElementAdapter = new VersionElementAdapter(elements);

        // Validar permiso para escribir
        if (getIntent().hasExtra(VersionsListActivity.CAN_WRITE) && getIntent().getBooleanExtra(VersionsListActivity.CAN_WRITE, false))
        {
            versionElementAdapter.setListener(view ->
            {
                // Obtener elemento
                VersionElement element = elements.get(list.getChildAdapterPosition(view));
                addEditElement(element);
            });

            versionElementAdapter.setLongClickListener(view ->
            {
                removeVersionElementDialog(list.getChildAdapterPosition(view));
                return false;
            });
        }

        // Asignar adaptador
        list.setAdapter(versionElementAdapter);
        list.setLayoutManager(new LinearLayoutManager(this));
    }

    private void createVersionRoot()
    {
        elements.addAll(VersionsUtils.getVersionsRootElements(title));
    }

    private void setVersionData()
    {
        // Validar version principal
        if (versionName == null)
        {
            String message;
            if (title.equals(VersionsListActivity.EVO_5_VERSIONS))
            {
                message = "Definir version de Orion Main";
            }
            else
            {
                message = "Definir version de bOS";
            }

            showErrorDialog(message);
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Versión");

        // Obtener view
        View view = getLayoutInflater().inflate(R.layout.dialog_version_data, null);
        builder.setView(view);

        // Obtener views
        MaterialEditText versionEt = view.findViewById(R.id.version_name_et);
        MaterialEditText fechaEt = view.findViewById(R.id.fecha_liberacion_et);
        Button submitButton = view.findViewById(R.id.save_button);
        Button returnButton = view.findViewById(R.id.return_button);

        // Definir texto
        versionEt.setText(versionName);
        fechaEt.setText(releaseDate);

        // Definir filtros
        fechaEt.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        // Crear diálogo
        Dialog dialog = builder.create();
        dialog.show();

        // Definir clickListeners
        submitButton.setOnClickListener(view12 ->
        {
            // Validar
            boolean valid = true;
            if (versionEt.getText().toString().isEmpty())
            {
                versionEt.setError("Campo obligatorio");
                valid = false;
            }

            if (fechaEt.getText().toString().isEmpty())
            {
                fechaEt.setError("Campo obligatorio");
                valid = false;
            }

            if (!valid) return;

            // Obtener valores
            versionName = versionEt.getText().toString();
            releaseDate = fechaEt.getText().toString();

            // Subir a la base de datos
            saveVersion();

            // Cerrar diálogo
            dialog.dismiss();
        });

        returnButton.setOnClickListener(view1 -> dialog.dismiss());
    }

    private void addEditElement(VersionElement element)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Módulo / Ensamble");

        // Obtener view
        View view = getLayoutInflater().inflate(R.layout.dialog_version_element, null);
        builder.setView(view);

        // Obtener views
        MaterialEditText nameEt = view.findViewById(R.id.modulo_ensamble_et);
        MaterialEditText versionEt = view.findViewById(R.id.version_et);
        MaterialEditText checksumEt = view.findViewById(R.id.checksum_et);
        MaterialBetterSpinner mcSpinner = view.findViewById(R.id.mc_spinner);
        Button submitButton = view.findViewById(R.id.save_button);
        Button returnButton = view.findViewById(R.id.return_button);
        initMcSpinner(mcSpinner);

        // Definir texto
        if (element != null)
        {
            nameEt.setEnabled(false);
            nameEt.setText(element.getDescription());
            mcSpinner.setText(element.getMc());
            versionEt.setText(element.getVersion());
            checksumEt.setText(element.getCheck_sum());
        }
        else
        {
            nameEt.setEnabled(true);
        }

        // Crear diálogo
        Dialog dialog = builder.create();
        dialog.show();

        // Definir clickListeners
        submitButton.setOnClickListener(view1 ->
        {
            // Obtener valores
            String name, mc, version, checksum;
            name = nameEt.getText().toString();
            mc = mcSpinner.getText().toString();
            version = versionEt.getText().toString();
            checksum = checksumEt.getText().toString();

            if (element == null)
            {
                // Validar campos
                if (!nameEt.validateWith(Constants.emptyValidator))
                {
                    return;
                }

                // Validar que no exista algun elemento con la nueva descripción
                if (elementExists(name))
                {
                    nameEt.setError("El nombre del módulo/ensamble ya existe");
                    return;
                }

                // Si fue valido, crear nuevo elemento
                VersionElement newElement = new VersionElement();
                newElement.setDescription(name);
                newElement.setMc(mc.isEmpty() ? null : mc);
                newElement.setVersion(version);
                newElement.setCheck_sum(checksum);

                // Agregar nuevo elemento a la lista
                elements.add(newElement);

                // Ordenar lista
                Collections.sort(elements, (versionElement, t1) ->
                        versionElement.getDescription().toLowerCase().compareTo(t1.getDescription().toLowerCase()));
            }
            else
            {
                // Actualizar elemento
                element.setDescription(name);
                element.setMc(mc.isEmpty() ? null : mc);
                element.setVersion(version);
                element.setCheck_sum(checksum);
            }

            // Evaluar si es la version oficial
            if (name.equals("bOS") || name.equals("Orion Main"))
            {
                versionName = version;
            }

            // Actualizar lista
            versionElementAdapter.notifyDataSetChanged();

            // Cerrar diálogo
            dialog.dismiss();
        });

        returnButton.setOnClickListener(view12 -> dialog.dismiss());
    }

    private void initMcSpinner(MaterialBetterSpinner spinner)
    {
        String[] mcOptions = new String[]{
                VersionElement.AK1,
                VersionElement.ESP32,
                VersionElement.ESP8266,
                VersionElement.FLEXIS,
                VersionElement.KINETIS,
                VersionElement.QG8,
        };

        // Crear adaptador
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, mcOptions);
        spinner.setAdapter(arrayAdapter);

        // Definir longClickListener para borrar
        spinner.setOnLongClickListener(view ->
        {
            spinner.setText("");
            return true;
        });
    }

    private void removeVersionElementDialog(int listPos)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remover módulo/ensamble");

        String dialogText = "Se removerá " + elements.get(listPos).getDescription() + " de la " +
                "lista";
        builder.setMessage(dialogText);

        builder.setPositiveButton("Remover", (dialogInterface, i) ->
        {
            elements.remove(listPos);
            versionElementAdapter.notifyItemRemoved(listPos);
        });
        builder.setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());

        Dialog dialog = builder.create();
        dialog.show();
    }

    boolean elementExists(String elementDesc)
    {
        boolean isContained = false;
        for (VersionElement element : elements)
        {
            if (element.getDescription().equals(elementDesc))
            {
                isContained = true;
                break;
            }
        }

        return isContained;
    }
}
