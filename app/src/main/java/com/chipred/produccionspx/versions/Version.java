package com.chipred.produccionspx.versions;

public class Version
{
    private String id;
    private String nombre;
    private String fechaLiberacion;

    public Version()
    {
    }

    public Version(String nombre, String fechaLiberacion)
    {
        this.nombre = nombre;
        this.fechaLiberacion = fechaLiberacion;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getFechaLiberacion()
    {
        return fechaLiberacion;
    }

    public void setFechaLiberacion(String fechaLiberacion)
    {
        this.fechaLiberacion = fechaLiberacion;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
}
