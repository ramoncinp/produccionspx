package com.chipred.produccionspx.versions;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;

import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class VersionsUtils
{
    public static void showAccesoriesMenu(Activity activity, boolean canModify)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        View content = activity.getLayoutInflater().inflate(R.layout.dialog_accesories_menu, null);
        builder.setView(content);

        RelativeLayout brickOption = content.findViewById(R.id.brick_versions_element);
        RelativeLayout lectorOption = content.findViewById(R.id.lector32_versions_element);
        RelativeLayout retornosOption = content.findViewById(R.id.retornos_versions_element);

        brickOption.setOnClickListener(view ->
        {
            Intent intent = new Intent(activity, VersionsListActivity.class);
            intent.putExtra(VersionsListActivity.CAN_WRITE, canModify);
            intent.putExtra(VersionsListActivity.MODEL_VERSIONS, VersionAccesory.BRICK_ACCESORY);
            activity.startActivity(intent);
        });

        lectorOption.setOnClickListener(view ->
        {
            Intent intent = new Intent(activity, VersionsListActivity.class);
            intent.putExtra(VersionsListActivity.CAN_WRITE, canModify);
            intent.putExtra(VersionsListActivity.MODEL_VERSIONS, VersionAccesory.LECTOR32_ACCESORY);
            activity.startActivity(intent);
        });

        retornosOption.setOnClickListener(view ->
        {
            Intent intent = new Intent(activity, VersionsListActivity.class);
            intent.putExtra(VersionsListActivity.CAN_WRITE, canModify);
            intent.putExtra(VersionsListActivity.MODEL_VERSIONS, VersionAccesory.RETORNOS_ACCESORY);
            activity.startActivity(intent);
        });

        // Crear diálogo
        Dialog dialog = builder.create();
        dialog.show();
    }

    public static ArrayList<VersionElement> getVersionsRootElements(String evoType)
    {
        ArrayList<VersionElement> elements = new ArrayList<>();

        if (evoType.equals(VersionsListActivity.EVO_5_VERSIONS))
        {
            // EVO5
            // Modulos / pca's
            elements.add(new VersionElement("PCA Optos", VersionElement.AK1));
            elements.add(new VersionElement("PCA Micro 5", VersionElement.FLEXIS));
            elements.add(new VersionElement("PCA Ledmatrix", VersionElement.FLEXIS));
            elements.add(new VersionElement("Init Ledmatrix", VersionElement.FLEXIS));
            elements.add(new VersionElement("Pca Pulsador Hall", VersionElement.QG8));
            elements.add(new VersionElement("Init Pulsador G", VersionElement.QG8));
            elements.add(new VersionElement("Init Pulsador TH", VersionElement.QG8));
            elements.add(new VersionElement("PCA Lector", VersionElement.QG8));
            elements.add(new VersionElement("Orion Main"));
            elements.add(new VersionElement("Orion Side"));
            elements.add(new VersionElement("Orion Control Center"));
            elements.add(new VersionElement("Simkraken"));
            elements.add(new VersionElement("Paquete firmware"));
            elements.add(new VersionElement("Paquete eddy dispensarios 1"));
            elements.add(new VersionElement("Paquete eddy dispensarios 2"));
        }
        else
        {
            // EVO6
            // Modulos / pca's
            elements.add(new VersionElement("PCA Hub", VersionElement.FLEXIS));
            elements.add(new VersionElement("PCA Micro 6", VersionElement.FLEXIS));
            elements.add(new VersionElement("PCA Ledmatrix", VersionElement.FLEXIS));
            elements.add(new VersionElement("PCA Hulk PMP", VersionElement.KINETIS));
            elements.add(new VersionElement("PCA Hulk SPX15", VersionElement.KINETIS));
            elements.add(new VersionElement("bOS"));
            elements.add(new VersionElement("bOSMain"));
            elements.add(new VersionElement("Orion Control Center"));
            elements.add(new VersionElement("Simkraken"));
            elements.add(new VersionElement("Bootloader Flexis"));
            elements.add(new VersionElement("Bootloader Kinetis Pulsador"));
            elements.add(new VersionElement("Paquete firmware"));
            elements.add(new VersionElement("Paquete eddy dispensarios 1"));
            elements.add(new VersionElement("Paquete eddy dispensarios 2"));
        }

        return elements;
    }
}
