package com.chipred.produccionspx;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Constants {
    //Colecciones
    public static final String ALARMS_COLLECTION = "alarmas";
    public static final String DISPENSERS_COLLECTION = "dispensarios";
    public static final String PROJECTS_COLLECTION = "proyectos";
    public static final String PROJECTS_REFERENCES_COLLECTION = "referencias_proyectos";
    public static final String PROJECTS_CHANGES_COLLECTION = "cambios_proyectos";
    public static final String TRIPS_COLLECTION = "viajes";

    //Status de proyectos
    public static final String EN_PROCESO = "En Proceso";
    public static final String TERMINADO = "Terminado";
    public static final String ENTREGADO = "Entregado";

    //Status de administrativo de proyectos
    public static final String COTIZADO = "Cotizado";
    public static final String PREFACTURA = "Prefactura";
    public static final String FACTURA = "Factura";
    public static final String PARCIALIDAD = "Parcialidad";
    public static final String PAGADO = "Pagado";

    //Status de proyecto
    // En espera -> En proceso -> Concluido
    public static final String EN_ESPERA = "En espera";
    public static final String CONCLUIDO = "Concluido";
    public static final String CANCELADO = "Cancelado";

    //Tipos de venta
    public static final String ARRENDAMIENTO = "arrendamiento";
    public static final String VENTA = "venta";

    //Parámetros de actividades
    public static final String ADD_OR_MODIFY_PROJECT = "addOrModify";
    public static final String CAN_MODIFY = "canModify";
    public static final String PROJECT_NAME = "nombreDeProyecto";
    public static final String PROJECT_ORDER_NUMBER = "numeroDeOrden";
    public static final String VERSION_ID = "idVersion";
    public static final String OPEN_SIGNS_DIALOG = "verFirmas";
    public static final String RECEIVE_ALARM_PROD = "alarmasProd";
    public static final String RECEIVE_ALARM_ADMN = "alarmasAdmin";

    //Bloques para permitir acciones de usuarios
    public static final int READ_PROJECTS = 0;
    public static final int WRITE_PROJECTS = 1;
    public static final int LISTEN_PROJECTS = 2;
    public static final int READ_EKITS = 3;
    public static final int WRITE_EKITS = 4;
    public static final int USE_PASS_GENERATOR = 5;
    public static final int READ_PASS_REGISTERS = 6;
    public static final int READ_REPORTS = 7;
    public static final int LISTEN_PROJECT_ALARM_PROD = 8;
    public static final int LISTEN_PROJECT_ALARM_ADMN = 9;
    public static final int READ_VERSIONS = 10;
    public static final int WRITE_VERSIONS = 11;
    public static final int VEHICLES_USER = 12;
    public static final int VEHICLES_ADMIN = 13;
    public static final int VEHICLES_LOCATION = 14;
    static final int SET_PERMISSIONS = 99;

    //Constantes
    public static final String SHARED_PREFERENCES = "sharedPreferences";
    public static final String NOTIFICATIONS = "notifications";
    public final static String GENERAL_NOTIFICATIONS_ID = "general";
    public final static String PROJECTS_TOPIC = "projects";
    public final static String ALARMS_TOPIC = "alarms";
    public final static String VEHICLE_ADMIN_TOPIC = "vehicleAdmin";
    public final static String VEHICLE_USER_TOPIC = "vehicleUser";
    public final static String PROJECTS_LIST_FILTER = "projectsListFilter";
    public final static long PROJECT_ALARM_TIME_MS = 604800000; // 7 días en milisegundos

    public final static DecimalFormat litersDecimalFormat = new DecimalFormat("#0.000");
    public final static DecimalFormat distanceFormat = new DecimalFormat("###,###,##0");

    public final static METValidator emptyValidator = new METValidator("Campo obligatorio") {
        @Override
        public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
            return !isEmpty;
        }
    };

    public static ArrayList<String> getDays() {
        ArrayList<String> days = new ArrayList<>();

        for (int i = 1; i <= 31; i++) {
            String number = String.format(Locale.getDefault(), "%02d", i);
            days.add(number);
        }

        return days;
    }

    public static ArrayList<String> getMonthsText() {
        ArrayList<String> months = new ArrayList<>();

        for (int i = 1; i <= 12; i++) {
            months.add(monthNumberToString(i));
        }

        return months;
    }

    public static ArrayList<String> getMonths() {
        ArrayList<String> months = new ArrayList<>();

        for (int i = 1; i <= 12; i++) {
            String number = String.format(Locale.getDefault(), "%02d", i);
            months.add(number);
        }

        return months;
    }

    public static ArrayList<String> getStringMonths() {
        String[] stringMonths = new String[]{"ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL",
                "AGO", "SEP", "OCT", "NOV", "DIC"};

        return new ArrayList<>(Arrays.asList(stringMonths));
    }

    public static ArrayList<String> getYears() {
        ArrayList<String> months = new ArrayList<>();

        for (int i = 0; i <= 30; i++) {
            String number = String.format(Locale.getDefault(), "%02d", i);
            months.add(number);
        }

        return months;
    }

    public static String twoDigits(int n) {
        return (n <= 9) ? ("0" + n) : String.valueOf(n);
    }

    public static String monthNumberToString(int monthNumber) {
        switch (monthNumber) {
            case 1:
                return "ENE";

            case 2:
                return "FEB";

            case 3:
                return "MAR";

            case 4:
                return "ABR";

            case 5:
                return "MAY";

            case 6:
                return "JUN";

            case 7:
                return "JUL";

            case 8:
                return "AGO";

            case 9:
                return "SEP";

            case 10:
                return "OCT";

            case 11:
                return "NOV";

            case 12:
                return "DIC";

            default:
                return (monthNumber <= 9) ? ("0" + monthNumber) : String.valueOf(monthNumber);
        }
    }

    public static String monthStringToNumber(String monthString) {
        switch (monthString) {
            case "ENE":
                return "01";

            case "FEB":
                return "02";

            case "MAR":
                return "03";

            case "ABR":
                return "04";

            case "MAY":
                return "05";

            case "JUN":
                return "06";

            case "JUL":
                return "07";

            case "AGO":
                return "08";

            case "SEP":
                return "09";

            case "OCT":
                return "10";

            case "NOV":
                return "11";

            case "DIC":
                return "12";

            default:
                return monthString;
        }
    }

    public static boolean isValidMonth(String inMonth) {
        for (String month : getMonthsText()) {
            if (month.equals(inMonth)) return true;
        }

        return false;
    }

    public static boolean isValidDay(String day) {
        // Convertir día a int
        int mDay = Integer.parseInt(day);

        // Validar
        return mDay > 0 && mDay <= 31;
    }

    public static Date dateStringToObject(String dateString) {
        //Esperando una fecha como "30/ENE/2019 10:22"...
        //Convertir a objeto de Fecha

        try {
            //Obtener el primer índice del caracter "/"
            int idx1 = dateString.indexOf("/");
            //Obtener el último índice del caracter "/"
            int idx2 = dateString.lastIndexOf("/");

            //Obtener el mes de la fecha
            String month = dateString.substring(idx1 + 1, idx2);

            //Reemplazar su abreviación con un número
            dateString =
                    dateString.substring(0, idx1 + 1) + monthStringToNumber(month) + dateString.substring(idx2);
        } catch (Exception e) {
            return null;
        }

        //Definir dateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());

        //Crear objeto de fecha
        Date date;

        //Convertir String a Fecha
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return date;
    }

    public static Date dateStringToObject2(String dateString) {
        //Esperando una fecha como "30/ENE/2019"...
        //Convertir a objeto de Fecha

        try {
            //Obtener el primer índice del caracter "/"
            int idx1 = dateString.indexOf("/");
            //Obtener el último índice del caracter "/"
            int idx2 = dateString.lastIndexOf("/");

            //Obtener el mes de la fecha
            String month = dateString.substring(idx1 + 1, idx2);

            //Reemplazar su abreviación con un número
            dateString =
                    dateString.substring(0, idx1 + 1) + monthStringToNumber(month) + dateString.substring(idx2);
        } catch (Exception e) {
            return null;
        }

        //Definir dateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        //Crear objeto de fecha
        Date date;

        //Convertir String a Fecha
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return date;
    }

    public static String dateObjectToString(Date date) {
        String dateString;

        //Definir dateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        //Convertir Fecha a String
        try {
            dateString = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        try {
            //Obtener el primer índice del caracter "/"
            int idx1 = dateString.indexOf("/");
            //Obtener el último índice del caracter "/"
            int idx2 = dateString.lastIndexOf("/");

            //Obtener el mes de la fecha
            int month = Integer.parseInt(dateString.substring(idx1 + 1, idx2));

            //Reemplazar su abreviación con un número
            dateString =
                    dateString.substring(0, idx1 + 1) + monthNumberToString(month) + dateString.substring(idx2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return dateString;
    }

    public static String dateObjectToYearMonthString(Date date) {
        String dateString;

        //Definir dateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy", Locale.getDefault());

        //Convertir Fecha a String
        try {
            dateString = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        try {
            int idx1 = 0;
            //Obtener el último índice del caracter "/"
            int idx2 = dateString.lastIndexOf("/");

            //Obtener el mes de la fecha
            int month = Integer.parseInt(dateString.substring(idx1, idx2));

            //Reemplazar su abreviación con un número
            dateString =
                    dateString.substring(0, idx1) + monthNumberToString(month) + dateString.substring(idx2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return dateString;
    }

    public static String dateObjectToString2(Date date) {
        String dateString;

        //Definir dateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                Locale.getDefault());

        //Convertir Fecha a String
        try {
            dateString = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        try {
            //Obtener el primer índice del caracter "/"
            int idx1 = dateString.indexOf("/");
            //Obtener el último índice del caracter "/"
            int idx2 = dateString.lastIndexOf("/");

            //Obtener el mes de la fecha
            int month = Integer.parseInt(dateString.substring(idx1 + 1, idx2));

            //Reemplazar su abreviación con un número
            dateString =
                    dateString.substring(0, idx1 + 1) + monthNumberToString(month) + dateString.substring(idx2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return dateString;
    }

    public static String getLastModifText(long timeStamp) {
        String text = "Última modificación ";

        //Obtener fecha actual
        long timeDiff = new Date().getTime() - timeStamp;

        //Convertir diferencia de milisegundos a segundos
        timeDiff /= 1000;

        //Evaluar si puede ser expresada en segundos
        if (timeDiff < 60) {
            text += "hace ";
            text += String.valueOf(timeDiff);
            text += " segundos";
        }
        //Evaluar si puede ser expresada en minutos
        else if (timeDiff < 3600) {
            int minutesDiff = (int) (timeDiff / 60);
            text += "hace ";
            text += String.valueOf(minutesDiff);

            if (minutesDiff > 1)
                text += " minutos";
            else
                text += " minuto";
        }
        //Evaluar si puede ser expresada en minutos
        else if (timeDiff < 86400) {
            int hoursDiff = (int) (timeDiff / 3600);
            text += "hace ";
            text += String.valueOf(hoursDiff);

            if (hoursDiff > 1)
                text += " horas";
            else
                text += " hora";
        } else {
            Date modifDate = new Date(timeStamp);
            text += Constants.dateObjectToString2(modifDate);
        }

        return text;
    }

    static String getDatesTextDifference(Date date1, Date date2) {
        String text = "";

        //Obtener fecha actual
        long timeDiff = date1.getTime() - date2.getTime();

        //Convertir diferencia de milisegundos a segundos
        timeDiff /= 1000;

        //Evaluar si puede ser expresada en segundos
        if (timeDiff < 60) {
            text += String.valueOf(timeDiff);
            text += " segundos";
        }
        //Evaluar si puede ser expresada en minutos
        else if (timeDiff < 3600) {
            int minutesDiff = (int) (timeDiff / 60);
            text += String.valueOf(minutesDiff);

            if (minutesDiff > 1)
                text += " minutos";
            else
                text += " minuto";
        }
        //Evaluar si puede ser expresada en minutos
        else {
            int hoursDiff = (int) (timeDiff / 3600);
            text += String.valueOf(hoursDiff);

            if (hoursDiff > 1)
                text += " horas";
            else
                text += " hora";
        }

        return text;
    }

    public static void disableLinearLayoutChilds(LinearLayout linearLayout, Context context) {
        for (int i = 0; i < linearLayout.getChildCount(); i++) {
            View child = linearLayout.getChildAt(i);

            if (child instanceof LinearLayout) {
                disableLinearLayoutChilds((LinearLayout) child, context);
            } else if (child instanceof MaterialBetterSpinner) {
                //Definir adaptador
                ArrayAdapter<String> emptyAdapter = new ArrayAdapter<>(context
                        , android.R
                        .layout.simple_dropdown_item_1line, new ArrayList<>());

                child.setFocusable(false);
                child.setClickable(false);
                child.setOnLongClickListener(null);
                ((MaterialBetterSpinner) child).setAdapter(emptyAdapter);
            } else if (child instanceof MaterialEditText) {
                child.setFocusable(false);
            } else {
                child.setOnClickListener(null);
                child.setClickable(false);
                child.setFocusable(false);
            }
        }
    }

    public static String dateToRelative(Date date) {
        String dateString = null;

        //Definir dateFormat
        SimpleDateFormat timeDateFormat = new SimpleDateFormat("HH:mm:ss",
                Locale.getDefault());

        //Evaluar relatividad de la fecha
        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        Calendar inDate = Calendar.getInstance();
        inDate.setTime(date);

        // Comparar días del año
        int dayDiff = today.get(Calendar.DAY_OF_YEAR) - inDate.get(Calendar.DAY_OF_YEAR);
        if (dayDiff == 0) {
            dateString = "Hoy a las ";
        } else if (dayDiff == 1) {
            dateString = "Ayer a las ";
        } else if (dayDiff == -1) {
            dateString = "Mañana a las ";
        }

        // Evaluar si se obtuvo un resultado
        if (dateString == null) {
            dateString = dateObjectToString2(date);
        } else {
            // Agregar hora
            dateString += timeDateFormat.format(date);
        }

        return dateString;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        if (view == null) return;

        InputMethodManager imm =
                (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Comparator<DispModel> dispModelComparator = new Comparator<DispModel>() {
        @Override
        public int compare(DispModel o1, DispModel o2) {
            return o1.getSerialNumber().compareTo(o2.getSerialNumber());
        }
    };

    public static Comparator<String> stringComparator = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }
    };

    public static boolean isTimeFormValid(MaterialEditText hour, MaterialEditText minute) {
        boolean isValid = hour.validateWith(new METValidator("La hora no es válida") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                if (isEmpty) return false;

                try {
                    int val = Integer.parseInt(text.toString());
                    return val >= 0 && val <= 12;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });

        isValid &= minute.validateWith(new METValidator("El minuto no es válido") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                if (isEmpty) return false;

                try {
                    int val = Integer.parseInt(text.toString());
                    return val >= 0 && val <= 59;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });

        return isValid;
    }

    public static String timeFormToSring(
            MaterialEditText hourEt,
            MaterialEditText minuteEt,
            MaterialBetterSpinner timeFormatSpinner
    ) {
        String timeFormatVal = timeFormatSpinner.getText().toString();
        int hour = Integer.parseInt(hourEt.getText().toString());
        int minute = Integer.parseInt(minuteEt.getText().toString());

        // Ajustar si es AM o PM
        if (timeFormatVal.equals("P.M.")) {
            // Si es PM, sumar 12 horas solo si la hora no es 12
            if (hour != 12) hour += 12;
        } else if (hour == 12) {
            // Si es AM y es 12 la hora, restar 12
            hour -= 12;
        }

        // Formatear cadena
        return String.format(Locale.getDefault(), "%02d:%02d", hour, minute);
    }

    public static String[] timeFormFromString(String fullDate) {
        // Formato de entrada -> 10/FEB/2020 15:40
        // [0] -> Fecha (10/FEB/2020)
        // [1] -> Hora (15)
        // [2] -> Minuto (40)
        // [3] -> Formato (A.M. o P.M.)

        // Asegurar el formato de entrada
        fullDate = fullDate.substring(0, 17);

        String[] dateData = new String[4];
        String date = fullDate.substring(0, fullDate.indexOf(" "));
        String time = fullDate.substring(fullDate.indexOf(" ") + 1);
        String hour = time.substring(0, time.indexOf(":"));
        String minute = time.substring(time.indexOf(":") + 1);
        String format = "A.M.";

        int hourVal = Integer.parseInt(hour);
        if (hourVal > 12) {
            hour = String.valueOf(hourVal - 12);
            format = "P.M.";
        } else if (hourVal == 0) {
            hour = "12";
        } else if (hourVal == 12) {
            format = "P.M.";
        }

        dateData[0] = date;
        dateData[1] = hour;
        dateData[2] = minute;
        dateData[3] = format;

        return dateData;
    }

    public static List<String> toQueryData(String value) {
        final List<String> queryElements = new ArrayList<>();
        StringBuilder temp = new StringBuilder();

        value = value.toUpperCase();
        for (int idx = 0; idx < value.length(); idx++) {
            temp.append(value.charAt(idx));
            queryElements.add(temp.toString());
        }

        return queryElements;
    }

    public static void showDialog(Activity activity, String title, String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", onClickListener);

        final Dialog dialog = builder.create();
        dialog.show();
    }
}