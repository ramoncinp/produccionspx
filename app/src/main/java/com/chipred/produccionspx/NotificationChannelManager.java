package com.chipred.produccionspx;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

class NotificationChannelManager
{
    private Context context;

    NotificationChannelManager(Context context)
    {
        this.context = context;
    }

    void createDefaultNotificationChannel()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            CharSequence name = context.getResources().getString(R.string
                    .default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            // Crear canal
            NotificationChannel channel = new NotificationChannel(Constants
                    .GENERAL_NOTIFICATIONS_ID, name, importance);

            // Definir propiedades
            channel.enableLights(true);
            channel.enableVibration(true);

            NotificationManager notificationManager = context.getSystemService
                    (NotificationManager.class);
            if (notificationManager != null) notificationManager.createNotificationChannel(channel);
        }
    }
}
