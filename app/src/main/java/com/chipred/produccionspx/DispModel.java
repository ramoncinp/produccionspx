package com.chipred.produccionspx;

import java.util.HashMap;
import java.util.Map;

public class DispModel {
    private String id;
    private String model;
    private String serialNumber = "";
    private String manufactureDate = "";
    private String products = "";
    private String sae = "";
    private String chasis = "";
    private String ordenInterna;
    private String kitElectronico;
    private String invoiceNumber;
    private Long finishedDate;
    private Long invoiceDate;
    private boolean hasChanged = false;
    private boolean saeOk = false;
    private boolean finished = false;
    private boolean usesGeneralInvoiceData = true;


    public DispModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isHasChanged() {
        return hasChanged;
    }

    public void setHasChanged(boolean hasChanged) {
        this.hasChanged = hasChanged;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getSae() {
        return sae;
    }

    public void setSae(String sae) {
        this.sae = sae;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public String getOrdenInterna() {
        return ordenInterna;
    }

    public void setOrdenInterna(String ordenInterna) {
        this.ordenInterna = ordenInterna;
    }

    public Long getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(Long finishedDate) {
        this.finishedDate = finishedDate;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Long getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Long invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public boolean isUsesGeneralInvoiceData() {
        return usesGeneralInvoiceData;
    }

    public void setUsesGeneralInvoiceData(boolean usesGeneralInvoiceData) {
        this.usesGeneralInvoiceData = usesGeneralInvoiceData;
    }

    @Override
    public String toString() {
        if (!serialNumber.isEmpty()) {
            return "#" + serialNumber;
        } else {
            return model;
        }
    }

    public String getKitElectronico() {
        if (kitElectronico == null) return "";
        else return kitElectronico;
    }

    public void setKitElectronico(String kitElectronico) {
        this.kitElectronico = kitElectronico;
    }

    public boolean isSaeOk() {
        return saeOk;
    }

    public void setSaeOk(boolean saeOk) {
        this.saeOk = saeOk;
    }

    public Map<String, Object> toMap() {
        // Crear Map
        Map<String, Object> dispMap = new HashMap<>();

        // Agregar datos
        dispMap.put("chasis", chasis);
        dispMap.put("manufactureDate", manufactureDate);
        dispMap.put("model", model);
        dispMap.put("products", products);
        dispMap.put("sae", sae);
        dispMap.put("saeOk", saeOk);
        dispMap.put("serialNumber", serialNumber);
        dispMap.put("finished", finished);
        dispMap.put("finishedDate", finishedDate);

        return dispMap;
    }
}