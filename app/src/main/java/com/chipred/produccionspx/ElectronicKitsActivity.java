package com.chipred.produccionspx;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.chipred.produccionspx.projects.ProjectOverview;
import com.chipred.produccionspx.users.UserData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.adapters.ElectronicKitOverviewAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class ElectronicKitsActivity extends AppCompatActivity {
    // Constantes
    private static final String TAG = ElectronicKitsActivity.class.getSimpleName();
    public static final String CAN_MODIFY_EKIT = "canModifyEKit";

    // Filtros
    public static final String[] LAST_MODIF_DATE = {"epoch", "Fecha de modif."};
    public static final String[] KIT_NUMBER = {"numero_kit", "Número de kit"};
    public static final String[] PROJECT = {"proyecto", "Proyecto"};

    //Variables
    private boolean canModifyKits = false;

    private int gotProjectsCounter, gotUsersCounter;
    private String currentFilter;

    //Views
    private Dialog filterSelectorDialog;
    private FloatingActionButton floatingActionButton;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private TextView noElectronicKitsTv;

    //Adaptador
    private ElectronicKitOverviewAdapter adapter;

    //Listas
    private ArrayList<ElectronicKitOverview> electronicKitOverviews = new ArrayList<>();
    private ArrayList<String> macAddressList = new ArrayList<>();
    private ArrayList<ProjectOverview> projectsOverviews = new ArrayList<>();
    private ArrayList<String> projectsIds = new ArrayList<>();
    private ArrayList<UserData> savedUsers = new ArrayList<>();
    private ArrayList<String> serialNumbers = new ArrayList<>();

    // Objetos
    private CollectionReference eKitsCollection;
    private Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electronic_kits);

        //Obtener permiso para modificar kit electrónico
        if (getIntent().hasExtra(CAN_MODIFY_EKIT)) {
            canModifyKits = getIntent().getBooleanExtra(CAN_MODIFY_EKIT, true);
        }

        //Agregar flecha para regresar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Inicializar views
        initViews();

        //Crear instancia para obtener el query
        eKitsCollection = FirebaseFirestore.getInstance().collection("kits_electronicos");

        // Obtener filtro default
        currentFilter = getResources().getString(R.string.fek_fecha_modificacion);

        //Crear query default
        query = eKitsCollection.orderBy(LAST_MODIF_DATE[0], Query.Direction.DESCENDING);
    }

    @Override
    protected void onStart() {
        super.onStart();

        //Inicializar base de datos
        executeQuery();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Obtener layout
        getMenuInflater().inflate(R.menu.filter_list_menu, menu);

        // Obtener searchView
        SearchView searchView = (SearchView) menu.findItem(R.id.search_menu).getActionView();
        searchView.setQueryHint(getResources().getString(R.string.search_kits));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d("SearchKit", "BuscarFinalmente -> " + query);
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d("SearchKit", "Buscar -> " + newText);
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    private void executeQuery() {
        // Reiniciar contadores
        gotProjectsCounter = 0;
        gotUsersCounter = 0;

        // Ejecutar query
        query.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null || queryDocumentSnapshots == null) {
                    noElectronicKitsTv.setVisibility(View.VISIBLE);
                } else {
                    if (queryDocumentSnapshots.getDocuments().isEmpty()) {
                        noElectronicKitsTv.setVisibility(View.VISIBLE);
                    } else {
                        //Mostrar kits electrónicos registrados
                        getElecKits(queryDocumentSnapshots);
                    }
                }
            }
        });
    }

    private void getElecKits(QuerySnapshot query) {
        //Limpiar lista
        electronicKitOverviews.clear();

        //Leer todos los documentos
        for (DocumentSnapshot document : query.getDocuments()) {
            //Obtener numero de kit
            String kitNumber = document.getId();

            //Crear objeto de vista previa
            ElectronicKitOverview overview = new ElectronicKitOverview(kitNumber);

            //Obtener proyecto
            String proyectId = document.getString("proyecto");

            //Obtener usuario
            String userId = document.getString("id_usuario");

            //Agregar a kit electrónico
            overview.setProjectId(proyectId);
            overview.setEpoch(document.getLong("epoch"));
            overview.setUserId(userId);

            //Agregar kit obtenido
            electronicKitOverviews.add(overview);

            //Obtener seriales
            addSerialsToList(document);

            //Obtener información del proyecto
            getProject(proyectId, overview);

            //Obtener información del usuario
            getUser(userId, overview);
        }
    }

    private void getProject(final String proyectId, ElectronicKitOverview elecKit) {
        //Validar el id de proyecto
        if (proyectId.isEmpty()) {
            //Incrementar contador
            gotProjectsCounter++;
            return;
        }

        //Crear objeto de vistaPrevia de proyecto
        ProjectOverview projectOverview;

        //Si ya fue consultado el proyecto
        if ((projectOverview = isProjectContained(proyectId)) != null) {
            //Actualizar elementos
            elecKit.setClient(projectOverview.getClient());
            elecKit.setStation(projectOverview.getStation());

            //Incrementar contador
            gotProjectsCounter++;

            showElecKitsListIfReady();
        } else {
            //Obtenerlo de la base de datos
            FirebaseFirestore.getInstance().collection("proyectos").document(proyectId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    // Incrementar contador
                    gotProjectsCounter++;

                    // Obtener respuesta
                    if (task.isSuccessful() && task.getResult() != null) {
                        //Obtener documento
                        DocumentSnapshot snapshot = task.getResult();

                        //Agregar a la lista de proyectos
                        projectsIds.add(snapshot.getId());

                        //Crear vista previa de proyecto
                        ProjectOverview projectOverview =
                                new ProjectOverview(snapshot.getId());

                        //Obtener datos
                        projectOverview.setName(snapshot.getString("nombre"));

                        //Obtener datos generales
                        Map<String, Object> generalData =
                                (Map<String, Object>) snapshot.get(
                                        "datos_generales");

                        projectOverview.setClient(generalData.get("cliente").toString());
                        projectOverview.setStation(generalData.get("estacion").toString());

                        //Agregar info
                        projectsOverviews.add(projectOverview);

                        //Actualizar kits
                        updateElectronicKitsOverview(projectOverview);
                    } else {
                        Toast.makeText(ElectronicKitsActivity.this, "Error al obtener el " +
                                "proyecto" +
                                " " + proyectId, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void updateElectronicKitsOverview(ProjectOverview projectOverview) {
        //Actualizar datos a kits electrónicos con el número de orden del proyecto que
        // entra al
        // método
        for (ElectronicKitOverview electronicKitOverview : electronicKitOverviews) {
            if (electronicKitOverview.getProjectId().equals(projectOverview.getOrderNumber())) {
                electronicKitOverview.setClient(projectOverview.getClient());
                electronicKitOverview.setStation(projectOverview.getStation());

                Log.d(TAG, "Actualizando datos de proyecto a kits electrónicos");
                Log.d(TAG, "Kit: " + electronicKitOverview.getKitNumber());
                Log.d(TAG, "Cliente: " + projectOverview.getClient());
                Log.d(TAG, "Estacion: " + projectOverview.getStation());
            } else {
                Log.d(TAG, "No coincide el id de proyecto con el registrado en el kit");
            }
        }

        showElecKitsListIfReady();
    }

    private void updateElectronicKitsOverview(UserData userData) {
        //Actualizar datos a kits electrónicos con el número de orden del proyecto que
        // entra al
        // método

        for (ElectronicKitOverview electronicKitOverview : electronicKitOverviews) {
            if (electronicKitOverview.getUserId().equals(userData.getId())) {
                electronicKitOverview.setUserInitials(userData.getInitials());
            }
        }

        showElecKitsListIfReady();
    }

    private void getUser(final String userId, ElectronicKitOverview elecKit) {
        UserData userData;

        //Si ya fue consultado el usuario
        if ((userData = isUserContained(userId)) != null) {
            //Actualizar elementos
            elecKit.setUserInitials(userData.getInitials());

            // Incrementar contador
            gotUsersCounter++;

            showElecKitsListIfReady();
        } else {
            //Obtenerlo de la base de datos
            FirebaseFirestore.getInstance().collection("usuarios").document(userId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    // Incrementar contador
                    gotUsersCounter++;

                    // Obtener respuesta
                    if (task.isSuccessful() && task.getResult() != null) {
                        //Obtener documento
                        DocumentSnapshot snapshot = task.getResult();

                        if (snapshot == null) {
                            Toast.makeText(ElectronicKitsActivity.this, "El usuario no " +
                                            "existe",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        //Crear objeto de usuario
                        UserData mUser = snapshot.toObject(UserData.class);

                        //Agregar a la lista de usuarios
                        savedUsers.add(mUser);

                        //Actualizar kits
                        updateElectronicKitsOverview(mUser);
                    } else {
                        Toast.makeText(ElectronicKitsActivity.this, "Error al obtener usuario" +
                                " " + userId, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private ProjectOverview isProjectContained(String projectId) {
        for (ProjectOverview projectOverview : projectsOverviews) {
            if (projectOverview.getOrderNumber().equals(projectId)) {
                return projectOverview;
            }
        }

        return null;
    }

    private UserData isUserContained(String userId) {
        for (UserData userData : savedUsers) {
            if (userData.getId().equals(userId)) {
                return userData;
            }
        }

        return null;
    }

    private void addSerialsToList(DocumentSnapshot snapshot) {
        for (Map.Entry<String, Object> entry : snapshot.getData().entrySet()) {
            //Validar si el elemento es un Map
            if (entry.getValue() instanceof HashMap && !entry.getKey().equals("pruebas")) {
                Map<String, Object> nestedMap = (Map) entry.getValue();

                //Obtener seriales
                for (Map.Entry<String, Object> nestedEntry : nestedMap.entrySet()) {
                    String serial = (String) nestedEntry.getValue();
                    if (serial != null && !serial.isEmpty()) {
                        //Si no esta vacío el dato, agregar a la lista
                        if (!serialNumbers.contains(serial) && serialNumbers.size() < 200)
                            serialNumbers.add(serial);
                    }
                }
            }

            if (entry.getKey().equals("mac_add")) {
                String macAddress = (String) entry.getValue();
                if (macAddress != null && !macAddress.isEmpty()) {
                    if (!macAddressList.contains(macAddress) && macAddressList.size() <= 100)
                        macAddressList.add(macAddress);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
        } else if (id == R.id.project_filter) {
            showFilterSelectorDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        //Obtener referencias de views
        floatingActionButton = findViewById(R.id.add_e_kit_button);
        noElectronicKitsTv = findViewById(R.id.no_electronic_kits_tv);
        progressBar = findViewById(R.id.progress_bar);
        recyclerView = findViewById(R.id.electronic_kits_list);

        //Mostrar título
        setTitle("Kits Electrónicos");

        //Evaluar permiso para crear kit electrónico
        if (!canModifyKits) {
            floatingActionButton.hide();
        } else {
            floatingActionButton.show();
        }

        //Definir listener
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ElectronicKitsActivity.this, ElectronicKitForm.class);
                intent.putExtra(ElectronicKitForm.SERIALS_SUGGESTION_LIST, serialNumbers);
                intent.putExtra(ElectronicKitForm.MAC_ADDRESS_SUGGESTION_LIST, macAddressList);
                startActivity(intent);
            }
        });

        //Definir adaptador
        adapter = new ElectronicKitOverviewAdapter(electronicKitOverviews);
        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = recyclerView.getChildAdapterPosition(v);

                //Obtener kit seleccionado
                String kitNumber = adapter.getEkit(position).getKitNumber();

                //Iniciar actividad para modificar kit
                Intent intent = new Intent(ElectronicKitsActivity.this, ElectronicKitForm.class);
                intent.putExtra(ElectronicKitForm.KIT_NUMBER, kitNumber);
                intent.putExtra(CAN_MODIFY_EKIT, canModifyKits);
                intent.putExtra(ElectronicKitForm.SERIALS_SUGGESTION_LIST, serialNumbers);
                intent.putExtra(ElectronicKitForm.MAC_ADDRESS_SUGGESTION_LIST, macAddressList);

                //Iniciar actividad
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter);

        //Definir layoutManager como Grid de 3 columnas
        recyclerView.setLayoutManager(new GridLayoutManager(this,
                getResources().getInteger(R.integer.grid_layout_columns)));
    }

    private void showElecKitsListIfReady() {
        // Validar si ya fueron recibidos todos los datos
        if (gotUsersCounter != electronicKitOverviews.size() || gotProjectsCounter != electronicKitOverviews.size()) {
            return;
        }

        // Evaluar si es por proyecto
        if (currentFilter.equals(PROJECT[0])) {
            Collections.sort(electronicKitOverviews, new Comparator<ElectronicKitOverview>() {
                @Override
                public int compare(ElectronicKitOverview o1, ElectronicKitOverview o2) {
                    return o1.getStation().compareTo(o2.getStation());
                }
            });
        }

        //Actualizar adaptador
        adapter.notifyDataSetChanged();

        //Hacer visible la lista
        noElectronicKitsTv.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void showFilterSelectorDialog() {
        if (filterSelectorDialog == null) {
            // Crear builder
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // Crear layout
            View content = getLayoutInflater().inflate(R.layout.dialog_ekit_filter_selector,
                    null);

            // Definir layout
            builder.setView(content);

            // Definir boton de regresar
            builder.setNegativeButton("Regresar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            // Crear diálogo
            filterSelectorDialog = builder.create();

            // Crear listener para los elementos
            View.OnClickListener filterSelected = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Obtener texto de radio button
                    RadioButton selectedButton = (RadioButton) v;
                    String text = selectedButton.getText().toString();

                    // Definir filtro actual
                    currentFilter = text;

                    // Obtener item seleccionado
                    if (text.equals(getResources().getString(R.string.fek_numero_kit))) {
                        query = eKitsCollection.orderBy(KIT_NUMBER[0], Query.Direction.DESCENDING);
                    } else if (text.equals(getResources().getString(R.string.fek_proyecto))) {
                        query = eKitsCollection.orderBy(PROJECT[0], Query.Direction.DESCENDING);
                    } else if (text.equals(getResources().getString(R.string.fek_fecha_modificacion))) {
                        query = eKitsCollection.orderBy(LAST_MODIF_DATE[0],
                                Query.Direction.DESCENDING);
                    }

                    // Cerar diálogo
                    filterSelectorDialog.dismiss();

                    // Ejecutar query
                    executeQuery();

                    // Ocultar lista
                    progressBar.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            };

            // Obtener layout y seleccionar el filtro actual
            RadioGroup list = content.findViewById(R.id.filters_radio_group);
            for (int i = 0; i < list.getChildCount(); i++) {
                View view = list.getChildAt(i);
                if (view instanceof RadioButton) {
                    // Hacer cast a RadioButton del view actual
                    RadioButton mRadioButton = (RadioButton) view;

                    // Definir clickListener
                    mRadioButton.setOnClickListener(filterSelected);
                }
            }
        }

        // Mostrar diálogo
        filterSelectorDialog.show();
    }
}
