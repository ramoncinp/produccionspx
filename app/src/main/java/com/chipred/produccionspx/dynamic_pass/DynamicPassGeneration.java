package com.chipred.produccionspx.dynamic_pass;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chipred.produccionspx.R;
import com.chipred.produccionspx.RegistersActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DynamicPassGeneration extends AppCompatActivity
{
    //Constantes
    public static final String TAG = DynamicPassGeneration.class.getSimpleName();
    public static final String USER_ID = "userId";
    public static final String CAN_READ_REGISTERS = "readRegisters";

    //Variables
    private boolean canReadRegisters = false;
    private String userId;

    //Objetos
    private PasswordDinamico passGenerator;

    //Views
    private Button generatePass;
    private CardView passCv;
    private MaterialEditText comments;
    private MaterialEditText macAdd1;
    private MaterialEditText macAdd2;
    private MaterialEditText macAdd3;
    private MaterialEditText macAdd4;
    private MaterialEditText macAdd5;
    private MaterialEditText macAdd6;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_pass_generation);

        //Escribir titulo
        setTitle(getResources().getString(R.string.dyn_pass_title));

        //Agregar flecha para regresar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Obtener id de usuario
        if (getIntent().hasExtra(USER_ID))
        {
            userId = getIntent().getStringExtra(USER_ID);
        }

        //Obtener permiso para leer registros
        if (getIntent().hasExtra(CAN_READ_REGISTERS))
        {
            canReadRegisters = getIntent().getBooleanExtra(CAN_READ_REGISTERS, true);
        }

        //Inicializar views
        initViews();

        //Definir listener para generar password
        generatePass = findViewById(R.id.generate_pass_button);
        generatePass.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                validateAndGenerate();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //Crear menu si es que tiene permisos
        if (canReadRegisters)
            getMenuInflater().inflate(R.menu.dyn_pass_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        switch (id)
        {
            case android.R.id.home:
                this.finish();
                break;

            case R.id.registers:
                Intent intent = new Intent(this, RegistersActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews()
    {
        //Obtener referencias de views
        macAdd1 = findViewById(R.id.mac1);
        macAdd2 = findViewById(R.id.mac2);
        macAdd3 = findViewById(R.id.mac3);
        macAdd4 = findViewById(R.id.mac4);
        macAdd5 = findViewById(R.id.mac5);
        macAdd6 = findViewById(R.id.mac6);
        comments = findViewById(R.id.comments);

        //Definir listeners
        macAdd1.addTextChangedListener(new GenericTextWatcher(macAdd1));
        macAdd2.addTextChangedListener(new GenericTextWatcher(macAdd2));
        macAdd3.addTextChangedListener(new GenericTextWatcher(macAdd3));
        macAdd4.addTextChangedListener(new GenericTextWatcher(macAdd4));
        macAdd5.addTextChangedListener(new GenericTextWatcher(macAdd5));
        macAdd6.addTextChangedListener(new GenericTextWatcher(macAdd6));
    }

    private void validateAndGenerate()
    {
        MaterialEditText station = findViewById(R.id.station);
        MaterialEditText hour = findViewById(R.id.hour);
        MaterialEditText day = findViewById(R.id.day);
        MaterialEditText month = findViewById(R.id.month);
        MaterialEditText year = findViewById(R.id.year);

        //Definir filtro
        METValidator emptyValidator = new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        };

        //Aplicar filtros
        boolean valid = station.validateWith(emptyValidator);
        valid &= hour.validateWith(emptyValidator);
        valid &= day.validateWith(emptyValidator);
        valid &= month.validateWith(emptyValidator);
        valid &= year.validateWith(emptyValidator);
        valid &= macAdd1.validateWith(emptyValidator);
        valid &= macAdd2.validateWith(emptyValidator);
        valid &= macAdd3.validateWith(emptyValidator);
        valid &= macAdd4.validateWith(emptyValidator);
        valid &= macAdd5.validateWith(emptyValidator);
        valid &= macAdd6.validateWith(emptyValidator);
        valid &= comments.validateWith(emptyValidator);

        //Salir si no fue válido
        if (!valid) return;

        //Crear instancia de generador de password
        if (passGenerator == null) passGenerator = new PasswordDinamico();

        //Obtener datos
        passGenerator.setHora(Integer.parseInt(hour.getText().toString()));
        passGenerator.setDia(Integer.parseInt(day.getText().toString()));
        passGenerator.setMes(Integer.parseInt(month.getText().toString()));
        passGenerator.setAnio(Integer.parseInt(year.getText().toString()));

        String macAddress = "";
        macAddress += macAdd1.getText().toString();
        macAddress += macAdd2.getText().toString();
        macAddress += macAdd3.getText().toString();
        macAddress += macAdd4.getText().toString();
        macAddress += macAdd5.getText().toString();
        macAddress += macAdd6.getText().toString();

        //Definir macAddress
        passGenerator.setMacAddress(macAddress);

        //Calcular password
        String thePassWord = String.format(Locale.getDefault(), "%.0f",
                (passGenerator.getPassword() / 100));

        //Mostrar password
        TextView passTv = findViewById(R.id.generated_pass_tv);
        passTv.setText(thePassWord);

        //Cambiar vistas
        setGeneratedViews();

        //Subir a base de datos
        addRegister();
    }

    private void setGeneratedViews()
    {
        //Mostrar password
        passCv = findViewById(R.id.generated_pass_cv);
        passCv.setVisibility(View.VISIBLE);

        //Cambiar botón de submit
        generatePass = findViewById(R.id.generate_pass_button);

        //Definir otro texto y color del botón
        generatePass.setText(getResources().getString(R.string.dyn_pass_generate_again));

        //Definir listener para generar otro código
        generatePass.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Definir otro texto y color del botón
                generatePass.setText(getResources().getString(R.string.dyn_pass_generate));

                //Ocultar password generada
                passCv.setVisibility(View.GONE);

                //Redefinir el listener
                generatePass.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        validateAndGenerate();
                    }
                });
            }
        });
    }

    private void addRegister()
    {
        //Obtener referencia de la colección
        CollectionReference registersCollection = FirebaseFirestore.getInstance().collection(
                "generacion_password");

        //Armar documento
        Map<String, Object> register = new HashMap<>();

        //Agregar fecha en milisegundos
        register.put("fecha", new Date().getTime());

        //Agregar id de usuario
        register.put("usuario", userId);

        //Agregar id de estacion
        MaterialEditText stationId = findViewById(R.id.station);
        register.put("id_estacion", stationId.getText().toString());

        //Agregar comentarios
        MaterialEditText comments = findViewById(R.id.comments);
        register.put("commentarios", comments.getText().toString());

        Log.d(TAG, "Agregando registro -> " + register.toString());

        //Subir registro a base de datos
        registersCollection.add(register).addOnCompleteListener(new OnCompleteListener<DocumentReference>()
        {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task)
            {
                if (task.isSuccessful())
                {
                    Log.d(TAG, "Registro agregado");
                }
                else
                {
                    Log.e(TAG, "Error al intentar subir registro");
                }
            }
        });
    }

    public class GenericTextWatcher implements TextWatcher
    {
        private View view;

        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable)
        {
            String text = editable.toString();
            switch (view.getId())
            {
                case R.id.mac1:
                    if (text.length() == 2)
                        macAdd2.requestFocus();
                    break;
                case R.id.mac2:
                    if (text.length() == 2)
                        macAdd3.requestFocus();
                    break;
                case R.id.mac3:
                    if (text.length() == 2)
                        macAdd4.requestFocus();
                    break;
                case R.id.mac4:
                    if (text.length() == 2)
                        macAdd5.requestFocus();
                    break;
                case R.id.mac5:
                    if (text.length() == 2)
                        macAdd6.requestFocus();
                    break;
                case R.id.mac6:
                    if (text.length() == 2)
                        comments.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
        {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
        {
        }
    }
}
