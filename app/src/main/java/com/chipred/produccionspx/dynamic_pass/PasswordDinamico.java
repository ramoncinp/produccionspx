package com.chipred.produccionspx.dynamic_pass;

import java.util.Locale;

public class PasswordDinamico
{
    private String macAddress;
    private int dia;
    private int mes;
    private int anio;
    private int hora;

    public PasswordDinamico()
    {
    }

    public void setMacAddress(String macAddress)
    {
        this.macAddress = macAddress.toUpperCase();
    }

    public void setDia(int dia)
    {
        this.dia = dia;
    }

    public void setMes(int mes)
    {
        this.mes = mes;
    }

    public void setAnio(int anio)
    {
        this.anio = anio;
    }

    public void setHora(int hora)
    {
        this.hora = hora;
    }

    public double getPassword()
    {
        //Crear variables para semillas
        char seed1 = 0;
        char seed2 = 0;
        char seed3 = 0;

        //Arreglo donde se almacena el password
        char[] passBytes = new char[4];

        //Convertir macAddress a arreglo de hexadecimales
        char[] macAddressArray = macStringToArray();

        //Formar semilla 1 y 2
        for (int i = 0; i < 6; i++)
        {
            if (i < 3)
            {
                seed1 = keyCRC(seed1, macAddressArray[i]);
            }
            else
            {
                seed2 = keyCRC(seed2, macAddressArray[i]);
            }
        }

        //Formar semilla 3
        seed3 = keyCRC(seed3, (char) dia);
        seed3 = keyCRC(seed3, (char) mes);
        seed3 = keyCRC(seed3, (char) anio);

        //Calcular password
        passBytes[0] = (char) ((seed1 ^ hora ^ dia) & 0xFF);
        passBytes[1] = (char) ((seed1 ^ hora ^ mes) & 0xFF);
        passBytes[2] = (char) ((seed2 ^ hora ^ anio) & 0xFF);
        passBytes[3] = (char) ((seed3 ^ hora ^ dia) & 0xFF);

        String passText1 = String.format(Locale.getDefault(), "%03d", (int) passBytes[0]);
        String passText2 = String.format(Locale.getDefault(), "%03d", (int) passBytes[1]);
        String passText3 = String.format(Locale.getDefault(), "%03d", (int) passBytes[2]);
        String passText4 = String.format(Locale.getDefault(), "%03d", (int) passBytes[3]);

        String command = "";
        command += passText1.charAt(1);
        command += passText1.charAt(2);
        command += passText2.charAt(1);
        command += passText2.charAt(2);
        command += passText3.charAt(1);
        command += passText3.charAt(2);
        command += passText4.charAt(1);
        command += passText4.charAt(2);
        command += 0;
        command += 0;

        double passToReturn = Double.parseDouble(command);

        //Si fue menor a ocho dígitos, ajustar
        if (passToReturn < 10000000)
        {
            passToReturn += 10000000;
        }

        return passToReturn;
    }

    private char keyCRC(char crc_c, char data)
    {
        int index;
        byte fb;

        index = 8;
        do
        {
            fb = (byte) ((crc_c ^ data) & 0x01);
            data >>= 1;
            crc_c >>= 1;
            if (fb > 0) crc_c ^= 0x8C;
        }
        while ((--index) > 0);

        return (char) (crc_c & 0xFF);
    }

    private char[] macStringToArray()
    {
        char[] bytesArray = new char[6];

        for (int i = 0, mi = 0; i < macAddress.length(); i += 2)
        {
            char byteValue = 0;

            //Obtener nibble alto (primer caracter del par)
            byteValue |= (charToValue(macAddress.charAt(i)) << 4);
            //Obtener bajo alto (segundo caracter del par)
            byteValue |= (charToValue(macAddress.charAt(i + 1)));

            //Agregar al arreglo
            bytesArray[mi++] = byteValue;
        }

        return bytesArray;
    }

    private char charToValue(char mChar)
    {
        //Si es un numero
        if (mChar >= '0' && mChar <= '9')
        {
            return (char) (mChar - 0x30);
        }
        else
        {
            return (char) (mChar - 0x37);
        }
    }
}
