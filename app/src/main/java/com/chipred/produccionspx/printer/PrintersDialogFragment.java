package com.chipred.produccionspx.printer;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.R;
import com.epson.eposprint.Print;
import com.epson.epsonio.DevType;
import com.epson.epsonio.EpsonIoException;
import com.epson.epsonio.FilterOption;
import com.epson.epsonio.Finder;
import com.epson.epsonio.IoStatus;

import java.util.ArrayList;

import static com.epson.epsonio.Finder.getDeviceInfoList;

public class PrintersDialogFragment extends DialogFragment
{
    //Constantes
    private static final String TAG = PrintersDialogFragment.class.getSimpleName();

    //Views
    private ConstraintLayout contentLayout;
    private LinearLayout buttonsLayout;
    private ProgressBar progressBar;
    private RecyclerView printersList;
    private TextView noPrinters;
    private TextView selectPrinter;

    //Objetos
    private Activity activity;
    private PrinterAdapter printerAdapter;

    //Interfaces
    private PrinterDialogInterface printerDialogInterface;

    public static PrintersDialogFragment newInstance(PrinterDialogInterface listener)
    {
        PrintersDialogFragment fragment = new PrintersDialogFragment();
        fragment.setPrinterDialogInterface(listener);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        //Obtener layout
        View view = inflater.inflate(R.layout.dialog_fragment_printers, null);

        //Referenciar views
        contentLayout = view.findViewById(R.id.content_layout);
        buttonsLayout = view.findViewById(R.id.buttons_layout);
        noPrinters = view.findViewById(R.id.no_printers_text);
        progressBar = view.findViewById(R.id.progress_bar);
        printersList = view.findViewById(R.id.printers_list);
        selectPrinter = view.findViewById(R.id.select_printer_text);

        //Inicializar adaptador
        printerAdapter = new PrinterAdapter(new ArrayList<>(), getContext());

        //Asignar adapter a recyclerView
        printersList.setAdapter(printerAdapter);
        printersList.setLayoutManager(new LinearLayoutManager(getContext()));

        //Asignar listener
        printerAdapter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Mostrar progreso
                showProgressBar();
                Toast.makeText(activity, "Imprimiendo...", Toast.LENGTH_SHORT).show();

                //Obtener impresora seleccionada e imprimir
                print(printerAdapter.getPrinters().get(printersList.getChildAdapterPosition(v)));
            }
        });

        //Agregar listener a botones
        Button searchButton = view.findViewById(R.id.search_button);
        searchButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                scannPrinters();
                showProgressBar();
            }
        });

        Button returnButton = view.findViewById(R.id.return_button);
        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getDialog().dismiss();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        //Obtener instancia del a actividad padre
        activity = getActivity();

        //Iniciar a buscar impresoras
        scannPrinters();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    private void showContent()
    {
        contentLayout.setVisibility(View.VISIBLE);
        buttonsLayout.setVisibility(View.VISIBLE);
        printersList.setVisibility(View.VISIBLE);
        selectPrinter.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        noPrinters.setVisibility(View.GONE);
    }

    public void showProgressBar()
    {
        contentLayout.setVisibility(View.GONE);
        buttonsLayout.setVisibility(View.GONE);
        printersList.setVisibility(View.GONE);
        selectPrinter.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        noPrinters.setVisibility(View.GONE);
    }

    private void showNoPrinters()
    {
        contentLayout.setVisibility(View.VISIBLE);
        buttonsLayout.setVisibility(View.VISIBLE);
        noPrinters.setVisibility(View.VISIBLE);
        selectPrinter.setVisibility(View.GONE);
        printersList.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void scannPrinters()
    {
        //Vaciar lista de impresoras
        printerAdapter.setPrinters(new ArrayList<Printer>());

        printersList.setVisibility(View.VISIBLE);
        noPrinters.setVisibility(View.GONE);
        Thread searchPrinters = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    search();
                    Thread.sleep(1000);
                    getResult();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                    stopDiscovery();
                }
            }
        });
        searchPrinters.start();
    }

    private void search()
    {
        int errStatus = IoStatus.SUCCESS;

        try
        {
            Finder.start(activity, DevType.TCP, "255.255.255.255");
        }
        catch (EpsonIoException e)
        {
            errStatus = e.getStatus();
        }

        if (errStatus == IoStatus.SUCCESS)
        {
            Log.d(TAG, "Iniciando escaneo...");
        }
        else
        {
            Log.d(TAG, "Hubo un problema al escanear");
        }
    }

    private void stopDiscovery()
    {
        try
        {
            Finder.stop();
            activity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    if (printerAdapter.getPrinters().size() != 0)
                    {
                        showContent();
                    }
                    else
                    {
                        showNoPrinters();
                    }
                }
            });
        }
        catch (EpsonIoException e)
        {
            e.printStackTrace();
            Log.d(TAG, "Error al detener escaneo");
        }
    }

    private void getResult()
    {
        com.epson.epsonio.DeviceInfo[] mList;

        //Get device list
        try
        {
            mList = getDeviceInfoList(FilterOption.PARAM_DEFAULT);
            for (com.epson.epsonio.DeviceInfo deviceInfo : mList)
            {
                Printer printer = new Printer();
                printer.setDeviceName(deviceInfo.getPrinterName());
                printer.setCurrentIp(deviceInfo.getIpAddress());
                printerAdapter.addPrinter(printer);
                Log.d(TAG, "Nueva impresora añadida");
            }
        }
        catch (EpsonIoException e)
        {
            Log.e(TAG, "Error al obtener impresora");
            Log.e(TAG, e.toString());
        }
        catch (NullPointerException e)
        {
            Log.e(TAG, "Error al obtener impresoras");
            Log.e(TAG, e.toString());
        }

        stopDiscovery();
    }

    private void print(Printer selectedPrinter)
    {
        Thread task = new Thread(() ->
        {
            //Retornar la ip a través de la interfaz
            printerDialogInterface.onIpSelected(selectedPrinter.getCurrentIp());
        });

        task.start();
    }

    public void setPrinterDialogInterface(PrinterDialogInterface printerDialogInterface)
    {
        this.printerDialogInterface = printerDialogInterface;
    }

    public interface PrinterDialogInterface
    {
        void onIpSelected(String ip);
    }
}
