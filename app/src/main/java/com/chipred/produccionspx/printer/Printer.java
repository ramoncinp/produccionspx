package com.chipred.produccionspx.printer;

public class Printer
{
    private String deviceName;
    private String currentIp;

    public Printer()
    {

    }

    public String getDeviceName()
    {
        return deviceName;
    }

    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    public String getCurrentIp()
    {
        return currentIp;
    }

    public void setCurrentIp(String currentIp)
    {
        this.currentIp = currentIp;
    }
}
