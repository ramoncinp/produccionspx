package com.chipred.produccionspx.printer;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class PrinterAdapter extends RecyclerView.Adapter<PrinterAdapter
        .PrinterViewHolder> implements View.OnClickListener
{
    private ArrayList<Printer> printers;
    private Context context;
    private View.OnClickListener listener;

    public PrinterAdapter(ArrayList<Printer> printers, Context context)
    {
        this.printers = printers;
        this.context = context;
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    @NonNull
    public PrinterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.printer_device_row,
                parent, false);

        PrinterViewHolder printerViewHolder = new PrinterViewHolder(v);
        v.setOnClickListener(this);

        return printerViewHolder;
    }

    void addPrinter(Printer printer)
    {
        printers.add(printer);
    }

    void setPrinters(ArrayList<Printer> printers)
    {
        this.printers = printers;
    }

    public ArrayList<Printer> getPrinters()
    {
        return printers;
    }

    @Override
    public void onBindViewHolder(@NonNull PrinterViewHolder holder, int position)
    {
        Printer printer = printers.get(position);

        holder.description.setText(printer.getDeviceName());
        holder.ipAddress.setText(printer.getCurrentIp());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount()
    {
        return printers.size();
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null) listener.onClick(view);
    }

    static class PrinterViewHolder extends RecyclerView.ViewHolder
    {
        private TextView description;
        private TextView ipAddress;

        PrinterViewHolder(View itemView)
        {
            super(itemView);

            description = itemView.findViewById(R.id.printer_description_tv);
            ipAddress = itemView.findViewById(R.id.printer_ip_address_tv);
        }
    }
}
