package com.chipred.produccionspx.printer;

import android.content.Context;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.projects.Contact;
import com.chipred.produccionspx.DispModel;
import com.chipred.produccionspx.projects.ProjectOverview;
import com.epson.epos2.Epos2CallbackCode;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

public class PrinterManager
{
    //Constantes
    private static final int PRINTTER_PAGE_WIDTH = 42;
    private static final String TAG = PrinterManager.class.getSimpleName();
    private static final String DEBUG_PRINTER = "DebugPrinter";

    //Variables
    private String ipAddress;

    //Objetos
    private Context context;
    private Printer printer;
    private PrinterListener printerListener;

    public interface PrinterListener
    {
        void onSuccesfulPrint(String msg);

        void onPrinterError(String msg);

        void onWarning(String msg);
    }

    public PrinterManager(Context context, String ipAddress, PrinterListener printerListener)
    {
        this.context = context;
        this.ipAddress = "TCP:" + ipAddress;
        this.printerListener = printerListener;

        //Inicializar impresora
        initPrinter();
    }

    private void initPrinter()
    {
        try
        {
            printer = new Printer(Printer.TM_T88, Printer.MODEL_ANK, context);
        }
        catch (Epos2Exception e)
        {
            e.printStackTrace();
            printerListener.onPrinterError("Error al inicializar impresora");
            return;
        }

        printer.setReceiveEventListener(new ReceiveListener()
        {
            @Override
            public void onPtrReceive(Printer printer, int i, PrinterStatusInfo printerStatusInfo,
                                     String s)
            {
                if (i == Epos2CallbackCode.CODE_SUCCESS)
                {
                    printerListener.onSuccesfulPrint("Impresión realizada correctamente");
                }
                else
                {
                    printerListener.onPrinterError("Error al imprimir");
                }

                new Thread(new Runnable()
                {
                    @Override
                    public synchronized void run()
                    {
                        abortProcess();
                    }
                }).start();
            }
        });
    }

    private void abortProcess()
    {
        try
        {
            printer.endTransaction();
            printer.disconnect();
            printer.clearCommandBuffer();
            printer.setReceiveEventListener(null);
        }
        catch (Epos2Exception e)
        {
            e.printStackTrace();
            printerListener.onPrinterError("Error al abortar proceso");
        }
    }

    public void printProyectInfo(Map<String, Object> projectRoot,
                                 ArrayList<DispModel> dispModels)
    {
        try
        {
            printer.addTextAlign(Printer.ALIGN_CENTER);
            printer.addTextSize(1, 1);
            setBoldText(true);

            //Escribir título
            String text = "ORDEN DE COMPRA DISPENSARIOS EVO6\n\n";
            printer.addText(text);

            //Obtener datos generales
            Map<String, Object> generalData = (Map) projectRoot.get("datos_generales");

            //ESCRIBIR DATOS
            printer.addTextAlign(Printer.ALIGN_CENTER);
            printer.addText("==========================================\n");
            printer.addText("DATOS GENERALES\n");
            printer.addText("==========================================\n");
            printer.addTextAlign(Printer.ALIGN_LEFT);
            setBoldText(false);

            //Datos generales
            addKeyValueText("Orden interna", (String) generalData.get("orden_interna"));
            addPairKeyValueText("Fecha solicitud", "Fecha terminado", (String) generalData.get(
                    "fecha_solicitud_cliente"), (String) generalData.get("fecha_terminado"));
            addPairKeyValueText("Fecha embarque", "Fecha arranque", (String) generalData.get(
                    "fecha_embarque"), (String) generalData.get("fecha_arranque"));
            addPairKeyValueText("Cliente", "Estacion", (String) generalData.get(
                    "cliente"), (String) generalData.get("estacion"));
            addKeyValueText("Numero de estacion", (String) generalData.get("numero_estacion"));

            //Contactos
            //Obtener objetos
            Contact mainContact = new Contact();
            Contact techContact = new Contact();

            // Asignarles informacion
            mainContact.setNombre((String) ((Map) generalData.get("contacto")).get("nombre"));
            mainContact.setPhone((String) ((Map) generalData.get("contacto")).get("phone"));
            mainContact.setEmail((String) ((Map) generalData.get("contacto")).get("email"));
            techContact.setNombre((String) ((Map) generalData.get("contacto_tecnico")).get(
                    "nombre"));
            techContact.setPhone((String) ((Map) generalData.get("contacto_tecnico")).get("phone"));
            techContact.setEmail((String) ((Map) generalData.get("contacto_tecnico")).get("email"));

            //Escribir info de contactos
            setBoldText(true);
            printer.addText("Contacto\n");
            setBoldText(false);
            printer.addText(removeSpecialChars(mainContact.getNombre()));
            printer.addFeedLine(1);
            printer.addText(mainContact.getEmail());
            printer.addFeedLine(1);
            printer.addText(mainContact.getPhone());
            printer.addFeedLine(2);

            setBoldText(true);
            printer.addText("Contacto tecnico\n");
            setBoldText(false);
            printer.addText(techContact.getNombre());
            printer.addFeedLine(1);
            printer.addText(techContact.getEmail());
            printer.addFeedLine(1);
            printer.addText(techContact.getPhone());
            printer.addFeedLine(2);

            //DISPENSARIOS
            setBoldText(true);
            printer.addTextAlign(Printer.ALIGN_CENTER);
            printer.addText("==========================================\n");
            printer.addText("DISPENSARIOS\n");
            printer.addText("==========================================\n");
            printer.addTextAlign(Printer.ALIGN_LEFT);
            setBoldText(false);

            //Mostrar datos de cada dispensario
            for (int i = 0; i < dispModels.size(); i += 2)
            {
                //Validar si "i" no es el último índice
                //Si sí lo es, imprimir individual y ni por pares
                if (i + 1 == dispModels.size())
                {
                    DispModel disp1 = dispModels.get(i);
                    setBoldText(true);
                    printer.addText(disp1.getSae() + "\n");
                    setBoldText(false);
                    printer.addText(disp1.getSerialNumber() + "\n");
                    printer.addText(disp1.getModel() + "\n");
                    printer.addText(disp1.getManufactureDate() + "\n");
                    printer.addText(disp1.getProducts() + "\n");
                    String saeStatusString = disp1.isSaeOk() ? "SAE concluido\n" : "\n";
                    printer.addText(saeStatusString);
                }
                else
                {
                    DispModel disp1 = dispModels.get(i);
                    DispModel disp2 = dispModels.get(i + 1);
                    setBoldText(true);
                    printer.addText(String.format(Locale.getDefault(), "%1$-21s%2$-21s\n",
                            disp1.getSae(), disp2.getSae()));
                    setBoldText(false);
                    printer.addText(String.format(Locale.getDefault(), "%1$-21s%2$-21s\n",
                            disp1.getSerialNumber(), disp2.getSerialNumber()));
                    printer.addText(String.format(Locale.getDefault(), "%1$-21s%2$-21s\n",
                            disp1.getModel(), disp2.getModel()));
                    printer.addText(String.format(Locale.getDefault(), "%1$-21s%2$-21s\n",
                            disp1.getManufactureDate(), disp2.getManufactureDate()));
                    printer.addText(String.format(Locale.getDefault(), "%1$-21s%2$-21s\n",
                            disp1.getProducts(), disp2.getProducts()));
                    String saeStatusString1 = disp1.isSaeOk() ? "SAE concluido" : "";
                    String saeStatusString2 = disp2.isSaeOk() ? "SAE concluido" : "";
                    printer.addText(String.format(Locale.getDefault(), "%1$-21s%2$-21s\n",
                            saeStatusString1, saeStatusString2));
                }
                printer.addFeedLine(1);
            }

            //ACCESORIOS
            setBoldText(true);
            printer.addFeedLine(1);
            printer.addTextAlign(Printer.ALIGN_CENTER);
            printer.addText("==========================================\n");
            printer.addText("ACCESORIOS EVO6\n");
            printer.addText("==========================================\n");
            printer.addTextAlign(Printer.ALIGN_LEFT);
            setBoldText(false);

            Map<String, Object> accesories = (Map) projectRoot.get("accesorios_embarque");
            Map<String, Object> evo6Components = (Map) projectRoot.get("componentes_evo6");
            if (accesories != null)
            {
                printer.addText("Tags despachadores verdes - ");
                printer.addText(String.valueOf((accesories.get("tags_verdes"))));
                printer.addText("\nTags local azules         - ");
                printer.addText(String.valueOf((accesories.get("tags_azules"))));
                printer.addText("\nTags jarreo rojos         - ");
                printer.addText(String.valueOf((accesories.get("tags_rojos"))));
                printer.addText("\nTapas negras              - ");
                printer.addText(String.valueOf((accesories.get("tapas_negras"))));
                printer.addText("\nCopias NOM                - ");
                printer.addText(String.valueOf((accesories.get("nom_copies"))));
            }

            if (evo6Components != null)
            {
                String retornos;
                String tablero;

                retornos = (String) evo6Components.get("caja_retornos");
                tablero = (String) evo6Components.get("tablero_electrico");

                if (retornos != null && !retornos.equals(""))
                {
                    printer.addText("\nCaja retornos             - ");
                    printer.addText(retornos);
                }

                if (tablero != null && !tablero.equals(""))
                {
                    printer.addText("\nTablero electrico         - ");
                    printer.addText(tablero);
                }
            }

            printer.addFeedLine(2);

            //Imprimir fecha
            String date = Constants.dateObjectToString2(new Date());
            printer.addFeedLine(Printer.ALIGN_CENTER);
            printer.addText(date);
            printer.addFeedLine(2);
            printer.addCut(Printer.CUT_FEED);

            //Imprimir
            connectAndSend();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir información de proyecto");
        }
    }

    public void printProyectsList(String filter, ArrayList<ProjectOverview> projectOverviews,
                                  HashMap<String, HashSet<DispModel>> dispData)
    {
        try
        {
            printer.addTextAlign(Printer.ALIGN_CENTER);
            printer.addTextSize(1, 1);
            setBoldText(true);

            //Escribir título
            String text = "LISTA DE PROYECTOS\n\n";
            printer.addText(text);

            //Escribir filtro actual
            printer.addTextAlign(Printer.ALIGN_CENTER);
            printer.addText("==========================================\n");
            printer.addText("FILTRO: " + filter + "\n");
            printer.addText("==========================================\n");
            printer.addTextAlign(Printer.ALIGN_LEFT);
            setBoldText(false);
            printer.addFeedLine(1);

            // Imprimir proyectos
            for (ProjectOverview projectOverview : projectOverviews)
            {
                //Orden interna, estacion y cliente
                printer.addText(String.format(Locale.getDefault(), "%1$-6s",
                        projectOverview.getOrderNumber()));

                //Definir texto en negritas
                setBoldText(true);
                printer.addText(String.format(Locale.getDefault(), "%1$-22s\n",
                        projectOverview.getStation()));

                //Ticket CRM y cliente
                setBoldText(true);
                printer.addText(String.format(Locale.getDefault(), "%1$-6s",
                        projectOverview.getCrmTicket()));

                //Definir texto normal
                setBoldText(false);
                printer.addText(String.format(Locale.getDefault(), "%1$-14s\n",
                        projectOverview.getClient()));

                setBoldText(false);
                printer.addText(String.format(Locale.getDefault(), "%1$-28s%2$-14s\n",
                        "      Producion: ",
                        projectOverview.getStatus()));

                printer.addText(String.format(Locale.getDefault(), "%1$-28s%2$-14s\n",
                        "      Administracion: ",
                        projectOverview.getAdminStatus()));

                printer.addText(String.format(Locale.getDefault(), "%1$-28s%2$-14s\n",
                        "      Proyecto: ",
                        projectOverview.getProjectStatus()));

                // Validar si hay información de dispensarios
                if (dispData.containsKey(projectOverview.getOrderNumber()))
                {
                    // Obtener los dispensarios del proyecto correspondiente
                    HashSet<DispModel> dispModels = dispData.get(projectOverview.getOrderNumber());
                    if (dispModels != null)
                    {
                        // Contar los modelos
                        HashMap<String, Integer> modelsMap = new HashMap<>();
                        for (DispModel dispModel : dispModels)
                        {
                            // Si ya esta este modelo, sumar la cuenta
                            if (modelsMap.containsKey(dispModel.getModel()))
                            {
                                int val = modelsMap.get(dispModel.getModel());
                                modelsMap.put(dispModel.getModel(), val + 1);
                            }
                            else // Si no, agregar el modelo con valor de 1
                            {
                                modelsMap.put(dispModel.getModel(), 1);
                            }
                        }

                        for (String key : modelsMap.keySet())
                        {
                            printer.addText(String.format(Locale.getDefault(), "      " +
                                    "%1$-2d%2$-20s\n", modelsMap.get(key), key));
                        }
                        printer.addFeedLine(1);
                    }
                }
            }

            //Imprimir fecha
            String date = Constants.dateObjectToString2(new Date());
            printer.addFeedLine(Printer.ALIGN_CENTER);
            printer.addText(date);
            printer.addFeedLine(2);
            printer.addCut(Printer.CUT_FEED);

            //Imprimir
            connectAndSend();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir lista de proyectos");
        }
    }

    private void addKeyValueText(String key, String value)
    {
        try
        {
            setBoldText(true);
            printer.addText(key);
            printer.addFeedLine(1);
            setBoldText(false);
            printer.addText(removeSpecialChars(value));
            printer.addFeedLine(2);
        }
        catch (Epos2Exception e)
        {
            e.printStackTrace();
        }
    }

    private void addPairKeyValueText(String key1, String key2, String value1, String value2)
    {
        try
        {
            setBoldText(true);
            printer.addText(String.format(Locale.getDefault(), "%1$-21s%2$-21s",
                    removeSpecialChars(key1), removeSpecialChars(key2)));
            printer.addFeedLine(1);
            setBoldText(false);

            if (value1 == null) value1 = "";
            if (value2 == null) value2 = "";

            printer.addText(String.format(Locale.getDefault(), "%1$-21s%2$-21s",
                    removeSpecialChars(value1), removeSpecialChars(value2)));
            printer.addFeedLine(2);
        }
        catch (Epos2Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setBoldText(boolean boldText)
    {
        try
        {
            if (boldText)
            {
                printer.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.TRUE, Printer
                        .PARAM_DEFAULT);
            }
            else
            {
                printer.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer
                        .PARAM_DEFAULT);
            }
        }
        catch (Epos2Exception e)
        {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir");
        }
    }

    private static String removeSpecialChars(String input)
    {
        // Cadena de caracteres original a sustituir.
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        // Cadena de caracteres ASCII que reemplazarán los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i = 0; i < original.length(); i++)
        {
            // Reemplazamos los caracteres especiales.
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }//for i
        return output;
    }

    private void connectAndSend()
    {
        //Conectarse a la impresora
        try
        {
            printer.connect(ipAddress, Printer.PARAM_DEFAULT);
            printer.beginTransaction();
        }
        catch (Epos2Exception e)
        {
            e.printStackTrace();
            printerListener.onPrinterError("Error al tratar de imprimir");
            return;
        }

        PrinterStatusInfo statusInfo = printer.getStatus();
        if (statusInfo.getConnection() != 0 && statusInfo.getOnline() != 0)
        {
            try
            {
                printer.sendData(Printer.PARAM_DEFAULT);
            }
            catch (Epos2Exception e)
            {
                printerListener.onPrinterError("Error al enviar a impresora");
            }
        }
        else
        {
            printerListener.onPrinterError("Error al imprimir");
        }
    }
}