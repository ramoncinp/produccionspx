package com.chipred.produccionspx.users;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.chipred.produccionspx.R;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class UsersActivity extends AppCompatActivity
{
    // Objetos
    private ArrayList<UserData> users;
    private CollectionReference usersCollection;

    // Views
    private ProgressBar progressBar;
    private RecyclerView usersList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Mostrar título
        setTitle("Usuarios");

        // Obtener views
        progressBar = findViewById(R.id.progress_bar);
        usersList = findViewById(R.id.users_list);

        // Obtener referencia de coleccion
        usersCollection = FirebaseFirestore.getInstance().collection("usuarios");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        usersCollection.orderBy("name", Query.Direction.ASCENDING).addSnapshotListener(this,
                new EventListener<QuerySnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e)
                    {
                        if (queryDocumentSnapshots != null)
                        {
                            // Obtener usuarios
                            if (users == null) users = new ArrayList<>();
                            else users.clear();

                            for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments())
                            {
                                // Obtener usuario
                                UserData user = snapshot.toObject(UserData.class);

                                // Agregar a la lista
                                if (user != null) users.add(user);
                            }

                            // Mostrar datos
                            renderUsersList();
                        }
                    }
                });
    }

    private void renderUsersList()
    {
        // Crear adaptador
        UserAdapter userAdapter = new UserAdapter(this, false);
        userAdapter.setUsers(users);
        userAdapter.setClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Crear intent
                Intent intent = new Intent(UsersActivity.this, EditUserActivity.class);

                // Obtener cadena de usuario
                String userString = users.get(usersList.getChildAdapterPosition(v)).toJsonString();

                // Adjuntar usuario a modificar
                intent.putExtra("user", userString);

                // Iniciar actividad
                startActivity(intent);
            }
        });

        // Asignar a la lista
        usersList.setAdapter(userAdapter);
        usersList.setLayoutManager(new LinearLayoutManager(this));

        // Mostrar views
        progressBar.setVisibility(View.GONE);
        usersList.setVisibility(View.VISIBLE);
    }
}
