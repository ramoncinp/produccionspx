package com.chipred.produccionspx.users;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.R;

public class ProfilePictureActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_picture);

        // Esconder Action Bar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().hide();
        }

        // Obtener views
        ImageView userImage = findViewById(R.id.user_image_view);
        ImageView arrowBackButton = findViewById(R.id.arrow_back_button);

        // Definir listener para salir de actividad
        arrowBackButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        // Obtener path o Url de la imagen
        String image = getIntent().getStringExtra("imagen");

        // Validar imagen
        if (image != null && !image.isEmpty())
        {
            Glide.with(this).load(image).dontAnimate().into(userImage);
        }
    }
}
