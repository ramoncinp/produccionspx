package com.chipred.produccionspx.users;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.cars.Driver;

import java.util.ArrayList;

public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.UserViewHolder> implements View.OnClickListener
{
    private ArrayList<Driver> drivers;
    private Context context;
    private View.OnClickListener clickListener;

    public DriverAdapter(Context context)
    {
        this.context = context;
    }

    public void setDrivers(ArrayList<Driver> drivers)
    {
        this.drivers = drivers;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_layout, parent,
                false);

        UserViewHolder userViewHolder = new UserViewHolder(v);
        v.setOnClickListener(clickListener);

        return userViewHolder;
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position)
    {
        // Obtener usuario
        Driver driver = drivers.get(position);

        // Setear datos
        holder.name.setText(driver.getName());

        // Mostrar imagen
        if (driver.getPhotoUrl() == null || driver.getPhotoUrl().isEmpty())
        {
            Glide.with(context).load(context.getDrawable(R.drawable.ic_binarium)).dontAnimate
                    ().into(holder.image);
        }
        else
        {
            Glide.with(context).load(driver.getPhotoUrl()).dontAnimate().into(holder.image);
        }
    }

    @Override
    public int getItemCount()
    {
        return drivers.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView image;
        private TextView name;

        UserViewHolder(@NonNull View itemView)
        {
            super(itemView);
            image = itemView.findViewById(R.id.user_image);
            name = itemView.findViewById(R.id.user_name);
        }
    }
}
