package com.chipred.produccionspx.users;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.cars.Driver;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Collections;

public class SelectDriverFragment extends DialogFragment
{
    // Views
    private ConstraintLayout contentLayout;
    private ProgressBar progressBar;
    private RecyclerView usersList;

    // Objetos
    private ArrayList<Driver> drivers;
    private CollectionReference usersReference;
    private SelectDriverInterface selectDriverInterface;

    public SelectDriverFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View content = inflater.inflate(R.layout.fragment_add_user, container, false);
        contentLayout = content.findViewById(R.id.content_layout);
        progressBar = content.findViewById(R.id.progress_bar);
        usersList = content.findViewById(R.id.users_list);

        // Inflate the layout for this fragment
        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        getDrivers();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    public void setSelectDriverInterface(SelectDriverInterface selectDriverInterface)
    {
        this.selectDriverInterface = selectDriverInterface;
    }

    private void getDrivers()
    {
        usersReference = FirebaseFirestore.getInstance().collection("usuarios");
        usersReference.whereArrayContains("allowedBlocks", Constants.VEHICLES_USER).get().addOnCompleteListener(task ->
        {
            if (task.isSuccessful() && task.getResult() != null)
            {
                // Crear lista
                drivers = new ArrayList<>();

                // Iterar en lista de resultados
                for (DocumentSnapshot document : task.getResult())
                {
                    // Crear usuario
                    Driver driver = document.toObject(Driver.class);

                    // Agregar a la lista
                    drivers.add(driver);
                }

                // Ordenar alfabéticamente
                Collections.sort(drivers, (driver, t1) -> driver.getName().compareTo(t1.getName()));

                // Definir adaptador
                DriverAdapter adapter = new DriverAdapter(getActivity());
                adapter.setDrivers(drivers);
                adapter.setClickListener(view ->
                {
                    // Obtener usuario
                    Driver selectedDriver = drivers.get(usersList.getChildAdapterPosition(view));

                    // Retornar conductor
                    selectDriverInterface.onDriverSelected(selectedDriver);

                    // Cerrar diálogo
                    if (getDialog() != null)
                    {
                        getDialog().dismiss();
                    }
                });
                usersList.setAdapter(adapter);
                usersList.setLayoutManager(new LinearLayoutManager(getActivity()));

                // Mostrar views
                contentLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al obtener usuarios",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface SelectDriverInterface
    {
        void onDriverSelected(Driver selectedDriver);
    }
}
