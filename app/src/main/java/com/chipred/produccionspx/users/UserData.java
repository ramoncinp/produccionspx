package com.chipred.produccionspx.users;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserData
{
    private String id;
    private String name;
    private String initials;
    private String charge;
    private String photoUrl;

    private ArrayList<Integer> allowedBlocks;

    public UserData()
    {

    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getInitials()
    {
        return initials;
    }

    public void setInitials(String initials)
    {
        this.initials = initials;
    }

    public ArrayList<Integer> getAllowedBlocks()
    {
        return allowedBlocks;
    }

    public void setAllowedBlocks(ArrayList<Integer> allowedBlocks)
    {
        this.allowedBlocks = allowedBlocks;
    }

    public String getPhotoUrl()
    {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl)
    {
        this.photoUrl = photoUrl;
    }

    public String getCharge()
    {
        if (charge == null) return "";
        else return charge;
    }

    public void setCharge(String charge)
    {
        this.charge = charge;
    }

    public String toJsonString()
    {
        // Crear objeto JSON
        JSONObject jsonObject = new JSONObject();

        // Definir datos
        try
        {
            // Crear arreglo de permisos
            JSONArray permissions = new JSONArray();
            for (Integer permission : allowedBlocks)
            {
                permissions.put(permission);
            }

            // Pasar datos
            jsonObject.put("allowedBlocks", permissions);
            jsonObject.put("charge", charge);
            jsonObject.put("id", id);
            jsonObject.put("initials", initials);
            jsonObject.put("name", name);
            jsonObject.put("photoUrl", photoUrl);

            // Convertir objeto a String
            return jsonObject.toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return "";
    }

    public boolean fromJsonString(String jsonString)
    {
        try
        {
            // Convertir cadena a JSON
            JSONObject jsonObject = new JSONObject(jsonString);

            // Definir datos a la clase

            // Obtener arreglo de permisos
            JSONArray permissions = jsonObject.getJSONArray("allowedBlocks");
            allowedBlocks = new ArrayList<>();

            // Iterar en arreglo JSON
            for (int i = 0; i < permissions.length(); i++)
            {
                allowedBlocks.add(permissions.getInt(i));
            }

            // Setear los demás datos
            charge = jsonObject.getString("charge");
            id = jsonObject.getString("id");
            initials = jsonObject.getString("initials");
            name = jsonObject.getString("name");
            photoUrl = jsonObject.getString("photoUrl");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
