package com.chipred.produccionspx.users;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Collections;

public class AddDriverFragment extends DialogFragment
{
    // Views
    private ConstraintLayout contentLayout;
    private ProgressBar progressBar;
    private RecyclerView usersList;

    // Objetos
    private ArrayList<UserData> users;
    private CollectionReference usersReference;

    public AddDriverFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View content = inflater.inflate(R.layout.fragment_add_user, container, false);
        contentLayout = content.findViewById(R.id.content_layout);
        progressBar = content.findViewById(R.id.progress_bar);
        usersList = content.findViewById(R.id.users_list);

        // Inflate the layout for this fragment
        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        getUsers();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    private void getUsers()
    {
        usersReference = FirebaseFirestore.getInstance().collection("usuarios");
        usersReference.get().addOnCompleteListener(task ->
        {
            if (task.isSuccessful() && task.getResult() != null)
            {
                // Crear lista
                users = new ArrayList<>();

                // Iterar en lista de resultados
                for (DocumentSnapshot document : task.getResult())
                {
                    // Crear usuario
                    UserData userData = document.toObject(UserData.class);

                    // Agregar a la lista
                    users.add(userData);
                }

                // Ordenar alfabéticamente
                Collections.sort(users, (userData, t1) -> userData.getName().compareTo(t1.getName()));

                // Definir adaptador
                UserAdapter adapter = new UserAdapter(getActivity(), true);
                adapter.setUsers(users);
                adapter.setClickListener(view ->
                {
                    // Obtener usuario
                    UserData selectedUser =
                            users.get(usersList.getChildAdapterPosition(view));

                    if (!selectedUser.getAllowedBlocks().contains(Constants.VEHICLES_USER))
                    {
                        // Agregar usuario
                        addDriver(selectedUser);
                    }
                });
                usersList.setAdapter(adapter);
                usersList.setLayoutManager(new LinearLayoutManager(getActivity()));

                // Mostrar views
                contentLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
            else
            {
                Toast.makeText(getActivity(), "Error al obtener usuarios",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addDriver(UserData userData)
    {
        // Ocultar contenido
        contentLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        // Agregar permiso para ser conductor
        userData.getAllowedBlocks().add(Constants.VEHICLES_USER);

        // Actualizar en la nube
        usersReference.document(userData.getId()).set(userData).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if (task.isSuccessful())
                {
                    Toast.makeText(getContext(), "Conductor agregado correctamente",
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getContext(), "Error al agregar conductor",
                            Toast.LENGTH_SHORT).show();
                }
                if (getDialog() != null) getDialog().dismiss();
            }
        });
    }
}
