package com.chipred.produccionspx.users;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> implements View.OnClickListener
{
    private boolean evaluateCarUser;
    private ArrayList<UserData> users;
    private Context context;
    private View.OnClickListener clickListener;

    public UserAdapter(Context context, boolean evaluateCarUser)
    {
        this.context = context;
        this.evaluateCarUser = evaluateCarUser;
    }

    public void setUsers(ArrayList<UserData> users)
    {
        this.users = users;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_layout, parent,
                false);

        UserViewHolder userViewHolder = new UserViewHolder(v);
        v.setOnClickListener(clickListener);

        return userViewHolder;
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position)
    {
        // Obtener usuario
        UserData userData = users.get(position);

        // Setear datos
        holder.name.setText(userData.getName());

        // Mostrar imagen
        if (userData.getPhotoUrl() == null || userData.getPhotoUrl().isEmpty())
        {
            Glide.with(context).load(context.getDrawable(R.drawable.ic_binarium)).dontAnimate
                    ().into(holder.image);
        }
        else
        {
            Glide.with(context).load(userData.getPhotoUrl()).dontAnimate().into(holder.image);
        }

        if (evaluateCarUser)
        {
            // Modificar alpha de views dependiendo si ya estan registrados o no
            if (userData.getAllowedBlocks().contains(Constants.VEHICLES_USER))
            {
                holder.name.setAlpha(0.5f);
                holder.image.setAlpha(0.5f);
            }
            else
            {
                holder.name.setAlpha(1f);
                holder.image.setAlpha(1f);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return users.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView image;
        private TextView name;

        UserViewHolder(@NonNull View itemView)
        {
            super(itemView);
            image = itemView.findViewById(R.id.user_image);
            name = itemView.findViewById(R.id.user_name);
        }
    }
}
