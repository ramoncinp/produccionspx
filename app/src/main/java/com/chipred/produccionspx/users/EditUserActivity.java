package com.chipred.produccionspx.users;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;

public class EditUserActivity extends AppCompatActivity
{
    // Switchs
    private Switch readProjects;
    private Switch writeProjects;
    private Switch listenProjects;
    private Switch readEkits;
    private Switch writeEkits;
    private Switch userDynPass;
    private Switch dynPassRegisters;
    private Switch readReports;
    private Switch listenProdAlarms;
    private Switch listenAdminAlarms;
    private Switch readVersions;
    private Switch writeVersions;
    private Switch vehiclesUser;
    private Switch vehiclesAdmin;
    private Switch vehiclesLocation;

    private UserData user;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        // Mostrar flecha para regresar en ActionBar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Inicializar views
        ImageView image = findViewById(R.id.user_image);
        TextView name = findViewById(R.id.username_tv);

        // Inicializar switchs
        readProjects = findViewById(R.id.read_projects_sw);
        writeProjects = findViewById(R.id.write_projects_sw);
        listenProjects = findViewById(R.id.notifications_projects_sw);
        readEkits = findViewById(R.id.read_ekits_sw);
        writeEkits = findViewById(R.id.write_ekits_sw);
        userDynPass = findViewById(R.id.use_dyn_pass_sw);
        dynPassRegisters = findViewById(R.id.read_dyn_pass_registers_sw);
        readReports = findViewById(R.id.read_reports_sw);
        listenProdAlarms = findViewById(R.id.prod_alarms_sw);
        listenAdminAlarms = findViewById(R.id.admin_alarms_sw);
        readVersions = findViewById(R.id.read_versions_sw);
        writeVersions = findViewById(R.id.write_versions_sw);
        vehiclesUser = findViewById(R.id.vehicles_user_sw);
        vehiclesAdmin = findViewById(R.id.vehicles_admin_sw);
        vehiclesLocation = findViewById(R.id.vehicles_location_sw);

        // Obtener usuario
        String userString = getIntent().getStringExtra("user");
        user = new UserData();
        user.fromJsonString(userString);

        // Mostrar informacion
        if (user.getPhotoUrl() != null && !user.getPhotoUrl().isEmpty())
        {
            Glide.with(this).load(user.getPhotoUrl()).dontAnimate().into(image);
        }
        name.setText(user.getName());

        // Inicializar switches
        setSwitchesValues();
        setSwitchListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;

            case R.id.save:
                updateUser();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setSwitchesValues()
    {
        // Obtener lista
        readProjects.setChecked(user.getAllowedBlocks().contains(Constants.READ_PROJECTS));
        writeProjects.setChecked(user.getAllowedBlocks().contains(Constants.WRITE_PROJECTS));
        listenProjects.setChecked(user.getAllowedBlocks().contains(Constants.LISTEN_PROJECTS));
        readEkits.setChecked(user.getAllowedBlocks().contains(Constants.READ_EKITS));
        writeEkits.setChecked(user.getAllowedBlocks().contains(Constants.WRITE_EKITS));
        userDynPass.setChecked(user.getAllowedBlocks().contains(Constants.USE_PASS_GENERATOR));
        dynPassRegisters.setChecked(user.getAllowedBlocks().contains(Constants.READ_PASS_REGISTERS));
        readReports.setChecked(user.getAllowedBlocks().contains(Constants.READ_REPORTS));
        listenProdAlarms.setChecked(user.getAllowedBlocks().contains(Constants.LISTEN_PROJECT_ALARM_PROD));
        listenAdminAlarms.setChecked(user.getAllowedBlocks().contains(Constants.LISTEN_PROJECT_ALARM_ADMN));
        readVersions.setChecked(user.getAllowedBlocks().contains(Constants.READ_VERSIONS));
        writeVersions.setChecked(user.getAllowedBlocks().contains(Constants.WRITE_VERSIONS));
        vehiclesUser.setChecked(user.getAllowedBlocks().contains(Constants.VEHICLES_USER));
        vehiclesAdmin.setChecked(user.getAllowedBlocks().contains(Constants.VEHICLES_ADMIN));
        vehiclesLocation.setChecked(user.getAllowedBlocks().contains(Constants.VEHICLES_LOCATION));
    }

    private void setSwitchListeners()
    {
        CompoundButton.OnCheckedChangeListener onCheckedChangeListener =
                (buttonView, isChecked) ->
                {
                    Integer permission = null;

                    switch (buttonView.getId())
                    {
                        case R.id.read_projects_sw:
                            permission = Constants.READ_PROJECTS;
                            break;

                        case R.id.write_projects_sw:
                            permission = Constants.WRITE_PROJECTS;
                            break;

                        case R.id.notifications_projects_sw:
                            permission = Constants.LISTEN_PROJECTS;
                            break;

                        case R.id.read_ekits_sw:
                            permission = Constants.READ_EKITS;
                            break;

                        case R.id.write_ekits_sw:
                            permission = Constants.WRITE_EKITS;
                            break;

                        case R.id.use_dyn_pass_sw:
                            permission = Constants.USE_PASS_GENERATOR;
                            break;

                        case R.id.read_dyn_pass_registers_sw:
                            permission = Constants.READ_PASS_REGISTERS;
                            break;

                        case R.id.read_reports_sw:
                            permission = Constants.READ_REPORTS;
                            break;

                        case R.id.prod_alarms_sw:
                            permission = Constants.LISTEN_PROJECT_ALARM_PROD;
                            break;

                        case R.id.admin_alarms_sw:
                            permission = Constants.LISTEN_PROJECT_ALARM_ADMN;
                            break;

                        case R.id.read_versions_sw:
                            permission = Constants.READ_VERSIONS;
                            break;

                        case R.id.write_versions_sw:
                            permission = Constants.WRITE_VERSIONS;
                            break;

                        case R.id.vehicles_user_sw:
                            permission = Constants.VEHICLES_USER;
                            break;

                        case R.id.vehicles_admin_sw:
                            permission = Constants.VEHICLES_ADMIN;
                            break;

                        case R.id.vehicles_location_sw:
                            permission = Constants.VEHICLES_LOCATION;
                            break;
                    }

                    // Modificar permisos del usuario
                    if (permission != null)
                        addOrRemovePermission(isChecked, permission);
                };

        // Definir listeners a switches
        readProjects.setOnCheckedChangeListener(onCheckedChangeListener);
        writeProjects.setOnCheckedChangeListener(onCheckedChangeListener);
        listenProjects.setOnCheckedChangeListener(onCheckedChangeListener);
        readEkits.setOnCheckedChangeListener(onCheckedChangeListener);
        writeEkits.setOnCheckedChangeListener(onCheckedChangeListener);
        userDynPass.setOnCheckedChangeListener(onCheckedChangeListener);
        dynPassRegisters.setOnCheckedChangeListener(onCheckedChangeListener);
        readReports.setOnCheckedChangeListener(onCheckedChangeListener);
        listenProdAlarms.setOnCheckedChangeListener(onCheckedChangeListener);
        listenAdminAlarms.setOnCheckedChangeListener(onCheckedChangeListener);
        readVersions.setOnCheckedChangeListener(onCheckedChangeListener);
        writeVersions.setOnCheckedChangeListener(onCheckedChangeListener);
        vehiclesUser.setOnCheckedChangeListener(onCheckedChangeListener);
        vehiclesAdmin.setOnCheckedChangeListener(onCheckedChangeListener);
        vehiclesLocation.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    private void addOrRemovePermission(boolean add, int permission)
    {
        // Buscar si NO contiene el dato, para agregarlo
        if (add && !user.getAllowedBlocks().contains(permission))
        {
            user.getAllowedBlocks().add(permission);
        }
        // Buscar si SÍ contiene el dato, para removerlo
        else if (!add && user.getAllowedBlocks().contains(permission))
        {
            user.getAllowedBlocks().remove((Integer) permission);
        }
    }

    private void updateUser()
    {
        FirebaseFirestore.getInstance().collection("usuarios").document(user.getId()).update(
                "allowedBlocks", user.getAllowedBlocks()).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
                Toast.makeText(EditUserActivity.this, "Usuario actualizado correctamente",
                        Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(EditUserActivity.this, "Error al actualizar usuario",
                        Toast.LENGTH_SHORT).show();
        });
    }
}
