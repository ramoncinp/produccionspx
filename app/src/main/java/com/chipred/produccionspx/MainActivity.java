package com.chipred.produccionspx;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;

import com.chipred.produccionspx.alarms.AlarmsActivity;
import com.chipred.produccionspx.cars.CarElementActivity;
import com.chipred.produccionspx.notifications.NotificationsActivity;
import com.chipred.produccionspx.pcn.PcnListActivity;
import com.chipred.produccionspx.projects.ProjectsActivity;
import com.chipred.produccionspx.reports.CalendarViewActivity;
import com.chipred.produccionspx.reports.CarTripsChartsActivity;
import com.chipred.produccionspx.reports.ProjectChartsActivity;
import com.chipred.produccionspx.users.ProfilePictureActivity;
import com.chipred.produccionspx.users.UserData;
import com.chipred.produccionspx.users.UsersActivity;
import com.chipred.produccionspx.versions.VersionsListActivity;
import com.chipred.produccionspx.versions.VersionsUtils;
import com.google.android.gms.tasks.Continuation;
import com.google.android.material.navigation.NavigationView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.dynamic_pass.DynamicPassGeneration;
import com.firebase.ui.auth.AuthMethodPickerLayout;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.chipred.produccionspx.notifications.Notification.ALARMS_MENU;
import static com.chipred.produccionspx.notifications.Notification.NEW_PROJECT_GROUP;
import static com.chipred.produccionspx.notifications.Notification.NOTIFICATIONS_MENU;
import static com.chipred.produccionspx.notifications.Notification.PROJECT_MODIFIED_GROUP;
import static com.chipred.produccionspx.notifications.Notification.SIGN_GROUP;
import static com.chipred.produccionspx.notifications.Notification.VEHICLE_NOTIFICATION;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    // Constante para login Firebase
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 123;
    private static final int REPORT_ACTIVITY = 111;
    private static final int GALLERY_REQUEST_CODE = 112;
    private static final int STORAGE_REQUEST_CODE = 114;

    //Instancias de firebase
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseStorage firebaseStorage;

    //Variables
    private boolean canWriteProjects = false;
    private boolean canReadRegisters = false;
    private boolean canModifyeKits = false;
    private boolean showHideNotificationBell = false;
    private boolean showHideAlarmsIcon = false;
    private boolean notificationIntentStarted = false;

    //Objetos
    private SharedPreferences sharedPreferences;
    private UserData currentUser;

    //Views
    private CardView projectsCv;
    private CardView eKitsCv;
    private CardView passCv;
    private CardView chartCv;
    private CardView usersCv;
    private CardView versionsCv;
    private CardView vehiclesCv;
    private CircleImageView userImage;
    private Dialog setInitialsDialog;
    private Dialog updateVersionDialog;
    private ConstraintLayout content;
    private MenuItem menuNotificationsItem, menuAlarmsItem;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private TextView userNameTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicializar vistas
        initViews();

        //Inicializar base de datos
        initDB();

        //Inicializar sharedPreferences
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES,
                Context.MODE_PRIVATE);

        //Inicializar componentes de la aplicación
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkNotifications();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            confirmUserInfo(user);
        }

        //Definir listener para la version de la app
        setVersionListener();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Sesión iniciada", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Inicio de sesión cancelado", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (requestCode == REPORT_ACTIVITY) {
            if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
        } else if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                // Obtener path de la imagen seleccionada
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null,
                        null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                // Obtener el valor del path
                String imageAbsolutePath = cursor.getString(columnIndex);
                cursor.close();

                // Subir imagen
                uploadAndSetUserImage(imageAbsolutePath);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        firebaseAuth.addAuthStateListener(authStateListener);

        checkNotifications();
    }

    @Override
    protected void onPause() {
        super.onPause();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menuNotificationsItem = menu.findItem(R.id.menu_notifications);
        menuNotificationsItem.setVisible(showHideNotificationBell);
        menuAlarmsItem = menu.findItem(R.id.menu_alarms);
        menuAlarmsItem.setVisible(showHideAlarmsIcon);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_notifications) {
            // Iniciar actividad de notificaciones
            Intent notifications = new Intent(this, NotificationsActivity.class);
            startActivity(notifications);
            return true;
        } else if (id == R.id.menu_alarms) {
            // Iniciar actividad de alarmas
            Intent alarms = new Intent(this, AlarmsActivity.class);
            startActivity(alarms);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.app_info) {
            showVersionDialog();
        } else if (item.getItemId() == R.id.log_out) {
            AuthUI.getInstance().signOut(this);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void showVersionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Versión");
        builder.setMessage("v" + BuildConfig.VERSION_NAME);

        builder.setNegativeButton("Regresar", (dialog, which) -> dialog.dismiss());

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void initViews() {
        //Inicializar TextView
        userImage = findViewById(R.id.user_image);
        userNameTv = findViewById(R.id.username_tv);
        progressBar = findViewById(R.id.progress_bar);
        content = findViewById(R.id.main_activity_content);

        // Definir clickListener a imagen de usuario
        userImage.setOnClickListener(v -> showUserImageOptions());

        //Elementos
        projectsCv = findViewById(R.id.projects_cv);
        projectsCv.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ProjectsActivity.class);
            intent.putExtra(ProjectsActivity.CAN_WRITE_PROJECTS, canWriteProjects);
            startActivity(intent);
        });

        eKitsCv = findViewById(R.id.e_kits_cv);
        eKitsCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ElectronicKitsActivity.class);
                intent.putExtra(ElectronicKitsActivity.CAN_MODIFY_EKIT, canModifyeKits);
                startActivity(intent);
            }
        });

        passCv = findViewById(R.id.generate_pass_cv);
        passCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Crear intent
                Intent intent = new Intent(MainActivity.this, DynamicPassGeneration.class);
                //Agregar id de usuario actual
                intent.putExtra(DynamicPassGeneration.USER_ID, currentUser.getId());
                //Agregar bandera para leer registros
                intent.putExtra(DynamicPassGeneration.CAN_READ_REGISTERS, canReadRegisters);
                //Iniciar actividad
                startActivity(intent);
            }
        });

        chartCv = findViewById(R.id.chart_cv);
        chartCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showReportsMenu();
            }
        });

        versionsCv = findViewById(R.id.veriones_cv);
        versionsCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showVersionsMenu();
            }
        });

        vehiclesCv = findViewById(R.id.cars_cv);
        vehiclesCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentUser.getAllowedBlocks().contains(Constants.VEHICLES_ADMIN)) {
                    showCarsMenu();
                } else {
                    Intent intent = new Intent(MainActivity.this, CarElementActivity.class);
                    intent.putExtra(CarElementActivity.CAR_ELEMENT_TYPE,
                            CarElementActivity.TRIPS_ELEMENTS);
                    intent.putExtra("isUser", true);
                    startActivity(intent);
                }
            }
        });

        usersCv = findViewById(R.id.manage_users_cv);
        usersCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UsersActivity.class);
                startActivity(intent);
            }
        });

        //Listeners
        userNameTv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                askNewUserName();
                return false;
            }
        });
    }

    private void initDB() {
        //Inicializar instancias de Firebase
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();

        //Agregar listeners para autenticacion
        authStateListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                //Sesion activa
                onSignedIn(user);

                //Confirmar que exista el usuario
                confirmUserInfo(user);
            } else {
                onSignedOut();

                AuthMethodPickerLayout customLayout = new AuthMethodPickerLayout
                        .Builder(R.layout.custom_login_layout)
                        .setEmailButtonId(R.id.email_button)
                        .setGoogleButtonId(R.id.google_button)
                        .build();

                //No hay sesion iniciada
                startActivityForResult(
                        AuthUI.getInstance()
                                .createSignInIntentBuilder()
                                .setAuthMethodPickerLayout(customLayout)
                                .setTheme(R.style.AppTheme_NoActionBar)
                                .setAvailableProviders(Arrays.asList(
                                        new AuthUI.IdpConfig.GoogleBuilder().build(),
                                        new AuthUI.IdpConfig.EmailBuilder().build()))
                                .build(),
                        RC_SIGN_IN
                );
            }
        };
    }

    private void checkNotifications() {
        notificationIntentStarted = false;
        if (sharedPreferences.contains(Constants.NOTIFICATIONS)) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) notificationManager.cancelAll();

            sharedPreferences.edit().remove("notifications").apply();
        }
    }

    private void checkIntentExtras() {
        if (notificationIntentStarted) return;
        if (getIntent().hasExtra("notification_action") && getIntent().hasExtra("project_id")) {
            Log.d("Notificaciones", "Aun existen los extras del intent pendiente");

            // Intent iniciado
            notificationIntentStarted = true;

            // Crear intent que se lanzará
            Intent mIntent;

            int projectToView = getIntent().getIntExtra("notification_action", 0);
            String projectId = getIntent().getStringExtra("project_id");

            if (projectId == null) return;

            // Evaluar actividad a abrir
            if (projectToView == NOTIFICATIONS_MENU + 1000) {
                mIntent = new Intent(this, NotificationsActivity.class);
                startActivity(mIntent);
            } else if (projectToView == ALARMS_MENU + 1000) {
                mIntent = new Intent(this, AlarmsActivity.class);
                startActivity(mIntent);
            } else if (projectToView == VEHICLE_NOTIFICATION + 1000) {
                mIntent = new Intent(MainActivity.this, CarElementActivity.class);
                mIntent.putExtra(CarElementActivity.CAR_ELEMENT_TYPE,
                        CarElementActivity.TRIPS_ELEMENTS);
                mIntent.putExtra("isUser",
                        !currentUser.getAllowedBlocks().contains(Constants.VEHICLES_ADMIN));
                mIntent.putExtra("carsLocation",
                        currentUser.getAllowedBlocks().contains(Constants.VEHICLES_LOCATION));

                // Evaluar si tiene id de viaje
                if (!projectId.isEmpty()) mIntent.putExtra("tripId", projectId);

                startActivity(mIntent);
            } else {
                //Preparar intent
                mIntent = new Intent(this, ProjectsActivity.class);
                switch (projectToView) {
                    case NEW_PROJECT_GROUP + 1000:
                    case PROJECT_MODIFIED_GROUP + 1000:
                        mIntent.putExtra(Constants.PROJECT_ORDER_NUMBER, projectId);
                        break;

                    case SIGN_GROUP + 1000:
                        mIntent.putExtra(Constants.PROJECT_ORDER_NUMBER, projectId);
                        mIntent.putExtra(Constants.OPEN_SIGNS_DIALOG, true);
                        break;
                }

                //Iniciar actividad
                startActivity(mIntent);
            }

            //Limpiar intent
            getIntent().removeExtra("project_id");
            getIntent().removeExtra("notification_action");
        }
    }

    private void onSignedIn(FirebaseUser user) {
        //Mostrar nombre
        userNameTv.setText(user.getDisplayName());
        progressBar.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);
    }

    private void onSignedOut() {
        //Cerrando sesión
        //Des-suscribirse de las notificaciones de chipRED
        FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.PROJECTS_TOPIC);
        FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.ALARMS_TOPIC);
    }

    private void confirmUserInfo(FirebaseUser mUser) {
        //Obtener id
        String userId = mUser.getUid();

        //Obtener referencia de coleccion de usuarios
        CollectionReference usersCollection = FirebaseFirestore.getInstance().collection(
                "usuarios");

        //Verificar que exista el usuario
        usersCollection.document(userId).addSnapshotListener(this,
                (snapshot, e) -> {
                    if (e != null) {
                        Log.e("DB", e.toString());
                    } else {
                        if (snapshot != null) {
                            if (snapshot.exists()) {
                                Log.d("Usuarios", "El usuario existe");
                                confirmUserData(snapshot.toObject(UserData.class));

                                // Una vez obtenidos los datos de usuario
                                // Validar extras por si viene de una notificación
                                checkIntentExtras();
                            } else {
                                Log.d("Usuarios", "El usuario NO existe");
                                //Agregar usuario
                                addUser();
                            }
                        } else {
                            Log.d("Usuarios", "El usuario no existe");
                        }
                    }
                });
    }

    private void confirmUserData(UserData userData) {
        //Definir usuario de la clase
        currentUser = userData;

        //Pedir iniciales si no estan definidas
        if (userData.getInitials() == null) {
            //Crear diálogo
            showUserDataDialog();
        }

        if (userData.getPhotoUrl() != null && !userData.getPhotoUrl().isEmpty()) {
            Glide.with(this).load(userData.getPhotoUrl()).dontAnimate().into(userImage);
        }

        boolean canReceive = false;
        boolean canReceiveProdAlarms = false;
        boolean canReceiveAdminAlarms = false;
        boolean canReceiveVehicleUser = false;
        boolean canReceiveVehicleAdmin = false;
        canWriteProjects = false;

        //Deshabilitar todos los cardViews
        projectsCv.setEnabled(false);
        projectsCv.setAlpha(0.5f);
        eKitsCv.setEnabled(false);
        eKitsCv.setAlpha(0.5f);
        passCv.setEnabled(false);
        passCv.setAlpha(0.5f);
        chartCv.setEnabled(false);
        chartCv.setAlpha(0.5f);
        versionsCv.setEnabled(false);
        versionsCv.setAlpha(0.5f);
        vehiclesCv.setEnabled(false);
        vehiclesCv.setAlpha(0.5f);
        usersCv.setEnabled(false);
        usersCv.setAlpha(0.5f);

        // Ocultar por default icono de notificaciones del menu
        if (menuNotificationsItem != null) {
            menuNotificationsItem.setVisible(false);
        } else {
            showHideNotificationBell = false;
        }

        // Ocultar por default icono de alarmas del menu
        if (menuAlarmsItem != null) {
            menuAlarmsItem.setVisible(false);
        } else {
            showHideAlarmsIcon = false;
        }

        //Revisar si debe o no recibir notificaciones
        for (Integer block : userData.getAllowedBlocks()) {
            if (block == Constants.READ_PROJECTS) {
                projectsCv.setEnabled(true);
                projectsCv.setAlpha(1f);
            }

            if (block == Constants.WRITE_PROJECTS) {
                canWriteProjects = true;
            }

            if (block == Constants.READ_EKITS) {
                eKitsCv.setEnabled(true);
                eKitsCv.setAlpha(1f);
            }

            if (block == Constants.USE_PASS_GENERATOR) {
                passCv.setEnabled(true);
                passCv.setAlpha(1f);
            }

            if (block == Constants.READ_PASS_REGISTERS) {
                canReadRegisters = true;
            }

            if (block == Constants.WRITE_EKITS) {
                canModifyeKits = true;
            }

            if (block == Constants.LISTEN_PROJECTS) {
                canReceive = true;
                if (menuNotificationsItem != null) {
                    menuNotificationsItem.setVisible(true);
                } else {
                    showHideNotificationBell = true;
                }
            }

            if (block == Constants.READ_REPORTS) {
                chartCv.setEnabled(true);
                chartCv.setAlpha(1f);
            }

            if (block == Constants.READ_VERSIONS) {
                versionsCv.setEnabled(true);
                versionsCv.setAlpha(1f);
            }

            if (block == Constants.VEHICLES_USER) {
                vehiclesCv.setEnabled(true);
                vehiclesCv.setAlpha(1f);
                canReceiveVehicleUser = true;
            }

            if (block == Constants.VEHICLES_ADMIN) {
                vehiclesCv.setEnabled(true);
                vehiclesCv.setAlpha(1f);
                canReceiveVehicleAdmin = true;
            }

            if (block == Constants.LISTEN_PROJECT_ALARM_PROD || block == Constants.LISTEN_PROJECT_ALARM_ADMN) {
                if (menuAlarmsItem != null) {
                    menuAlarmsItem.setVisible(true);
                } else {
                    showHideAlarmsIcon = true;
                }

                if (block == Constants.LISTEN_PROJECT_ALARM_PROD) {
                    canReceiveProdAlarms = true;
                }

                if (block == Constants.LISTEN_PROJECT_ALARM_ADMN) {
                    canReceiveAdminAlarms = true;
                }
            }

            if (block == Constants.SET_PERMISSIONS) {
                usersCv.setEnabled(true);
                usersCv.setAlpha(1f);
            }
        }

        if (canReceive) {
            //Suscribirse al tema de proyectos
            FirebaseMessaging.getInstance().subscribeToTopic(Constants.PROJECTS_TOPIC);

            //Crear canal de notificaciones para Android O
            new NotificationChannelManager(this).createDefaultNotificationChannel();

            //Log de subsripción
            Log.d("Usuario", "Usuario subscrito a canal de notificaciones");
        } else {
            //Suscribirse al tema de proyectos
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.PROJECTS_TOPIC);

            //Log de subsripción
            Log.d("Usuario", "Usuario DESsubscrito de canal de notificaciones");
        }

        if (canReceiveProdAlarms || canReceiveAdminAlarms) {
            //Suscribirse al tema de proyectos
            FirebaseMessaging.getInstance().subscribeToTopic(Constants.ALARMS_TOPIC);

            //Crear canal de notificaciones para Android O
            new NotificationChannelManager(this).createDefaultNotificationChannel();

            //Log de subsripción
            Log.d("Usuario", "Usuario subscrito a canal de alarmas");
        } else {
            //Suscribirse al tema de proyectos
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.ALARMS_TOPIC);

            //Log de subsripción
            Log.d("Usuario", "Usuario DESsubscrito de canal de alarmas");
        }

        if (canReceiveVehicleAdmin) {
            //Suscribirse al tema de proyectos
            FirebaseMessaging.getInstance().subscribeToTopic(Constants.VEHICLE_ADMIN_TOPIC);

            //Crear canal de notificaciones para Android O
            new NotificationChannelManager(this).createDefaultNotificationChannel();
        } else {
            //Suscribirse al tema de proyectos
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.VEHICLE_ADMIN_TOPIC);

            //Log de subsripción
            Log.d("Usuario", "Usuario DESsubscrito de canal de administrador de vehículos");
        }
        // Desuscribirse de canales de prueba
        FirebaseMessaging.getInstance().unsubscribeFromTopic("vehicleAdmin_st");

        if (canReceiveVehicleUser) {
            //Suscribirse al tema de viajes para este usuario
            FirebaseMessaging.getInstance().subscribeToTopic(Constants.VEHICLE_USER_TOPIC + "_" + userData.getId());
            //Suscribirse a tema de viajes de interés general
            FirebaseMessaging.getInstance().subscribeToTopic(Constants.VEHICLE_USER_TOPIC);

            //Crear canal de notificaciones para Android O
            new NotificationChannelManager(this).createDefaultNotificationChannel();

            //Log de subsripción
            Log.d("Usuario", "Usuario subscrito a canal -> " + Constants.VEHICLE_USER_TOPIC + "_"
                    + userData.getId());
        } else {
            //Suscribirse al tema de proyectos
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.VEHICLE_USER_TOPIC +
                    "_" + userData.getId());
            //DesSuscribirse a tema de viajes de interés general
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.VEHICLE_USER_TOPIC);

            //Log de subsripción
            Log.d("Usuario",
                    "Usuario DESsubscrito de canal -> " + Constants.VEHICLE_USER_TOPIC + "_" + userData.getId());
        }
        // Desuscribirse de canales de prueba
        FirebaseMessaging.getInstance().unsubscribeFromTopic("vehicleUser_st");

        // Actualizar banderas de recepción de alertas
        sharedPreferences.edit().putBoolean(Constants.RECEIVE_ALARM_PROD, canReceiveProdAlarms).apply();
        sharedPreferences.edit().putBoolean(Constants.RECEIVE_ALARM_ADMN, canReceiveAdminAlarms).apply();
    }

    private void showUserDataDialog() {
        if (setInitialsDialog != null && setInitialsDialog.isShowing()) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View content = getLayoutInflater().inflate(R.layout.dialog_user_data, null);
        builder.setView(content);

        final MaterialEditText met = content.findViewById(R.id.edit_text);

        //Redefinir longitud maxima
        int maxLength = 3;
        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        fArray[1] = new InputFilter.AllCaps();
        met.setFilters(fArray);

        setInitialsDialog = builder.create();
        setInitialsDialog.setCancelable(false);
        setInitialsDialog.show();

        Button submit = content.findViewById(R.id.submit_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean valid = met.validateWith(new METValidator("Campo obligatorio") {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                        return !isEmpty;
                    }
                });

                if (valid) {
                    //Obtener iniciales
                    currentUser.setInitials(met.getText().toString());

                    //Actualizar usuario
                    updateUser();

                    //Cerrar diálogo
                    setInitialsDialog.dismiss();
                }
            }
        });

        Button cancel = content.findViewById(R.id.cancel_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInitialsDialog.dismiss();
            }
        });
    }

    private void addUser() {
        //Obtener usuario Firebase
        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mUser == null) return;

        //Obtener id
        String userId = mUser.getUid();
        String name = mUser.getDisplayName();
        String photoUrl = "";

        if (mUser.getPhotoUrl() != null)
            photoUrl = mUser.getPhotoUrl().toString();

        //Obtener referencia de coleccion de usuarios
        CollectionReference usersCollection = FirebaseFirestore.getInstance().collection(
                "usuarios");

        //Crear usuario
        UserData userData = new UserData();
        userData.setId(userId);
        userData.setName(name);
        userData.setPhotoUrl(photoUrl);

        //Definir bloques
        ArrayList<Integer> allowedBlocks = new ArrayList<>();
        userData.setAllowedBlocks(allowedBlocks);

        //Definir como el usuario de la clase
        currentUser = userData;

        //Agregar usuario a la base de datos
        usersCollection.document(userId).set(userData).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("Usuarios", "Usuario agregado correctamente");
                //confirmUserData(currentUser);
            } else {
                Log.d("Usuarios", "Error al agregar usuario");
            }
        });
    }

    private void updateUser() {
        //Obtener referencia de coleccion de usuarios
        CollectionReference usersCollection = FirebaseFirestore.getInstance().collection(
                "usuarios");

        usersCollection.document(currentUser.getId()).set(currentUser, SetOptions.merge()).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d("Usuarios", "Usuario actualizado correctamente");
            } else {
                Log.d("Usuarios", "Error al actualizar usuario");
            }
        });
    }

    private void askNewUserName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View content = getLayoutInflater().inflate(R.layout.dialog_user_data, null);
        builder.setView(content);

        //Obtener referencias
        Button submit = content.findViewById(R.id.submit_button);
        final Button cancel = content.findViewById(R.id.cancel_button);
        final MaterialEditText editText = content.findViewById(R.id.edit_text);
        TextView title = content.findViewById(R.id.dialog_title);

        cancel.setVisibility(View.VISIBLE);
        title.setVisibility(View.GONE);
        editText.setHint(getResources().getString(R.string.user_display_name));
        editText.setFloatingLabel(MaterialEditText.FLOATING_LABEL_NORMAL);
        editText.setFloatingLabelText(getResources().getString(R.string.user_display_name));
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        //Redefinir longitud maxima
        int maxLength = 30;
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        editText.setFilters(fArray);

        //Escribir nombre
        editText.setText(currentUser.getName());

        final Dialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        cancel.setOnClickListener(v -> dialog.dismiss());
        submit.setOnClickListener(v -> {
            //Validar
            boolean valid = editText.validateWith(new METValidator("Campo obligatorio") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return !isEmpty;
                }
            });

            if (valid) {
                //Obtener valor
                String name = editText.getText().toString();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                        .setDisplayName(name)
                        .build();

                user.updateProfile(profileUpdates)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d("User", "User profile updated.");
                                    onProfileUpdated();
                                } else {
                                    Toast.makeText(MainActivity.this, "Error al actualizar " +
                                                    "datos de usuario",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                //Cerrar diálogo
                dialog.dismiss();
            }
        });
    }

    private void onProfileUpdated() {
        //Obtener usuario Firebase
        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mUser == null) return;

        String name = mUser.getDisplayName();
        currentUser.setName(name);

        //Mostrar cambios
        onSignedIn(mUser);

        //Actualizar en bd
        updateUser();
    }

    private void downloadApk() {
        //Crear diálogo
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //Obtener layout
        View content = getLayoutInflater().inflate(R.layout.download_apk_dialog, null);
        builder.setView(content);

        //Obtener view
        final ProgressBar progressBarPercentage =
                content.findViewById(R.id.progress_bar_percentage);
        final TextView textPercentage = content.findViewById(R.id.percentage_text);

        //Obtener referencia
        StorageReference storageReference = firebaseStorage.getReferenceFromUrl("gs://produccion-spx.appspot.com");
        StorageReference apkStorageReference = storageReference.child(
                "apk/produccion_spx.apk");

        //Obtener archivo
        File rootPath = new File(Environment.getExternalStorageDirectory(), "/download" +
                "/");
        if (!rootPath.exists()) {
            rootPath.mkdirs();
        }

        final File apkFile = new File(rootPath, "produccion_spx.apk");

        //Mostrar diálogo
        final Dialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        //Descargar archivo
        apkStorageReference.getFile(apkFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.v("DownloadApk", "Apk descargada");
                //Cerrar diálogo
                dialog.dismiss();

                //Instalar aplicación
                installApk();
            }
        }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                int progress =
                        (int) (100 * (float) taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                String progressText = progress + "%";

                progressBarPercentage.setProgress(progress);
                textPercentage.setText(progressText);

                Log.i("DownloadApk", "Progress -> " + progress);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("DownloadApk", "Error al descargar apk");
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);

            if (requestCode == STORAGE_REQUEST_CODE)
                //Descargar nueva version
                downloadApk();
        }
    }

    private void setVersionListener() {
        FirebaseFirestore.getInstance().collection("version").document("version_actual").get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot snapshot = task.getResult();
                    if (snapshot != null) {
                        //Obtener nombre de version
                        String lastVersion = (String) task.getResult().get("nombre");

                        //Evaluar version actual con la última registrada
                        if (lastVersion != null && !lastVersion.equals(BuildConfig.VERSION_NAME)) {
                            //Mostrar diálogo
                            showUpdateDialog();
                        }
                    }
                } else {
                    Log.e(TAG, "Error al obtener version actual");
                }
            }
        });
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);

                return;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
        }

        downloadApk();
    }

    private void showUpdateDialog() {
        if (!isFinishing() && updateVersionDialog != null && updateVersionDialog.isShowing())
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.update_dialog_title));
        builder.setMessage(getResources().getString(R.string.update_dialog_text));
        builder.setPositiveButton("SÍ", (dialog, which) ->
        {
            checkPermissions();
            dialog.dismiss();
        }).setNegativeButton("No", (dialog, which) -> dialog.dismiss());

        updateVersionDialog = builder.create();
        updateVersionDialog.setCancelable(false);
        updateVersionDialog.show();
    }

    private void installApk() {
        String destination = Environment.getExternalStorageDirectory() + "/download" +
                "/";
        String fileName = "produccion_spx.apk";
        destination += fileName;

        //Delete update file if exists
        File file = new File(destination);
        if (file.exists()) {
            Intent intent;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri apkUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID +
                        ".fileprovider", file);
                intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                intent.setData(apkUri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            } else {
                Uri apkUri = Uri.fromFile(file);
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
        } else {
            Log.e("DownloadApk", "No existe el apk a instalar");
        }
    }

    private void showReportsMenu() {
        // Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Obtener layout
        View content = getLayoutInflater().inflate(R.layout.dialog_reports_menu, null);

        // Asignar view a builder
        builder.setView(content);

        // Obtener referencia de los layouts internos
        RelativeLayout calendarLayout;
        RelativeLayout numbersLayout;
        RelativeLayout chartsLayout;

        calendarLayout = content.findViewById(R.id.calendar_view_layout);
        numbersLayout = content.findViewById(R.id.numbers_view_layout);
        chartsLayout = content.findViewById(R.id.charts_view_layout);

        // Crear diálogo
        Dialog dialog = builder.create();
        dialog.show();

        calendarLayout.setOnClickListener(v ->
        {
            progressDialog = ProgressDialog.show(MainActivity.this, "", "Cargando...");
            progressDialog.setCancelable(true);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Crear intent
                    Intent intent = new Intent(MainActivity.this, CalendarViewActivity.class);

                    //Iniciar actividad
                    startActivityForResult(intent, REPORT_ACTIVITY);
                }
            }, 100);

            dialog.dismiss();
        });

        numbersLayout.setOnClickListener(view ->
        {

        });

        chartsLayout.setOnClickListener(v ->
        {
            showChartsMenu();
            dialog.dismiss();
        });
    }

    private void showVersionsMenu() {
        // Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Obtener layout
        View content = getLayoutInflater().inflate(R.layout.dialog_evo_versions_menu, null);

        // Asignar view a builder
        builder.setView(content);

        // Obtener referencia de los layouts internos
        RelativeLayout evo6Layout = content.findViewById(R.id.evo6_layout);
        RelativeLayout evo5Layout = content.findViewById(R.id.evo5_layout);
        RelativeLayout accesoriesLayout = content.findViewById(R.id.accesories_layout);
        RelativeLayout pcnLayout = content.findViewById(R.id.pcn_layout);

        // Crear diálogo
        Dialog dialog = builder.create();
        dialog.show();

        // Obtener permisos
        boolean canModify = currentUser.getAllowedBlocks().contains(Constants.WRITE_VERSIONS);

        // Definir listeners
        evo5Layout.setOnClickListener(view ->
        {
            Intent intent = new Intent(MainActivity.this, VersionsListActivity.class);
            intent.putExtra(VersionsListActivity.CAN_WRITE, canModify);
            intent.putExtra(VersionsListActivity.MODEL_VERSIONS,
                    VersionsListActivity.EVO_5_VERSIONS);
            startActivity(intent);

            // Ocultar diálogo
            dialog.dismiss();
        });
        evo6Layout.setOnClickListener(view ->
        {
            Intent intent = new Intent(MainActivity.this, VersionsListActivity.class);
            intent.putExtra(VersionsListActivity.CAN_WRITE, canModify);
            intent.putExtra(VersionsListActivity.MODEL_VERSIONS,
                    VersionsListActivity.EVO_6_VERSIONS);
            startActivity(intent);

            // Ocultar diálogo
            dialog.dismiss();
        });
        accesoriesLayout.setOnClickListener(view ->
        {
            // Ocultar diálogo
            dialog.dismiss();

            // Mostrar menu de accesorios
            VersionsUtils.showAccesoriesMenu(this, canModify);
        });
        pcnLayout.setOnClickListener(view ->
        {
            Intent intent = new Intent(MainActivity.this, PcnListActivity.class);
            intent.putExtra(PcnListActivity.CAN_WRITE, canModify);
            startActivity(intent);
        });
    }

    private void showCarsMenu() {
        // Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Obtener layout
        View content = getLayoutInflater().inflate(R.layout.dialog_cars_menu, null);

        // Asignar view a builder
        builder.setView(content);

        // Obtener referencia de los layouts internos
        RelativeLayout tripsLayout;
        RelativeLayout driversLayout;
        RelativeLayout vehiclesLayout;

        tripsLayout = content.findViewById(R.id.trips_layout);
        driversLayout = content.findViewById(R.id.drivers_layout);
        vehiclesLayout = content.findViewById(R.id.vehicles_layout);

        // Crear diálogo
        Dialog dialog = builder.create();
        dialog.show();

        vehiclesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CarElementActivity.class);
                intent.putExtra(CarElementActivity.CAR_ELEMENT_TYPE,
                        CarElementActivity.CAR_ELEMENTS);
                startActivity(intent);
            }
        });

        driversLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CarElementActivity.class);
                intent.putExtra(CarElementActivity.CAR_ELEMENT_TYPE,
                        CarElementActivity.DRIVERS_ELEMENTS);
                startActivity(intent);
            }
        });

        tripsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CarElementActivity.class);
                intent.putExtra(CarElementActivity.CAR_ELEMENT_TYPE,
                        CarElementActivity.TRIPS_ELEMENTS);
                intent.putExtra("carsLocation",
                        currentUser.getAllowedBlocks().contains(Constants.VEHICLES_LOCATION));
                startActivity(intent);
            }
        });
    }

    private void showChartsMenu() {
        // Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Obtener layout
        View content = getLayoutInflater().inflate(R.layout.dialog_chart_reports_menu, null);

        // Asignar view a builder
        builder.setView(content);

        // Obtener referencia de los layouts internos
        RelativeLayout finishedProjectsLayout;
        RelativeLayout carTripsLayout;

        finishedProjectsLayout = content.findViewById(R.id.finished_projects_report);
        carTripsLayout = content.findViewById(R.id.car_trips_report);

        // Crear diálogo
        Dialog dialog = builder.create();
        dialog.show();

        finishedProjectsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog.show(MainActivity.this, "", "Cargando...");
                progressDialog.setCancelable(true);

                Handler handler = new Handler();
                handler.postDelayed(() ->
                {
                    //Crear intent
                    Intent intent = new Intent(MainActivity.this, ProjectChartsActivity.class);

                    //Iniciar actividad
                    startActivityForResult(intent, REPORT_ACTIVITY);
                }, 100);

                dialog.dismiss();
            }
        });

        carTripsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = ProgressDialog.show(MainActivity.this, "", "Cargando...");
                progressDialog.setCancelable(true);

                Handler handler = new Handler();
                handler.postDelayed(() ->
                {
                    //Crear intent
                    Intent intent = new Intent(MainActivity.this, CarTripsChartsActivity.class);

                    //Iniciar actividad
                    startActivityForResult(intent, REPORT_ACTIVITY);
                }, 100);

                dialog.dismiss();
            }
        });
    }

    private void showUserImageOptions() {
        String[] options = {"Ver foto de perfil", "Seleccionar foto de perfil"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Crear adaptador para mostrar opciones
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1);

        // Agregar opciones
        arrayAdapter.add(options[0]);
        arrayAdapter.add(options[1]);

        // Definir clickListener
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Obtener opcion seleccionada
                String selectedOption = arrayAdapter.getItem(which);

                if (selectedOption != null) {
                    if (selectedOption.equals(options[0])) {
                        // Mostrar imagen en pantalla completa
                        Intent showFullImage = new Intent(MainActivity.this,
                                ProfilePictureActivity.class);
                        showFullImage.putExtra("imagen", currentUser.getPhotoUrl());

                        // Iniciar actividad
                        startActivity(showFullImage);
                    } else if (selectedOption.equals(options[1])) {
                        // Crear intent tipo "Action pick"
                        Intent intent = new Intent(Intent.ACTION_PICK);

                        // Seleccionar tipo imagen
                        intent.setType("image/*");

                        // Definir los formatos
                        String[] mimeTypes = {"image/jpeg", "image/png"};
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

                        // Launching the Intent
                        startActivityForResult(intent, GALLERY_REQUEST_CODE);
                    }
                }

                dialog.dismiss();
            }
        });

        // Mostrar
        builder.show();
    }

    private void uploadAndSetUserImage(String imagePath) {
        // Obtener referencia
        StorageReference storageReference =
                firebaseStorage.getReference().child("fotosDePerfil/" + currentUser.getId());

        // Obtener archivo
        File imageFile = new File(imagePath);

        // Convertir a arreglo de bytes
        int size = (int) imageFile.length();
        byte[] data = new byte[size];

        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(imageFile));
            buf.read(data, 0, data.length);
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error al leer el archivo a subir", Toast.LENGTH_SHORT).show();
            return;
        }

        // Subir archivo
        UploadTask uploadTask = storageReference.putBytes(data);

        // Definir proceso
        uploadTask.continueWithTask(task -> {
            // Manejar primer resultado
            if (!task.isSuccessful()) {
                throw Objects.requireNonNull(task.getException());
            }

            return storageReference.getDownloadUrl();
        }).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // Obtener Uri del archivo recien subido
                Uri downloadUri = task.getResult();

                // Validar Uri
                if (downloadUri != null) {
                    // Asignar al usuario
                    currentUser.setPhotoUrl(downloadUri.toString());

                    // Actualizar usuario
                    updateUser();
                }
            } else {
                Toast.makeText(MainActivity.this, "Error al subir archivo",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
