package com.chipred.produccionspx.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Date;

public class PendingTripAdapter extends ArrayAdapter<Trip>
{
    private ArrayList<Trip> trips;
    private Context context;

    public PendingTripAdapter(Context context, ArrayList<Trip> trips)
    {
        super(context, R.layout.requested_car_item, trips);
        this.context = context;
        this.trips = trips;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        // Obtener viaje correspondiente
        Trip trip = trips.get(position);

        // Crear Viewholder
        ViewHolder viewHolder;

        if (convertView == null)
        {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());

            convertView = inflater.inflate(R.layout.requested_car_item, parent, false);
            viewHolder.userTv = convertView.findViewById(R.id.trip_user_tv);
            viewHolder.placesTv = convertView.findViewById(R.id.trip_place_tv);
            viewHolder.dateTv = convertView.findViewById(R.id.trip_start_date_tv);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Obtener lista de lugares
        StringBuilder places = new StringBuilder();
        for (int idx = 0; idx < trip.getPlaces().size(); idx++)
        {
            places.append(trip.getPlaces().get(idx));
            if (idx != trip.getPlaces().size() - 1)
            {
                places.append("\n");
            }
        }

        // Obtener fecha de inicio de viaje en versión String
        String dateStr = Constants.dateObjectToString2(new Date(trip.getStartDate()));
        if (dateStr != null) dateStr = dateStr.substring(dateStr.indexOf(" ") + 1);

        viewHolder.userTv.setText(trip.getUserName().replace(" ", "\n"));
        viewHolder.placesTv.setText(places.toString());
        viewHolder.dateTv.setText(dateStr);

        if (trip.getCurrentState().equals(Trip.State.SOLICITADA.toString()))
        {
            ((CardView) convertView).setCardBackgroundColor(ContextCompat.getColor(context,
                    R.color.colorAccent));
        }
        else if (trip.getCurrentState().equals(Trip.State.PENDIENTE.toString()))
        {
            ((CardView) convertView).setCardBackgroundColor(ContextCompat.getColor(context,
                    android.R.color.holo_red_dark));
        }
        else if (trip.getCurrentState().equals(Trip.State.AUTORIZADA.toString()))
        {
            ((CardView) convertView).setCardBackgroundColor(ContextCompat.getColor(context,
                    android.R.color.holo_orange_dark));
        }

        return convertView;
    }

    @Override
    public int getCount()
    {
        return trips.size();
    }

    private static class ViewHolder
    {
        TextView userTv;
        TextView placesTv;
        TextView dateTv;
    }
}
