package com.chipred.produccionspx.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.TripOverviewViewHolder> implements View.OnClickListener, View.OnLongClickListener
{
    private final Context context;
    private final ArrayList<Trip> trips;
    private View.OnClickListener clickListener;
    private View.OnLongClickListener longClickListener;

    TripAdapter(Context context, ArrayList<Object> trips)
    {
        this.context = context;

        // Convertir lista
        this.trips = new ArrayList<>();
        for (Object object : trips)
        {
            this.trips.add((Trip) object);
        }
    }

    @NonNull
    @Override
    public TripOverviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.trip_layout,
                        viewGroup, false);

        TripOverviewViewHolder viewHolder = new TripOverviewViewHolder(v);
        v.setOnClickListener(this);
        v.setOnLongClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TripOverviewViewHolder tripOverviewViewHolder,
                                 int i)
    {
        // Obtener vehículo
        Trip trip = trips.get(i);

        // Mostrar lugares
        tripOverviewViewHolder.places.setText("");
        for (int idx = 0; idx < trip.getPlaces().size(); idx++)
        {
            String place = trip.getPlaces().get(idx);
            if (idx != 0) place = '\n' + place;
            tripOverviewViewHolder.places.append(place);
        }

        // Mostrar vehículo
        tripOverviewViewHolder.vehicle.setText(trip.getVehicleDesc() != null ?
                trip.getVehicleDesc() : "");

        String currentState = trip.getCurrentState().toUpperCase();
        if (currentState.contains("_")) currentState = currentState.replace('_', '\n');
        tripOverviewViewHolder.stateText.setText(currentState);

        // Mostrar usuario, si es que existen los views
        if (tripOverviewViewHolder.userImage != null)
        {
            if (trip.getUserName() != null)
            {
                tripOverviewViewHolder.userText.setText(trip.getUserName().replace(' ', '\n'));
            }

            if (trip.getUserPhotoUrl() != null && !trip.getUserPhotoUrl().isEmpty())
            {
                Glide.with(context).load(trip.getUserPhotoUrl()).into(tripOverviewViewHolder.userImage);
            }
            else
            {
                Glide.with(context).load(R.drawable.ic_binarium).into(tripOverviewViewHolder.userImage);
            }
        }

        if (trip.getCurrentState().equals(Trip.State.SOLICITADA.toString()))
        {
            Glide.with(context).load(R.drawable.ic_requested).into(tripOverviewViewHolder.stateIcon);
            ((CardView) tripOverviewViewHolder.itemView).setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));

            tripOverviewViewHolder.startDateLayout.setVisibility(View.VISIBLE);
            tripOverviewViewHolder.endDateLayout.setVisibility(View.GONE);
            tripOverviewViewHolder.startDate.setText(Constants.dateToRelative(new Date(trip.getStartDate())));

            setSubtitlesColor(tripOverviewViewHolder, ContextCompat.getColor(context,
                    android.R.color.darker_gray));
        }
        else if (trip.getCurrentState().equals(Trip.State.PENDIENTE.toString()))
        {
            Glide.with(context).load(R.drawable.ic_hourglass).into(tripOverviewViewHolder.stateIcon);
            ((CardView) tripOverviewViewHolder.itemView).setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));

            tripOverviewViewHolder.startDateLayout.setVisibility(View.VISIBLE);
            tripOverviewViewHolder.endDateLayout.setVisibility(View.GONE);
            tripOverviewViewHolder.startDate.setText(Constants.dateToRelative(new Date(trip.getStartDate())));

            setSubtitlesColor(tripOverviewViewHolder, ContextCompat.getColor(context,
                    android.R.color.darker_gray));
        }
        else if (trip.getCurrentState().equals(Trip.State.AUTORIZADA.toString()))
        {
            Glide.with(context).load(R.drawable.ic_check_black).into(tripOverviewViewHolder.stateIcon);
            ((CardView) tripOverviewViewHolder.itemView).setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_orange_dark));

            tripOverviewViewHolder.startDateLayout.setVisibility(View.VISIBLE);
            tripOverviewViewHolder.endDateLayout.setVisibility(View.GONE);
            tripOverviewViewHolder.startDate.setText(Constants.dateToRelative(new Date(trip.getStartDate())));

            setSubtitlesColor(tripOverviewViewHolder, ContextCompat.getColor(context,
                    R.color.colorPrimary));
        }
        else if (trip.getCurrentState().equals(Trip.State.ACTIVA.toString()))
        {
            Glide.with(context).load(R.drawable.ic_car).into(tripOverviewViewHolder.stateIcon);
            ((CardView) tripOverviewViewHolder.itemView).setCardBackgroundColor(ContextCompat.getColor(context, R.color.dark_green));

            tripOverviewViewHolder.startDateLayout.setVisibility(View.VISIBLE);
            tripOverviewViewHolder.endDateLayout.setVisibility(View.GONE);
            tripOverviewViewHolder.startDate.setText(Constants.dateToRelative(new Date(trip.getStartDate())));

            setSubtitlesColor(tripOverviewViewHolder, ContextCompat.getColor(context,
                    android.R.color.darker_gray));
        }
        else if (trip.getCurrentState().equals(Trip.State.TERMINADA.toString()))
        {
            Glide.with(context).load(R.drawable.ic_check_black).into(tripOverviewViewHolder.stateIcon);
            ((CardView) tripOverviewViewHolder.itemView).setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_blue_dark));

            tripOverviewViewHolder.startDateLayout.setVisibility(View.GONE);
            tripOverviewViewHolder.endDateLayout.setVisibility(View.VISIBLE);
            tripOverviewViewHolder.endDate.setText(Constants.dateToRelative(new Date(trip.getFinishDate())));

            setSubtitlesColor(tripOverviewViewHolder, ContextCompat.getColor(context,
                    android.R.color.darker_gray));
        }
    }

    private void setSubtitlesColor(TripOverviewViewHolder overviewViewHolder, int colorResource)
    {
        overviewViewHolder.placeTitle.setTextColor(colorResource);
        overviewViewHolder.vehicleTitle.setTextColor(colorResource);
        overviewViewHolder.startDateTitle.setTextColor(colorResource);
        overviewViewHolder.endDateTitle.setTextColor(colorResource);
    }

    @Override
    public int getItemCount()
    {
        return trips.size();
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    public void setLongClickListener(View.OnLongClickListener longClickListener)
    {
        this.longClickListener = longClickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if (longClickListener != null)
        {
            longClickListener.onLongClick(view);
            return true;
        }

        return false;
    }

    ArrayList<Trip> getTrips()
    {
        return trips;
    }

    void addTrip(Trip trip)
    {
        trips.add(trip);
    }

    static class TripOverviewViewHolder extends RecyclerView.ViewHolder
    {
        private final ImageView stateIcon;
        private final CircleImageView userImage;
        private final TextView stateText;
        private final TextView places;
        private final TextView vehicle;
        private final TextView startDate;
        private final TextView endDate;
        private final TextView userText;
        private final LinearLayout startDateLayout;
        private final LinearLayout endDateLayout;
        private final TextView placeTitle;
        private final TextView vehicleTitle;
        private final TextView startDateTitle;
        private final TextView endDateTitle;

        TripOverviewViewHolder(@NonNull View itemView)
        {
            super(itemView);
            stateIcon = itemView.findViewById(R.id.trip_state_icon);
            stateText = itemView.findViewById(R.id.trip_state_tv);
            userImage = itemView.findViewById(R.id.trip_user_image);
            userText = itemView.findViewById(R.id.trip_user_text);
            places = itemView.findViewById(R.id.place_tv);
            vehicle = itemView.findViewById(R.id.vehicle_tv);
            startDate = itemView.findViewById(R.id.start_date_tv);
            endDate = itemView.findViewById(R.id.end_date_tv);
            startDateLayout = itemView.findViewById(R.id.start_date_layout);
            endDateLayout = itemView.findViewById(R.id.end_date_layout);
            placeTitle = itemView.findViewById(R.id.place_title_tv);
            vehicleTitle = itemView.findViewById(R.id.vehicle_title_tv);
            startDateTitle = itemView.findViewById(R.id.start_date_title_tv);
            endDateTitle = itemView.findViewById(R.id.end_date_title_tv);
        }
    }
}
