package com.chipred.produccionspx.cars;

import com.chipred.produccionspx.Constants;

import java.util.Date;

public class CarDocument
{
    public static final String REFRENDO = "refrendo";
    public static final String VERIFICACION = "verificacion";

    private Long date = new Long(0);
    private String photoUrl;
    private String type;

    public CarDocument()
    {
    }

    public String getPhotoUrl()
    {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl)
    {
        this.photoUrl = photoUrl;
    }

    public Long getDate()
    {
        return date;
    }

    public void setDate(Long date)
    {
        this.date = date;
    }

    public String getYear()
    {
        String dateString = Constants.dateObjectToString(new Date(date));
        if (dateString == null)
        {
            return "0";
        }
        else
        {
            // Obtener solo el año
            dateString = dateString.substring(dateString.lastIndexOf("/") + 1);
            return dateString;
        }
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}
