package com.chipred.produccionspx.cars;

import java.util.ArrayList;
import java.util.Map;

public class Trip {
    public static final String START_DATE_TYPE = "Fecha de salida";
    public static final String END_DATE_TYPE = "Fecha de llegada";

    public enum State {
        SOLICITADA,
        PENDIENTE,
        AUTORIZADA,
        ACTIVA,
        TERMINADA
    }

    private ArrayList<String> passengers = new ArrayList<>();
    private ArrayList<String> places = new ArrayList<>();
    private Integer fuelStart;
    private Integer fuelEnd;
    private Long startDate;
    private Long endDate;
    private Long odoStart;
    private Long odoEnd;
    private Long requestDate;
    private Long startedDate;
    private Long finishDate;
    private Long lastModifiedDate;
    private Map<String, Double> coordinates;
    private String id;
    private String currentState;
    private String crmTicket;
    private String responsibleUserId;
    private String vehicleId;
    private String vehicleDesc;
    private String description;
    private String userName;
    private String userPhotoUrl;
    private String signPhotoUrl;
    private String observations;
    private String lastModifierUserId;

    public Trip() {
    }

    public ArrayList<String> getPlaces() {
        return places;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPlaces(ArrayList<String> places) {
        this.places = places;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Long getOdoStart() {
        return odoStart;
    }

    public void setOdoStart(Long odoStart) {
        this.odoStart = odoStart;
    }

    public Long getOdoEnd() {
        return odoEnd;
    }

    public void setOdoEnd(Long odoEnd) {
        this.odoEnd = odoEnd;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getCrmTicket() {
        return crmTicket;
    }

    public void setCrmTicket(String crmTicket) {
        this.crmTicket = crmTicket;
    }

    public String getResponsibleUserId() {
        return responsibleUserId;
    }

    public void setResponsibleUserId(String responsibleUserId) {
        this.responsibleUserId = responsibleUserId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Integer getFuelStart() {
        return fuelStart;
    }

    public void setFuelStart(Integer fuelStart) {
        this.fuelStart = fuelStart;
    }

    public Integer getFuelEnd() {
        return fuelEnd;
    }

    public void setFuelEnd(Integer fuelEnd) {
        this.fuelEnd = fuelEnd;
    }

    public Long getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Long requestDate) {
        this.requestDate = requestDate;
    }

    public ArrayList<String> getPassengers() {
        return passengers;
    }

    public void setPassengers(ArrayList<String> passengers) {
        this.passengers = passengers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addPlace(String place) {
        places.add(place);
    }

    public void addPassenger(String passenger) {
        passengers.add(passenger);
    }

    public String getVehicleDesc() {
        return vehicleDesc;
    }

    public void setVehicleDesc(String vehicleDesc) {
        this.vehicleDesc = vehicleDesc;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    public String getSignPhotoUrl() {
        return signPhotoUrl;
    }

    public void setSignPhotoUrl(String signPhotoUrl) {
        this.signPhotoUrl = signPhotoUrl;
    }

    public Long getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Long finishDate) {
        this.finishDate = finishDate;
    }

    public Long getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(Long startedDate) {
        this.startedDate = startedDate;
    }

    public Map<String, Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Map<String, Double> coordinates) {
        this.coordinates = coordinates;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Long getTripDistance() {
        if (odoStart == null || odoEnd == null) return null;
        else return odoEnd - odoStart;
    }

    public Long getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Long lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifierUserId() {
        return lastModifierUserId;
    }

    public void setLastModifierUserId(String lastModifierUserId) {
        this.lastModifierUserId = lastModifierUserId;
    }
}
