package com.chipred.produccionspx.cars;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Date;

public class TripOverviewAdapter extends RecyclerView.Adapter<TripOverviewAdapter.TripOverviewViewHolder>
{
    private ArrayList<Trip> trips;

    public TripOverviewAdapter(ArrayList<Trip> trips)
    {
        this.trips = trips;
    }

    @NonNull
    @Override
    public TripOverviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.trip_overview_row,
                        viewGroup, false);

        return new TripOverviewViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TripOverviewViewHolder tripOverviewViewHolder,
                                 int i)
    {
        // Obtener vehículo
        Trip trip = trips.get(i);

        // Obtener lista de lugares
        StringBuilder places = new StringBuilder();
        for (int idx = 0; idx < trip.getPlaces().size(); idx++)
        {
            places.append(trip.getPlaces().get(idx));
            if (idx != trip.getPlaces().size() - 1)
            {
                places.append("\n");
            }
        }

        // Obtener fecha de inicio de viaje en versión String
        String dateStr = Constants.dateObjectToString2(new Date(trip.getStartDate()));
        if (dateStr != null)
            dateStr = dateStr.replace(" ", "\n");

        // Obtener kilometraje del viaje
        Long tripDistance = trip.getTripDistance();
        String formattedTripDistance = "";
        if (tripDistance != null)
            formattedTripDistance = Constants.distanceFormat.format(tripDistance);

        tripOverviewViewHolder.userTv.setText(trip.getUserName().replace(" ", "\n"));
        tripOverviewViewHolder.placesTv.setText(places.toString());
        tripOverviewViewHolder.dateTv.setText(dateStr);
        tripOverviewViewHolder.kilometersTv.setText(formattedTripDistance);
    }

    @Override
    public int getItemCount()
    {
        return trips.size();
    }

    static class TripOverviewViewHolder extends RecyclerView.ViewHolder
    {
        private TextView userTv;
        private TextView placesTv;
        private TextView dateTv;
        private TextView kilometersTv;

        TripOverviewViewHolder(@NonNull View itemView)
        {
            super(itemView);
            userTv = itemView.findViewById(R.id.trip_user_tv);
            placesTv = itemView.findViewById(R.id.trip_place_tv);
            dateTv = itemView.findViewById(R.id.trip_start_date_tv);
            kilometersTv = itemView.findViewById(R.id.trip_kilometes_tv);
        }
    }
}
