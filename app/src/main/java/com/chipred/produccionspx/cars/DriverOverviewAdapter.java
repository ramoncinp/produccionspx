package com.chipred.produccionspx.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DriverOverviewAdapter extends RecyclerView.Adapter<DriverOverviewAdapter.CarOverviewViewHolder> implements View.OnClickListener
{
    private Context context;
    private ArrayList<Driver> drivers;
    private View.OnClickListener clickListener;

    public DriverOverviewAdapter(Context context, ArrayList<Object> drivers)
    {
        this.context = context;

        // Convertir lista
        this.drivers = new ArrayList<>();
        for (Object object : drivers)
        {
            this.drivers.add((Driver) object);
        }
    }

    @NonNull
    @Override
    public CarOverviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.driver_overview_layout,
                        viewGroup, false);

        CarOverviewViewHolder viewHolder = new CarOverviewViewHolder(v);
        v.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarOverviewViewHolder carOverviewViewHolder,
                                 int i)
    {
        // Obtener vehículo
        Driver driver = drivers.get(i);

        // Mostrar datos
        carOverviewViewHolder.name.setText(driver.getName());

        // Mostrar imagen
        if (driver.getPhotoUrl() != null && !driver.getPhotoUrl().isEmpty())
        {
            Glide.with(context).load(driver.getPhotoUrl()).dontAnimate().into(carOverviewViewHolder.image);
        }
        else
        {
            Glide.with(context).load(R.drawable.ic_steering_wheel).dontAnimate().into(carOverviewViewHolder.image);
        }
    }

    @Override
    public int getItemCount()
    {
        return drivers.size();
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    static class CarOverviewViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name;
        private CircleImageView image;

        CarOverviewViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.text_1);
            image = itemView.findViewById(R.id.trip_user_image);
        }
    }
}
