package com.chipred.produccionspx.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unchecked")
public class TripNamesAdapter extends ArrayAdapter<TripPlace>
{
    private List<TripPlace> placesNamesListFull;

    TripNamesAdapter(@NonNull Context context, @NonNull List<TripPlace> placeNameList)
    {
        super(context, 0, placeNameList);
        placesNamesListFull = new ArrayList<>(placeNameList);
    }

    @NonNull
    @Override
    public Filter getFilter()
    {
        return productNameFilter;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        TripPlace tripPlace = getItem(position);

        String placeName = tripPlace == null ? "" : tripPlace.getNombre();
        if (convertView == null)
        {
            convertView =
                    LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_expandable_list_item_1,
                            parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        if (placeName != null)
        {
            viewHolder.bindBeer(placeName);
        }

        return convertView;
    }

    private Filter productNameFilter = new Filter()
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            FilterResults results = new FilterResults();
            List<TripPlace> suggestionList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0)
            {
                suggestionList.addAll(placesNamesListFull);
            }
            else
            {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (TripPlace tripPlace : placesNamesListFull)
                {
                    if (tripPlace.getNombre().toLowerCase().contains(filterPattern))
                    {
                        suggestionList.add(tripPlace);
                    }
                }

                // Ordenar por número de viajes
                Collections.sort(suggestionList, (tripPlace, t1) -> t1.getViajes().compareTo(tripPlace.getViajes()));
            }
            results.values = suggestionList;
            results.count = suggestionList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            List<TripPlace> productNameList = (List<TripPlace>) results.values;
            if (productNameList != null)
            {
                clear();
                addAll(productNameList);
            }
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue)
        {
            return ((TripPlace) resultValue).getNombre();
        }
    };

    private class ViewHolder
    {
        private TextView placeNameTextView;

        ViewHolder(View itemView)
        {
            placeNameTextView = itemView.findViewById(android.R.id.text1);
        }

        void bindBeer(String productName)
        {
            placeNameTextView.setText(productName);
        }
    }
}