package com.chipred.produccionspx.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unchecked")
public class TripPassengersAdapter extends ArrayAdapter<TripPassenger>
{
    private List<TripPassenger> passengersNamesListFull;

    TripPassengersAdapter(@NonNull Context context, @NonNull List<TripPassenger> passengerList)
    {
        super(context, 0, passengerList);
        passengersNamesListFull = new ArrayList<>(passengerList);
    }

    @NonNull
    @Override
    public Filter getFilter()
    {
        return passengerNameFilter;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        TripPassenger tripPassenger = getItem(position);

        String productName = tripPassenger == null ? "" : tripPassenger.getNombre();
        if (convertView == null)
        {
            convertView =
                    LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_expandable_list_item_1,
                            parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        if (productName != null)
        {
            viewHolder.bindBeer(productName);
        }

        return convertView;
    }

    private Filter passengerNameFilter = new Filter()
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            FilterResults results = new FilterResults();
            List<TripPassenger> suggestionList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0)
            {
                suggestionList.addAll(passengersNamesListFull);
            }
            else
            {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (TripPassenger tripPassenger : passengersNamesListFull)
                {
                    if (tripPassenger.getNombre().toLowerCase().contains(filterPattern))
                    {
                        suggestionList.add(tripPassenger);
                    }
                }
            }

            // Ordenar por número de viajes
            Collections.sort(suggestionList,
                    (tripPassenger, t1) -> t1.getViajes().compareTo(tripPassenger.getViajes()));

            results.values = suggestionList;
            results.count = suggestionList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            List<TripPassenger> passengerNameList = (List<TripPassenger>) results.values;
            if (passengerNameList != null)
            {
                clear();
                addAll(passengerNameList);
            }
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue)
        {
            return ((TripPassenger) resultValue).getNombre();
        }
    };

    private class ViewHolder
    {
        private TextView passengerNameTextView;

        ViewHolder(View itemView)
        {
            passengerNameTextView = itemView.findViewById(android.R.id.text1);
        }

        void bindBeer(String productName)
        {
            passengerNameTextView.setText(productName);
        }
    }
}