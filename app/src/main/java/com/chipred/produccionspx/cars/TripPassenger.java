package com.chipred.produccionspx.cars;

public class TripPassenger
{
    private String id;
    private String nombre;
    private Integer viajes;

    public TripPassenger()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Integer getViajes()
    {
        return viajes;
    }

    public void setViajes(Integer viajes)
    {
        this.viajes = viajes;
    }

    public void incrementCount()
    {
        viajes++;
    }
}