package com.chipred.produccionspx.cars;

import java.util.Comparator;

public class CarsComparator implements Comparator<CarDocument>
{
    @Override
    public int compare(CarDocument carDocument, CarDocument t1)
    {
        return Long.compare(t1.getDate(), carDocument.getDate());
    }

    @Override
    public boolean equals(Object o)
    {
        return false;
    }
}
