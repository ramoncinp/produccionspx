package com.chipred.produccionspx.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.UserViewHolder> implements View.OnClickListener
{
    private ArrayList<Car> cars;
    private Context context;
    private View.OnClickListener clickListener;

    public CarAdapter(Context context)
    {
        this.context = context;
    }

    public void setCars(ArrayList<Car> cars)
    {
        this.cars = cars;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_layout, parent,
                false);

        UserViewHolder userViewHolder = new UserViewHolder(v);
        v.setOnClickListener(clickListener);

        return userViewHolder;
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position)
    {
        // Obtener usuario
        Car car = cars.get(position);

        // Setear datos
        holder.name.setText(car.getModel());
        holder.status.setText(car.getStatus());

        // Mostrar imagen
        if (car.getImageUrl() == null || car.getImageUrl().isEmpty())
        {
            Glide.with(context).load(context.getDrawable(R.drawable.ic_binarium)).dontAnimate
                    ().into(holder.image);
        }
        else
        {
            Glide.with(context).load(car.getImageUrl()).dontAnimate().into(holder.image);
        }

        // Remover linea inferior
        if (position == cars.size() - 1)
        {
            holder.bottomView.setVisibility(View.INVISIBLE);
        }
        else
        {
            holder.bottomView.setVisibility(View.VISIBLE);
        }

        // Cambiar visibilidad
        if (car.getStatus().equals(Car.Status.Disponible.name()))
        {
            holder.itemView.setAlpha(1f);
        }
        else
        {
            holder.itemView.setAlpha(0.5f);
        }
    }

    @Override
    public int getItemCount()
    {
        return cars.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView image;
        private TextView name;
        private TextView status;
        private View bottomView;

        UserViewHolder(@NonNull View itemView)
        {
            super(itemView);
            image = itemView.findViewById(R.id.car_image);
            name = itemView.findViewById(R.id.car_model);
            status = itemView.findViewById(R.id.car_status);
            bottomView = itemView.findViewById(R.id.bottom_view);
        }
    }
}
