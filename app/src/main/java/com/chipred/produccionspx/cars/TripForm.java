package com.chipred.produccionspx.cars;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.network.Connectivity;
import com.chipred.produccionspx.users.UserData;
import com.chipred.produccionspx.utils.DatePickerFragment;
import com.chipred.produccionspx.DrawSignActivity;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.notifications.NotificationsManager;
import com.chipred.produccionspx.users.SelectDriverFragment;
import com.chipred.produccionspx.views.FuelGaugeView;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.chipred.produccionspx.Constants.TRIPS_COLLECTION;

public class TripForm extends AppCompatActivity {
    // Constantes
    private final static String TAG = TripForm.class.getSimpleName();
    private final static int OPEN_SIGN_PAD = 1;
    private final static int PLACE_LIST_ELEMENT = 2;
    private final static int PASSENGER_LIST_ELEMENT = 3;
    public final static String TRIP_ID = "tripId";
    public final static int MAX_DISTANCE_CONFIRMATION = 100;

    // Variables
    private boolean isUser;
    private boolean tripFinished = false;
    private String tripId;
    private String driverUserId;
    private String signImagePath;
    private String thisUserId = "";

    // Objetos
    private ArrayList<MaterialAutoCompleteTextView> tripsEditTexts = new ArrayList<>();
    private ArrayList<MaterialAutoCompleteTextView> passengersEditTexts = new ArrayList<>();
    private final List<TripPassenger> commonPassengers = new ArrayList<>();
    private final List<TripPlace> commonPlaces = new ArrayList<>();
    private Car selectedCar;
    private Driver selectedDriver;
    private NotificationsManager notificationsManager;
    private Trip mTrip = new Trip();
    private final UserData lastModifierUser = new UserData();

    // Referencias
    private final FirebaseFirestore root = FirebaseFirestore.getInstance();
    private CollectionReference tripsCollection;
    private FirebaseStorage firebaseStorage;

    // Views
    private Button requestCar;
    private Button holdTrip;
    private Button acceptCarRequest;
    private CardView signCardView;
    private CheckBox notifyUsers;
    private FuelGaugeView startFuelView;
    private FuelGaugeView endFuelView;
    private ImageView signView;
    private ImageView tripUserImage;
    private LinearLayout carAdminButtons;
    private LinearLayout morePlacesLayout;
    private LinearLayout morePassengersLayout;
    private LinearLayout startFuelLayout;
    private LinearLayout endFuelLayout;
    private MaterialAutoCompleteTextView tripPlaceEt;
    private MaterialAutoCompleteTextView passengerEt;
    private MaterialEditText startDateEt;
    private MaterialEditText startTimeHourEt;
    private MaterialEditText startTimeMinuteEt;
    private MaterialBetterSpinner startTimeFormatEt;
    private MaterialEditText endDateEt;
    private MaterialEditText endTimeHourEt;
    private MaterialEditText endTimeMinuteEt;
    private LinearLayout endTimeLayout;
    private MaterialBetterSpinner endTimeFormatEt;
    private MaterialEditText startOdoEt;
    private MaterialEditText endOdoEt;
    private MaterialEditText crmTicketEt;
    private MaterialEditText asuntoEt;
    private MaterialEditText observationsEt;
    private ProgressBar progressBar;
    private RelativeLayout signLayout;
    private ScrollView contentLayout;
    private SeekBar startFuelSeeker;
    private SeekBar endFuelSeeker;
    private TextView selectedCarText;
    private TextView tripUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_form);
        setTitle("Salida");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Obtener cargo del usuario
        isUser = getIntent().getBooleanExtra("isUser", false);
        tripId = getIntent().getStringExtra(TRIP_ID);

        // Obtener id de usuario
        getUserId();

        // Inicializar views
        initViews();

        // Mostrar progreso
        showProgressBar();

        // Preparar layout para el tipo de usuario
        prepareLayout();

        // Inicializar colección de base de datos
        initDb();

        if (tripId != null) {
            // Obtener datos del viaje
            getTrip();
        } else {
            // Obtener usuario correspondiente
            getTripUser();

            // Mostrar formulario
            hideProgressBar();

            // Obtener sugerencias de lugares y pasajeros
            getPlaces();
            getPassengers();
        }

        // Crear manejador de notificaciones
        notificationsManager = new NotificationsManager(new NotificationsManager.OnMessageReceived() {
            @Override
            public void onSuccess(String message) {
                Log.d("NotificationsManager", message);
            }

            @Override
            public void onError(String errorMsg) {
                Log.e("NotificationsManager", errorMsg);
            }
        });
    }

    private void getUserId() {
        // Obtener usuario
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        if (currentUser != null) {
            // Obtener id de usuario actual
            thisUserId = currentUser.getUid();
        }
    }

    private void initDb() {
        tripsCollection = root.collection(TRIPS_COLLECTION);
        firebaseStorage = FirebaseStorage.getInstance();
    }

    private void getTrip() {
        tripsCollection.document(tripId).get().addOnCompleteListener(task ->
        {
            if (task.isSuccessful() && task.getResult() != null && !task.getResult().getMetadata().isFromCache()) {
                // Obtener objeto
                Trip trip = task.getResult().toObject(Trip.class);

                if (trip != null) {
                    mTrip = trip;
                    mTrip.setId(tripId);

                    // Obtener estado
                    if (mTrip.getCurrentState() != null) {
                        if (mTrip.getCurrentState().equals(Trip.State.TERMINADA.name())) {
                            tripFinished = true;
                        }

                        // Si es administrador y el estado actual es SOLICITADA, PENDIENTE o
                        // AUTORIZADA
                        if (!isUser && (mTrip.getCurrentState().equals(Trip.State.SOLICITADA.name()) ||
                                mTrip.getCurrentState().equals(Trip.State.PENDIENTE.name()) ||
                                mTrip.getCurrentState().equals(Trip.State.AUTORIZADA.name()))) {
                            getPlaces();
                        }
                    }

                    // Obtener usuario
                    getTripUser();

                    // Mostrar datos
                    setTripData();

                    // Preparar layout por el estado del viaje
                    prepareLayout();
                }
            } else {
                // Salir de la actividad
                Toast.makeText(TripForm.this, "Error al obtener datos de la salida",
                        Toast.LENGTH_SHORT).show();
                endActivity(RESULT_CANCELED);
            }

            // Mostrar contenido
            hideProgressBar();
        });
    }

    private void initViews() {
        signLayout = findViewById(R.id.sign_layout);
        signLayout.setOnClickListener(view ->
        {
            Intent intent = new Intent(TripForm.this, DrawSignActivity.class);
            startActivityForResult(intent, OPEN_SIGN_PAD);
        });

        progressBar = findViewById(R.id.progress_bar);
        contentLayout = findViewById(R.id.content_layout);
        tripPlaceEt = findViewById(R.id.trip_place_et);
        passengerEt = findViewById(R.id.passenger_et);
        tripUserName = findViewById(R.id.trip_user_name);
        tripUserImage = findViewById(R.id.trip_user_image);
        signView = findViewById(R.id.sign_image_view);
        startFuelView = findViewById(R.id.start_fuel_gauge);
        endFuelView = findViewById(R.id.end_fuel_gauge);
        startFuelSeeker = findViewById(R.id.start_seek_bar);
        endFuelSeeker = findViewById(R.id.end_seek_bar);
        LinearLayout selectVehicleLayout = findViewById(R.id.select_vehicle_layout);

        // Views de fechas
        startDateEt = findViewById(R.id.start_day_et);
        endDateEt = findViewById(R.id.end_day_et);
        startTimeHourEt = findViewById(R.id.start_time_hour_et);
        startTimeMinuteEt = findViewById(R.id.start_time_minute_et);
        startTimeFormatEt = findViewById(R.id.start_time_format);
        endTimeHourEt = findViewById(R.id.end_time_hour_et);
        endTimeMinuteEt = findViewById(R.id.end_time_minute_et);
        endTimeFormatEt = findViewById(R.id.end_time_format);
        endTimeLayout = findViewById(R.id.end_time_layout);
        initTimeFormatSpinnters();

        notifyUsers = findViewById(R.id.notify_checkbox);
        crmTicketEt = findViewById(R.id.crm_ticket);
        startOdoEt = findViewById(R.id.start_odo_et);
        endOdoEt = findViewById(R.id.end_odo_et);
        asuntoEt = findViewById(R.id.asunto_et);
        observationsEt = findViewById(R.id.observaciones_et);
        selectedCarText = findViewById(R.id.trip_vehicle_text);
        LinearLayout tripUserLayout = findViewById(R.id.trip_user_layout);
        setSeekerListeners();

        startFuelLayout = findViewById(R.id.start_fuel_layout);
        endFuelLayout = findViewById(R.id.end_fuel_layout);
        signCardView = findViewById(R.id.sign_card_view);

        //Definir filtro de maysúculas
        passengerEt.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        selectVehicleLayout.setOnClickListener(view ->
        {
            CarListFragment carListFragment = new CarListFragment();
            carListFragment.show(getSupportFragmentManager(), "select_car");
            carListFragment.setCarListInterface(car ->
            {
                TripForm.this.selectedCar = car;
                TripForm.this.onSelectedCar();
            });
        });

        tripUserLayout.setOnClickListener(view ->
        {
            if (!isUser && tripId == null) {
                SelectDriverFragment selectDriverFragment = new SelectDriverFragment();
                selectDriverFragment.setSelectDriverInterface(this::onDriverSelected);
                selectDriverFragment.show(getSupportFragmentManager(), "selectDriverFragment");
            }
        });

        morePlacesLayout = findViewById(R.id.more_trips_layout);
        morePassengersLayout = findViewById(R.id.more_passengers_layout);

        // Definir listeners para agregar destinos y pasajeros
        CardView addPlaceButton = findViewById(R.id.add_trip_place);
        addPlaceButton.setOnClickListener(view -> addElementToLayoutList("", PLACE_LIST_ELEMENT));

        CardView addPassengerButton = findViewById(R.id.add_passenger);
        addPassengerButton.setOnClickListener(view -> addElementToLayoutList("",
                PASSENGER_LIST_ELEMENT));

        View.OnClickListener datePickerListener = view ->
        {
            DatePickerFragment fragment = DatePickerFragment.newInstance((datePicker, i, i1, i2) ->
            {
                String gottenDate =
                        Constants.twoDigits(i2) + "/" + Constants.monthNumberToString
                                (i1 + 1) + "/" + i;

                final MaterialEditText dateEt = (MaterialEditText) view;
                dateEt.setText(gottenDate);
            });

            fragment.show(TripForm.this.getSupportFragmentManager(), "datePicker");
        };

        // Definir listeners para fechas
        startDateEt.setOnClickListener(datePickerListener);
        endDateEt.setOnClickListener(datePickerListener);

        // Botón para solicitar vehículo
        requestCar = findViewById(R.id.request_car);
        requestCar.setOnClickListener(view ->
        {
            if (getTripData()) {
                showProgressBar();
                if (tripId != null) {
                    editTrip();
                } else {
                    if (isUser) getScheludedTrips();
                    else addNewTrip();
                }
            }
        });

        carAdminButtons = findViewById(R.id.car_admin_buttons);
    }

    private boolean getTripData() {
        // Validar conexión
        if (!Connectivity.isGoodConnection(getApplicationContext())) {
            Constants.showDialog(this, "Aviso", "No tiene buena conexión a internet, compruébela e intente de nuevo", (dialogInterface, i) -> dialogInterface.dismiss());
            return false;
        }

        METValidator emptyValidator = new METValidator("Campo obligatorio") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty;
            }
        };

        // Validar
        boolean valid = tripPlaceEt.validateWith(emptyValidator);
        valid &= asuntoEt.validateWith(emptyValidator);
        valid &= selectedCar != null;
        valid &= startDateEt.validateWith(new METValidator("La fecha no es válida") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                // Validar la longitud mínima del formato
                return text.length() == 11;
            }
        });
        valid &= Constants.isTimeFormValid(startTimeHourEt, startTimeMinuteEt);

        if (selectedCar == null) {
            selectedCarText.setTextColor(ContextCompat.getColor(this,
                    android.R.color.holo_red_dark));
        }

        // Evaluar con base en estado actual
        if (mTrip.getCurrentState() != null) {
            if (mTrip.getCurrentState().equals(Trip.State.AUTORIZADA.name())) {
                valid &= startOdoEt.validateWith(emptyValidator);
                valid &= signImagePath != null && !signImagePath.isEmpty();
                if (signImagePath == null || signImagePath.isEmpty()) {
                    Toast.makeText(this, "Firma obligatoria", Toast.LENGTH_SHORT).show();
                }
            } else if (mTrip.getCurrentState().equals(Trip.State.ACTIVA.name())) {
                valid &= endOdoEt.validateWith(new METValidator("Kilometraje no válido") {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                        boolean isValid = false;
                        if (!isEmpty) {
                            // Obtener kilometraje final registrado
                            long endOdo = Long.parseLong(text.toString());

                            // Obtener kilometraje inicial
                            long startOdo = mTrip.getOdoStart();

                            // Validar que el kilometraje final sea mayor o igual al inicial
                            isValid = endOdo >= startOdo;
                        }

                        return isValid;
                    }
                });

                // Validar combustible final
                boolean endFuelValid =
                        !(startFuelView.getFuelLevel() == endFuelView.getFuelLevel());
                valid &= endFuelValid;
                TextView errorTv = findViewById(R.id.end_fuel_error_tv);
                errorTv.setVisibility(endFuelValid ? View.GONE : View.VISIBLE);
            }
        }

        if (!valid) {
            return false;
        }

        // Obtener usuario
        mTrip.setResponsibleUserId(driverUserId);
        mTrip.setUserName(selectedDriver.getName());

        // Obtener lugares
        mTrip.setPlaces(new ArrayList<>());
        mTrip.addPlace(tripPlaceEt.getText().toString().trim());
        if (morePlacesLayout.getChildCount() > 0) {
            for (int i = 0; i < morePlacesLayout.getChildCount(); i++) {
                View editTextLayout = morePlacesLayout.getChildAt(i);
                MaterialAutoCompleteTextView editText =
                        editTextLayout.findViewById(R.id.new_trip_place);

                // Validar que el contenido del texto no sea vacío
                if (!editText.getText().toString().isEmpty())
                    mTrip.addPlace(editText.getText().toString().trim());
            }
        }

        // Obtener acompañantes
        mTrip.setPassengers(new ArrayList<>());

        if (passengerEt.getText().length() > 0)
            mTrip.addPassenger(passengerEt.getText().toString().trim());

        if (morePassengersLayout.getChildCount() > 0) {
            for (int i = 0; i < morePassengersLayout.getChildCount(); i++) {
                View editTextLayout = morePassengersLayout.getChildAt(i);
                MaterialAutoCompleteTextView editText =
                        editTextLayout.findViewById(R.id.new_passenger_et);

                // Validar que el contenido del texto no sea vacío
                if (!editText.getText().toString().isEmpty())
                    mTrip.addPassenger(editText.getText().toString().trim());
            }
        }

        // Obtener fecha de inicio de viaje
        String startDateString = startDateEt.getText().toString() + " " + Constants.timeFormToSring(
                startTimeHourEt,
                startTimeMinuteEt,
                startTimeFormatEt
        );
        Date startDateObj = Constants.dateStringToObject(startDateString);
        mTrip.setStartDate(startDateObj != null ? startDateObj.getTime() : null);

        // Obtener fecha de final de viaje
        if (!endDateEt.getText().toString().isEmpty()) {
            // Obtener fecha de inicio de viaje
            String endDateString = endDateEt.getText().toString() + " " + Constants.timeFormToSring(
                    endTimeHourEt,
                    endTimeMinuteEt,
                    endTimeFormatEt
            );
            Date endDateObj = Constants.dateStringToObject(endDateString);
            mTrip.setFinishDate(endDateObj != null ? endDateObj.getTime() : null);
        }

        // Obtener asunto
        mTrip.setDescription(asuntoEt.getText().toString());

        // Obtener observaciones
        mTrip.setObservations(observationsEt.getText().toString());

        // Obtener kilometraje inicial
        if (startOdoEt.validateWith(emptyValidator)) {
            mTrip.setOdoStart(Long.parseLong(startOdoEt.getText().toString()));
        }

        // Obtener kilometraje final
        if (endOdoEt.validateWith(emptyValidator)) {
            mTrip.setOdoEnd(Long.parseLong(endOdoEt.getText().toString()));
        }

        // Obtener nivel de combustible de inicio
        Integer startFuelLevel = (int) (startFuelView.getFuelLevel() * 100);
        mTrip.setFuelStart(startFuelLevel);

        // Obtener nivel de combustible del final
        Integer endFuelLevel = (int) (endFuelView.getFuelLevel() * 100);
        mTrip.setFuelEnd(endFuelLevel);

        // Obtener ticketCRM
        if (crmTicketEt.validateWith(emptyValidator)) {
            mTrip.setCrmTicket(crmTicketEt.getText().toString());
        }

        return true;
    }

    private void setTripData() {
        // Reiniciar variables
        tripsEditTexts = new ArrayList<>();
        morePlacesLayout.removeAllViews();

        passengersEditTexts = new ArrayList<>();
        morePassengersLayout.removeAllViews();

        // Obtener lugares
        for (int idx = 0; idx < mTrip.getPlaces().size(); idx++) {
            String place = mTrip.getPlaces().get(idx);
            if (idx == 0) tripPlaceEt.setText(place);
            else {
                addElementToLayoutList(place, PLACE_LIST_ELEMENT);
            }
        }

        // Obtener pasajeros
        for (int idx = 0; idx < mTrip.getPassengers().size(); idx++) {
            String passenger = mTrip.getPassengers().get(idx);
            if (idx == 0) passengerEt.setText(passenger);
            else {
                addElementToLayoutList(passenger, PASSENGER_LIST_ELEMENT);
            }
        }

        // Mostrar fecha de salida
        String startDateStr = mTrip.getStartDate() != null ?
                Constants.dateObjectToString2(new Date(mTrip.getStartDate())) : "";

        if (!startDateStr.isEmpty()) {
            String[] dateDate = Constants.timeFormFromString(startDateStr);
            startDateEt.setText(dateDate[0]);
            startTimeHourEt.setText(dateDate[1]);
            startTimeMinuteEt.setText(dateDate[2]);
            startTimeFormatEt.setText(dateDate[3]);
        }

        // Mostrar fecha de llegada
        String endDateStr = mTrip.getFinishDate() != null ?
                Constants.dateObjectToString2(new Date(mTrip.getFinishDate())) : "";

        if (!endDateStr.isEmpty()) {
            String[] dateDate = Constants.timeFormFromString(endDateStr);
            endDateEt.setText(dateDate[0]);
            endTimeHourEt.setText(dateDate[1]);
            endTimeMinuteEt.setText(dateDate[2]);
            endTimeFormatEt.setText(dateDate[3]);
        }

        // Mostrar asunto
        asuntoEt.setText(mTrip.getDescription());

        // Mostrar observaciones
        observationsEt.setText(mTrip.getObservations());

        // Obtener vehículo
        getTripVehicle();

        // Mostrar datos de vehículo
        if (mTrip.getOdoStart() != null)
            startOdoEt.setText(String.valueOf(mTrip.getOdoStart()));

        if (mTrip.getOdoEnd() != null)
            endOdoEt.setText(String.valueOf(mTrip.getOdoEnd()));

        if (mTrip.getFuelStart() != null) {
            startFuelView.setFuelLevel((float) (mTrip.getFuelStart() / 100.0));
            startFuelSeeker.setProgress(mTrip.getFuelStart());
        }

        if (mTrip.getFuelEnd() != null) {
            endFuelView.setFuelLevel((float) (mTrip.getFuelEnd() / 100.0));
            endFuelSeeker.setProgress(mTrip.getFuelEnd());
        }

        crmTicketEt.setText(mTrip.getCrmTicket());

        // Obtener firma si es que existe
        getResponsibleSign();
    }

    private void addElementToLayoutList(String element, int type) {
        LinearLayout linearLayoutList;
        View elementLayout;
        MaterialAutoCompleteTextView newElement;
        String newHint;

        if (type == PLACE_LIST_ELEMENT) {
            elementLayout = getLayoutInflater().inflate(R.layout.trip_place_list_element, null);
            morePlacesLayout.addView(elementLayout);

            // Obtener edit text
            newElement = elementLayout.findViewById(R.id.new_trip_place);
            newElement.setDropDownBackgroundResource(android.R.color.white);
            newElement.setThreshold(1);
            if (commonPlaces != null)
                newElement.setAdapter(new TripNamesAdapter(this, new ArrayList<>(commonPlaces)));

            // Obtener indice
            final int idx = tripsEditTexts.size();

            // Definir hint
            newHint = "Destino " + (idx + 2);
            newElement.setHint(newHint);
            newElement.setFloatingLabelText(newHint);
            newElement.setText(element);

            tripsEditTexts.add(newElement);
            linearLayoutList = morePlacesLayout;
        } else {
            elementLayout = getLayoutInflater().inflate(R.layout.passenger_list_element, null);
            morePassengersLayout.addView(elementLayout);

            // Obtener edit text
            newElement = elementLayout.findViewById(R.id.new_passenger_et);
            newElement.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            newElement.setDropDownBackgroundResource(android.R.color.white);
            newElement.setThreshold(1);
            if (commonPassengers != null)
                newElement.setAdapter(new TripPassengersAdapter(this,
                        new ArrayList<>(commonPassengers)));

            // Obtener indice
            final int idx = passengersEditTexts.size();

            // Definir hint
            newHint = "Acompañante " + (idx + 2);
            newElement.setHint(newHint);
            newElement.setFloatingLabelText(newHint);
            newElement.setText(element);

            passengersEditTexts.add(newElement);
            linearLayoutList = morePassengersLayout;
        }

        // Modificar lista
        onListModified(linearLayoutList, type);
    }

    private void onListModified(LinearLayout layout, int type) {
        //Obtener cantidad de editText agregados
        int elementCount = layout.getChildCount();
        //Redefinir listeners para remover cada uno
        for (int i = 0; i < elementCount; i++) {
            View mElementLayout = layout.getChildAt(i);
            //Obtener MaterialEditText
            MaterialAutoCompleteTextView newElement =
                    mElementLayout.findViewById(type == PLACE_LIST_ELEMENT ?
                            R.id.new_trip_place : R.id.new_passenger_et);

            final int elementNumber = i;
            //Modificar hint
            String newHint =
                    (type == PLACE_LIST_ELEMENT ? "Destino " : "Acompañante ") + (elementNumber + 2);
            newElement.setHint(newHint);
            newElement.setFloatingLabelText(newHint);

            //Definir botón para remover
            CardView removeElement = mElementLayout.findViewById(R.id.remove_button);
            removeElement.setOnClickListener(view -> {
                //Obtener Linenar layout que contiene EditText y CardView
                LinearLayout parentLayout = (LinearLayout) view.getParent();
                //Obtener Linear layout donde se encuentra el padre
                LinearLayout principalLayout = (LinearLayout) parentLayout.getParent();
                principalLayout.removeView(parentLayout);

                //Eliminar último elemento
                if (type == PLACE_LIST_ELEMENT) {
                    tripsEditTexts.remove(elementNumber);
                } else {
                    passengersEditTexts.remove(elementNumber);
                }

                //Actualizar listeners
                onListModified(layout, type);
            });
        }
    }

    private void onSelectedCar() {
        ImageView carImage = findViewById(R.id.trip_vehicle_image);

        if (selectedCar.getImageUrl() != null && !selectedCar.getImageUrl().isEmpty()) {
            Glide.with(this).load(selectedCar.getImageUrl()).dontAnimate().into(carImage);
        }

        // Escribir datos de vehículo seleccionado en views de formulario
        selectedCarText.setText(selectedCar.getModel());

        // Sugerir odometro solamente cuando este autorizado le viaje
        if (mTrip.getCurrentState() != null && mTrip.getCurrentState().equals(Trip.State.AUTORIZADA.name())) {
            startOdoEt.setText(String.valueOf(selectedCar.getOdometer()));
            startFuelView.setFuelLevel((float) (selectedCar.getFuelPercentage() / 100.0));
            startFuelSeeker.setProgress(selectedCar.getFuelPercentage());
        }

        // Definir vehículo a viaje
        mTrip.setVehicleId(selectedCar.getId());
        mTrip.setVehicleDesc(selectedCar.getModel());

        // Evaluar si se debe calcular el rendimiento
        if (mTrip.getCurrentState() != null && mTrip.getCurrentState().equals(Trip.State.TERMINADA.name()) && !isUser) {
            MaterialEditText carPerformanceEt = findViewById(R.id.car_performance_et);

            if (mTrip.getFuelEnd() > mTrip.getFuelStart()) {
                String message = "Se realizó carga de gasolina";
                carPerformanceEt.setText(message);
            } else {
                Long distance = mTrip.getOdoEnd() - mTrip.getOdoStart();
                Double fuelConsumption =
                        ((mTrip.getFuelStart() - mTrip.getFuelEnd()) * 0.01) * selectedCar.getTankCapacity();
                Double performance = distance / fuelConsumption;

                carPerformanceEt.setText(Constants.litersDecimalFormat.format(performance));
            }

            carPerformanceEt.setVisibility(View.VISIBLE);
        }

        if (mTrip.getCurrentState() != null && mTrip.getCurrentState().equals(Trip.State.SOLICITADA.name()) && !isUser) {
            if (selectedCar.kmsToMtto() != null && selectedCar.kmsToMtto() <= Car.MAX_ODO_MTTO_DIFFERENCE) {
                showOdoMttoAlert();
            }
        }
    }

    private void setSeekerListeners() {
        startFuelSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                startFuelView.setFuelLevel((float) (i / 100.0));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        endFuelSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                endFuelView.setFuelLevel((float) (i / 100.0));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.trip_form_menu, menu);
        if (tripId == null) menu.findItem(R.id.trip_info).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            endActivity(RESULT_CANCELED);
        } else if (item.getItemId() == R.id.trip_info) {
            showMoreTripData();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OPEN_SIGN_PAD && resultCode == RESULT_OK) {
            // Obtener bitmap
            if (data != null) {
                // Mostrar imagen
                signImagePath = data.getStringExtra("imagePath");
                signLayout.setBackgroundColor(ContextCompat.getColor(TripForm.this,
                        android.R.color.white));
                Glide.with(TripForm.this).load(signImagePath).dontAnimate().into(signView);
            }
        }
    }

    private void showMoreTripData() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Información");

        String message = "";
        if (mTrip.getLastModifiedDate() == null) {
            message = "No disponible";
        } else {
            String lastModifierUserName = lastModifierUser == null ? "" : lastModifierUser.getName();
            String lastModifiedDate = Constants.dateObjectToString2(new Date(mTrip.getLastModifiedDate()));
            message = "Última modificación el " + lastModifiedDate + '\n';
            message += "Por " + lastModifierUserName;
        }

        builder.setMessage(message);
        builder.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    }

    private void prepareLayout() {
        if (isUser) {
            // Ocultar views de administrador
            crmTicketEt.setVisibility(View.GONE);
            startOdoEt.setVisibility(View.GONE);
            endOdoEt.setVisibility(View.GONE);
            signCardView.setVisibility(View.GONE);
            startFuelLayout.setVisibility(View.GONE);
            endFuelLayout.setVisibility(View.GONE);
            observationsEt.setVisibility(View.GONE);
            notifyUsers.setVisibility(View.VISIBLE);

            if (tripId != null) {
                // Evaluar estados
                if (mTrip.getCurrentState() == null) {
                    return;
                }

                if (mTrip.getCurrentState().equals(Trip.State.SOLICITADA.toString()) || mTrip.getCurrentState().equals(Trip.State.PENDIENTE.toString())) {
                    requestCar.setText(getResources().getString(R.string.editar));
                    notifyUsers.setVisibility(View.GONE);
                } else if (mTrip.getCurrentState().equals(Trip.State.AUTORIZADA.toString())) {
                    carAdminButtons.setVisibility(View.GONE);
                    requestCar.setVisibility(View.GONE);
                    notifyUsers.setVisibility(View.GONE);
                } else if (mTrip.getCurrentState().equals(Trip.State.TERMINADA.toString())) {
                    carAdminButtons.setVisibility(View.GONE);
                    requestCar.setVisibility(View.GONE);
                    startOdoEt.setVisibility(View.VISIBLE);
                    endOdoEt.setVisibility(View.VISIBLE);
                    notifyUsers.setVisibility(View.GONE);
                } else if (mTrip.getCurrentState().equals(Trip.State.ACTIVA.toString())) {
                    // Mostrar views para terminar viaje
                    startOdoEt.setVisibility(View.VISIBLE);
                    endOdoEt.setVisibility(View.VISIBLE);
                    startFuelLayout.setVisibility(View.VISIBLE);
                    endFuelLayout.setVisibility(View.VISIBLE);
                    carAdminButtons.setVisibility(View.VISIBLE);
                    requestCar.setVisibility(View.GONE);
                    notifyUsers.setVisibility(View.GONE);

                    acceptCarRequest = findViewById(R.id.accept_car_request);
                    holdTrip = findViewById(R.id.add_request_to_queue_button);
                    holdTrip.setVisibility(View.GONE);

                    acceptCarRequest.setText(getResources().getString(R.string.finish_trip));
                    acceptCarRequest.setOnClickListener(view ->
                    {
                        if (getTripData()) {
                            showProgressBar();
                            mTrip.setCurrentState(Trip.State.TERMINADA.toString());

                            // Validar odometro
                            if (mTrip.getOdoEnd() - mTrip.getOdoStart() >= MAX_DISTANCE_CONFIRMATION) {
                                // Mostrar confirmación
                                confirmOdo(mTrip.getOdoEnd(), clickListenerView -> editTrip());
                            } else {
                                editTrip();
                            }
                        }
                    });
                }
            }

            // Si el viaje no es del usuario, no permitir editar
            if (mTrip.getResponsibleUserId() != null && !mTrip.getResponsibleUserId().equals(thisUserId)) {
                if (acceptCarRequest != null) acceptCarRequest.setVisibility(View.GONE);
                if (requestCar != null) requestCar.setVisibility(View.GONE);
                if (holdTrip != null) holdTrip.setVisibility(View.GONE);
            }
        } else {
            if (mTrip.getCurrentState() == null) return;

            // Definir listeners para botones de administrador
            acceptCarRequest = findViewById(R.id.accept_car_request);
            holdTrip = findViewById(R.id.add_request_to_queue_button);

            acceptCarRequest.setOnClickListener(view ->
            {
                if (mTrip.getCurrentState().equals(Trip.State.SOLICITADA.toString()) || mTrip.getCurrentState().equals(Trip.State.PENDIENTE.toString())) {
                    showProgressBar();
                    mTrip.setCurrentState(Trip.State.AUTORIZADA.toString());
                } else if (mTrip.getCurrentState().equals(Trip.State.AUTORIZADA.toString())) {
                    startTrip();
                    return;
                } else if (mTrip.getCurrentState().equals(Trip.State.ACTIVA.toString())) {
                    if (getTripData()) {
                        showProgressBar();
                        mTrip.setCurrentState(Trip.State.TERMINADA.toString());

                        // Validar odometro
                        if (mTrip.getOdoEnd() - mTrip.getOdoStart() >= MAX_DISTANCE_CONFIRMATION) {
                            // Mostrar confirmación
                            confirmOdo(mTrip.getOdoEnd(), clickListenerView -> editTrip());
                        } else {
                            editTrip();
                        }
                    }
                    return;
                }
                editTrip();
            });

            holdTrip.setOnClickListener(view ->
            {
                showProgressBar();
                mTrip.setCurrentState(Trip.State.PENDIENTE.toString());
                editTrip();
            });

            // Evaluar estado del viaje y mostrar u ocultar views correspondientes
            if (mTrip.getCurrentState().equals(Trip.State.SOLICITADA.toString())) {
                crmTicketEt.setVisibility(View.GONE);
                startOdoEt.setVisibility(View.GONE);
                endOdoEt.setVisibility(View.GONE);
                signCardView.setVisibility(View.GONE);

                startFuelLayout.setVisibility(View.GONE);
                endFuelLayout.setVisibility(View.GONE);
                carAdminButtons.setVisibility(View.VISIBLE);
                requestCar.setVisibility(View.GONE);
            } else if (mTrip.getCurrentState().equals(Trip.State.PENDIENTE.toString())) {
                crmTicketEt.setVisibility(View.GONE);
                startOdoEt.setVisibility(View.GONE);
                endOdoEt.setVisibility(View.GONE);
                signCardView.setVisibility(View.GONE);

                startFuelLayout.setVisibility(View.GONE);
                endFuelLayout.setVisibility(View.GONE);

                holdTrip.setVisibility(View.GONE);
                carAdminButtons.setVisibility(View.VISIBLE);
                requestCar.setVisibility(View.GONE);
            } else if (mTrip.getCurrentState().equals(Trip.State.AUTORIZADA.toString())) {
                endOdoEt.setVisibility(View.GONE);
                endFuelLayout.setVisibility(View.GONE);

                carAdminButtons.setVisibility(View.VISIBLE);
                holdTrip.setVisibility(View.GONE);
                requestCar.setVisibility(View.GONE);
                acceptCarRequest.setText(getResources().getString(R.string.iniciar_viaje));
            } else if (mTrip.getCurrentState().equals(Trip.State.ACTIVA.toString())) {
                carAdminButtons.setVisibility(View.VISIBLE);
                holdTrip.setVisibility(View.GONE);
                requestCar.setVisibility(View.GONE);
                acceptCarRequest.setText(getResources().getString(R.string.finish_trip));
            } else if (mTrip.getCurrentState().equals(Trip.State.TERMINADA.toString())) {
                carAdminButtons.setVisibility(View.GONE);
                endDateEt.setVisibility(View.VISIBLE);
                endTimeLayout.setVisibility(View.VISIBLE);
                requestCar.setVisibility(View.VISIBLE);
                requestCar.setText(getResources().getString(R.string.save));
            }
        }
    }

    private void getTripUser() {
        String userId;
        if (tripId == null) {
            // Obtener usuario
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = firebaseAuth.getCurrentUser();

            if (currentUser == null) return;
            userId = currentUser.getUid();
        } else {
            if (mTrip.getResponsibleUserId() != null) {
                userId = mTrip.getResponsibleUserId();
            } else {
                return;
            }
        }

        final CollectionReference usersCollection = root.collection("usuarios");
        usersCollection.document(userId).get().addOnCompleteListener(task ->
        {
            // Validar payload
            if (task.isSuccessful() && task.getResult() != null) {
                // Obtener conductor
                Driver driver = task.getResult().toObject(Driver.class);
                onDriverSelected(driver);
            } else {
                // Mostrar error
                Toast.makeText(TripForm.this, "Error al obtener usuario",
                        Toast.LENGTH_SHORT).show();

                endActivity(RESULT_CANCELED);
            }
        });

        if (mTrip.getLastModifierUserId() != null) {
            usersCollection.document(mTrip.getLastModifierUserId()).get().addOnCompleteListener(task ->
            {
                // Validar payload
                if (task.isSuccessful() && task.getResult() != null) {
                    // Obtener usuario que modifico por ultima vez el viaje
                    UserData userData = task.getResult().toObject(UserData.class);
                    if (userData != null) {
                        lastModifierUser.setName(userData.getName());
                    }
                } else {
                    Log.e(TAG, "Error al obtener nombre de último usuario que modificó el viaje");
                }
            });
        }
    }

    private void getTripVehicle() {
        if (mTrip.getVehicleId() == null) {
            return;
        }

        root.collection("autos").document(mTrip.getVehicleId()).get().addOnCompleteListener(task ->
        {
            if (task.isSuccessful() && task.getResult() != null) {
                Car car = task.getResult().toObject(Car.class);
                if (car != null) {
                    car.setId(task.getResult().getId());
                    selectedCar = car;
                    onSelectedCar();
                }
            }
        });
    }

    private void getResponsibleSign() {
        if (mTrip.getSignPhotoUrl() != null && !mTrip.getSignPhotoUrl().isEmpty()) {
            signLayout.setBackgroundColor(ContextCompat.getColor(TripForm.this,
                    android.R.color.white));
            Glide.with(this).load(mTrip.getSignPhotoUrl()).dontAnimate().into(signView);
        }
    }

    private void showErrorDialog(String title, String message, boolean finish) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (!title.isEmpty()) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Salir", (dialogInterface, i) ->
        {
            if (finish) endActivity(RESULT_CANCELED);
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void addNewTrip() {
        // Agregar nuevo viaje
        mTrip.setRequestDate(new Date().getTime());
        mTrip.setCurrentState(Trip.State.SOLICITADA.toString());
        // Obtener datos de modificación
        mTrip.setLastModifiedDate(new Date().getTime());
        mTrip.setLastModifierUserId(thisUserId);

        tripsCollection.add(mTrip).addOnCompleteListener(task ->
        {
            if (task.isSuccessful()) {
                Toast.makeText(TripForm.this, "Vehículo solicitado, espere confirmación",
                        Toast.LENGTH_LONG).show();

                // Notificar a administrador
                if (task.getResult() != null) {
                    mTrip.setId(task.getResult().getId());
                    notificationsManager.onNewCarRequest(mTrip);

                    // Evaluar si se debe notificar a los demás usuarios
                    if (isUser && notifyUsers.isChecked()) {
                        notificationsManager.onNewGeneralCarRequest(mTrip);
                    }
                }
            } else {
                Toast.makeText(TripForm.this, "Error al pedir vehículo, intente de nuevo",
                        Toast.LENGTH_LONG).show();
            }

            endActivity(RESULT_OK);
        });
    }

    private void addPlacesToDb() {
        final String collectionName = "viajes_lugares";

        // Crear referencia de la colección
        CollectionReference placesRef = root.collection(collectionName);

        // Iterar en cada uno de los lugares visitados
        for (String place : mTrip.getPlaces()) {
            // Ejecutar query para cada lugar, y así revisar si ya existe en el registro
            placesRef.whereEqualTo("nombre", place).limit(1).get().addOnCompleteListener(task ->
            {
                if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {
                    DocumentSnapshot documentSnapshot = task.getResult().getDocuments().get(0);
                    String documentId = documentSnapshot.getId();

                    // Convertir documento a TripPlace
                    TripPlace tripPlace = documentSnapshot.toObject(TripPlace.class);
                    tripPlace.setId(documentId);

                    // Aumentar contador del viaje
                    tripPlace.incrementCount();

                    // Actualizar registro
                    placesRef.document(tripPlace.getId()).update("viajes", tripPlace.getViajes());
                } else {
                    // No existe el lugar en la lista, agregar
                    TripPlace tripPlace = new TripPlace();
                    tripPlace.setNombre(place);
                    tripPlace.setViajes(1);

                    // Agregar a la base de datos
                    placesRef.add(tripPlace);
                }
            });
        }
    }

    private void addPassengersToDb() {
        if (mTrip.getPassengers().isEmpty()) {
            return;
        }

        final String collectionName = "viajes_pasajeros";

        // Crear referencia de la colección
        CollectionReference passengerRef = root.collection(collectionName);

        // Iterar en cada uno de los lugares visitados
        for (String passenger : mTrip.getPassengers()) {
            // Ejecutar query para cada lugar, y así revisar si ya existe en el registro
            passengerRef.whereEqualTo("nombre", passenger).limit(1).get().addOnCompleteListener(task ->
            {
                if (task.isSuccessful() && task.getResult().getDocuments() != null) {
                    final List<DocumentSnapshot> documentsList = task.getResult().getDocuments();

                    // Validar elementos de lista
                    if (!documentsList.isEmpty()) {
                        DocumentSnapshot documentSnapshot = documentsList.get(0);
                        String documentId = documentSnapshot.getId();

                        // Convertir documento a TripPassenger
                        TripPassenger tripPassenger = documentSnapshot.toObject(TripPassenger.class);
                        tripPassenger.setId(documentId);

                        // Aumentar contador del viaje
                        tripPassenger.incrementCount();

                        // Actualizar registro
                        passengerRef.document(tripPassenger.getId()).update("viajes",
                                tripPassenger.getViajes());
                    }
                } else {
                    // No existe el lugar en la lista, agregar
                    TripPassenger tripPassenger = new TripPassenger();
                    tripPassenger.setNombre(passenger);
                    tripPassenger.setViajes(1);

                    // Agregar a la base de datos
                    passengerRef.add(tripPassenger);
                }
            });
        }
    }

    private void editTrip() {
        // Buscar modificar el estado del vehículo
        if (mTrip.getCurrentState().equals(Trip.State.ACTIVA.name())) {
            selectedCar.setStatus(Car.Status.Ocupado.name().replace('_', ' '));
            mTrip.setStartedDate(new Date().getTime());
            modifyCar();
        } else if (mTrip.getCurrentState().equals(Trip.State.TERMINADA.name()) && !tripFinished) {
            mTrip.setFinishDate(new Date().getTime());
            selectedCar.setStatus(Car.Status.Disponible.name());
            selectedCar.setFuelPercentage(mTrip.getFuelEnd());
            selectedCar.setOdometer(mTrip.getOdoEnd());
            modifyCar();

            // Sumar lugares y pasajeros a la estadistica
            addPlacesToDb();
            addPassengersToDb();
        }

        // Obtener datos de modificación
        mTrip.setLastModifiedDate(new Date().getTime());
        mTrip.setLastModifierUserId(thisUserId);

        tripsCollection.document(tripId).set(mTrip).addOnCompleteListener(task ->
        {
            if (task.isSuccessful()) {
                Toast.makeText(TripForm.this, "Solicitud modificada",
                        Toast.LENGTH_LONG).show();

                onTripModified();
            } else {
                Toast.makeText(TripForm.this, "Error al modificar solicitud",
                        Toast.LENGTH_LONG).show();
            }

            endActivity(RESULT_OK);
        });
    }

    private void onTripModified() {
        // Enviar notificacion de viaje editado solo cuando:
        //  - Sea administrador o sea para terminar viaje (usuario)
        //  - Si el viaje no estaba terminado antes de entrar

        if (!isUser || mTrip.getCurrentState().equals(Trip.State.TERMINADA.name()) && !tripFinished) {
            notificationsManager.onCarRequestNewState(mTrip);
        }
    }

    private void startTrip() {
        if (getTripData()) {
            showProgressBar();
            mTrip.setCurrentState(Trip.State.ACTIVA.toString());

            // Obtener imagen de firma y subirla
            if (signImagePath != null && !signImagePath.isEmpty()) {
                uploadImage(signImagePath, TRIPS_COLLECTION + "/" + tripId);
            } else // Si no hubo firma, subir de una vez
            {
                editTrip();
            }
        }
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }

    private void uploadImage(String imagePath, String bdPath) {
        // Obtener referencia
        StorageReference storageReference =
                firebaseStorage.getReference().child(bdPath);

        // Obtener archivo
        File imageFile = new File(imagePath);

        // Convertir a arreglo de bytes
        int size = (int) imageFile.length();
        byte[] data = new byte[size];

        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(imageFile));
            buf.read(data, 0, data.length);
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error al leer el archivo a subir", Toast.LENGTH_SHORT).show();
            return;
        }

        // Subir archivo
        UploadTask uploadTask = storageReference.putBytes(data);

        // Definir proceso
        uploadTask.continueWithTask(task ->
        {
            // Manejar primer resultado
            if (!task.isSuccessful()) {
                throw task.getException();
            }

            return storageReference.getDownloadUrl();
        }).addOnCompleteListener(task ->
        {
            if (task.isSuccessful()) {
                // Obtener Uri del archivo recien subido
                Uri downloadUri = task.getResult();

                // Validar Uri
                if (downloadUri != null) {
                    mTrip.setSignPhotoUrl(downloadUri.toString());
                }
            } else {
                Toast.makeText(TripForm.this, "Error al subir archivo",
                        Toast.LENGTH_SHORT).show();
            }

            // Guardar cambios
            editTrip();
        });
    }

    private void modifyCar() {
        root.collection("autos").document(mTrip.getVehicleId()).set(selectedCar).addOnCompleteListener(task ->
        {
            if (task.isSuccessful()) {
                Log.d(TAG, "Estado del vehículo modificado correctamente");
            } else {
                Log.d(TAG, "Error al modificar estado del vehículo");
            }
        });
    }

    private void getPlaces() {
        root.collection("viajes_lugares").get().addOnCompleteListener(placesTask ->
        {
            if (placesTask.isSuccessful()) {
                QuerySnapshot querySnapshot = placesTask.getResult();

                for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                    if (documentSnapshot.exists()) {
                        TripPlace tripPlace = documentSnapshot.toObject(TripPlace.class);
                        tripPlace.setId(documentSnapshot.getId());
                        commonPlaces.add(tripPlace);
                    }
                }

                if (commonPlaces != null) {
                    Log.d(TAG, "Lugares -> " + commonPlaces.toString());

                    // Definir adaptador
                    tripPlaceEt.setDropDownBackgroundResource(android.R.color.white);
                    tripPlaceEt.setThreshold(1);
                    tripPlaceEt.setAdapter(new TripNamesAdapter(TripForm.this,
                            new ArrayList<>(commonPlaces)));
                }

                // Evaluar si se debe de agregar un editText para nuevo viaje
                if (isUser && tripId == null) {
                    addElementToLayoutList("", PLACE_LIST_ELEMENT);
                }
            } else {
                Log.d(TAG, placesTask.getException().getMessage());
            }
        });
    }

    private void getPassengers() {
        root.collection("viajes_pasajeros").get().addOnCompleteListener(passengersTask ->
        {
            if (passengersTask.isSuccessful()) {
                QuerySnapshot querySnapshot = passengersTask.getResult();

                for (DocumentSnapshot documentSnapshot : querySnapshot.getDocuments()) {
                    if (documentSnapshot.exists()) {
                        TripPassenger tripPassenger =
                                documentSnapshot.toObject(TripPassenger.class);
                        tripPassenger.setId(documentSnapshot.getId());
                        commonPassengers.add(tripPassenger);
                    }
                }

                if (commonPassengers != null) {
                    Log.d(TAG, "Pasajeros -> " + commonPassengers.toString());

                    // Definir adaptador
                    passengerEt.setDropDownBackgroundResource(android.R.color.white);
                    passengerEt.setThreshold(1);
                    passengerEt.setAdapter(new TripPassengersAdapter(TripForm.this,
                            new ArrayList<>(commonPassengers)));
                }
            } else {
                Log.d(TAG, passengersTask.getException().getMessage());
            }
        });
    }

    private void showOdoMttoAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alerta");

        String message;
        long diff = selectedCar.kmsToMtto();

        if (diff >= 0) {
            message = String.format(Locale.getDefault(), "Faltan %d kilómetros para enviar " +
                    "vehículo a servicio", diff);
        } else {
            message = String.format(Locale.getDefault(), "Lleva %d kilómetros desde que se " +
                    "requería el servicio", Math.abs(diff));
        }

        builder.setMessage(message);
        builder.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss());

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void onDriverSelected(Driver driver) {
        // Validar conductor
        if (driver != null) {
            // Definir id de usuario
            driverUserId = driver.getId();
            selectedDriver = driver;

            // Validar licencia de conducir
            Long today = new Date().getTime();
            Long licenseDate = driver.getLicenseDate();

            if (licenseDate == null) {
                showErrorDialog("Error", "No se encontró licencia de conducir del" +
                        " usuario", true);
            } else if (today > licenseDate) {
                showErrorDialog("Error", "Licencia de conducir vencida", false);
            } else if (licenseDate - today < 2592000000L) {
                showErrorDialog("Aviso", "Su licencia de conducir vence en menos " +
                        "de 30 " +
                        "días", false);
            }

            // Mostrar datos
            tripUserName.setText(driver.getName());
            if (driver.getPhotoUrl() != null && !driver.getPhotoUrl().isEmpty()) {
                Glide.with(TripForm.this).load(driver.getPhotoUrl()).dontAnimate().into(tripUserImage);
            }
        } else {
            // Mostrar error
            Toast.makeText(TripForm.this, "Error al obtener usuario",
                    Toast.LENGTH_SHORT).show();

            endActivity(RESULT_CANCELED);
        }
    }

    private void endActivity(int resultCode) {
        Intent resultIntent = new Intent();
        setResult(resultCode, resultIntent);
        finish();
    }

    private void initTimeFormatSpinnters() {
        // Definir arreglo de opciones
        String[] timeFormats = {"A.M.", "P.M."};

        // Crear adaptador
        final ArrayAdapter<String> timeFormatsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1, timeFormats);

        // Definir arreglo en spinners
        startTimeFormatEt.setAdapter(timeFormatsAdapter);
        endTimeFormatEt.setAdapter(timeFormatsAdapter);

        // Inicializar
        startTimeFormatEt.setText(timeFormats[0]);
        endTimeFormatEt.setText(timeFormats[0]);

        // Sugerir hora de inicio
        String suggestedDateStr = Constants.dateObjectToString2(new Date());
        if (!suggestedDateStr.isEmpty()) {
            String[] dateDate = Constants.timeFormFromString(suggestedDateStr);
            startDateEt.setText(dateDate[0]);
            startTimeHourEt.setText(dateDate[1]);
            startTimeMinuteEt.setText(dateDate[2]);
            startTimeFormatEt.setText(dateDate[3]);
        }
    }

    private void getScheludedTrips() {
        // Obtener limites del día de la petición
        Date startTime = new Date(mTrip.getStartDate());
        Date endTime = new Date(mTrip.getStartDate());

        // Definir fecha de inicio (0 horas del día en el que se solicita el vehículo)
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        startTime.setTime(calendar.getTimeInMillis());

        // Definir fecha final (23:59:59 del día en el que se solicita el vehículo)
        calendar.setTime(startTime);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        endTime.setTime(calendar.getTimeInMillis());

        // Crear lista de queries para obtener viajes autorizados y solicitados
        List<Task<QuerySnapshot>> tasks = new ArrayList<>();
        tasks.add(
                tripsCollection.orderBy("startDate")
                        .whereEqualTo("currentState", Trip.State.SOLICITADA)
                        .whereGreaterThan("startDate", startTime.getTime())
                        .whereLessThanOrEqualTo("startDate", endTime.getTime())
                        .whereEqualTo("vehicleId", mTrip.getVehicleId())
                        .get()
        );
        tasks.add(
                tripsCollection.orderBy("startDate")
                        .whereEqualTo("currentState", Trip.State.AUTORIZADA)
                        .whereGreaterThan("startDate", startTime.getTime())
                        .whereLessThanOrEqualTo("startDate", endTime.getTime())
                        .whereEqualTo("vehicleId", mTrip.getVehicleId())
                        .get()
        );
        tasks.add(
                tripsCollection.orderBy("startDate")
                        .whereEqualTo("currentState", Trip.State.PENDIENTE)
                        .whereGreaterThan("startDate", startTime.getTime())
                        .whereLessThanOrEqualTo("startDate", endTime.getTime())
                        .whereEqualTo("vehicleId", mTrip.getVehicleId())
                        .get()
        );

        Tasks.whenAllComplete(tasks).addOnCompleteListener(task ->
        {
            final ArrayList<Trip> gottenTrips = new ArrayList<>();

            if (task.isSuccessful() && task.getResult() != null) {
                // Obtener resultados
                for (Task<?> mTask : task.getResult()) {
                    // Obtener documentos de viajes de tarea actual
                    List<DocumentSnapshot> tripDocuments =
                            ((Task<QuerySnapshot>) mTask).getResult().getDocuments();

                    // Convertir documentos de viajes a Trip y agregarlos a la lista
                    for (DocumentSnapshot tripDocument : tripDocuments) {
                        // Convertir documento obtenido
                        Trip trip = tripDocument.toObject(Trip.class);
                        gottenTrips.add(trip);
                    }
                }

                Log.d(TAG, gottenTrips.toString());
            } else {
                Log.e(TAG, "Error al obtener viajes autorizados y solicitados");
            }

            if (!gottenTrips.isEmpty()) showScheludedTrips(gottenTrips);
            else addNewTrip();
        });
    }

    private void showScheludedTrips(ArrayList<Trip> scheludedTrips) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        @SuppressLint("InflateParams") View content =
                getLayoutInflater().inflate(R.layout.dialog_requested_car_trips, null);

        // Obtener ListView
        final ListView tripsList = content.findViewById(R.id.requested_cars_list);
        // Definir adaptador
        final PendingTripAdapter pendingTripAdapter = new PendingTripAdapter(this, scheludedTrips);
        // Asignar adaptador
        tripsList.setAdapter(pendingTripAdapter);

        // Obtener botones
        final Button requestButton = content.findViewById(R.id.dialog_request_car_button);
        final Button returnButton = content.findViewById(R.id.dialog_return_button);

        // Definir layout
        builder.setView(content);

        // Crear diálogo y mostrar
        Dialog dialog = builder.create();
        dialog.show();

        // Definir listeners
        requestButton.setOnClickListener(view ->
        {
            addNewTrip();
            dialog.dismiss();
        });
        returnButton.setOnClickListener(view ->
        {
            hideProgressBar();
            dialog.dismiss();
        });
    }

    private void confirmOdo(Long odo, View.OnClickListener onConfirm) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmación");

        final View content = getLayoutInflater().inflate(R.layout.dialog_confirm_odo, null);
        builder.setView(content);

        final TextView registeredOdoTextView = content.findViewById(R.id.registered_odo_tv);
        final TextView registeredDistanceTextView = content.findViewById(R.id.registered_distance_tv);
        final String odoValue = Constants.distanceFormat.format(odo) + " KM";
        registeredOdoTextView.setText(odoValue);

        final String distanceValue = Constants.distanceFormat.format(mTrip.getOdoEnd() - mTrip.getOdoStart()) + " KM";
        registeredDistanceTextView.setText(distanceValue);

        final Dialog dialog = builder.create();
        dialog.show();

        final Button confirmButton = content.findViewById(R.id.continue_button);
        confirmButton.setOnClickListener(view -> {
            onConfirm.onClick(view);
            dialog.dismiss();
        });

        final Button returnButton = content.findViewById(R.id.return_button);
        returnButton.setOnClickListener(view -> {
            hideProgressBar();
            dialog.dismiss();
        });
    }
}