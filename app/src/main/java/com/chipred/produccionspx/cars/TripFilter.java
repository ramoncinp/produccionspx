package com.chipred.produccionspx.cars;

public class TripFilter
{
    private FILTER filter = FILTER.NONE;
    private long startDate;
    private long endDate;
    private String vehicleId;
    private String userId;
    private String dateType;
    private String place;

    public enum FILTER
    {
        NONE,
        DATE,
        VEHICLE,
        USER
    }

    public TripFilter()
    {
    }

    public FILTER getFilter()
    {
        return filter;
    }

    public void setFilter(FILTER filter)
    {
        this.filter = filter;
    }

    public long getStartDate()
    {
        return startDate;
    }

    public void setStartDate(long startDate)
    {
        this.startDate = startDate;
    }

    public long getEndDate()
    {
        return endDate;
    }

    public void setEndDate(long endDate)
    {
        this.endDate = endDate;
    }

    public String getVehicleId()
    {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId)
    {
        this.vehicleId = vehicleId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getDateType()
    {
        return dateType;
    }

    public void setDateType(String dateType)
    {
        this.dateType = dateType;
    }

    public String getPlace()
    {
        return place;
    }

    public void setPlace(String place)
    {
        this.place = place;
    }
}
