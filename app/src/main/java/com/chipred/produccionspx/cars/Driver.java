package com.chipred.produccionspx.cars;

import androidx.annotation.NonNull;

import com.chipred.produccionspx.users.UserData;

public class Driver extends UserData
{
    private String licensePhotoUrl;
    private Long licenseDate;

    public Driver()
    {
    }

    public String getLicensePhotoUrl()
    {
        return licensePhotoUrl;
    }

    public void setLicensePhotoUrl(String licensePhotoUrl)
    {
        this.licensePhotoUrl = licensePhotoUrl;
    }

    public Long getLicenseDate()
    {
        return licenseDate;
    }

    public void setLicenseDate(Long licenseDate)
    {
        this.licenseDate = licenseDate;
    }

    @NonNull
    @Override
    public String toString()
    {
        return getName();
    }
}
