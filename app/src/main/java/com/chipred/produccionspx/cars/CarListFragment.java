package com.chipred.produccionspx.cars;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CarListFragment extends DialogFragment
{
    // Views
    private ConstraintLayout contentLayout;
    private ProgressBar progressBar;
    private RecyclerView carsList;

    // Objetos
    private ArrayList<Car> cars;
    private CollectionReference carsReference;
    private CarListInterface carListInterface;

    public CarListFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View content = inflater.inflate(R.layout.fragment_car_list, container, false);
        contentLayout = content.findViewById(R.id.content_layout);
        progressBar = content.findViewById(R.id.progress_bar);
        carsList = content.findViewById(R.id.cars_list);

        // Inflate the layout for this fragment
        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        getCars();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    public void setCarListInterface(CarListInterface carListInterface)
    {
        this.carListInterface = carListInterface;
    }

    private void getCars()
    {
        carsReference = FirebaseFirestore.getInstance().collection("autos");
        carsReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task)
            {
                if (task.isSuccessful() && task.getResult() != null)
                {
                    // Crear lista
                    cars = new ArrayList<>();

                    // Iterar en lista de resultados
                    for (DocumentSnapshot document : task.getResult())
                    {
                        // Crear usuario
                        Car car = document.toObject(Car.class);
                        if (car != null) car.setId(document.getId());

                        // Agregar a la lista
                        cars.add(car);
                    }

                    // Ordenar alfabéticamente
                    Collections.sort(cars, new Comparator<Car>()
                    {
                        @Override
                        public int compare(Car car, Car t1)
                        {
                            return car.getModel().compareTo(t1.getModel());
                        }
                    });

                    // Definir adaptador
                    CarAdapter adapter = new CarAdapter(getActivity());
                    adapter.setCars(cars);
                    adapter.setClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            // Obtener usuario
                            Car selectedCar =
                                    cars.get(carsList.getChildAdapterPosition(view));

                            // Regresar vehículo seleccionado
                            carListInterface.onSelectedCar(selectedCar);

                            // Cerrar diálogo
                            CarListFragment.this.dismiss();
                        }
                    });
                    carsList.setAdapter(adapter);
                    carsList.setLayoutManager(new LinearLayoutManager(getActivity()));

                    // Mostrar views
                    contentLayout.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
                else
                {
                    Toast.makeText(getActivity(), "Error al obtener usuarios",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public interface CarListInterface
    {
        void onSelectedCar(Car car);
    }
}
