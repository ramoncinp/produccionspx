package com.chipred.produccionspx.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CarOverviewAdapter extends RecyclerView.Adapter<CarOverviewAdapter.CarOverviewViewHolder> implements View.OnClickListener
{
    private Context context;
    private ArrayList<Car> cars;
    private ArrayList<Car> filteredKits;
    private View.OnClickListener clickListener;

    public CarOverviewAdapter(Context context, ArrayList<Object> cars)
    {
        this.context = context;

        // Convertir lista
        this.cars = new ArrayList<>();
        this.filteredKits = new ArrayList<>();
        for (Object object : cars)
        {
            this.cars.add((Car) object);
            this.filteredKits.add((Car) object);
        }
    }

    @NonNull
    @Override
    public CarOverviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_overview_layout,
                        viewGroup, false);

        CarOverviewViewHolder viewHolder = new CarOverviewViewHolder(v);
        v.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarOverviewViewHolder carOverviewViewHolder,
                                 int i)
    {
        // Obtener vehículo
        Car car = cars.get(i);

        // Mostrar datos
        carOverviewViewHolder.model.setText(car.getModel());
        carOverviewViewHolder.brand.setText(car.getBrand());

        // Mostrar imagen
        if (car.getImageUrl() != null && !car.getImageUrl().isEmpty())
        {
            Glide.with(context).load(car.getImageUrl()).dontAnimate().into(carOverviewViewHolder.image);
        }
        else
        {
            Glide.with(context).load(R.drawable.ic_car).dontAnimate().into(carOverviewViewHolder.image);
        }
    }

    @Override
    public int getItemCount()
    {
        return cars.size();
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    static class CarOverviewViewHolder extends RecyclerView.ViewHolder
    {
        private TextView model;
        private TextView brand;
        private CircleImageView image;

        CarOverviewViewHolder(@NonNull View itemView)
        {
            super(itemView);
            model = itemView.findViewById(R.id.text_1);
            brand = itemView.findViewById(R.id.text_2);
            image = itemView.findViewById(R.id.trip_user_image);
        }
    }
}
