package com.chipred.produccionspx.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class CarDocumentAdapter extends RecyclerView.Adapter<CarDocumentAdapter.CarDocumentViewHolder> implements View.OnClickListener
{
    private Context context;
    private ArrayList<CarDocument> carDocuments;
    private HashMap<String, Integer> years = new HashMap<>();
    private View.OnClickListener clickListener;

    public CarDocumentAdapter(Context context, ArrayList<CarDocument> carDocuments)
    {
        this.context = context;
        this.carDocuments = carDocuments;
    }

    public void refresh()
    {
        // Ordenar
        Collections.sort(carDocuments, new CarsComparator());
        // Crear map para contar coincidencias
        years = new HashMap<>();
        // Mostrar datos
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CarDocumentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_document_layout,
                        viewGroup, false);

        CarDocumentViewHolder viewHolder = new CarDocumentViewHolder(v);
        v.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarDocumentViewHolder carDocumentViewHolder,
                                 int i)
    {
        // Obtener vehículo
        CarDocument carDocument = carDocuments.get(i);

        // Mostrar icono para agregar cuando es el último elemento
        if (i == carDocuments.size() - 1)
        {
            carDocumentViewHolder.addButton.setVisibility(View.VISIBLE);
            carDocumentViewHolder.carDocumentYear.setVisibility(View.GONE);
            carDocumentViewHolder.imageLayout.setVisibility(View.GONE);
        }
        else
        {
            carDocumentViewHolder.addButton.setVisibility(View.GONE);
            carDocumentViewHolder.carDocumentYear.setVisibility(View.VISIBLE);
            carDocumentViewHolder.imageLayout.setVisibility(View.VISIBLE);

            // Obtener año
            String year = carDocument.getYear();
            int number = 1;

            // Evaluar si ya hay en la lista
            if (years.containsKey(year))
            {
                // Obtener valor
                number = years.get(year);

                // Acumular
                number++;
            }

            // Guardar
            years.put(year, number);

            // Mostrar datos
            carDocumentViewHolder.carDocumentYear.setText(year + " - " + number);

            // Mostrar imagen
            if (carDocument.getPhotoUrl() != null && !carDocument.getPhotoUrl().isEmpty())
            {
                Glide.with(context).load(carDocument.getPhotoUrl()).dontAnimate().into(carDocumentViewHolder.carDocumentImage);
            }
            else
            {
                Glide.with(context).load(R.drawable.refrendo_vehicular).dontAnimate().into(carDocumentViewHolder.carDocumentImage);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return carDocuments.size();
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    static class CarDocumentViewHolder extends RecyclerView.ViewHolder
    {
        private RelativeLayout imageLayout;
        private TextView carDocumentYear;
        private ImageView addButton;
        private ImageView carDocumentImage;

        CarDocumentViewHolder(@NonNull View itemView)
        {
            super(itemView);
            carDocumentYear = itemView.findViewById(R.id.car_document_year);
            addButton = itemView.findViewById(R.id.add_car_document);
            carDocumentImage = itemView.findViewById(R.id.car_document_photo);
            imageLayout = itemView.findViewById(R.id.car_document_image_layout);
        }
    }
}
