package com.chipred.produccionspx.cars;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.utils.DatePickerFragment;
import com.chipred.produccionspx.R;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class TripFilterFragment extends DialogFragment
{
    // Objetos
    private ArrayAdapter<Car> carsAdapter;
    private ArrayAdapter<Driver> driversAdapter;
    private ArrayList<Car> cars = new ArrayList<>();
    private ArrayList<Driver> drivers = new ArrayList<>();
    private TripFilter tripFilter = new TripFilter();
    private Car filteredCar;
    private Driver filteredDriver;

    // Firestore
    private CollectionReference carsRef = FirebaseFirestore.getInstance().collection("autos");
    private CollectionReference driversRef = FirebaseFirestore.getInstance().collection("usuarios");

    // Views
    private MaterialBetterSpinner dateTypeSpinner;
    private MaterialBetterSpinner carsSpinner;
    private MaterialBetterSpinner driversSpinners;
    private MaterialEditText startDateEt;
    private MaterialEditText endDateEt;
    private MaterialEditText placeEt;

    private RadioButton dateRb;
    private RadioButton carsRb;
    private RadioButton userRb;
    private RadioButton noneRb;

    private Button submitButton;
    private Button cancelButton;

    private TripFilterInterface tripFilterInterface;

    public TripFilterFragment()
    {
        // Required empty public constructor
    }

    void setTripFilterInterface(TripFilterInterface tripFilterInterface)
    {
        this.tripFilterInterface = tripFilterInterface;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View content = inflater.inflate(R.layout.dialog_trips_filter, container, false);

        carsSpinner = content.findViewById(R.id.trip_vehicle_spinner);
        driversSpinners = content.findViewById(R.id.trip_user_spinner);
        startDateEt = content.findViewById(R.id.start_date_et);
        endDateEt = content.findViewById(R.id.end_date_et);
        placeEt = content.findViewById(R.id.place_et);
        dateTypeSpinner = content.findViewById(R.id.trip_date_filter_type);

        dateRb = content.findViewById(R.id.trip_dates_filter_rb);
        carsRb = content.findViewById(R.id.trip_vehicles_filter_rb);
        userRb = content.findViewById(R.id.trip_users_filter_rb);
        noneRb = content.findViewById(R.id.none_trip_filter_rb);

        submitButton = content.findViewById(R.id.submit_button);
        cancelButton = content.findViewById(R.id.cancel_button);

        setListeners();

        // Inflate the layout for this fragment
        return content;
    }

    private void setListeners()
    {
        // Definir listeners de radio-buttons
        dateRb.setOnCheckedChangeListener((compoundButton, checked) ->
        {
            if (checked)
            {
                // Quitar checks de los demás
                carsRb.setChecked(false);
                userRb.setChecked(false);
                noneRb.setChecked(false);

                // Mostrar views correspondientes
                startDateEt.setVisibility(View.VISIBLE);
                endDateEt.setVisibility(View.VISIBLE);
                dateTypeSpinner.setVisibility(View.VISIBLE);
                placeEt.setVisibility(View.VISIBLE);

                // Ocultar views
                carsSpinner.setVisibility(View.GONE);
                driversSpinners.setVisibility(View.GONE);
            }
        });
        carsRb.setOnCheckedChangeListener((compoundButton, checked) ->
        {
            if (checked)
            {
                // Quitar checks de los demás
                dateRb.setChecked(false);
                userRb.setChecked(false);
                noneRb.setChecked(false);

                // Mostrar views correspondientes
                carsSpinner.setVisibility(View.VISIBLE);

                // Ocultar views
                startDateEt.setVisibility(View.GONE);
                endDateEt.setVisibility(View.GONE);
                dateTypeSpinner.setVisibility(View.GONE);
                driversSpinners.setVisibility(View.GONE);
                placeEt.setVisibility(View.GONE);
            }
        });
        userRb.setOnCheckedChangeListener((compoundButton, checked) ->
        {
            if (checked)
            {
                // Quitar checks de los demás
                dateRb.setChecked(false);
                carsRb.setChecked(false);
                noneRb.setChecked(false);

                // Mostrar views correspondientes
                driversSpinners.setVisibility(View.VISIBLE);

                // Ocultar views
                startDateEt.setVisibility(View.GONE);
                endDateEt.setVisibility(View.GONE);
                carsSpinner.setVisibility(View.GONE);
                dateTypeSpinner.setVisibility(View.GONE);
                placeEt.setVisibility(View.GONE);
            }
        });
        noneRb.setOnCheckedChangeListener((compoundButton, checked) ->
        {
            if (checked)
            {
                // Quitar checks de los demás
                dateRb.setChecked(false);
                carsRb.setChecked(false);
                userRb.setChecked(false);

                // Ocultar views
                startDateEt.setVisibility(View.GONE);
                endDateEt.setVisibility(View.GONE);
                driversSpinners.setVisibility(View.GONE);
                carsSpinner.setVisibility(View.GONE);
                dateTypeSpinner.setVisibility(View.GONE);
                placeEt.setVisibility(View.GONE);
            }
        });

        // Inicializar DatePickers
        View.OnClickListener dateTimePickerListener = v ->
        {
            DatePickerFragment fragment = DatePickerFragment.newInstance((datePicker, i, i1, i2) ->
            {
                String gottenDate =
                        Constants.twoDigits(i2) + "/" + Constants.monthNumberToString
                                (i1 + 1) + "/" + i;

                final MaterialEditText dateEt = (MaterialEditText) v;
                dateEt.setText(gottenDate);

                //Obtener hora actual
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                //Crear instancia de timePicker
                TimePickerDialog timePickerDialog =
                        new TimePickerDialog(getActivity(),
                                (view, hourOfDay, minute1) -> dateEt.append(String.format(Locale.getDefault(),
                                        " %02d:%02d", hourOfDay, minute1)), hour, minute, true);

                timePickerDialog.setTitle("Seleccione la hora");
                timePickerDialog.show();
            });

            fragment.show(getActivity().getSupportFragmentManager(), "datePicker");
        };

        startDateEt.setOnClickListener(dateTimePickerListener);
        endDateEt.setOnClickListener(dateTimePickerListener);

        setCarsSpinner();
        setDriversSpinner();

        ArrayList<String> dateTypesList = new ArrayList<>();
        dateTypesList.add(Trip.START_DATE_TYPE);
        dateTypesList.add(Trip.END_DATE_TYPE);
        dateTypeSpinner.setAdapter(new ArrayAdapter<>(getContext(),
                android.R.layout.simple_expandable_list_item_1, dateTypesList));
        dateTypeSpinner.setText(dateTypesList.get(0));

        // Evaluar filtro actual
        switch (tripFilter.getFilter())
        {
            case DATE:
                dateRb.setChecked(true);
                break;

            case VEHICLE:
                carsRb.setChecked(true);
                break;

            case USER:
                userRb.setChecked(true);
                break;
        }

        // Definir listeners de botones
        submitButton.setOnClickListener(view ->
        {
            if (validateAndGetData())
            {
                // Retornar filtro
                tripFilterInterface.onFilterSelected(tripFilter);
                if (getDialog() != null) getDialog().dismiss();
            }
        });

        cancelButton.setOnClickListener(view ->
        {
            if (getDialog() != null) getDialog().dismiss();
        });
    }

    private void setCarsSpinner()
    {
        carsAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_expandable_list_item_1, cars);
        carsSpinner.setAdapter(carsAdapter);
        carsSpinner.setOnItemClickListener((adapterView, view, i, l) ->
        {
            filteredCar = ((Car) carsAdapter.getItem(i));
            if (filteredCar != null)
                tripFilter.setVehicleId(filteredCar.getId());
        });
    }

    private void setDriversSpinner()
    {
        driversAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_expandable_list_item_1, drivers);
        driversSpinners.setAdapter(driversAdapter);
        driversSpinners.setOnItemClickListener((adapterView, view, i, l) ->
        {
            filteredDriver = ((Driver) driversAdapter.getItem(i));
            if (filteredDriver != null)
                tripFilter.setUserId(filteredDriver.getId());
        });
    }

    private boolean validateAndGetData()
    {
        boolean isValid = false;

        // Validar filtro seleccionado
        if (noneRb.isChecked())
        {
            isValid = true;
            tripFilter.setFilter(TripFilter.FILTER.NONE);
        }
        else if (dateRb.isChecked())
        {
            METValidator dateValidator = new METValidator("Fecha no valida")
            {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
                {
                    // Debe tener siempre 17 caracteres
                    return text.length() == 17;
                }
            };

            if (startDateEt.validateWith(dateValidator) && endDateEt.validateWith(dateValidator))
            {
                isValid = true;

                // Obtener datos
                Date startDate = Constants.dateStringToObject(startDateEt.getText().toString());
                Date endDate = Constants.dateStringToObject(endDateEt.getText().toString());
                String dateType = dateTypeSpinner.getText().toString();
                String place = placeEt.getText().toString();

                if (startDate == null)
                {
                    startDate = new Date();
                    startDate.setTime(0);
                }

                if (endDate == null)
                {
                    endDate = new Date();
                }

                // Definir en objeto principal
                tripFilter.setStartDate(startDate.getTime());
                tripFilter.setEndDate(endDate.getTime());
                tripFilter.setFilter(TripFilter.FILTER.DATE);
                tripFilter.setDateType(dateType);
                tripFilter.setPlace(place);
            }
        }
        else if (userRb.isChecked())
        {
            isValid = driversSpinners.validateWith(Constants.emptyValidator);
            tripFilter.setFilter(TripFilter.FILTER.USER);
        }
        else if (carsRb.isChecked())
        {
            isValid = carsSpinner.validateWith(Constants.emptyValidator);
            tripFilter.setFilter(TripFilter.FILTER.VEHICLE);
        }

        return isValid;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
        queryCars();
        queryDrivers();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null)
        {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    private void queryCars()
    {
        cars.clear();

        // Obtener autos
        carsRef.get().addOnCompleteListener(task ->
        {
            if (task.getResult() != null)
            {
                // Obtener documentos
                for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments())
                {
                    // Obtener documento
                    final Car car = documentSnapshot.toObject(Car.class);

                    // Agregar a la lista
                    if (!cars.contains(car))
                        cars.add(car);
                }

                // Ordenar por orden alfabético
                Collections.sort(cars, (car, t1) -> car.getModel().compareTo(t1.getModel()));

                // Definir adaptador
                setCarsSpinner();
            }
        });
    }

    private void queryDrivers()
    {
        // Vaciar lista
        drivers.clear();

        // Obtener solo conductores
        driversRef.whereArrayContains("allowedBlocks",
                Constants.VEHICLES_USER).get().addOnCompleteListener(task ->
        {
            if (task.getResult() != null)
            {
                // Obtener documentos
                for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments())
                {
                    // Obtener documento
                    final Driver driver = documentSnapshot.toObject(Driver.class);

                    // Agregar a la lista
                    drivers.add(driver);
                }

                // Ordenar por orden alfabético
                Collections.sort(drivers, (driver, t1) -> driver.getName().compareTo(t1.getName()));

                // Actualizar adaptador
                setDriversSpinner();
            }
        });
    }

    public interface TripFilterInterface
    {
        void onFilterSelected(TripFilter tripFilter);
    }
}
