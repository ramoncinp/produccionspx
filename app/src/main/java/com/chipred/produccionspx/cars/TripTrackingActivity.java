package com.chipred.produccionspx.cars;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chipred.produccionspx.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;

public class TripTrackingActivity extends FragmentActivity implements OnMapReadyCallback
{
    private GoogleMap mMap;
    private HashMap<String, Trip> trips = new HashMap<>();
    private HashMap<String, Bitmap> vehiclesImages = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_tracking);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        initDb();
    }

    private void initDb()
    {
        FirebaseFirestore.getInstance().collection("viajes").whereEqualTo("currentState",
                Trip.State.ACTIVA.name()).addSnapshotListener(this,
                new EventListener<QuerySnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e)
                    {
                        if (queryDocumentSnapshots != null)
                        {
                            for (DocumentChange documentChange :
                                    queryDocumentSnapshots.getDocumentChanges())
                            {
                                Log.d("TrackingActivity", documentChange.getDocument().toString());
                                Trip trip = documentChange.getDocument().toObject(Trip.class);

                                Log.d("TrackingActivity", "Viaje a " + trip.getPlaces().get(0) +
                                        " cambió a -> " + trip.getCurrentState());

                                if (trip.getCoordinates() != null)
                                {
                                    // Agregar a la lista
                                    trips.put(trip.getId(), trip);
                                }
                            }

                            onTripsChanged();
                        }
                    }
                });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(20.666963, -103.352447)));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(8), 2000, null);
        mMap.setTrafficEnabled(true);
    }

    private void onTripsChanged()
    {
        mMap.clear();

        for (String tripId : trips.keySet())
        {
            Trip trip = trips.get(tripId);
            if (trip != null)
                getTripVehicle(trip);
        }
    }

    private void getImageFromUrl(String tripId, String imageUrl)
    {
        Glide.with(this).asBitmap().load(imageUrl).into(new CustomTarget<Bitmap>(100, 100)
        {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<?
                    super Bitmap> transition)
            {
                vehiclesImages.put(tripId, resource);
                setMarker(tripId);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder)
            {

            }
        });
    }

    private void setMarker(String tripId)
    {
        // Obtener imagen
        BitmapDescriptor icon =
                BitmapDescriptorFactory.fromBitmap(getCircleBitmap(vehiclesImages.get(tripId)));

        LatLng location;

        Trip mTrip = trips.get(tripId);
        Double latitude = mTrip.getCoordinates().get("latitude");
        Double longitude = mTrip.getCoordinates().get("longitude");
        if (latitude != null && longitude != null)
            location = new LatLng(latitude, longitude);
        else
            return;

        MarkerOptions mMarker = new MarkerOptions();
        mMarker.position(location);
        mMarker.title(mTrip.getUserName());
        mMarker.snippet(mTrip.getVehicleDesc());
        mMarker.icon(icon);

        mMap.addMarker(mMarker);
    }

    private Bitmap getCircleBitmap(Bitmap bitmap)
    {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    private void getTripVehicle(Trip trip)
    {
        if (trip.getVehicleId() == null)
        {
            return;
        }

        FirebaseFirestore.getInstance().collection("autos").document(trip.getVehicleId()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task)
            {
                if (task.isSuccessful() && task.getResult() != null)
                {
                    Car car = task.getResult().toObject(Car.class);
                    if (car != null)
                    {
                        getImageFromUrl(trip.getId(), car.getImageUrl());
                    }
                }
            }
        });
    }
}
