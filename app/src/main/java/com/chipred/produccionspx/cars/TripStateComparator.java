package com.chipred.produccionspx.cars;

import java.util.Comparator;

public class TripStateComparator implements Comparator<Object>
{
    @Override
    public int compare(Object trip, Object t1)
    {
        int result = 100;
        final String stateReference = Trip.State.TERMINADA.name();
        String state1 = ((Trip) trip).getCurrentState();
        String state2 = ((Trip) t1).getCurrentState();

        if (state1.equals(stateReference) && state2.equals(stateReference))
        {
            result = 0;
        }
        else if (state1.equals(stateReference))
        {
            result = 1;
        }
        else if (state2.equals(stateReference))
        {
            result = -1;
        }

        return result;
    }
}
