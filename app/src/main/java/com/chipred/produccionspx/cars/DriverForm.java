package com.chipred.produccionspx.cars;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.BuildConfig;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.utils.DatePickerFragment;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.users.ProfilePictureActivity;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

public class DriverForm extends AppCompatActivity
{
    private int CAMERA_REQUEST_INTENT = 1;
    private static final int ASK_PERMISSIONS_CODE = 2;

    // Objetos
    private CollectionReference usersCollection;
    private Driver driver;

    // Variables
    private String driverId;
    private String photoFilePath;

    // Views
    private CardView licenseImageCv;
    private ImageView driverImageView;
    private ImageView licenseImageView;
    private MaterialEditText fechaVencimiento;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_form);

        setTitle(getIntent().getStringExtra(CarElementActivity.CAR_ELEMENT_NAME_KEY));

        final Toolbar toolbar = findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Obtener id
        driverId = getIntent().getStringExtra(CarElementActivity.CAR_ELEMENT_ID_KEY);

        // Inicializar views
        initViews();

        // Inicializar coleccion
        usersCollection = FirebaseFirestore.getInstance().collection("usuarios");
    }

    private void initViews()
    {
        fechaVencimiento = findViewById(R.id.fecha_vencimiento_et);
        licenseImageView = findViewById(R.id.user_license_photo);
        driverImageView = findViewById(R.id.driver_image_view);
        licenseImageCv = findViewById(R.id.license_image_cv);

        fechaVencimiento.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                DatePickerFragment fragment = DatePickerFragment.newInstance(new DatePickerDialog
                        .OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2)
                    {
                        String gottenDate =
                                Constants.twoDigits(i2) + "/" + Constants.monthNumberToString
                                        (i1 + 1) + "/" + i;

                        MaterialEditText dateEt = (MaterialEditText) view;
                        dateEt.setText(gottenDate);
                    }
                });

                fragment.show(DriverForm.this.getSupportFragmentManager(), "datePicker");
            }
        });

        licenseImageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (licenseImageView.getDrawable() == null)
                {
                    askForPermissions();
                }
                else
                {
                    showDriverImageOptions(photoFilePath == null || photoFilePath.isEmpty() ?
                            driver.getLicensePhotoUrl() : photoFilePath);
                }
            }
        });

        progressDialog = ProgressDialog.show(this, "", "Cargando...");
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        getUser();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        else if (item.getItemId() == R.id.save)
        {
            getDriverData();
            save();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_INTENT)
        {
            if (resultCode == RESULT_OK)
            {
                // Mostrar imagen tomada
                setLicenseImage();
            }
        }
    }

    private void askForPermissions()
    {
        // Si no hay permisos para cámara
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            // Si es android superior Marshmallow
            if (Build.VERSION.SDK_INT >= 23)
            {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, ASK_PERMISSIONS_CODE);
            }
        }
        else
        {
            startPhotoIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ASK_PERMISSIONS_CODE)
        {
            if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_DENIED))
            {
                // Deshabilitar listener para tomar foto
                licenseImageCv.setOnClickListener(null);
                // Esconder layout
                licenseImageCv.setVisibility(View.GONE);
            }
            else
            {
                startPhotoIntent();
            }
        }
    }

    private void showDriverImageOptions(String imagePath)
    {
        String[] options = {"Ver imagen", "Abrir cámara"};
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(this);

        // Crear adaptador para mostrar opciones
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1);

        // Agregar opciones
        arrayAdapter.add(options[0]);
        arrayAdapter.add(options[1]);

        // Definir clickListener
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                // Obtener opcion seleccionada
                String selectedOption = arrayAdapter.getItem(which);

                if (selectedOption != null)
                {
                    if (selectedOption.equals(options[0]))
                    {
                        // Mostrar imagen en pantalla completa
                        Intent showFullImage = new Intent(DriverForm.this,
                                ProfilePictureActivity.class);

                        // Añadir dato de la imagen
                        showFullImage.putExtra("imagen", imagePath);

                        // Iniciar actividad
                        startActivity(showFullImage);
                    }
                    else if (selectedOption.equals(options[1]))
                    {
                        // Abrir cámara
                        startPhotoIntent();
                    }
                }

                dialog.dismiss();
            }
        });

        // Mostrar
        builder.show();
    }

    private void setLicenseImage()
    {
        // Validar ruta del a imagen
        if (photoFilePath != null && !photoFilePath.isEmpty())
        {
            // Mostrar imagen
            Glide.with(this).load(photoFilePath).dontAnimate().into(licenseImageView);

            // Guardar imagen en la nube
            uploadImage();
        }
    }

    private void getUser()
    {
        usersCollection.document(driverId).get().addOnCompleteListener(this,
                new OnCompleteListener<DocumentSnapshot>()
                {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task)
                    {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (task.isSuccessful() && task.getResult() != null)
                        {
                            // Obtener información de conductor
                            driver = task.getResult().toObject(Driver.class);
                            if (driver != null)
                            {
                                setUserData();
                            }
                        }
                        else
                        {
                            Toast.makeText(DriverForm.this, "Error al obtener datos de conductor",
                                    Toast.LENGTH_SHORT).show();
                            DriverForm.this.finish();
                        }
                    }
                });
    }

    private void setUserData()
    {
        if (driver.getPhotoUrl() != null && !driver.getPhotoUrl().isEmpty())
        {
            Glide.with(this).load(driver.getPhotoUrl()).into(driverImageView);
        }

        if (driver.getLicensePhotoUrl() != null && !driver.getLicensePhotoUrl().isEmpty())
        {
            Glide.with(this).load(driver.getLicensePhotoUrl()).into(licenseImageView);
        }

        if (driver.getLicenseDate() != null)
        {
            String date = Constants.dateObjectToString(new Date(driver.getLicenseDate()));
            fechaVencimiento.setText(date);
        }
    }

    private void getDriverData()
    {
        // Obtener datos y validar
        if (fechaVencimiento.getText().toString().isEmpty())
        {
            fechaVencimiento.setError("Campo obligatorio");
            return;
        }

        Date licenseDate = Constants.dateStringToObject2(fechaVencimiento.getText().toString());
        if (licenseDate != null) driver.setLicenseDate(licenseDate.getTime());
    }

    private void save()
    {
        // Guardar en base de datos
        usersCollection.document(driver.getId()).set(driver).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if (task.isSuccessful())
                {
                    Toast.makeText(DriverForm.this, "Conductor modificado correctamente",
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(DriverForm.this, "Error al actualizar conductor",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void startPhotoIntent()
    {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(getPackageManager()) != null)
        {
            File photoFile;
            try
            {
                photoFile = createPhotoFile();
                if (photoFile != null)
                {
                    photoFilePath = photoFile.getAbsolutePath();
                    Uri photoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID +
                            ".fileprovider", photoFile);
                    takePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    startActivityForResult(takePicture, CAMERA_REQUEST_INTENT);
                }
            }
            catch (Exception e)
            {
                Toast.makeText(this, "Error al iniciar cámara", Toast.LENGTH_LONG).show();
            }
        }
    }

    private File createPhotoFile()
    {
        // Validar directorio padre
        File storageDir = new File(getFilesDir(), "autos");
        if (!storageDir.exists())
        {
            storageDir.mkdir();
        }

        File image = null;
        try
        {
            image = File.createTempFile("licencia_conductor", ".jpg", storageDir);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Log.e("PhotoStorage", "Error al crear archivo temporal");
        }

        return image;
    }

    private void uploadImage()
    {
        // Mostrar progreso
        progressDialog = ProgressDialog.show(this, "", "Subiendo imagen...");

        // Definir path
        String imagePath = driverId + "/licencia.jpg";

        // Obtener referencia
        StorageReference storageReference =
                FirebaseStorage.getInstance().getReference().child(imagePath);

        // Obtener archivo
        File imageFile = new File(photoFilePath);

        // Convertir a arreglo de bytes
        int size = (int) imageFile.length();
        byte[] data = new byte[size];

        try
        {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(imageFile));
            buf.read(data, 0, data.length);
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al leer el archivo a subir", Toast.LENGTH_SHORT).show();
            return;
        }

        // Subir archivo
        UploadTask uploadTask = storageReference.putBytes(data);

        // Definir proceso
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot,
                Task<Uri>>()
        {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
            {
                // Manejar primer resultado
                if (!task.isSuccessful())
                {
                    throw task.getException();
                }

                return storageReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>()
        {
            @Override
            public void onComplete(@NonNull Task<Uri> task)
            {
                if (task.isSuccessful())
                {
                    // Obtener Uri del archivo recien subido
                    Uri downloadUri = task.getResult();

                    // Validar Uri
                    if (downloadUri != null)
                    {
                        driver.setLicensePhotoUrl(downloadUri.toString());
                    }

                    // Guardar cambios en la nube
                    save();
                }
                else
                {
                    Toast.makeText(DriverForm.this, "Error al subir archivo",
                            Toast.LENGTH_SHORT).show();
                }

                // Ocultar progress
                if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
            }
        });
    }
}