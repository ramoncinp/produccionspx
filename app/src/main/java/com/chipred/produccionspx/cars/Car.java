package com.chipred.produccionspx.cars;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Car
{
    // Diferencia máxima de kilometraje para altertar sobre mantenimiento
    public static final long MAX_ODO_MTTO_DIFFERENCE = 500;

    public enum Status
    {
        Disponible,
        Ocupado,
        No_disponible,
    }

    private String id;
    private String model;
    private String brand;
    private String year;
    private String color;
    private String imageUrl;
    private String plates;
    private String status = Status.Disponible.name();
    private Long insureDeadLine;
    private Long odometerMtto;
    private Long odometer;
    private Long lastMttoOdometer;
    private Integer tankCapacity;
    private Integer fuelPercentage;
    private ArrayList<CarDocument> refrendos;
    private ArrayList<CarDocument> verificaciones;

    public Car()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public long getOdometer()
    {
        return odometer;
    }

    public void setOdometer(Long odometer)
    {
        this.odometer = odometer;
    }

    public int getFuelPercentage()
    {
        return fuelPercentage;
    }

    public void setFuelPercentage(Integer fuelPercentage)
    {
        this.fuelPercentage = fuelPercentage;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getPlates()
    {
        return plates;
    }

    public void setPlates(String plates)
    {
        this.plates = plates;
    }

    public String getYear()
    {
        return year;
    }

    public void setYear(String year)
    {
        this.year = year;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public Long getInsureDeadLine()
    {
        return insureDeadLine;
    }

    public void setInsureDeadLine(Long insureDeadLine)
    {
        this.insureDeadLine = insureDeadLine;
    }

    public Long getOdometerMtto()
    {
        return odometerMtto;
    }

    public void setOdometerMtto(Long odometerMtto)
    {
        this.odometerMtto = odometerMtto;
    }

    public Integer getTankCapacity()
    {
        return tankCapacity;
    }

    public void setTankCapacity(Integer tankCapacity)
    {
        this.tankCapacity = tankCapacity;
    }

    public ArrayList<CarDocument> getRefrendos()
    {
        return refrendos;
    }

    public void setRefrendos(ArrayList<CarDocument> refrendos)
    {
        this.refrendos = refrendos;
    }

    public ArrayList<CarDocument> getVerificaciones()
    {
        return verificaciones;
    }

    public void setVerificaciones(ArrayList<CarDocument> verificaciones)
    {
        this.verificaciones = verificaciones;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getLastMttoOdometer()
    {
        return lastMttoOdometer;
    }

    public void setLastMttoOdometer(Long lastMttoOdometer)
    {
        this.lastMttoOdometer = lastMttoOdometer;
    }

    /*
        Retorna un valor mayor o igual a cero cuando se debe alertar para envío a servicio
     */
    public Long kmsToMtto()
    {
        Long diff = null;

        if (lastMttoOdometer != null && lastMttoOdometer != 0)
        {
            diff = (lastMttoOdometer + odometerMtto) - odometer;
        }

        return diff;
    }

    @NonNull
    @Override
    public String toString()
    {
        return getModel();
    }
}
