package com.chipred.produccionspx.cars;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chipred.produccionspx.BuildConfig;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.utils.DatePickerFragment;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.users.ProfilePictureActivity;
import com.chipred.produccionspx.views.FuelGaugeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class CarForm extends AppCompatActivity
{
    // Constantes
    private static final String TAG = "CarForm";
    private static final int CAMERA_REQUEST_INTENT = 1;
    private static final int ASK_PERMISSIONS_CODE = 2;

    // Variables
    private String carId;
    private String photoFilePath, carPhotoFilePath, carDocumentFilePath;

    // Colecciones
    private CarDocumentAdapter refrendosAdapter;
    private CarDocumentAdapter verificacionesAdapter;

    // Objetos
    private Car carData = new Car();
    private CarDocument focusedCarDocument;
    private CollectionReference carsReference;
    private FirebaseStorage firebaseStorage;

    // Views
    private Dialog carDocumentDialog;
    private FuelGaugeView fuelView;
    private ImageView carImageView;
    private MaterialBetterSpinner statusSpinner;
    private MaterialEditText modelEt;
    private MaterialEditText brandEt;
    private MaterialEditText odoEt;
    private MaterialEditText yearEt;
    private MaterialEditText colorEt;
    private MaterialEditText platesEt;
    private MaterialEditText tankCapacityEt;
    private MaterialEditText vencimientoSeguro;
    private MaterialEditText multiploKilometraje;
    private MaterialEditText lastOdoMttoEt;
    private ProgressDialog progressDialog;
    private RecyclerView listaRefrendos;
    private RecyclerView listaVerificaciones;
    private RelativeLayout noCarImage;
    private SeekBar fuelSeeker;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_form_dynamic);

        final Toolbar toolbar = findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //setTitle("Vehículo");
        CollapsingToolbarLayout toolbarLayout = findViewById(R.id.collapsing_toolbar);
        toolbarLayout.setTitle("Vehículo");

        // Obtener carId
        carId = getIntent().getStringExtra(CarElementActivity.CAR_ELEMENT_ID_KEY);

        // Inicializar vistas
        initViews();

        // Inicializar coleccion
        carsReference = FirebaseFirestore.getInstance().collection("autos");
        firebaseStorage = FirebaseStorage.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        initDataBase();
    }

    private void initDataBase()
    {
        if (carId != null)
        {
            // Mostrar progress
            progressDialog = ProgressDialog.show(this, "", "Obteniendo vehículo...");

            // Obtener datos del auto seleccionado
            carsReference.document(carId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
            {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task)
                {
                    if (task.isSuccessful())
                    {
                        // Guardar mapa en variable global
                        if (task.getResult() != null)
                        {
                            carData = task.getResult().toObject(Car.class);
                            if (carData != null)
                            {
                                getCarData();
                            }
                        }
                    }
                    else
                    {
                        Toast.makeText(CarForm.this, "Error al obtener vehículo",
                                Toast.LENGTH_SHORT).show();

                        CarForm.this.finish();
                    }

                    // Ocultar progress
                    if (progressDialog != null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }
                }
            });
        }
    }

    private void initViews()
    {
        carImageView = findViewById(R.id.car_image_view);
        modelEt = findViewById(R.id.car_model_et);
        brandEt = findViewById(R.id.car_brand_et);
        yearEt = findViewById(R.id.car_year_et);
        odoEt = findViewById(R.id.car_odometer_et);
        colorEt = findViewById(R.id.car_color_et);
        platesEt = findViewById(R.id.car_plates_et);
        tankCapacityEt = findViewById(R.id.car_fuel_capacity);
        statusSpinner = findViewById(R.id.car_status_spinner);
        lastOdoMttoEt = findViewById(R.id.last_mtto_odometer_et);

        fuelSeeker = findViewById(R.id.fuel_value);
        fuelView = findViewById(R.id.fuel_gauge);

        noCarImage = findViewById(R.id.no_car_image_layout);
        noCarImage.setOnClickListener(view -> askForPermissions());

        vencimientoSeguro = findViewById(R.id.vencimiento_seguro);
        vencimientoSeguro.setOnClickListener(view ->
        {
            DatePickerFragment fragment = DatePickerFragment.newInstance((datePicker, i, i1, i2) ->
            {
                String gottenDate =
                        Constants.twoDigits(i2) + "/" + Constants.monthNumberToString
                                (i1 + 1) + "/" + i;

                MaterialEditText dateEt = (MaterialEditText) view;
                dateEt.setText(gottenDate);
            });

            fragment.show(CarForm.this.getSupportFragmentManager(), "datePicker");
        });

        multiploKilometraje = findViewById(R.id.multiplo_kilometraje_mtto);

        // Definir listener para imageView
        carImageView.setOnClickListener(view ->
        {
            // Obtener imagen
            String imagePath = null;
            if (carData.getImageUrl() != null && !carData.getImageUrl().isEmpty())
            {
                imagePath = carData.getImageUrl();
            }
            else if (carPhotoFilePath != null && !carPhotoFilePath.isEmpty())
            {
                imagePath = carPhotoFilePath;
            }

            // Mostrar opciones
            showCarImageOptions(imagePath);

            // Validar
            /*if (imagePath != null && !imagePath.isEmpty())
            {
                showCarImageOptions(imagePath);
            }*/
        });

        if (carId == null)
        {
            // Nuevo vehículo
            fuelSeeker.setVisibility(View.VISIBLE);
            fuelSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b)
                {
                    fuelView.setFuelLevel((float) (i / 100.0));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {

                }
            });
        }
        else
        {
            // Vehículo existente
            fuelSeeker.setVisibility(View.GONE);
        }

        // Listas
        listaRefrendos = findViewById(R.id.lista_refrendos);
        listaVerificaciones = findViewById(R.id.lista_verificaciones);

        // Inicializar spinner
        String[] statusArray = new String[]{
                Car.Status.Disponible.name(),
                Car.Status.Ocupado.name(),
                Car.Status.No_disponible.name().replace("_", " ")
        };

        ArrayAdapter<String> statusAdapters = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, statusArray);

        statusSpinner.setAdapter(statusAdapters);
        statusSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String selectedStatus = (String) adapterView.getItemAtPosition(i);
                carData.setStatus(selectedStatus);
            }
        });
    }

    private void initCarDocumentsList()
    {
        // Listas
        if (carData.getRefrendos() == null)
        {
            // Inicializar lista
            carData.setRefrendos(new ArrayList<>());
            CarDocument addRefrendo = new CarDocument();
            addRefrendo.setType(CarDocument.REFRENDO);
            carData.getRefrendos().add(addRefrendo);
        }

        if (carData.getVerificaciones() == null)
        {
            // Inicializar lista
            carData.setVerificaciones(new ArrayList<>());
            CarDocument addVerificacion = new CarDocument();
            addVerificacion.setType(CarDocument.VERIFICACION);
            carData.getVerificaciones().add(addVerificacion);
        }

        // Ordenar listas
        Collections.sort(carData.getRefrendos(), new CarsComparator());
        Collections.sort(carData.getVerificaciones(), new CarsComparator());

        // Inicializar sus adapters
        refrendosAdapter = new CarDocumentAdapter(this, carData.getRefrendos());
        refrendosAdapter.setClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (listaRefrendos.getChildAdapterPosition(view) == carData.getRefrendos().size() - 1)
                {
                    // Agregar elemento
                    showCarDocumentDialog(null, CarDocument.REFRENDO);
                }
                else
                {
                    // Mostrar elemento
                    showCarDocumentDialog(carData.getRefrendos().get(listaRefrendos.getChildAdapterPosition(view)), CarDocument.REFRENDO);
                }
            }
        });

        verificacionesAdapter = new CarDocumentAdapter(this, carData.getVerificaciones());
        verificacionesAdapter.setClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (listaVerificaciones.getChildAdapterPosition(view) == carData.getVerificaciones().size() - 1)
                {
                    // Agregar elemento
                    showCarDocumentDialog(null, CarDocument.VERIFICACION);
                }
                else
                {
                    // Mostrar elemento
                    showCarDocumentDialog(carData.getRefrendos().get(listaVerificaciones.getChildAdapterPosition(view)), CarDocument.REFRENDO);
                }
            }
        });

        // Definir adapters
        listaRefrendos.setAdapter(refrendosAdapter);
        listaVerificaciones.setAdapter(verificacionesAdapter);

        // Definir layoutsManager
        listaRefrendos.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        listaVerificaciones.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));

        LinearLayout documentsLayout = findViewById(R.id.car_documents_layout);
        documentsLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_INTENT)
        {
            if (resultCode == RESULT_OK)
            {
                // Mostrar imagen tomada
                setCarImage();
            }
        }
    }

    private void askForPermissions()
    {
        // Si no hay permisos para cámara
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            // Si es android superior Marshmallow
            if (Build.VERSION.SDK_INT >= 23)
            {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, ASK_PERMISSIONS_CODE);
            }
        }
        else
        {
            startPhotoIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ASK_PERMISSIONS_CODE)
        {
            if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_DENIED))
            {
                // Deshabilitar listener para tomar foto
                noCarImage.setOnClickListener(null);
                // Esconder layout
                noCarImage.setVisibility(View.GONE);
            }
            else
            {
                startPhotoIntent();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        else if (item.getItemId() == R.id.save)
        {
            progressDialog = ProgressDialog.show(this, "", "Guardando...", true, true);
            saveCar();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showCarImageOptions(String imagePath)
    {
        String[] options = {"Ver imagen", "Abrir cámara"};
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(this);

        // Crear adaptador para mostrar opciones
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1);

        // Agregar opciones
        arrayAdapter.add(options[0]);
        arrayAdapter.add(options[1]);

        // Definir clickListener
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                // Obtener opcion seleccionada
                String selectedOption = arrayAdapter.getItem(which);

                if (selectedOption != null)
                {
                    if (selectedOption.equals(options[0]))
                    {
                        // Mostrar imagen en pantalla completa
                        Intent showFullImage = new Intent(CarForm.this,
                                ProfilePictureActivity.class);

                        // Añadir dato de la imagen
                        showFullImage.putExtra("imagen", imagePath);

                        // Iniciar actividad
                        startActivity(showFullImage);
                    }
                    else if (selectedOption.equals(options[1]))
                    {
                        // Abrir cámara
                        startPhotoIntent();
                    }
                }

                dialog.dismiss();
            }
        });

        // Mostrar
        builder.show();
    }

    private boolean setCarData()
    {
        // Validar los datos principales
        boolean valid = modelEt.validateWith(new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        valid &= brandEt.validateWith(new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        // Salir si no es válido
        if (!valid) return false;

        String odo = odoEt.getText().toString();
        String tankCap = tankCapacityEt.getText().toString();
        String insuranceDate = vencimientoSeguro.getText().toString();
        String odoMtto = multiploKilometraje.getText().toString();
        String lastOdoMtto = lastOdoMttoEt.getText().toString();

        carData.setModel(modelEt.getText().toString());
        carData.setBrand(brandEt.getText().toString());
        carData.setYear(yearEt.getText().toString());
        carData.setOdometer(odo.isEmpty() ? 0 : Long.parseLong(odo));
        carData.setColor(colorEt.getText().toString());
        carData.setPlates(platesEt.getText().toString());
        carData.setTankCapacity(tankCap.isEmpty() ? 0 : Integer.parseInt(tankCap));
        carData.setInsureDeadLine(insuranceDate.isEmpty() ? 0 :
                Constants.dateStringToObject2(insuranceDate).getTime());
        carData.setOdometerMtto(odoMtto.isEmpty() ? 0 : Long.parseLong(odoMtto));
        carData.setLastMttoOdometer(lastOdoMtto.isEmpty() ? 0 : Long.parseLong(lastOdoMtto));

        Integer fuelLevel = (int) (fuelView.getFuelLevel() * 100);
        carData.setFuelPercentage(fuelLevel);

        return true;
    }

    private void getCarData()
    {
        // Validar imagen
        if (carData.getImageUrl() != null && !carData.getImageUrl().isEmpty())
        {
            // Mostrar foto en imageView
            Glide.with(this).load(carData.getImageUrl()).dontAnimate().into(carImageView);
        }

        // Ocultar layout de "No imagen"
        noCarImage.setVisibility(View.GONE);

        // Escribir datos
        modelEt.setText(carData.getModel());
        brandEt.setText(carData.getBrand());
        yearEt.setText(carData.getYear());
        odoEt.setText(String.valueOf(carData.getOdometer()));
        colorEt.setText(carData.getColor());
        platesEt.setText(carData.getPlates());
        statusSpinner.setText(carData.getStatus());

        // Obtener nivel de conmbustible
        fuelView.setFuelLevel((float) (carData.getFuelPercentage() / 100.0));
        tankCapacityEt.setText(String.valueOf(carData.getTankCapacity()));

        // Validar fecha de vencimiento de seguro
        if (carData.getInsureDeadLine() != null && carData.getInsureDeadLine() != 0)
        {
            String insuranceDate =
                    Constants.dateObjectToString(new Date(carData.getInsureDeadLine()));
            vencimientoSeguro.setText(insuranceDate);
        }

        // Validar múltiplo de kilometraje
        if (carData.getOdometerMtto() != null && carData.getOdometerMtto() != 0)
        {
            multiploKilometraje.setText(String.valueOf(carData.getOdometerMtto()));
        }

        // Si ya tiene kilometraje definido, no dejar que se edite
        if (carData.getOdometer() != 0)
        {
            odoEt.setEnabled(false);
        }

        if (carData.getTankCapacity() != null && carData.getTankCapacity() != 0)
        {
            tankCapacityEt.setEnabled(false);
        }

        if (carData.getLastMttoOdometer() != null)
        {
            lastOdoMttoEt.setText(String.valueOf(carData.getLastMttoOdometer()));
        }

        // Inicializar lista de documentos
        initCarDocumentsList();
    }

    private void saveCar()
    {
        // Validar y obtener datos de vehículo
        if (!setCarData()) return;

        // Validar si es un auto nuevo o ya registrado
        if (carId == null)
        {
            // Es nuevo
            carsReference.add(carData).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    Toast.makeText(CarForm.this, "Vehículo guardado correctamente",
                            Toast.LENGTH_SHORT).show();

                    // Obtener id
                    carId = task.getResult().getId();
                    Log.d(TAG, "Id creado para vehículo -> " + carId);

                    // Evaluar si se debe subir la foto
                    if (carPhotoFilePath != null && !carPhotoFilePath.isEmpty())
                    {
                        Log.d(TAG, "carPhotoFilePath -> " + carPhotoFilePath);
                        uploadImage(carPhotoFilePath, "autos/" + carId);
                    }

                    // Mostrar para agregar comprobantes
                    initCarDocumentsList();
                }
                else
                {
                    Toast.makeText(CarForm.this, "Error al guardar vehículo",
                            Toast.LENGTH_SHORT).show();
                }

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            });
        }
        else
        {
            // Actualizar
            carsReference.document(carId).set(carData).addOnCompleteListener(new OnCompleteListener<Void>()
            {
                @Override
                public void onComplete(@NonNull Task<Void> task)
                {
                    if (task.isSuccessful())
                    {
                        Toast.makeText(CarForm.this, "Vehículo modificado correctamente",
                                Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(CarForm.this, "Error al modificar vehículo",
                                Toast.LENGTH_SHORT).show();
                    }

                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });
        }
    }

    private void setCarImage()
    {
        ImageView carImageView;

        // Evaluar donde se necesita mostrar la imagen
        if (carDocumentDialog != null && carDocumentDialog.isShowing())
        {
            // Se tomó foto de un documento de vehículo
            carImageView = carDocumentDialog.findViewById(R.id.dialog_car_document_photo);
            carDocumentFilePath = photoFilePath;
        }
        else
        {
            // Se tomó foto del vehículo
            carImageView = this.carImageView;
            carPhotoFilePath = photoFilePath;

            // Evaluar si se debe subir de una vez
            if (carId != null)
            {
                if (carPhotoFilePath == null)
                {
                    Toast.makeText(this, "Error al obtener imagen", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    uploadImage(carPhotoFilePath, "autos/" + carId);
                }
            }
        }

        // Mostrar foto en imageView
        Glide.with(this).load(photoFilePath).dontAnimate().into(carImageView);

        // Ocultar layout para tomar foto
        noCarImage.setVisibility(View.GONE);
    }

    private void startPhotoIntent()
    {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(getPackageManager()) != null)
        {
            File photoFile;
            try
            {
                photoFile = createPhotoFile();
                if (photoFile != null)
                {
                    photoFilePath = photoFile.getAbsolutePath();
                    Uri photoUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID +
                            ".fileprovider", photoFile);
                    takePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    takePicture.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
                            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    startActivityForResult(takePicture, CAMERA_REQUEST_INTENT);
                }
            }
            catch (Exception e)
            {
                Toast.makeText(this, "Error al iniciar cámara", Toast.LENGTH_LONG).show();
            }
        }
    }

    private File createPhotoFile()
    {
        // Validar directorio padre
        File storageDir = new File(getFilesDir(), "autos");
        if (!storageDir.exists())
        {
            storageDir.mkdir();
        }

        File image = null;
        try
        {
            String carName;
            if (carId == null) carName = "auto_nuevo";
            else carName = carId;
            image = File.createTempFile(carName, ".jpg", storageDir);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Log.e("PhotoStorage", "Error al crear archivo temporal");
        }

        return image;
    }

    private void showCarDocumentDialog(CarDocument carDocument, String documentType)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View content = getLayoutInflater().inflate(R.layout.dialog_car_document, null);
        builder.setView(content);

        // Obtener views de diálogo
        MaterialEditText documentDateEt = content.findViewById(R.id.car_document_date_et);
        documentDateEt.setOnClickListener(view ->
        {
            DatePickerFragment fragment = DatePickerFragment.newInstance((datePicker, i, i1, i2) ->
            {
                String gottenDate =
                        Constants.twoDigits(i2) + "/" + Constants.monthNumberToString
                                (i1 + 1) + "/" + i;

                MaterialEditText dateEt = (MaterialEditText) view;
                dateEt.setText(gottenDate);
            });

            fragment.show(CarForm.this.getSupportFragmentManager(), "datePicker");
        });

        ImageView carDocumentPhoto = content.findViewById(R.id.dialog_car_document_photo);
        carDocumentPhoto.setOnClickListener(view ->
        {
            if (carDocumentPhoto.getDrawable() == null)
            {
                startPhotoIntent();
            }
            else
            {
                showCarImageOptions(carDocument.getPhotoUrl() == null ?
                        carPhotoFilePath : carDocument.getPhotoUrl());
            }
        });

        // Mostrar datos si existe un objeto como parámetro
        if (carDocument != null)
        {
            if (carDocument.getPhotoUrl() != null && !carDocument.getPhotoUrl().isEmpty())
            {
                Glide.with(CarForm.this).load(carDocument.getPhotoUrl()).dontAnimate().into(carDocumentPhoto);
            }
            else
            {
                Glide.with(CarForm.this).load(R.drawable.refrendo_vehicular).dontAnimate().into(carDocumentPhoto);
            }

            documentDateEt.setText(Constants.dateObjectToString(new Date(carDocument.getDate())));
        }

        Button returnButton = content.findViewById(R.id.return_button);
        returnButton.setOnClickListener(view -> carDocumentDialog.dismiss());

        Button saveButton = content.findViewById(R.id.save_button);
        saveButton.setOnClickListener(view ->
        {
            // Validar
            if (documentDateEt.getText().toString().isEmpty())
            {
                documentDateEt.setError("Campo obligatorio");
                return;
            }

            // Obtener datos
            String dateString = documentDateEt.getText().toString();
            String fileName = null;

            // Convertir a objeto
            Date documentDate = Constants.dateStringToObject2(dateString);

            // Crear documento
            focusedCarDocument.setType(documentType);
            focusedCarDocument.setDate(documentDate.getTime());

            // Agregar a la lista correspondiente
            if (documentType.equals(CarDocument.REFRENDO))
            {
                // Agregar
                carData.getRefrendos().add(focusedCarDocument);
                // Actualizar
                refrendosAdapter.refresh();
                // Definir nombre del archivo
                fileName =
                        "autos/" + carId + "/" + focusedCarDocument.getType() + "_" + carData.getRefrendos().size();
            }
            else
            {
                // Agregar
                carData.getVerificaciones().add(focusedCarDocument);
                // Actualizar
                verificacionesAdapter.refresh();
                // Definir nombre del archivo
                fileName =
                        "autos/" + carId + "/" + focusedCarDocument.getType() + "_" + carData.getVerificaciones().size();
            }

            // Validar si hay imagen que guardar
            if (carDocumentFilePath != null && !carDocumentFilePath.isEmpty())
            {
                // Subir imagen del documento
                uploadImage(carDocumentFilePath, fileName);
            }

            carDocumentDialog.dismiss();
        });

        // Crear diálogo
        carDocumentDialog = builder.create();
        carDocumentDialog.show();

        // Crear objeto CarDocument
        focusedCarDocument = new CarDocument();
    }

    private void uploadImage(String imagePath, String bdPath)
    {
        // Mostrar progreso
        if (progressDialog != null && progressDialog.isShowing())
        {
            progressDialog.dismiss();
            progressDialog = ProgressDialog.show(this, "", "Subiendo imagen", true, true);
        }

        // Obtener referencia
        StorageReference storageReference =
                firebaseStorage.getReference().child(bdPath);

        // Obtener archivo
        File imageFile = new File(imagePath);

        // Convertir a arreglo de bytes
        int size = (int) imageFile.length();
        byte[] data = new byte[size];

        try
        {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(imageFile));
            buf.read(data, 0, data.length);
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al leer el archivo a subir", Toast.LENGTH_SHORT).show();
            return;
        }

        // Subir archivo
        UploadTask uploadTask = storageReference.putBytes(data);

        // Definir proceso
        uploadTask.continueWithTask(task ->
        {
            // Manejar primer resultado
            if (!task.isSuccessful())
            {
                throw task.getException();
            }

            return storageReference.getDownloadUrl();
        }).addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                // Obtener Uri del archivo recien subido
                Uri downloadUri = task.getResult();

                // Validar Uri
                if (downloadUri != null)
                {
                    // Validar si es para el vehículo o un documento
                    if (focusedCarDocument == null)
                    {
                        // Asignar al vehículo
                        carData.setImageUrl(downloadUri.toString());

                        // Actualizar ImageView
                        Glide.with(CarForm.this).load(carData.getImageUrl()).dontAnimate().into(carImageView);

                        // Actualizar vehículo
                        saveCar();
                    }
                    else
                    {
                        // Definir url en el objeto
                        focusedCarDocument.setPhotoUrl(downloadUri.toString());

                        // Actualizar lista
                        if (focusedCarDocument.getType().equals(CarDocument.REFRENDO))
                        {
                            refrendosAdapter.refresh();
                        }
                        else
                        {
                            verificacionesAdapter.refresh();
                        }

                        // Actualizar datos de vehículo en la nube
                        saveCar();

                        // Hacer nulo el objeto
                        focusedCarDocument = null;
                    }
                }
            }
            else
            {
                Toast.makeText(CarForm.this, "Error al subir archivo",
                        Toast.LENGTH_SHORT).show();
            }

            // Ocultar progress
            if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
        });
    }
}
