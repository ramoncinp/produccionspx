package com.chipred.produccionspx.cars;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.network.Connectivity;
import com.chipred.produccionspx.service.LocationUpdatesService;
import com.chipred.produccionspx.users.AddDriverFragment;
import com.chipred.produccionspx.users.UserData;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.chipred.produccionspx.Constants.TRIPS_COLLECTION;

public class CarElementActivity extends AppCompatActivity {
    private static final String TAG = CarElementActivity.class.getSimpleName();
    public static final int ASK_PERMISSIONS_CODE = 0;
    public static final int ASK_LOCATION_PERMISSIONS_CODE = 1;
    public static final int TRIP_ACTIVITY_RESULT = 2;
    public static final String CAR_ELEMENT_ID_KEY = "carElementId";
    public static final String CAR_ELEMENT_NAME_KEY = "carElementName";
    public static final String CAR_ELEMENT_TYPE = "tipo";
    public static final String CAR_ELEMENTS = "Vehículos";
    public static final String DRIVERS_ELEMENTS = "Conductores";
    public static final String TRIPS_ELEMENTS = "Salidas";
    public static final int TRIPS_LIMIT = 10;

    // Variables
    private boolean isUser;
    private boolean carLocation;
    private boolean allDocuments = false;
    private boolean fetchingMoreDocuments = false;
    private boolean reOrderTrips = false;
    private String carElementsType = CAR_ELEMENTS;
    private String tripIdFromNotification;

    // Objetos
    private CollectionReference elementsReference;
    private CollectionReference carsReference;
    private CollectionReference usersReference;
    private DocumentSnapshot lastFetchedDocument;
    private HashMap<String, String> carDescriptions;
    private HashMap<String, UserData> users;
    private Query elementQuery;
    private RecyclerView.Adapter elementsAdapter;
    private TripFilter tripFilter;
    private TripFilterFragment tripFilterFragment;

    // Colecciones
    private ArrayList<Object> elements;

    // Views
    private ProgressBar progressBar;
    private ProgressBar paginationProgress;
    private RecyclerView elementsList;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView noElements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_element);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Obtener tipo de usuario
        isUser = getIntent().getBooleanExtra("isUser", false);
        carLocation = getIntent().getBooleanExtra("carsLocation", false);

        // Obtener tipo de elemento
        carElementsType = getIntent().getStringExtra(CAR_ELEMENT_TYPE);
        setTitle(carElementsType);

        // Inicializar views
        initViews();

        // Request permissions
        //requestActivityPermissions();

        // Crear referencia de colección
        elementsReference = FirebaseFirestore.getInstance().collection(getCollectionName());

        // Evaluar si hay un tripId para abrir desde notificacion
        tripIdFromNotification = getIntent().getStringExtra("tripId");
        if (tripIdFromNotification != null) getIntent().removeExtra("tripId");

        getElements();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.trip_list_menu, menu);

        MenuItem locationItem = menu.findItem(R.id.get_location);
        MenuItem filterItem = menu.findItem(R.id.set_filter);
        if (carElementsType.equals(TRIPS_ELEMENTS)) {
            if (locationItem != null && !isUser && carLocation) {
                locationItem.setVisible(true);
            }

            if (filterItem != null && !isUser) {
                filterItem.setVisible(true);
            }
        }

        return super.onCreateOptionsMenu(menu);
    }

    private String getCollectionName() {
        switch (carElementsType) {
            case CAR_ELEMENTS:
                return "autos";

            case DRIVERS_ELEMENTS:
                return "usuarios";

            case TRIPS_ELEMENTS:
                return TRIPS_COLLECTION;
        }

        return "";
    }

    private void initViews() {
        elementsList = findViewById(R.id.recycler_view);
        noElements = findViewById(R.id.no_elements_tv);
        paginationProgress = findViewById(R.id.pagination_progress_bar);
        progressBar = findViewById(R.id.progress_bar);
        swipeRefreshLayout = findViewById(R.id.swipe_layout);

        FloatingActionButton fab = findViewById(R.id.car_element_fab);
        fab.setOnClickListener(view ->
        {
            if (carElementsType.equals(CAR_ELEMENTS)) {
                openCarElementForm(null, null);
            } else if (carElementsType.equals(DRIVERS_ELEMENTS)) {
                showAddDriverDialog();
            } else if (carElementsType.equals(TRIPS_ELEMENTS)) {
                Intent intent = new Intent(CarElementActivity.this, TripForm.class);
                intent.putExtra("isUser", isUser);
                startActivityForResult(intent, TRIP_ACTIVITY_RESULT);
            }
        });

        // Definir listener para swipeUp
        swipeRefreshLayout.setOnRefreshListener(() ->
        {
            if (tripFilter != null) {
                setFilteredTripsQuery(tripFilter);
            } else {
                getAllElements();
            }
        });

        // Ocultar fab cuando el usuario es admin y los elementos son viajes
        //if (!isUser && carElementsType.equals(TRIPS_ELEMENTS)) fab.hide();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.get_location) {
            Intent intent = new Intent(this, TripTrackingActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.set_filter) {
            // Crear fragment si no existe
            if (tripFilterFragment == null) {
                tripFilterFragment = new TripFilterFragment();
                tripFilterFragment.setTripFilterInterface(new TripFilterFragment.TripFilterInterface() {
                    @Override
                    public void onFilterSelected(TripFilter tripFilter) {
                        CarElementActivity.this.tripFilter = tripFilter;
                        setFilteredTripsQuery(tripFilter);
                    }
                });
            }

            // Mostrar fragment
            tripFilterFragment.show(getSupportFragmentManager(), "tripFilterDialog");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TRIP_ACTIVITY_RESULT && resultCode == RESULT_OK) {
            getAllElements();
        }
    }

    private void getElements() {
        // Mostrar progressBar y ocultar lista
        progressBar.setVisibility(View.VISIBLE);
        elementsList.setVisibility(View.GONE);

        // Crear query
        if (carElementsType.equals(TRIPS_ELEMENTS)) {
            if (setTripsQuery()) {
                executeQuery();
            }

            /*if (serviceIsRunningInForeground(this))
            {
                Log.d("LocationUpdatesService", "El servicio de localizacion esta corriendo");
            }*/
        } else {
            if (carElementsType.equals(CAR_ELEMENTS)) {
                elementQuery = elementsReference.orderBy("model", Query.Direction.ASCENDING);
            } else if (carElementsType.equals(DRIVERS_ELEMENTS)) {
                elementQuery = elementsReference.whereArrayContains("allowedBlocks",
                        Constants.VEHICLES_USER);
            }

            executeQuery();
        }
    }

    private void executeQuery() {
        elementQuery.get().addOnCompleteListener(task ->
        {
            if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
            if (task.isSuccessful() && task.getResult() != null && !task.getResult().getMetadata().isFromCache()) {
                parseDocuments(task.getResult().getDocuments());
            } else {
                final String message = "Error al obtener " + carElementsType;
                Constants.showDialog(this, "Aviso", message, (dialogInterface, i) -> finish());
            }
        }).addOnFailureListener(e ->
        {
            Log.e(TAG, e.toString());
            Log.e(TAG, e.getMessage());
        });
    }

    private void parseDocuments(List<DocumentSnapshot> documentsList) {
        if (elements == null) elements = new ArrayList<>();
        //else elements.clear();

        //if (elementsAdapter != null) ((TripAdapter) elementsAdapter).clearList();

        // Obtener cada elemento de la lista
        for (DocumentSnapshot document : documentsList) {
            // Crear elemento a partir de documento
            addCarElement(document);
        }

        // Mostrar elementos
        showElementsList();

        if (carElementsType.equals(TRIPS_ELEMENTS)) {
            // Obtener referencias de usuarios y vehículos
            getReferencedTripsData(0);

            // Evaluar si hay que abrir algun viaje
            if (tripIdFromNotification != null) {
                // Abrir formulario
                Intent intent = new Intent(CarElementActivity.this, TripForm.class);
                intent.putExtra("isUser", isUser);
                intent.putExtra(TripForm.TRIP_ID, tripIdFromNotification);
                startActivityForResult(intent, TRIP_ACTIVITY_RESULT);
                Log.d("Notificaciones", "Abrir formulario de nuevo viaje");

                //Limpiar valor
                tripIdFromNotification = null;
            }
        }

        // Mostrar lista y ocultar progressBar
        progressBar.setVisibility(View.GONE);
        elementsList.setVisibility(View.VISIBLE);
    }

    private boolean setTripsQuery() {
        if (isUser) {
            if (lastFetchedDocument == null) {
                elementQuery =
                        elementsReference.orderBy("requestDate", Query.Direction.DESCENDING).whereEqualTo("responsibleUserId", getUserId()).limit(TRIPS_LIMIT);
            } else {
                // Query para hacer paginación
                elementQuery =
                        elementsReference.orderBy("requestDate", Query.Direction.DESCENDING).whereEqualTo("responsibleUserId", getUserId()).startAfter(lastFetchedDocument).limit(TRIPS_LIMIT);
            }

            return true;
        } else {
            if (lastFetchedDocument == null) {
                setTripsAdminQueryPipeline();
                return false;
            } else {
                // Query para hacer paginación
                elementQuery = elementsReference.whereEqualTo("currentState",
                        Trip.State.TERMINADA).orderBy("requestDate",
                        Query.Direction.DESCENDING).startAfter(lastFetchedDocument).limit(TRIPS_LIMIT);

                return true;
            }
        }
    }

    private void setTripsAdminQueryPipeline() {
        // Crear lista de queries
        List<Task<QuerySnapshot>> tasks = new ArrayList<>();

        // Crear un query para cada estado
        Task<QuerySnapshot> tripQuery1 = elementsReference.whereEqualTo("currentState",
                Trip.State.SOLICITADA).get();

        Task<QuerySnapshot> tripQuery2 = elementsReference.whereEqualTo("currentState",
                Trip.State.PENDIENTE).get();

        Task<QuerySnapshot> tripQuery3 = elementsReference.whereEqualTo("currentState",
                Trip.State.AUTORIZADA).get();

        Task<QuerySnapshot> tripQuery4 = elementsReference.whereEqualTo("currentState",
                Trip.State.ACTIVA).get();

        // Obtener viajes terminados
        Task<QuerySnapshot> tripQuery5 = elementsReference.whereEqualTo("currentState",
                Trip.State.TERMINADA).orderBy("requestDate",
                Query.Direction.DESCENDING).limit(TRIPS_LIMIT).get();

        // Agregar queries
        tasks.add(tripQuery1);
        tasks.add(tripQuery2);
        tasks.add(tripQuery3);
        tasks.add(tripQuery4);

        // Ejecitar lista de queries
        Tasks.whenAllComplete(tasks).addOnCompleteListener((Task<List<Task<?>>> task) ->
        {
            final ArrayList<Trip> queriedTrips = new ArrayList<>();

            if (task.isSuccessful() && task.getResult() != null) {
                for (Task<?> mTask : task.getResult()) {
                    // Obtener querySnapshot
                    QuerySnapshot querySnapshot = ((Task<QuerySnapshot>) mTask).getResult();

                    // Obtener lista de resultado
                    if (querySnapshot != null) {
                        for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                            Trip mTrip = document.toObject(Trip.class);
                            mTrip.setId(document.getId());
                            queriedTrips.add(mTrip);
                        }
                    }
                }
            }

            if (!queriedTrips.isEmpty()) {
                // Ordenar por fecha de solicitado
                Collections.sort(queriedTrips, (trip, t1) -> Long.compare(t1.getRequestDate(),
                        trip.getRequestDate()));

                Log.d(TAG, queriedTrips.toString());

                // Agregar viajes a lista general
                elements = new ArrayList<>(queriedTrips);
                elementsAdapter = new TripAdapter(this, elements);
            }

            // Obtener viajes terminados
            elementQuery = elementsReference.whereEqualTo("currentState",
                    Trip.State.TERMINADA).orderBy("requestDate",
                    Query.Direction.DESCENDING).limit(TRIPS_LIMIT);

            executeQuery();
        });
    }

    private void addCarElement(DocumentSnapshot document) {
        Object carElement;
        if (carElementsType.equals(CAR_ELEMENTS)) {
            carElement = document.toObject(Car.class);
            ((Car) carElement).setId(document.getId());
        } else if (carElementsType.equals(DRIVERS_ELEMENTS)) {
            carElement = document.toObject(Driver.class);
            ((Driver) carElement).setId(document.getId());
        } else {
            carElement = document.toObject(Trip.class);
            ((Trip) carElement).setId(document.getId());

            // Revisar si se debe filtrar por nombre de lugar
            if (tripFilter != null && tripFilter.getPlace() != null && !tripFilter.getPlace().isEmpty()) {
                // Obtener el lugar a filtrar
                String place = tripFilter.getPlace().toLowerCase();

                // Crear bandera para indicar si se encuentra el viaje
                boolean containsPlace = false;

                // Iterar en lista de lugares
                for (String tripPlace : ((Trip) carElement).getPlaces()) {
                    if (tripPlace.toLowerCase().contains(place)) {
                        containsPlace = true;
                        break;
                    }
                }

                // Validar si cumple con el filtro
                if (!containsPlace) return;
            }

            // Agregar al adaptador
            if (elementsAdapter != null) {
                ((TripAdapter) elementsAdapter).addTrip((Trip) carElement);
            }

            // Verificar si se debería volver a ordenar la lista de viajes
            // Se reordenan los viajes si se obtuvo alguno que no está terminado
            if (!((Trip) carElement).getCurrentState().equals(Trip.State.TERMINADA.name()) && !isUser) {
                reOrderTrips = true;
            }

            // Definir como último elemento agregado
            lastFetchedDocument = document;
        }

        // Agregar elemento a la lista
        elements.add(carElement);
    }

    private void showElementsList() {
        if (elements.isEmpty()) {
            String text;
            if (carElementsType.equals(TRIPS_ELEMENTS)) {
                text = "No hay " + carElementsType.toLowerCase() + " registradas";
            } else {
                text = "No hay " + carElementsType.toLowerCase() + " registrados";
            }
            noElements.setText(text);
            return;
        } else {
            noElements.setVisibility(View.GONE);
        }

        // Evaluar cual adaptador se debe crear
        if (carElementsType.equals(CAR_ELEMENTS)) {
            elementsAdapter = new CarOverviewAdapter(this, elements);
            ((CarOverviewAdapter) elementsAdapter).setClickListener(view ->
            {
                Car selectedCar =
                        ((Car) elements.get(elementsList.getChildAdapterPosition(view)));
                openCarElementForm(selectedCar.getId(), selectedCar.getModel());
            });

            elementsList.setLayoutManager(new GridLayoutManager(this,
                    getResources().getInteger(R.integer.grid_layout_columns)));
        } else if (carElementsType.equals(DRIVERS_ELEMENTS)) {
            // Ordenar alfabéticamente
            Collections.sort(elements,
                    (o, t1) -> ((Driver) o).getName().compareTo(((Driver) t1).getName()));

            elementsAdapter = new DriverOverviewAdapter(this, elements);
            ((DriverOverviewAdapter) elementsAdapter).setClickListener(view ->
            {
                Driver selectedDriver =
                        ((Driver) elements.get(elementsList.getChildAdapterPosition(view)));
                openCarElementForm(selectedDriver.getId(), selectedDriver.getName());
            });

            elementsList.setLayoutManager(new GridLayoutManager(this,
                    getResources().getInteger(R.integer.grid_layout_columns)));
        } else {
            // Mostrar lista de viajes
            // Ordenar por estatus
            Collections.sort(elements, new TripStateComparator());

            // Crear adaptador
            if (elementsAdapter == null) elementsAdapter = new TripAdapter(this, elements);
            ((TripAdapter) elementsAdapter).setClickListener(view ->
            {
                // Validar conexión
                if (!Connectivity.isGoodConnection(getApplicationContext())) {
                    Constants.showDialog(this, "Aviso", "No tiene buena conexión a internet, compruébela e intente de nuevo", (dialogInterface, i) -> dialogInterface.dismiss());
                    return;
                }

                // Obtener trip
                Trip trip = ((Trip) elements.get(elementsList.getChildAdapterPosition(view)));

                // Abrir formulario
                Intent intent = new Intent(CarElementActivity.this, TripForm.class);
                intent.putExtra("isUser", isUser);
                intent.putExtra(TripForm.TRIP_ID, trip.getId());
                startActivityForResult(intent, TRIP_ACTIVITY_RESULT);
            });

            ((TripAdapter) elementsAdapter).setLongClickListener(view ->
            {
                // Si el usuario es administrador y el viaje NO esta activo o terminado
                int listPosition = elementsList.getChildAdapterPosition(view);
                Trip trip = (Trip) (elements.get(listPosition));
                if (!isUser && !(trip.getCurrentState().equals(Trip.State.TERMINADA.name())))
                    showDeleteTripDialog(listPosition);
                return true;
            });

            // Definir layoutManager
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            elementsList.setLayoutManager(linearLayoutManager);

            // Definir scrollListener
            elementsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    int visibleItemCount = linearLayoutManager.getChildCount();
                    int totalItemCount = elements.size();
                    int firstVisibleItemPosition =
                            linearLayoutManager.findFirstVisibleItemPosition();

                    if (visibleItemCount + firstVisibleItemPosition == totalItemCount && !fetchingMoreDocuments && !allDocuments) {
                        fetchingMoreDocuments = true;
                        paginationProgress.setVisibility(View.VISIBLE);
                        queryMoreTrips();
                    }
                }
            });
        }

        // Definir adaptador
        elementsList.setAdapter(elementsAdapter);
    }

    private void openCarElementForm(String elementId, String name) {
        Intent intent = new Intent(CarElementActivity.this, carElementsType.equals(CAR_ELEMENTS)
                ? CarForm.class : DriverForm.class);
        intent.putExtra(CAR_ELEMENT_ID_KEY, elementId);
        intent.putExtra(CAR_ELEMENT_NAME_KEY, name);
        startActivity(intent);
    }

    private void showAddDriverDialog() {
        AddDriverFragment addDriverFragment = new AddDriverFragment();
        addDriverFragment.show(getSupportFragmentManager(), "add_driver_dialog");
    }

    private String getUserId() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null) {
            return currentUser.getUid();
        }

        return "";
    }

    private void getReferencedTripsData(int beginIdx) {
        for (int i = beginIdx; i < elements.size(); i++) {
            Object carElement = elements.get(i);

            // Obtener nombres extras vehículo
            if (((Trip) carElement).getVehicleId() != null) {
                getCarDescription(((Trip) carElement).getVehicleId(), i);
                if (!isUser) getUser(((Trip) carElement).getResponsibleUserId(), i);
            }
        }
    }

    private void getCarDescription(String carId, int listPosition) {
        if (carsReference == null) {
            carsReference = FirebaseFirestore.getInstance().collection("autos");
            carDescriptions = new HashMap<>();
        }

        // Buscar primero en HashMap
        if (carDescriptions.containsKey(carId)) {
            ((Trip) elements.get(listPosition)).setVehicleDesc(carDescriptions.get(carId));
            elementsAdapter.notifyItemChanged(listPosition);
        } else {
            // Buscar vehículo por Id en bd
            carsReference.document(carId).get().addOnCompleteListener(task ->
            {
                // Obtener datos
                if (task.isSuccessful() && task.getResult() != null) {
                    // Obtener vehículo
                    Car car = task.getResult().toObject(Car.class);

                    if (car != null) {
                        car.setId(task.getResult().getId());

                        // Agregar a objeto correspondiente
                        ((Trip) elements.get(listPosition)).setVehicleDesc(car.getModel());
                        elementsAdapter.notifyItemChanged(listPosition);

                        // Agregar a HashMap
                        carDescriptions.put(car.getId(), car.getModel());
                    } else {
                        Toast.makeText(CarElementActivity.this, "Error al obtener nombre de " +
                                "vehículo", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void getUser(String userId, int listPosition) {
        if (usersReference == null) {
            usersReference = FirebaseFirestore.getInstance().collection("usuarios");
            users = new HashMap<>();
        }

        // Buscar primero en HashMap
        if (users.containsKey(userId)) {
            ((Trip) elements.get(listPosition)).setUserName(users.get(userId).getName());
            ((Trip) elements.get(listPosition)).setUserPhotoUrl(users.get(userId).getPhotoUrl());
            elementsAdapter.notifyItemChanged(listPosition);
        } else {
            // Buscar vehículo por Id en bd
            usersReference.document(userId).get().addOnCompleteListener(task ->
            {
                // Obtener datos
                if (task.isSuccessful() && task.getResult() != null) {
                    // Obtener vehículo
                    UserData user = task.getResult().toObject(UserData.class);

                    if (user != null) {
                        user.setId(task.getResult().getId());

                        // Agregar a objeto correspondiente
                        ((Trip) elements.get(listPosition)).setUserName(user.getName());
                        ((Trip) elements.get(listPosition)).setUserPhotoUrl(user.getPhotoUrl());
                        elementsAdapter.notifyItemChanged(listPosition);

                        // Agregar a HashMap
                        users.put(user.getId(), user);
                    } else {
                        Toast.makeText(CarElementActivity.this, "Error al obtener nombre de " +
                                "usuario", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void showDeleteTripDialog(int listPosition) {
        Trip trip = (Trip) (elements.get(listPosition));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Eliminar viaje");
        builder.setMessage("¿Seguro que desea eliminar el viaje de " + trip.getUserName() + " a " + trip.getPlaces().get(0) + "?");
        builder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.setPositiveButton("Sí", (dialogInterface, i) -> removeTrip(trip.getId()));

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void removeTrip(String tripId) {
        FirebaseFirestore.getInstance().collection(TRIPS_COLLECTION).document(tripId).delete().addOnCompleteListener(task ->
        {
            if (task.isSuccessful()) {
                Toast.makeText(CarElementActivity.this, "Viaje eliminado correctamente",
                        Toast.LENGTH_SHORT).show();

                // Volver a pedir los elementos
                getAllElements();
            } else {
                Toast.makeText(CarElementActivity.this, "Error al eliminar viaje seleccionado",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestActivityPermissions() {
        if (isUser) {
            checkLocationPermissions();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ASK_LOCATION_PERMISSIONS_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permitido
                Log.v(TAG, "Permiso de localización concedido");
            } else {
                // Rechazado
                Log.v(TAG, "Permiso de localización rechazado");
            }
        }
    }

    private void checkLocationPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            switch (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                case PackageManager.PERMISSION_GRANTED:
                    Log.v(TAG, "Permission is granted");
                    break;

                case PackageManager.PERMISSION_DENIED:
                    Log.v(TAG, "Permission is revoked");
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            ASK_LOCATION_PERMISSIONS_CODE);
                    break;
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
        }
    }

    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (LocationUpdatesService.class.getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

    private void queryMoreTrips() {
        // Preparar query
        setTripsQuery();

        // Ejecutar query
        elementQuery.get().addOnCompleteListener(task ->
        {
            if (task.isSuccessful() && task.getResult() != null) {
                // Obtener lista de documentos
                List<DocumentSnapshot> documentsList = task.getResult().getDocuments();

                // Obtener último indice antes de agregar nuevos viajes
                int beginIdx = elements.size();
                int newElementsCount = 0;

                // Obtener cada elemento de la lista
                for (DocumentSnapshot document : documentsList) {
                    // Crear elemento a partir de documento
                    addCarElement(document);
                    newElementsCount++;
                }

                // Evaluar si ya se llegó al final
                if (newElementsCount < TRIPS_LIMIT) {
                    allDocuments = true;
                }

                // Definir indices para actualizar lista dependiendo si se necesita reordenar
                int startIdx = reOrderTrips ? 0 : beginIdx;
                int endIdx = reOrderTrips ? 0 : elements.size();

                // Ordenar lista de viajes si es que se necesita
                reOrderTrips();

                // Actualizar adaptador desde la primera posición del elemento que se agregó
                elementsAdapter.notifyItemRangeChanged(startIdx, endIdx);

                // Obtener referencias de usuarios y vehículos
                getReferencedTripsData(startIdx);
            } else {
                Toast.makeText(CarElementActivity.this,
                        "Error al obtener " + CAR_ELEMENTS.toLowerCase(),
                        Toast.LENGTH_SHORT).show();
            }

            fetchingMoreDocuments = false;
            paginationProgress.setVisibility(View.GONE);
        });
    }

    private void reOrderTrips() {
        if (reOrderTrips) {
            // Obtener lista de adaptador
            TripAdapter tripAdapter = (TripAdapter) elementsAdapter;

            // Ordenar lista
            Collections.sort(tripAdapter.getTrips(), new TripStateComparator());
            Collections.sort(elements, new TripStateComparator());

            // Apagar bandera
            reOrderTrips = false;
        }
    }

    /**
     * Filtros para obtener viajes
     * - Por fecha (fecha_inicio, fecha_final)
     * - startDate >= fecha inicio || endDate <= fecha_final
     * - Posibilidad de agregar el filtro POR VEHÍCULO
     */
    private void setFilteredTripsQuery(TripFilter tripFilter) {
        // Reiniciar variables y objetos
        allDocuments = false;
        lastFetchedDocument = null;
        elementsAdapter = null;
        elements = null;

        // Evaluar el filtro seleccionado
        if (tripFilter.getFilter() == TripFilter.FILTER.DATE) {
            // Definir query
            Query query;
            if (tripFilter.getDateType().equals(Trip.START_DATE_TYPE)) {
                query = elementsReference.orderBy("startDate", Query.Direction.DESCENDING).whereGreaterThanOrEqualTo("startDate",
                        tripFilter.getStartDate()).whereLessThanOrEqualTo("startDate",
                        tripFilter.getEndDate());
            } else {
                query = elementsReference.orderBy("endDate", Query.Direction.DESCENDING).whereGreaterThanOrEqualTo("endDate",
                        tripFilter.getStartDate()).whereLessThanOrEqualTo("endDate",
                        tripFilter.getEndDate());
            }

            // Definir listener
            query.get().addOnCompleteListener(task ->
            {
                if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
                if (task.getResult() != null) {
                    // Mostrar lista
                    parseDocuments(task.getResult().getDocuments());

                    // Indicar que son todos los documentos
                    allDocuments = true;
                }
            });

            // Mostrar progressBar y ocultar lista
            progressBar.setVisibility(View.VISIBLE);
            elementsList.setVisibility(View.GONE);
        } else if (tripFilter.getFilter().equals(TripFilter.FILTER.VEHICLE)) {
            elementsReference.orderBy("requestDate", Query.Direction.DESCENDING).whereEqualTo(
                    "vehicleId",
                    tripFilter.getVehicleId()).limit(30).get().addOnCompleteListener(task ->
            {
                if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
                if (task.getResult() != null) {
                    // Mostrar lista
                    parseDocuments(task.getResult().getDocuments());

                    // Indicar que son todos los documentos
                    allDocuments = true;
                }
            });

            // Mostrar progressBar y ocultar lista
            progressBar.setVisibility(View.VISIBLE);
            elementsList.setVisibility(View.GONE);
        } else if (tripFilter.getFilter().equals(TripFilter.FILTER.USER)) {
            elementsReference.orderBy("requestDate", Query.Direction.DESCENDING).whereEqualTo(
                    "responsibleUserId",
                    tripFilter.getUserId()).limit(30).get().addOnCompleteListener(task ->
            {
                if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
                if (task.getResult() != null) {
                    // Mostrar lista
                    parseDocuments(task.getResult().getDocuments());

                    // Indicar que son todos los documentos
                    allDocuments = true;
                }
            });

            // Mostrar progressBar y ocultar lista
            progressBar.setVisibility(View.VISIBLE);
            elementsList.setVisibility(View.GONE);
        } else if (tripFilter.getFilter().equals(TripFilter.FILTER.NONE)) {
            CarElementActivity.this.tripFilter = null;

            // Reiniciar variables y objetos
            getAllElements();
        }
    }

    private void getAllElements() {
        // Reiniciar variables y objetos
        allDocuments = false;
        lastFetchedDocument = null;
        elementsAdapter = null;
        elements = null;

        getElements();
    }
}
