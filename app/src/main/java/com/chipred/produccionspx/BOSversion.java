package com.chipred.produccionspx;

import androidx.annotation.NonNull;

public class BOSversion
{
    private String id;
    private String nombre;
    private String csCC;
    private String csMain;
    private String csSide;

    public BOSversion()
    {

    }

    public BOSversion(String nombre, String csCC, String csMain, String csSide)
    {
        this.nombre = nombre;
        this.csCC = csCC;
        this.csMain = csMain;
        this.csSide = csSide;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getCsCC()
    {
        return csCC;
    }

    public String getCsMain()
    {
        return csMain;
    }

    public String getCsSide()
    {
        return csSide;
    }

    @NonNull
    @Override
    public String toString()
    {
        return nombre;
    }
}
