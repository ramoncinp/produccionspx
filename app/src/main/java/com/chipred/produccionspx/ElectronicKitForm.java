package com.chipred.produccionspx;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.chipred.produccionspx.adapters.SerialsListAdapter;
import com.chipred.produccionspx.projects.ProjectOverview;
import com.chipred.produccionspx.utils.DatePickerFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ElectronicKitForm extends AppCompatActivity
{
    //Constantes
    public final static String TAG = ElectronicKitForm.class.getSimpleName();

    public final static String KIT_NUMBER = "numero_kit";

    private final static String UD6 = "UD6";
    private final static String ENS_LED_MATRIX_1 = "Led Matrix L1";
    private final static String ENS_LED_MATRIX_2 = "Led Matrix L2";
    private final static String SANDWICH_L1 = "Sándwich L1";
    private final static String SANDWICH_L2 = "Sándwich L2";
    private final static String PULSADOR = "Pulsador";
    private final static String MEDIDOR = "Medidor";
    private final static String ACCESORIOS = "Accesorios";

    private final static String UD6_KEY = "ud6";
    private final static String ENS_LED_MATRIX_1_KEY = "led_matrix_l1";
    private final static String ENS_LED_MATRIX_2_KEY = "led_matrix_l2";
    private final static String SANDWICH_L1_KEY = "sandwich_l1";
    private final static String SANDWICH_L2_KEY = "sandwich_l2";
    private final static String PULSADOR_KEY = "pulsador";
    private final static String MEDIDOR_KEY = "medidor";
    private final static String ACCESORIOS_KEY = "accesorios";
    private final static String PRUEBAS_KEY = "pruebas";

    private final static String DISPENSER_CV_VISIBLE = "disp_cv_visible";
    public final static String SERIALS_SUGGESTION_LIST = "serials_suggestions";
    public final static String MAC_ADDRESS_SUGGESTION_LIST = "macs_suggestions";
    public final static int SERIALS_SUGGESTION_THRESHOLD = 2;

    //Variables
    private boolean addOrModify = true; //True para agregar, false para modificar
    private boolean canModify = true;
    private boolean dispenserCvVisible = false;
    private String kitNumber = "";
    private String selectedDispId = "";
    private String selectedProjectId = "";
    private String selectedBosVersionId = "";

    //Listas y Adaptadores
    private ArrayAdapter dispensersAdapter;
    private ArrayAdapter projectsAdapter;
    private ArrayAdapter bosVersionsAdapter;
    private ArrayAdapter macsSuggestionsAdapter;
    private ArrayAdapter serialSuggestionsAdapter;
    private ArrayList<ArrayList<String>> dispensersReferences = new ArrayList<>();
    private ArrayList<DispModel> dispensersModels = new ArrayList<>();
    private ArrayList<ProjectOverview> projectOverviews = new ArrayList<>();
    private ArrayList<BOSversion> bosVersionsList;
    private ArrayList<String> macsSuggestions;
    private ArrayList<String> serialsSuggestions;

    //Objetos
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Map<String, Object> rootObject = new HashMap<>();

    //Views
    private CardView dispensersCv;
    private NestedScrollView content;
    private Dialog serialsListDialog;
    private Dialog testsDialog;
    private MaterialBetterSpinner bosVersionsSpinner;
    private MaterialBetterSpinner dispensersSpinner;
    private MaterialBetterSpinner projectsSpinner;
    private MaterialAutoCompleteTextView macAddressEditText;
    private ProgressBar progressBar;
    private TextView tiempoQuemadoTv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electronic_kit_form);

        //Agregar flecha para regresar
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //No enfocar a ningún view
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //Obtener las variables almacenadas
        getInstanceVariables(savedInstanceState);

        //Obtener lista de sugerencias para seriales
        if (getIntent().hasExtra(SERIALS_SUGGESTION_LIST))
        {
            serialsSuggestions = getIntent().getStringArrayListExtra(SERIALS_SUGGESTION_LIST);
            if (serialsSuggestions == null) serialsSuggestions = new ArrayList<>();
            Log.d(TAG, "Lista de seriales tiene " + serialsSuggestions.size() + " elementos");
        }

        //Inicializar vistas
        initViews();

        //Inicializar base de datos solo si se va a agregar un kit
        if (addOrModify)
        {
            initDb();
        }
        else
        {
            //Deshabilitar edit text para numero de kit
            MaterialEditText kitNumberEt = findViewById(R.id.e_kit_number);
            kitNumberEt.setEnabled(false);
        }

        //Obtener permiso para modificar kits electrónicos
        if (getIntent().hasExtra(ElectronicKitsActivity.CAN_MODIFY_EKIT))
        {
            canModify = getIntent().getBooleanExtra(ElectronicKitsActivity.CAN_MODIFY_EKIT, true);
            if (!canModify) disableViews();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (canModify)
            getMenuInflater().inflate(R.menu.save_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        switch (id)
        {
            case android.R.id.home:
                this.finish();
                break;

            case R.id.save:
                saveElecKit();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        //Guardar visibilidad de cardView
        outState.putBoolean(DISPENSER_CV_VISIBLE, dispenserCvVisible);
    }

    void getInstanceVariables(Bundle savedInstance)
    {
        //Validar que exista una instancia almacenada
        if (savedInstance == null) return;

        //Recuperar valores
        dispenserCvVisible = savedInstance.getBoolean(DISPENSER_CV_VISIBLE, false);
    }

    void disableViews()
    {
        //Obtener linear layout principal
        LinearLayout content = findViewById(R.id.linearLayout_content);
        Constants.disableLinearLayoutChilds(content, this);
    }

    void initViews()
    {
        //Inicializar views
        bosVersionsSpinner = findViewById(R.id.bos_versions_spinner);
        dispensersSpinner = findViewById(R.id.dispensers_spinner);
        projectsSpinner = findViewById(R.id.projects_spinner);
        progressBar = findViewById(R.id.progress_bar);
        content = findViewById(R.id.content);

        //Obtener número de kit si es que se va a modificar
        if (getIntent().hasExtra(KIT_NUMBER))
        {
            progressBar.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);

            kitNumber = getIntent().getStringExtra(KIT_NUMBER);
            addOrModify = false;

            //Hacer query del proyecto
            queryExistingKit();
        }

        //Inicializar adaptador de proyectos
        projectsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1,
                new ArrayList<ProjectOverview>());

        //Inicializar adaptador de proyectos
        projectsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1,
                new ArrayList<ProjectOverview>());

        //Inicialziar adaptador de dispensarios
        dispensersAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1, dispensersModels);
        dispensersSpinner.setAdapter(dispensersAdapter);

        //Definir adaptador de proyectos
        projectsSpinner.setAdapter(projectsAdapter);
        projectsSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                //Obtener numero de orden del proyecto
                onProjectSelected(position);
            }
        });

        //Obtener cardview de dispensarios
        dispensersCv = findViewById(R.id.dispensers_card_view);

        //Recuperar visibilidad de cardView
        if (dispenserCvVisible)
        {
            dispensersCv.setVisibility(View.VISIBLE);
        }
        else
        {
            dispensersCv.setVisibility(View.GONE);
        }

        //Inicializar lista de versiones
        bosVersionsList = new ArrayList<>();

        //Inicializar adaptador de versiones
        bosVersionsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1,
                new ArrayList<BOSversion>());

        //Definir adaptador de versiones
        bosVersionsSpinner.setAdapter(bosVersionsAdapter);

        //Botón para vizualizar las pruebas
        CardView testsButton = findViewById(R.id.tests_cv);
        testsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Mostrar diálogo con información de las pruebas
                showTestsDialog();
            }
        });

        //Inicializar EditText de fechas
        MaterialEditText inicioQuemadoEt = findViewById(R.id.inicio_quemado_et);
        MaterialEditText finQuemadoEt = findViewById(R.id.fin_quemado_et);
        tiempoQuemadoTv = findViewById(R.id.tiempo_quemado_tv);

        //Definir listener para EditTexts con DatePicker
        View.OnClickListener dateTimePickerListener = new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                DatePickerFragment fragment = DatePickerFragment.newInstance(new DatePickerDialog
                        .OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2)
                    {
                        String gottenDate =
                                Constants.twoDigits(i2) + "/" + Constants.monthNumberToString
                                        (i1 + 1) + "/" + i;

                        final MaterialEditText dateEt = (MaterialEditText) v;
                        dateEt.setText(gottenDate);

                        //Obtener hora actual
                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);

                        //Crear instancia de timePicker
                        TimePickerDialog timePickerDialog =
                                new TimePickerDialog(ElectronicKitForm.this,
                                        new TimePickerDialog.OnTimeSetListener()
                                        {
                                            @Override
                                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                                  int minute)
                                            {
                                                dateEt.append(String.format(Locale.getDefault(),
                                                        " %02d:%02d", hourOfDay, minute));

                                                //Calcular tiempo de quemado
                                                calculateTestTime();
                                            }
                                        }, hour, minute, true);

                        timePickerDialog.setTitle("Seleccione la hora");
                        timePickerDialog.show();
                    }
                });

                fragment.show(ElectronicKitForm.this.getSupportFragmentManager(), "datePicker");
            }
        };

        //Definir listeners
        inicioQuemadoEt.setOnClickListener(dateTimePickerListener);
        finQuemadoEt.setOnClickListener(dateTimePickerListener);

        //Mostrar elementos de listas
        showSerialsList();

        //Inicializar adaptador para sugerencias
        serialSuggestionsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1, serialsSuggestions);

        if (getIntent().hasExtra(MAC_ADDRESS_SUGGESTION_LIST))
        {
            macsSuggestions = getIntent().getStringArrayListExtra(MAC_ADDRESS_SUGGESTION_LIST);
            if (macsSuggestions == null) macsSuggestions = new ArrayList<>();
            Log.d(TAG, "Lista de MACAddress tiene " + macsSuggestions.size() + " elementos");
        }

        macsSuggestionsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1, macsSuggestions);

        macAddressEditText = findViewById(R.id.e_kit_mac_address);
        macAddressEditText.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
        macAddressEditText.setAdapter(macsSuggestionsAdapter);
    }

    void initDb()
    {
        //Obtener proyectos
        db.collection("proyectos").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task)
            {
                if (task.isSuccessful())
                {
                    //Validar que no sea nulo
                    if (task.getResult() == null) return;

                    //Obtener cada proyecto
                    for (DocumentSnapshot documentSnapshot : task.getResult())
                    {
                        //Instanciar en un objeto de proyecto
                        ProjectOverview projectOverview =
                                new ProjectOverview(documentSnapshot.getId());

                        //Obtener datos para visualizar
                        Map<String, Object> generalData =
                                (HashMap<String, Object>) documentSnapshot.get("datos_generales");

                        //Obtener cliente y estacion
                        String client = (String) generalData.get("cliente");
                        String station = (String) generalData.get("estacion");

                        //Obtener dispensarios del proyecto
                        ArrayList<String> projectDispensers =
                                (ArrayList<String>) (documentSnapshot.get("dispensarios"));

                        //Agregar al objeto
                        projectOverview.setClient(client);
                        projectOverview.setStation(station);

                        //Agregar objeto a la lista
                        projectOverviews.add(projectOverview);

                        //Agregar lista de dispensarios
                        dispensersReferences.add(projectDispensers);

                        Log.d("ElecKitForm", "Validando id's de proyectos");

                        //Evaluar si es el proyecto actual que se va a modificar o leer
                        if (!addOrModify && selectedProjectId.equals(projectOverview.getOrderNumber()))
                        {
                            //Mostrar proyecto actual
                            projectsSpinner.setText(projectOverview.toString());

                            //Ejecutar "onProjectSelected", que es el último elemento agregado
                            onProjectSelected(projectOverviews.size() - 1);
                        }
                    }

                    //Definir adaptador con lista actualizada
                    projectsAdapter = new ArrayAdapter<>(ElectronicKitForm.this,
                            android.R.layout.simple_expandable_list_item_1,
                            projectOverviews);

                    //Definir adaptador
                    projectsSpinner.setAdapter(projectsAdapter);
                }
                else
                {
                    Toast.makeText(ElectronicKitForm.this, "Error al obtener los proyectos",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        db.collection("versiones_bos").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task)
            {
                if (task.isSuccessful() && task.getResult() != null)
                {
                    //Limpiar listsa
                    bosVersionsList.clear();

                    //Obtener las versiones
                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments())
                    {
                        //Convertir a objeto
                        BOSversion sversion = documentSnapshot.toObject(BOSversion.class);

                        //Validar conversion de a objeto
                        if (sversion != null)
                        {
                            sversion.setId(documentSnapshot.getId());
                            bosVersionsList.add(sversion);

                            Log.d("ElecKitForm", "Validando versiones bOS");

                            //Evaluar si es la version del kit modificable
                            if (!addOrModify && sversion.getId().equals(selectedBosVersionId))
                            {
                                bosVersionsSpinner.setText(sversion.toString());
                            }
                        }
                        else
                        {
                            Toast.makeText(ElectronicKitForm.this, "Error al obtener versión de " +
                                    "bOS", Toast.LENGTH_SHORT).show();
                        }
                    }

                    //Inicializar adaptador de versiones
                    bosVersionsAdapter = new ArrayAdapter<>(ElectronicKitForm.this,
                            android.R.layout.simple_expandable_list_item_1,
                            bosVersionsList);

                    //Definir adaptador de versiones
                    bosVersionsSpinner.setAdapter(bosVersionsAdapter);
                    bosVersionsSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position,
                                                long id)
                        {
                            //Obtener version seleccionada
                            selectedBosVersionId = bosVersionsList.get(position).getId();
                        }
                    });
                }
                else
                {
                    Toast.makeText(ElectronicKitForm.this, "Error al obtener versiones de bOS",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void queryExistingKit()
    {
        db.collection("kits_electronicos").document(kitNumber).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
        {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task)
            {
                if (task.isSuccessful() && task.getResult() != null)
                {
                    //Obtener mapa raíz
                    rootObject = task.getResult().getData();

                    //Escribir con datos obtenidos
                    getElecKit();

                    //Inicializar base de datos
                    initDb();

                    //Mostrar contenido
                    progressBar.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                }
                else
                {
                    Toast.makeText(ElectronicKitForm.this, "Error al obtener kit electrónico",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showSerialsList()
    {
        //Crear lista para almacenar descripciones y claves
        final ArrayList<String> descriptions = getElecKitElements();

        //Obtener referencia de recyclerView
        final RecyclerView serialsList = findViewById(R.id.serials_list);

        //Crear adaptador
        SerialsListAdapter serialsListAdapter = new SerialsListAdapter(descriptions);
        //Definir onClickListener
        serialsListAdapter.setClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Obtener texto del elemento seleccionado
                String desc = descriptions.get(serialsList.getChildAdapterPosition(v));
                //Mostrar formulario
                createSerialsListDialog(desc);
            }
        });

        //Definir adaptador a la lista
        serialsList.setAdapter(serialsListAdapter);

        //Definir LayoutManager
        serialsList.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void onProjectSelected(int projectIdx)
    {
        //Definir como proyecto seleccionado
        selectedProjectId = projectOverviews.get(projectIdx).getOrderNumber();

        //Instancia de mensaje
        TextView noDispModels = findViewById(R.id.no_disp_models);

        //Obtener los proyectos del proyecto seleccionado
        final ArrayList<String> projectDispensers = dispensersReferences.get(projectIdx);

        //Preparar lista de modelos de dispensarios
        dispensersModels.clear();

        //Mostrar cardView
        dispensersCv.setVisibility(View.VISIBLE);
        dispenserCvVisible = true;

        //Validar tamaño de la lista
        if (projectDispensers.size() == 0)
        {
            noDispModels.setVisibility(View.VISIBLE);
            dispensersSpinner.setVisibility(View.GONE);

            return;
        }
        else
        {
            dispensersSpinner.setText("");
        }

        //Obtener datos del dispensario
        //Crear referencia de dispensarios en bd
        CollectionReference dispRef = db.collection("dispensarios");

        for (final String dispenserId : projectDispensers)
        {
            dispRef.document(dispenserId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>()
            {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task)
                {
                    if (task.isSuccessful() && task.getResult() != null)
                    {
                        String docId = task.getResult().getId();

                        //Convertir a modelo de dispensario
                        DispModel dispModel = task.getResult().toObject(DispModel.class);
                        dispModel.setId(docId);

                        //Validar que no tenga un kit asignado
                        if (dispModel.getKitElectronico().isEmpty())
                        {
                            //Agregar a la lista de modelos de dispensarios
                            dispensersModels.add(dispModel);

                            //Reasignar adaptador
                            dispensersAdapter = new ArrayAdapter<>(ElectronicKitForm.this,
                                    android.R.layout.simple_expandable_list_item_1,
                                    dispensersModels);
                            dispensersSpinner.setAdapter(dispensersAdapter);
                        }

                        //Mostrar cardView de los dispensarios
                        dispensersCv.setVisibility(View.VISIBLE);

                        Log.d("ElecKitForm", "Validando dispensarios");

                        //Si es el dispensario seleccionado en el kit
                        if (!addOrModify && selectedDispId.equals(docId))
                        {
                            //Mostrar dispensario seleccionado
                            dispensersSpinner.setText(dispModel.toString());

                            //Mostrar información de dispensario
                            onDispSelected(dispModel);
                        }
                    }
                    else
                    {
                        Toast.makeText(ElectronicKitForm.this,
                                "Error al obtener dispensario", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        //Definir onItemSelected
        dispensersSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id)
            {
                //Obtener modelo seleccionado
                DispModel mDispModel = dispensersModels.get(position);
                onDispSelected(mDispModel);
            }
        });
    }

    private void onDispSelected(DispModel dispModel)
    {
        //Obtener referencia de linearLayout donde se agregará la información de dispensario
        LinearLayout dispenserLayout = findViewById(R.id.dispenser_content_layout);

        //Obtener EditText's
        MaterialEditText model = dispenserLayout.findViewById(R.id.disp_model);
        MaterialEditText serialNumber = dispenserLayout.findViewById(R.id.disp_serial_number);
        MaterialEditText chasisNumber = dispenserLayout.findViewById(R.id.disp_chasis);
        MaterialEditText manufactureDate = dispenserLayout.findViewById(R.id.disp_manufacture_date);

        //Escribir información del modelo seleccionado
        model.setText(dispModel.getModel());
        serialNumber.setText(dispModel.getSerialNumber());
        chasisNumber.setText(dispModel.getChasis());
        manufactureDate.setText(dispModel.getManufactureDate());

        //Definir id global de dispensario seleccionado
        selectedDispId = dispModel.getId();
    }

    private void createSerialsListDialog(String group)
    {
        //Crear builder para diálogo
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //Obtener layout correspondiente al grupo
        View content = getGroupView(group);

        //Validar que se haya obtenido el contenido correctamente
        if (content == null)
        {
            Toast.makeText(this, "Error al obtener formulario", Toast.LENGTH_SHORT).show();
            return;
        }

        //Inicializar views
        setGetDialogData(content, group);

        //Definir view
        builder.setView(content);

        //Mostrar diálogo
        serialsListDialog = builder.create();
        serialsListDialog.show();
    }

    private View getGroupView(String group)
    {
        int layoutResource;

        switch (group)
        {
            case UD6:
                layoutResource = R.layout.elec_kits_serials_ud6;
                break;

            case ENS_LED_MATRIX_1:
            case ENS_LED_MATRIX_2:
                layoutResource = R.layout.elec_kits_serials_ens_led_matrix;
                break;

            case SANDWICH_L1:
            case SANDWICH_L2:
                layoutResource = R.layout.elec_kits_serials_sandwich;
                break;

            case PULSADOR:
                layoutResource = R.layout.elec_kits_serials_pulsador;
                break;

            case MEDIDOR:
                layoutResource = R.layout.elec_kits_serials_medidor;
                break;

            case ACCESORIOS:
                layoutResource = R.layout.elec_kits_serials_accesorios;
                break;

            default:
                return null;
        }

        return getLayoutInflater().inflate(layoutResource, null);
    }

    private void setGetDialogData(View content, String group)
    {
        //Obtener botones
        Button returnButton = content.findViewById(R.id.return_button);
        Button submitButton = content.findViewById(R.id.submit_button);

        //Crear filtro para mayusculas
        InputFilter[] inputFilterArray = new InputFilter[]{new InputFilter.AllCaps()};

        if (group.equals(UD6))
        {
            final MaterialAutoCompleteTextView ud6Et = content.findViewById(R.id.ud6);
            final MaterialAutoCompleteTextView pcaHubEt = content.findViewById(R.id.pca_hub);
            final MaterialAutoCompleteTextView pcaEddyEt = content.findViewById(R.id.pca_eddy);
            final MaterialAutoCompleteTextView portaEddyEt = content.findViewById(R.id.porta_eddy);
            final MaterialAutoCompleteTextView powerSupplyEt =
                    content.findViewById(R.id.power_supply);
            final MaterialAutoCompleteTextView lockEt = content.findViewById(R.id.lock);
            final MaterialAutoCompleteTextView bateriaEt = content.findViewById(R.id.bateria);
            final MaterialAutoCompleteTextView pcaTapaEt = content.findViewById(R.id.pca_tapa);

            ud6Et.setFilters(inputFilterArray);
            pcaHubEt.setFilters(inputFilterArray);
            pcaEddyEt.setFilters(inputFilterArray);
            portaEddyEt.setFilters(inputFilterArray);
            powerSupplyEt.setFilters(inputFilterArray);
            lockEt.setFilters(inputFilterArray);
            bateriaEt.setFilters(inputFilterArray);
            pcaTapaEt.setFilters(inputFilterArray);

            ud6Et.setAdapter(serialSuggestionsAdapter);
            pcaHubEt.setAdapter(serialSuggestionsAdapter);
            pcaEddyEt.setAdapter(serialSuggestionsAdapter);
            portaEddyEt.setAdapter(serialSuggestionsAdapter);
            powerSupplyEt.setAdapter(serialSuggestionsAdapter);
            lockEt.setAdapter(serialSuggestionsAdapter);
            bateriaEt.setAdapter(serialSuggestionsAdapter);
            pcaTapaEt.setAdapter(serialSuggestionsAdapter);

            ud6Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pcaHubEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pcaEddyEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            portaEddyEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            powerSupplyEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            lockEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            bateriaEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pcaTapaEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);

            //Si hay datos... agregar
            if (rootObject.containsKey(UD6_KEY))
            {
                //Obtener datos e inicializar
                Map<String, Object> ud6List =
                        (Map<String, Object>) rootObject.get(UD6_KEY);

                ud6Et.setText((String) ud6List.get("ud6"));
                pcaHubEt.setText((String) ud6List.get("pca_hub"));
                pcaEddyEt.setText((String) ud6List.get("pca_eddy"));
                portaEddyEt.setText((String) ud6List.get("porta_eddy"));
                powerSupplyEt.setText((String) ud6List.get("power_supply"));
                lockEt.setText((String) ud6List.get("lock"));
                bateriaEt.setText((String) ud6List.get("bateria"));
                pcaTapaEt.setText((String) ud6List.get("pca_tapa"));
            }

            submitButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Map<String, Object> ud6List = new HashMap<>();
                    ud6List.put("ud6", ud6Et.getText().toString());
                    ud6List.put("pca_hub", pcaHubEt.getText().toString());
                    ud6List.put("pca_eddy", pcaEddyEt.getText().toString());
                    ud6List.put("porta_eddy", portaEddyEt.getText().toString());
                    ud6List.put("power_supply", powerSupplyEt.getText().toString());
                    ud6List.put("lock", lockEt.getText().toString());
                    ud6List.put("bateria", bateriaEt.getText().toString());
                    ud6List.put("pca_tapa", pcaTapaEt.getText().toString());

                    rootObject.put("ud6", ud6List);
                    serialsListDialog.dismiss();
                }
            });
        }
        else if (group.equals(ENS_LED_MATRIX_1) || group.equals(ENS_LED_MATRIX_2))
        {
            final MaterialAutoCompleteTextView ensambleLedMatrixEt =
                    content.findViewById(R.id.ens_led_mtrx);
            final MaterialAutoCompleteTextView versionEt = content.findViewById(R.id.version);
            final MaterialAutoCompleteTextView ledMatrixE6Et =
                    content.findViewById(R.id.led_matrixe6);
            final MaterialAutoCompleteTextView pcaAmatrixEt =
                    content.findViewById(R.id.pca_amatrix);
            final MaterialAutoCompleteTextView backBoneEt = content.findViewById(R.id.back_bone);

            ensambleLedMatrixEt.setFilters(inputFilterArray);
            versionEt.setFilters(inputFilterArray);
            ledMatrixE6Et.setFilters(inputFilterArray);
            pcaAmatrixEt.setFilters(inputFilterArray);
            backBoneEt.setFilters(inputFilterArray);

            ensambleLedMatrixEt.setAdapter(serialSuggestionsAdapter);
            versionEt.setAdapter(serialSuggestionsAdapter);
            ledMatrixE6Et.setAdapter(serialSuggestionsAdapter);
            pcaAmatrixEt.setAdapter(serialSuggestionsAdapter);
            backBoneEt.setAdapter(serialSuggestionsAdapter);

            ensambleLedMatrixEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            versionEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            ledMatrixE6Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pcaAmatrixEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            backBoneEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);

            if (group.equals(ENS_LED_MATRIX_1))
            {
                //Si hay datos para setear
                if (rootObject.containsKey(ENS_LED_MATRIX_1_KEY))
                {
                    //Obtener datos e inicializar
                    Map<String, Object> ledMatrixEns =
                            (Map<String, Object>) rootObject.get(ENS_LED_MATRIX_1_KEY);

                    ensambleLedMatrixEt.setText((String) ledMatrixEns.get("ens_led_matrix"));
                    versionEt.setText((String) ledMatrixEns.get("version"));
                    ledMatrixE6Et.setText((String) ledMatrixEns.get("led_matrixe6"));
                    pcaAmatrixEt.setText((String) ledMatrixEns.get("pca_amatrix"));
                    backBoneEt.setText((String) ledMatrixEns.get("back_bone"));
                }

                submitButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Map<String, Object> ledMatrixEns = new HashMap<>();
                        ledMatrixEns.put("ens_led_matrix",
                                ensambleLedMatrixEt.getText().toString());
                        ledMatrixEns.put("version", versionEt.getText().toString());
                        ledMatrixEns.put("led_matrixe6", ledMatrixE6Et.getText().toString());
                        ledMatrixEns.put("pca_amatrix", pcaAmatrixEt.getText().toString());
                        ledMatrixEns.put("back_bone", backBoneEt.getText().toString());

                        rootObject.put(ENS_LED_MATRIX_1_KEY, ledMatrixEns);
                        serialsListDialog.dismiss();
                    }
                });
            }
            else
            {
                //Ocultar backBoneEt para el grupo 2
                backBoneEt.setVisibility(View.GONE);

                //Si hay datos para setear
                if (rootObject.containsKey(ENS_LED_MATRIX_2_KEY))
                {
                    //Obtener datos e inicializar
                    Map<String, Object> ledMatrixEns =
                            (Map<String, Object>) rootObject.get(ENS_LED_MATRIX_2_KEY);

                    ensambleLedMatrixEt.setText((String) ledMatrixEns.get("ens_led_matrix"));
                    versionEt.setText((String) ledMatrixEns.get("version"));
                    ledMatrixE6Et.setText((String) ledMatrixEns.get("led_matrixe6"));
                    pcaAmatrixEt.setText((String) ledMatrixEns.get("pca_amatrix"));
                }

                submitButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Map<String, Object> ledMatrixEns = new HashMap<>();
                        ledMatrixEns.put("ens_led_matrix",
                                ensambleLedMatrixEt.getText().toString());
                        ledMatrixEns.put("version", versionEt.getText().toString());
                        ledMatrixEns.put("led_matrixe6", ledMatrixE6Et.getText().toString());
                        ledMatrixEns.put("pca_amatrix", pcaAmatrixEt.getText().toString());

                        rootObject.put(ENS_LED_MATRIX_2_KEY, ledMatrixEns);
                        serialsListDialog.dismiss();
                    }
                });
            }
        }
        else if (group.equals(SANDWICH_L1) || group.equals(SANDWICH_L2))
        {
            final MaterialAutoCompleteTextView sandwichEt = content.findViewById(R.id.sandwich);
            final MaterialAutoCompleteTextView versionEt = content.findViewById(R.id.version);
            final MaterialAutoCompleteTextView pcaMicro6Et = content.findViewById(R.id.pca_micro);
            final MaterialAutoCompleteTextView pcaTelumEt = content.findViewById(R.id.pca_telum);
            final MaterialAutoCompleteTextView pcaTelumledEt =
                    content.findViewById(R.id.pca_telumled);
            final MaterialAutoCompleteTextView lectorNfcEt = content.findViewById(R.id.lector_nfc);
            final MaterialAutoCompleteTextView lcdDisplayEt =
                    content.findViewById(R.id.lcd_display);
            final MaterialAutoCompleteTextView imagenPantallasEt =
                    content.findViewById(R.id.imagen_pantallas);

            sandwichEt.setFilters(inputFilterArray);
            versionEt.setFilters(inputFilterArray);
            pcaMicro6Et.setFilters(inputFilterArray);
            pcaTelumEt.setFilters(inputFilterArray);
            pcaTelumledEt.setFilters(inputFilterArray);
            lectorNfcEt.setFilters(inputFilterArray);
            lcdDisplayEt.setFilters(inputFilterArray);
            imagenPantallasEt.setFilters(inputFilterArray);

            sandwichEt.setAdapter(serialSuggestionsAdapter);
            versionEt.setAdapter(serialSuggestionsAdapter);
            pcaMicro6Et.setAdapter(serialSuggestionsAdapter);
            pcaTelumEt.setAdapter(serialSuggestionsAdapter);
            pcaTelumledEt.setAdapter(serialSuggestionsAdapter);
            lectorNfcEt.setAdapter(serialSuggestionsAdapter);
            lcdDisplayEt.setAdapter(serialSuggestionsAdapter);
            imagenPantallasEt.setAdapter(serialSuggestionsAdapter);

            sandwichEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            versionEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pcaMicro6Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pcaTelumEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pcaTelumledEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            lectorNfcEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            lcdDisplayEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            imagenPantallasEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);

            if (group.equals(SANDWICH_L1))
            {
                //Si hay datos para setear...
                if (rootObject.containsKey(SANDWICH_L1_KEY))
                {
                    //Obtener datos e inicializar
                    Map<String, Object> sandwichList =
                            (Map<String, Object>) rootObject.get(SANDWICH_L1_KEY);

                    sandwichEt.setText((String) sandwichList.get("sandwich"));
                    versionEt.setText((String) sandwichList.get("version"));
                    pcaMicro6Et.setText((String) sandwichList.get("pca_micro6_et"));
                    pcaTelumEt.setText((String) sandwichList.get("pca_telum"));
                    pcaTelumledEt.setText((String) sandwichList.get("pca_telum_led"));
                    lectorNfcEt.setText((String) sandwichList.get("lector_nfc"));
                    lcdDisplayEt.setText((String) sandwichList.get("lcd_display"));
                    imagenPantallasEt.setText((String) sandwichList.get("imagen_pantallas"));
                }

                submitButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Map<String, Object> sandwichList = new HashMap<>();

                        sandwichList.put("sandwich", sandwichEt.getText().toString());
                        sandwichList.put("version", versionEt.getText().toString());
                        sandwichList.put("pca_micro6_et", pcaMicro6Et.getText().toString());
                        sandwichList.put("pca_telum", pcaTelumEt.getText().toString());
                        sandwichList.put("pca_telum_led", pcaTelumledEt.getText().toString());
                        sandwichList.put("lector_nfc", lectorNfcEt.getText().toString());
                        sandwichList.put("lcd_display", lcdDisplayEt.getText().toString());
                        sandwichList.put("imagen_pantallas",
                                imagenPantallasEt.getText().toString());

                        rootObject.put(SANDWICH_L1_KEY, sandwichList);
                        serialsListDialog.dismiss();
                    }
                });
            }
            else
            {
                //Si hay datos para setear...
                if (rootObject.containsKey(SANDWICH_L2_KEY))
                {
                    //Obtener datos e inicializar
                    Map<String, Object> sandwichList =
                            (Map<String, Object>) rootObject.get(SANDWICH_L2_KEY);

                    sandwichEt.setText((String) sandwichList.get("sandwich"));
                    versionEt.setText((String) sandwichList.get("version"));
                    pcaMicro6Et.setText((String) sandwichList.get("pca_micro6_et"));
                    pcaTelumEt.setText((String) sandwichList.get("pca_telum"));
                    pcaTelumledEt.setText((String) sandwichList.get("pca_telum_led"));
                    lectorNfcEt.setText((String) sandwichList.get("lector_nfc"));
                    lcdDisplayEt.setText((String) sandwichList.get("lcd_display"));
                    imagenPantallasEt.setText((String) sandwichList.get("imagen_pantallas"));
                }

                submitButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Map<String, Object> sandwichList = new HashMap<>();

                        sandwichList.put("sandwich", sandwichEt.getText().toString());
                        sandwichList.put("version", versionEt.getText().toString());
                        sandwichList.put("pca_micro6_et", pcaMicro6Et.getText().toString());
                        sandwichList.put("pca_telum", pcaTelumEt.getText().toString());
                        sandwichList.put("pca_telum_led", pcaTelumledEt.getText().toString());
                        sandwichList.put("lector_nfc", lectorNfcEt.getText().toString());
                        sandwichList.put("lcd_display", lcdDisplayEt.getText().toString());
                        sandwichList.put("imagen_pantallas",
                                imagenPantallasEt.getText().toString());

                        rootObject.put(SANDWICH_L2_KEY, sandwichList);
                        serialsListDialog.dismiss();
                    }
                });
            }
        }
        else if (group.equals(PULSADOR) || group.equals(MEDIDOR))
        {
            final MaterialAutoCompleteTextView verisonEt = content.findViewById(R.id.version);
            final MaterialAutoCompleteTextView dl1Et = content.findViewById(R.id.dl1);
            final MaterialAutoCompleteTextView ml1Et = content.findViewById(R.id.ml1);
            final MaterialAutoCompleteTextView pl1Et = content.findViewById(R.id.pl1);
            final MaterialAutoCompleteTextView dl2Et = content.findViewById(R.id.dl2);
            final MaterialAutoCompleteTextView ml2Et = content.findViewById(R.id.ml2);
            final MaterialAutoCompleteTextView pl2Et = content.findViewById(R.id.pl2);

            verisonEt.setFilters(inputFilterArray);
            dl1Et.setFilters(inputFilterArray);
            ml1Et.setFilters(inputFilterArray);
            pl1Et.setFilters(inputFilterArray);
            dl2Et.setFilters(inputFilterArray);
            ml2Et.setFilters(inputFilterArray);
            pl2Et.setFilters(inputFilterArray);

            verisonEt.setAdapter(serialSuggestionsAdapter);
            dl1Et.setAdapter(serialSuggestionsAdapter);
            ml1Et.setAdapter(serialSuggestionsAdapter);
            pl1Et.setAdapter(serialSuggestionsAdapter);
            dl2Et.setAdapter(serialSuggestionsAdapter);
            ml2Et.setAdapter(serialSuggestionsAdapter);
            pl2Et.setAdapter(serialSuggestionsAdapter);

            verisonEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            dl1Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            ml1Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pl1Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            dl2Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            ml2Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            pl2Et.setThreshold(SERIALS_SUGGESTION_THRESHOLD);

            if (group.equals(PULSADOR))
            {
                //Validar si hay datos por mostrar
                if (rootObject.containsKey(PULSADOR_KEY))
                {
                    //Obtener lista
                    Map<String, Object> pulsadorList =
                            (Map<String, Object>) rootObject.get(PULSADOR_KEY);

                    verisonEt.setText((String) pulsadorList.get("version"));
                    dl1Et.setText((String) pulsadorList.get("dl1"));
                    ml1Et.setText((String) pulsadorList.get("ml1"));
                    pl1Et.setText((String) pulsadorList.get("pl1"));
                    dl2Et.setText((String) pulsadorList.get("dl2"));
                    ml2Et.setText((String) pulsadorList.get("ml2"));
                    pl2Et.setText((String) pulsadorList.get("pl2"));
                }

                submitButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Map<String, Object> pulsadorList = new HashMap<>();
                        pulsadorList.put("dl1", dl1Et.getText().toString());
                        pulsadorList.put("ml1", ml1Et.getText().toString());
                        pulsadorList.put("pl1", pl1Et.getText().toString());
                        pulsadorList.put("dl2", dl2Et.getText().toString());
                        pulsadorList.put("ml2", ml2Et.getText().toString());
                        pulsadorList.put("pl2", pl2Et.getText().toString());
                        pulsadorList.put("version", verisonEt.getText().toString());

                        rootObject.put(PULSADOR_KEY, pulsadorList);
                        serialsListDialog.dismiss();
                    }
                });
            }
            else
            {
                //Validar si hay datos por mostrar
                if (rootObject.containsKey(MEDIDOR_KEY))
                {
                    //Obtener lista
                    Map<String, Object> medidorList =
                            (Map<String, Object>) rootObject.get(MEDIDOR_KEY);

                    verisonEt.setText((String) medidorList.get("version"));
                    dl1Et.setText((String) medidorList.get("dl1"));
                    ml1Et.setText((String) medidorList.get("ml1"));
                    pl1Et.setText((String) medidorList.get("pl1"));
                    dl2Et.setText((String) medidorList.get("dl2"));
                    ml2Et.setText((String) medidorList.get("ml2"));
                    pl2Et.setText((String) medidorList.get("pl2"));
                }

                submitButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Map<String, Object> medidorList = new HashMap<>();
                        medidorList.put("dl1", dl1Et.getText().toString());
                        medidorList.put("ml1", ml1Et.getText().toString());
                        medidorList.put("pl1", pl1Et.getText().toString());
                        medidorList.put("dl2", dl2Et.getText().toString());
                        medidorList.put("ml2", ml2Et.getText().toString());
                        medidorList.put("pl2", pl2Et.getText().toString());
                        medidorList.put("version", verisonEt.getText().toString());

                        rootObject.put(MEDIDOR_KEY, medidorList);
                        serialsListDialog.dismiss();
                    }
                });
            }
        }
        else if (group.equals(ACCESORIOS))
        {
            final MaterialAutoCompleteTextView printerEt = content.findViewById(R.id.impresora);
            final MaterialAutoCompleteTextView lectorM = content.findViewById(R.id.lector);
            final MaterialAutoCompleteTextView hammerTag = content.findViewById(R.id.hammer_tag);

            printerEt.setFilters(inputFilterArray);
            lectorM.setFilters(inputFilterArray);
            hammerTag.setFilters(inputFilterArray);

            printerEt.setAdapter(serialSuggestionsAdapter);
            lectorM.setAdapter(serialSuggestionsAdapter);
            hammerTag.setAdapter(serialSuggestionsAdapter);

            printerEt.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            lectorM.setThreshold(SERIALS_SUGGESTION_THRESHOLD);
            hammerTag.setThreshold(SERIALS_SUGGESTION_THRESHOLD);

            //Validar si hay datos por mostrar
            if (rootObject.containsKey(ACCESORIOS_KEY))
            {
                //Obtener lista
                Map<String, Object> accesoriosList =
                        (Map<String, Object>) rootObject.get(ACCESORIOS_KEY);

                printerEt.setText((String) accesoriosList.get("impresora"));
                lectorM.setText((String) accesoriosList.get("lector_m"));
                hammerTag.setText((String) accesoriosList.get("hammer_tag"));
            }

            submitButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //Crear lista
                    Map<String, Object> accesoriosList = new HashMap<>();
                    accesoriosList.put("impresora", printerEt.getText().toString());
                    accesoriosList.put("lector_m", lectorM.getText().toString());
                    accesoriosList.put("hammer_tag", hammerTag.getText().toString());

                    rootObject.put(ACCESORIOS_KEY, accesoriosList);
                    serialsListDialog.dismiss();
                }
            });
        }

        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Validar que el diálogo no sea nulo
                if (serialsListDialog != null)
                {
                    serialsListDialog.dismiss();
                }
            }
        });
    }

    private void showTestsDialog()
    {
        //Crear builder para diálogo
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //Obtener layout de pruebas
        View content = getLayoutInflater().inflate(R.layout.elec_kits_testing, null);

        //Definir view
        builder.setView(content);

        //Obtener vistas
        final CheckBox ledMatrixCb = content.findViewById(R.id.led_matrix_cb);
        final CheckBox pcaTapaCb = content.findViewById(R.id.pca_tapa_cb);
        final CheckBox errorSelloCb = content.findViewById(R.id.error_sello_cb);
        final CheckBox corteTurnoCb = content.findViewById(R.id.corte_turno_cb);
        final CheckBox impresoraCb = content.findViewById(R.id.impresora_cb);
        final CheckBox valvSkinnerCb = content.findViewById(R.id.valv_skinner_cb);
        final CheckBox pcaLockCb = content.findViewById(R.id.pca_lock_cb);
        final CheckBox enlazadoCb = content.findViewById(R.id.enlazado_cb);
        final CheckBox letrerosCb = content.findViewById(R.id.letreros_led_matrix_cb);
        final CheckBox hammerTagCb = content.findViewById(R.id.hammer_tag_cb);
        final MaterialEditText voltajeBateriaEt = content.findViewById(R.id.voltaje_bateria_et);
        final MaterialEditText minutosBateriaEt = content.findViewById(R.id.minutos_bateria_et);
        final MaterialEditText segundosBateriaEt = content.findViewById(R.id.segundos_bateria_et);
        Button returnButton = content.findViewById(R.id.return_button);
        Button saveButton = content.findViewById(R.id.save_button);

        //Validar si hay datos
        if (rootObject.containsKey(PRUEBAS_KEY))
        {
            //Obtener la información
            Map<String, Object> tests = (HashMap) rootObject.get(PRUEBAS_KEY);

            //Setear la información
            ledMatrixCb.setChecked((boolean) tests.get("led_matrix"));
            pcaTapaCb.setChecked((boolean) tests.get("pca_tapa"));
            errorSelloCb.setChecked((boolean) tests.get("error_sello"));
            corteTurnoCb.setChecked((boolean) tests.get("corte_turno_l1"));
            impresoraCb.setChecked((boolean) tests.get("impresora"));
            valvSkinnerCb.setChecked((boolean) tests.get("valvula_skinner"));
            pcaLockCb.setChecked((boolean) tests.get("pca_lock"));
            enlazadoCb.setChecked((boolean) tests.get("enlazado"));
            letrerosCb.setChecked((boolean) tests.get("letreros"));
            hammerTagCb.setChecked((boolean) tests.get("hammer_tag_lector"));

            voltajeBateriaEt.setText((String) tests.get("voltaje_bateria"));
            minutosBateriaEt.setText((String) tests.get("minutos_bateria"));
            segundosBateriaEt.setText((String) tests.get("segundos_bateria"));
        }

        saveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Recolectar datos de diálogo
                Map<String, Object> tests = new HashMap<>();
                tests.put("led_matrix", ledMatrixCb.isChecked());
                tests.put("pca_tapa", pcaTapaCb.isChecked());
                tests.put("error_sello", errorSelloCb.isChecked());
                tests.put("corte_turno_l1", corteTurnoCb.isChecked());
                tests.put("impresora", impresoraCb.isChecked());
                tests.put("valvula_skinner", valvSkinnerCb.isChecked());
                tests.put("pca_lock", pcaLockCb.isChecked());
                tests.put("enlazado", enlazadoCb.isChecked());
                tests.put("letreros", letrerosCb.isChecked());
                tests.put("hammer_tag_lector", hammerTagCb.isChecked());

                tests.put("voltaje_bateria", voltajeBateriaEt.getText().toString());
                tests.put("minutos_bateria", minutosBateriaEt.getText().toString());
                tests.put("segundos_bateria", segundosBateriaEt.getText().toString());

                //Guardar información de diálogo
                rootObject.put(PRUEBAS_KEY, tests);

                //Cerrar diálogo
                testsDialog.dismiss();
            }
        });

        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Cerrar diálogo
                testsDialog.dismiss();
            }
        });

        //Mostrar diálogo
        testsDialog = builder.create();
        testsDialog.show();
    }

    private void saveElecKit()
    {
        if (!validateElecKit())
        {
            Toast.makeText(this, "Datos no válidos", Toast.LENGTH_SHORT).show();
            return;
        }

        //Ocultar contenido
        content.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        //Agregar los datos faltantes
        //Los datos que se ingresan por diálogos ya estan almacenados en "rootObject"

        //Obtener referencia de número de kit
        MaterialEditText kitNumberEt = findViewById(R.id.e_kit_number);
        kitNumber = kitNumberEt.getText().toString();

        //Agregar número de kit a objeto raíz
        rootObject.put("numero_kit", kitNumber);

        //Obtener orden interna de proyecto seleccionado
        rootObject.put("proyecto", selectedProjectId);

        //Relacionar kit con dispensario
        assignKitToDispenser();

        //Obtener id de dispensario
        rootObject.put("dispensario", selectedDispId);

        //Obtener id de versión de bOS
        rootObject.put("version_bos", selectedBosVersionId);

        //Obtener MAC Address
        rootObject.put("mac_add", macAddressEditText.getText().toString());

        //Obtener fechas de quemado
        MaterialEditText inicioQuemadoEt = findViewById(R.id.inicio_quemado_et);
        rootObject.put("inicio_quemado", inicioQuemadoEt.getText().toString());

        MaterialEditText finQuemadoEt = findViewById(R.id.fin_quemado_et);
        rootObject.put("fin_quemado", finQuemadoEt.getText().toString());

        //Obtener observaciones
        MaterialEditText obsv = findViewById(R.id.observaciones_et);
        rootObject.put("observaciones", obsv.getText().toString());

        //Agregar epoch
        rootObject.put("epoch", new Date().getTime());

        //Definir mensaje de resupuesta
        final String wordSucces, wordFail;
        if (addOrModify)
        {
            wordSucces = "agregado";
            wordFail = "agregar";
        }
        else
        {
            wordSucces = "modificado";
            wordFail = "modificar";
        }

        //Definir listeners
        OnSuccessListener<Void> successListener = new OnSuccessListener<Void>()
        {
            @Override
            public void onSuccess(Void aVoid)
            {
                Toast.makeText(ElectronicKitForm.this, "Kit " + wordSucces + " correctamente",
                        Toast.LENGTH_SHORT).show();

                content.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        };

        OnFailureListener failureListener = new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                Toast.makeText(ElectronicKitForm.this, "Error al " + wordFail + " kit",
                        Toast.LENGTH_SHORT).show();

                content.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        };

        if (addOrModify)
        {
            //Definir usuario
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String uid = user.getUid();

            //Definir usuario
            rootObject.put("id_usuario", uid);

            //Agregar proyecto
            db.collection("kits_electronicos").document(kitNumber).set(rootObject).addOnSuccessListener(successListener).addOnFailureListener(failureListener);
        }
        else
        {
            //Modificar proyecto
            db.collection("kits_electronicos").document(kitNumber).update(rootObject).addOnSuccessListener(successListener).addOnFailureListener(failureListener);
        }
    }

    private void assignKitToDispenser()
    {
        //Validar id de dispensario
        if (selectedDispId.isEmpty()) return;

        db.collection("dispensarios").document(selectedDispId).update("kitElectronico",
                kitNumber).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if (task.isSuccessful() && task.getResult() != null)
                {
                    Log.d(TAG, "Dispensario actualizado correctamente");
                }
                else
                {
                    Log.d(TAG, "Error al actualizar dispensario");
                }
            }
        });
    }

    private boolean validateElecKit()
    {
        boolean isValid;

        //Obtener referencia de número de kit
        MaterialEditText kitNumber = findViewById(R.id.e_kit_number);

        //Crear validador de campos vacíos
        METValidator emptyValidator = new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        };

        //Validar campos
        isValid = kitNumber.validateWith(emptyValidator);

        return isValid;
    }

    private void getElecKit()
    {
        //Obtener referencia de número de kit
        MaterialEditText kitNumberEt = findViewById(R.id.e_kit_number);
        kitNumberEt.setText(kitNumber);

        //Agregar número de kit a objeto raíz
        rootObject.put("numero_kit", kitNumber);

        Log.d("ElecKitForm", "Definiendo valores de id's");

        //Obtener orden interna de proyecto seleccionado
        selectedProjectId = (String) rootObject.get("proyecto");
        selectedDispId = (String) rootObject.get("dispensario");
        selectedBosVersionId = (String) rootObject.get("version_bos");

        //Obtener MAC Address
        macAddressEditText = findViewById(R.id.e_kit_mac_address);
        macAddressEditText.setText((String) rootObject.get("mac_add"));

        //Obtener fechas de quemado
        MaterialEditText inicioQuemadoEt = findViewById(R.id.inicio_quemado_et);
        inicioQuemadoEt.setText((String) rootObject.get("inicio_quemado"));

        MaterialEditText finQuemadoEt = findViewById(R.id.fin_quemado_et);
        finQuemadoEt.setText((String) rootObject.get("fin_quemado"));

        //Calcular tiempo de quemado
        calculateTestTime();

        //Obtener observaciones
        MaterialEditText obsv = findViewById(R.id.observaciones_et);
        obsv.setText((String) rootObject.get("observaciones"));
    }

    static ArrayList<String> getElecKitElements()
    {
        ArrayList<String> elements = new ArrayList<>();
        elements.add(UD6);
        elements.add(ENS_LED_MATRIX_1);
        elements.add(ENS_LED_MATRIX_2);
        elements.add(SANDWICH_L1);
        elements.add(SANDWICH_L2);
        elements.add(PULSADOR);
        elements.add(MEDIDOR);
        elements.add(ACCESORIOS);

        return elements;
    }

    private void calculateTestTime()
    {
        MaterialEditText start = findViewById(R.id.inicio_quemado_et);
        MaterialEditText end = findViewById(R.id.fin_quemado_et);

        if (!start.getText().toString().isEmpty() && !end.getText().toString().isEmpty())
        {
            Date startDate = Constants.dateStringToObject(start.getText().toString());
            Date endDate = Constants.dateStringToObject(end.getText().toString());

            if (startDate != null && endDate != null)
            {
                //Obtener la diferencia de las fechas en milisegundos
                String diff = Constants.getDatesTextDifference(endDate, startDate);

                //Escribir en textView
                MaterialEditText diffText = findViewById(R.id.tiempo_quemado_tv);
                diffText.setText(diff);
                diffText.setVisibility(View.VISIBLE);
            }
        }
    }
}
