package com.chipred.produccionspx.reports;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chipred.produccionspx.BuildConfig;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.DispModel;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.adapters.ProjectDispensersAdapter;
import com.chipred.produccionspx.pdf.ProjectsReportGenerator;
import com.chipred.produccionspx.projects.ProjectDispensers;
import com.chipred.produccionspx.projects.ProjectOverview;
import com.chipred.produccionspx.utils.DateUtils;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;


public class ProjectChartsActivity extends AppCompatActivity
{
    // Constantes
    private static final String TAG = ProjectChartsActivity.class.getSimpleName();
    private static final int REQUEST_STORAGE_PERMISSION = 1;

    private final SimpleDateFormat monthYearFormat = new SimpleDateFormat("LLL yyyy",
            Locale.getDefault());

    // Variables
    private String selectedMonth;

    // Colecciones
    private ArrayList<ProjectDispensers> selectedMonthProjects;
    private final ArrayList<String> monthsChartValues = new ArrayList<>();
    private final ArrayList<YearMonthProjectsCollection> projectMonthCollections = new ArrayList<>();
    private final Map<String, YearMonthProjectsCollection> monthProjectsMap = new HashMap<>();
    private final Map<String, ProjectOverview> projectsMap = new HashMap<>();

    // Objetos
    private Date startDate;
    private Date endDate;
    private MenuItem generatePdfMenuItem;
    private ProjectDispensersAdapter projectDispensersAdapter;

    // Instancia de bases de datos
    private final CollectionReference projectsCollection = FirebaseFirestore.getInstance().collection(
            Constants.PROJECTS_COLLECTION);
    private final CollectionReference dispsCollection = FirebaseFirestore.getInstance().collection(
            Constants.DISPENSERS_COLLECTION);

    // Views
    private HorizontalBarChart projectsCountChart;
    private LinearLayout monthProjectsLayout;
    private RecyclerView projectsList;
    private ProgressDialog progressDialog;
    private ProgressBar progressBar;
    private ProgressBar projectsListProgress;
    private TextView selectedMonthTv;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTitle("Proyectos terminados");
        setContentView(R.layout.activity_charts);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        monthProjectsLayout = findViewById(R.id.chart_element_details_layout);
        projectsCountChart = findViewById(R.id.horizontal_bar_chart);
        progressBar = findViewById(R.id.progress_bar);
        projectsListProgress = findViewById(R.id.projects_list_progress);
        projectsList = findViewById(R.id.projects_list);
        selectedMonthTv = findViewById(R.id.selected_chart_element_tv);

        // Obtener proyectos
        setDates();
        getFinishedDispensers();
    }

    private void setDates()
    {
        startDate = DateUtils.getFirstDateOfTheMonth();
        endDate = DateUtils.getLastDateOfTheMonth();

        // Restar 3 meses a la fecha actual
        startDate = DateUtils.substractMonths(startDate, 3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.project_month_menu, menu);
        generatePdfMenuItem = menu.findItem(R.id.pdf);
        generatePdfMenuItem.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        else if (item.getItemId() == R.id.pdf)
        {
            checkPermissions();
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)
            {
                Log.v(TAG, "Permission is granted");
                generatePdf();
            }
            else
            {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);
            }
        }
        else
        {
            Log.v(TAG, "Permission is granted");
            generatePdf();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE_PERMISSION)
        {
            // Obtener primer resultado
            if (grantResults.length > 0)
            {
                if (grantResults[0] == RESULT_OK)
                {
                    Toast.makeText(this, "Permiso concedido", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, "Permiso no concedido", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(this, "Permiso no concedido", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getFinishedDispensers()
    {
        dispsCollection.orderBy("finishedDate")
                .whereGreaterThanOrEqualTo("finishedDate", startDate.getTime())
                .whereLessThan("finishedDate", endDate.getTime())
                .get().addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                if (task.getResult() != null)
                {
                    for (DocumentSnapshot document : task.getResult().getDocuments())
                    {
                        DispModel dispModel = document.toObject(DispModel.class);
                        if (dispModel != null && dispModel.isFinished())
                        {
                            dispModel.setId(document.getId());

                            // Agregar al mes corres
                            addDispenserToMonthCollection(dispModel);
                        }
                    }
                }

                Log.d(TAG, "fetchedDispensers");

                // Validar si hubo resultados
                if (!monthProjectsMap.isEmpty())
                {
                    projectsHashMapToList();
                    setChartData();

                    progressBar.setVisibility(View.GONE);
                    projectsCountChart.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                Log.e(TAG, "Error al obtener máquinas terminadas");
                Toast.makeText(this, "Error al obtener máquinas terminadas", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setChartData()
    {
        List<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < projectMonthCollections.size(); i++)
        {
            entries.add(new BarEntry(i, projectMonthCollections.get(i).getDispCount()));
        }

        CustomBarDataSet barDataSet = new CustomBarDataSet(entries, "Dispensarios terminados");
        barDataSet.setColors(ContextCompat.getColor(this, android.R.color.holo_green_dark),
                ContextCompat.getColor(this, android.R.color.darker_gray));
        barDataSet.setValueTextSize(12);
        barDataSet.setFormSize(20);
        barDataSet.setValueFormatter(new ValueFormatter()
        {
            @Override
            public String getFormattedValue(float value)
            {
                return "" + (int) value;
            }
        });

        BarData data = new BarData(barDataSet);

        XAxis xAxis = projectsCountChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(monthsChartValues));
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setTextSize(12f);

        Description description = new Description();
        description.setText("");
        projectsCountChart.setDescription(description);
        projectsCountChart.getAxisRight().setEnabled(false);
        projectsCountChart.setData(data);
        projectsCountChart.setScaleXEnabled(false);
        projectsCountChart.animateY(500);
        projectsCountChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener()
        {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {
                onMonthSelected((int) e.getX());
                monthProjectsLayout.setVisibility(View.VISIBLE);
                generatePdfMenuItem.setVisible(true);
            }

            @Override
            public void onNothingSelected()
            {
                monthProjectsLayout.setVisibility(View.GONE);
                generatePdfMenuItem.setVisible(false);
            }
        });
    }

    private void addDispenserToMonthCollection(DispModel dispModel)
    {
        // Obtener fecha
        Date finishedDateObj = new Date(dispModel.getFinishedDate());

        // Crear instancia calendario a partir de la fecha obtenida
        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(finishedDateObj);

        // Obtener año y mes
        int year = endDateCalendar.get(Calendar.YEAR);
        int month = endDateCalendar.get(Calendar.MONTH);

        // Crear nombre de mes
        String yearMonthName = monthYearFormat.format(finishedDateObj);

        // Obtener instancia para almacenar referencia de proyecto
        YearMonthProjectsCollection thisMonth;
        if (monthProjectsMap.containsKey(yearMonthName))
        {
            thisMonth = monthProjectsMap.get(yearMonthName);
        }
        else
        {
            // Crear instancia para almacenar
            thisMonth = new YearMonthProjectsCollection();
            thisMonth.setMonth(month);
            thisMonth.setYear(year);
            thisMonth.setName(yearMonthName);

            // Crear fechas de inicio y fin de mes
            Calendar thisMonthLimitsCalendar = Calendar.getInstance();
            thisMonthLimitsCalendar.setTime(endDateCalendar.getTime());

            // Definir inicio
            thisMonthLimitsCalendar.set(Calendar.DAY_OF_MONTH,
                    thisMonthLimitsCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            thisMonth.setStartTime(thisMonthLimitsCalendar.getTime().getTime());

            // Definir fin
            thisMonthLimitsCalendar.set(Calendar.DAY_OF_MONTH,
                    thisMonthLimitsCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            thisMonth.setStartTime(thisMonthLimitsCalendar.getTime().getTime());

            monthProjectsMap.put(yearMonthName, thisMonth);
        }

        if (thisMonth != null)
        {
            String order = dispModel.getOrdenInterna();
            ProjectDispensers project = thisMonth.projects.get(order);

            if (project == null)
            {
                // No existe el proyecto, crear instancia
                project = new ProjectDispensers();
                project.setOrderNumber(order);

                // Crear proyecto a colección del mes actual
                thisMonth.projects.put(order, project);
            }

            // Agregar dispensario
            project.addDispenser(dispModel);

            // Agregar proyecto al grupo obtenido
            thisMonth.addProjectDispensers(project);
        }
        else
        {
            Toast.makeText(this, "Error al agregar proyecto", Toast.LENGTH_SHORT).show();
        }
    }

    private void projectsHashMapToList()
    {
        for (String key : monthProjectsMap.keySet())
        {
            // Obtener lista que corresponde al actual "key"
            projectMonthCollections.add(monthProjectsMap.get(key));
        }

        // Ordenar lista
        Collections.sort(projectMonthCollections,
                (yearMonthProjectsCollection, t1) -> Long.compare(yearMonthProjectsCollection.getStartTime(),
                        t1.getStartTime()));

        // Obtener nombres
        for (YearMonthProjectsCollection monthProjectsCollection : projectMonthCollections)
        {
            monthsChartValues.add(monthProjectsCollection.name);
        }
    }

    private ArrayList<ProjectDispensers> selectedProjectsHashMapToList(Map<String,
            ProjectDispensers> projects)
    {
        ArrayList<ProjectDispensers> projectDispensers = new ArrayList<>();

        for (String key : projects.keySet())
        {
            // Obtener lista que corresponde al actual "key"
            projectDispensers.add(projects.get(key));
        }

        // Ordenar lista
        Collections.sort(projectDispensers, (projectDispensers1, t1) ->
                {
                    // Obtener numero de serie de primer máquina de cada proyecto
                    int serial1 =
                            Integer.parseInt(projectDispensers1.getDispModels().get(0).getSerialNumber());
                    int serial2 = Integer.parseInt(t1.getDispModels().get(0).getSerialNumber());

                    return Integer.compare(serial1, serial2);
                }
        );

        return projectDispensers;
    }

    private void onMonthSelected(int x)
    {
        projectsList.setVisibility(View.GONE);
        projectsListProgress.setVisibility(View.VISIBLE);

        selectedMonth = projectMonthCollections.get(x).name;
        selectedMonthTv.setText(String.format(
                Locale.getDefault(),
                "%s - %d",
                selectedMonth,
                projectMonthCollections.get(x).getDispCount()));

        getProjectsValues(projectMonthCollections.get(x).projects);
    }

    @SuppressWarnings({"unchecked", "null"})
    private void getProjectsValues(Map<String, ProjectDispensers> projectDispensers)
    {
        List<Task<DocumentSnapshot>> tasks = new ArrayList<>();

        for (String projectId : projectDispensers.keySet())
        {
            if (projectsMap.containsKey(projectId))
            {
                ProjectOverview projectOverview = projectsMap.get(projectId);
                Objects.requireNonNull(projectDispensers.get(projectId)).updateProjectData(projectOverview);
            }
            else
            {
                tasks.add(projectsCollection.document(projectId).get());
            }
        }

        // Si hay tareas que realizar, ejecutar query
        if (!tasks.isEmpty())
        {
            projectsList.setVisibility(View.GONE);
            projectsListProgress.setVisibility(View.VISIBLE);

            Tasks.whenAllComplete(tasks).addOnCompleteListener(task ->
            {
                if (task.isSuccessful() && task.getResult() != null)
                {
                    // Obtener lista de resultados de dispenasrios
                    for (Task<?> mTask : task.getResult())
                    {
                        DocumentSnapshot dispDocument =
                                ((Task<DocumentSnapshot>) mTask).getResult();

                        if (dispDocument != null)
                        {
                            // Crear instancia
                            ProjectOverview projectOverview =
                                    new ProjectOverview(dispDocument.getId());

                            HashMap<String, Object> generalData =
                                    (HashMap<String, Object>) dispDocument.get(
                                            "datos_generales");

                            int dispsCount =
                                    ((List<Object>) dispDocument.get("dispensarios")).size();

                            projectOverview.setClient((String) generalData.get("estacion"));
                            projectOverview.setStation((String) generalData.get("cliente"));
                            projectOverview.setAllDispsCount(dispsCount);

                            // Actualizar objeto correspondiente
                            Objects.requireNonNull(projectDispensers.get(dispDocument.getId())).updateProjectData(projectOverview);

                            // Agregar datos de proyecto al mapa
                            projectsMap.put(dispDocument.getId(), projectOverview);
                        }
                        else
                        {
                            Log.e(TAG, "Error al convertir documento a objeto");
                        }
                    }
                }
                else
                {
                    Toast.makeText(ProjectChartsActivity.this, "Error al obtener " +
                            "dispensarios de proyectos ", Toast.LENGTH_SHORT).show();
                }

                showProjectsList(projectDispensers);
            });
        }
        else
        {
            showProjectsList(projectDispensers);
        }
    }

    private void showProjectsList(Map<String, ProjectDispensers> projectDispensers)
    {
        // Definir colección de proyectos del mes seleccionado
        selectedMonthProjects = selectedProjectsHashMapToList(projectDispensers);

        // Mostrar lista de proyectos
        if (projectDispensersAdapter == null)
        {
            projectDispensersAdapter = new ProjectDispensersAdapter(this, selectedMonthProjects);
            projectsList.setLayoutManager(new LinearLayoutManager(this));
            projectsList.setAdapter(projectDispensersAdapter);
        }
        else
        {
            projectDispensersAdapter.setProjectDispensersList(selectedMonthProjects);
            projectDispensersAdapter.notifyDataSetChanged();
        }

        projectsList.setVisibility(View.VISIBLE);
        projectsListProgress.setVisibility(View.GONE);
    }

    private void generatePdf()
    {
        progressDialog = ProgressDialog.show(this, "", "Generando pdf...");
        progressDialog.setCancelable(false);

        ProjectsReportGenerator projectsReportGenerator = new ProjectsReportGenerator(
                new Handler(message ->
                {
                    String filePath = (String) message.obj;

                    progressDialog.dismiss();

                    if (!filePath.isEmpty())
                    {
                        exportPdf(new File(filePath));
                    }
                    else
                    {
                        Toast.makeText(this, "Error al generar PDF", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Error al obtener path de archivo");
                    }

                    return false;
                })
        );

        projectsReportGenerator.generateMonthDispensersReport(this, selectedMonth,
                selectedMonthProjects);
    }

    private void exportPdf(File pdfFile)
    {
        try
        {
            Uri fileUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                fileUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID +
                        ".fileprovider", pdfFile);
            }
            else
            {
                fileUri = Uri.fromFile(pdfFile);
            }

            if (fileUri != null)
            {
                String mime = getContentResolver().getType(fileUri);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(fileUri, mime);
                intent.setFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);

                startActivity(intent);
            }
        }
        catch (NullPointerException | IllegalArgumentException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al exportar a PDF", Toast.LENGTH_SHORT).show();
        }
    }

    private static class CustomBarDataSet extends BarDataSet
    {
        public CustomBarDataSet(List<BarEntry> barEntries, String label)
        {
            super(barEntries, label);
        }

        @Override
        public int getColor(int index)
        {
            BarEntry entry = getEntryForIndex(index);
            float val = entry.getY();

            return val >= 25 ? mColors.get(0) : mColors.get(1);
        }
    }

    private static class YearMonthProjectsCollection
    {
        private String name;
        private long startTime;
        private long endTime;
        private int month;
        private int year;
        private Map<String, ProjectDispensers> projects = new HashMap<>();

        public YearMonthProjectsCollection()
        {
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public void setStartTime(long startTime)
        {
            this.startTime = startTime;
        }

        public void setEndTime(long endTime)
        {
            this.endTime = endTime;
        }

        public void setMonth(int month)
        {
            this.month = month;
        }

        public void setYear(int year)
        {
            this.year = year;
        }

        public String getName()
        {
            return name;
        }

        public long getStartTime()
        {
            return startTime;
        }

        public long getEndTime()
        {
            return endTime;
        }

        public int getMonth()
        {
            return month;
        }

        public int getYear()
        {
            return year;
        }

        public void addProjectDispensers(ProjectDispensers projectDispensers)
        {
            projects.put(projectDispensers.getOrderNumber(), projectDispensers);
        }

        public Map<String, ProjectDispensers> getProjects()
        {
            return projects;
        }

        public int getDispCount()
        {
            int count = 0;
            for (String key : projects.keySet())
            {
                count += Objects.requireNonNull(projects.get(key)).getDispModels().size();
            }

            return count;
        }
    }
}