package com.chipred.produccionspx.reports;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.adapters.IconArrayAdapter;
import com.chipred.produccionspx.cars.Trip;
import com.chipred.produccionspx.cars.TripOverviewAdapter;
import com.chipred.produccionspx.utils.DateRangePickerFragment;
import com.chipred.produccionspx.utils.DateUtils;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


public class CarTripsChartsActivity extends AppCompatActivity
{
    // Constantes
    private static final String TAG = CarTripsChartsActivity.class.getSimpleName();
    private static final int REQUEST_STORAGE_PERMISSION = 1;
    private static final String[] TRIP_FILTERS = {"Vehículo", "Conductor"};
    private static final String[] CHART_DATA_TYPES = {"Salidas", "Kilometros"};

    // Variables
    private boolean gotTrips = false;
    private Date startDate;
    private Date endDate;
    private String chartDataType = CHART_DATA_TYPES[0];
    private String selectedTripFilter = TRIP_FILTERS[0];

    // Objetos
    private ArrayList<String> xAxisEntries = new ArrayList<>();

    // Colecciones
    private HashMap<String, Trip> tripsMap = new HashMap<>();
    private HashMap<String, ArrayList<String>> tripsByVehicle = new HashMap<>();
    private HashMap<String, ArrayList<String>> tripsByUser = new HashMap<>();
    private HashMap<String, ArrayList<String>> tripsByPlace = new HashMap<>();

    // Instancia de bases de datos
    private CollectionReference tripsCollection = FirebaseFirestore.getInstance().collection(
            "viajes");

    // Views
    private HorizontalBarChart horizontalChart;
    private LinearLayout detailsLayout;
    private RecyclerView detailsList;
    private ProgressBar progressBar;
    private ProgressBar detailsListProgress;
    private TextView periodTv;
    private TextView selectedChartElementTv;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTitle("Salidas");
        setContentView(R.layout.activity_charts);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        detailsLayout = findViewById(R.id.chart_element_details_layout);
        horizontalChart = findViewById(R.id.horizontal_bar_chart);
        progressBar = findViewById(R.id.progress_bar);
        detailsListProgress = findViewById(R.id.projects_list_progress);
        detailsList = findViewById(R.id.projects_list);
        selectedChartElementTv = findViewById(R.id.selected_chart_element_tv);
        periodTv = findViewById(R.id.period_tv);

        // Definir fechas
        setInstanceValues(savedInstanceState);

        // Obtener salidas en el rango de fecha seleccionado
        getTrips();
    }

    private void setInstanceValues(Bundle savedInstanceState)
    {
        Long startDateTime = null;
        Long endDateTime = null;

        if (savedInstanceState != null)
        {
            // Obtener fechas
            startDateTime = savedInstanceState.getLong("startDate");
            endDateTime = savedInstanceState.getLong("endDate");

            if (startDateTime == 0) startDateTime = null;
            if (endDateTime == 0) endDateTime = null;

            // Obtener filtros
            chartDataType = savedInstanceState.getString("chartDataType", CHART_DATA_TYPES[0]);
            selectedTripFilter = savedInstanceState.getString("tripFilter", TRIP_FILTERS[0]);
        }

        if (startDateTime != null && endDateTime != null)
        {
            startDate = new Date(startDateTime);
            endDate = new Date(endDateTime);
        }
        else
        {
            // Obtener rango de semana actual como rango default
            startDate = DateUtils.getFirstDateOfTheWeek();
            endDate = DateUtils.getLastDateOfTheWeek();
        }

        // Obtener textos
        String startDateStr = Constants.dateObjectToString(startDate);
        String endDateStr = Constants.dateObjectToString(endDate);
        String periodStr = String.format(Locale.getDefault(), "%s - %s", startDateStr, endDateStr);
        periodTv.setText(periodStr);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState)
    {
        if (startDate != null)
            outState.putLong("startDate", startDate.getTime());
        if (endDate != null)
            outState.putLong("endDate", endDate.getTime());
        outState.putString("chartDataType", chartDataType);
        outState.putString("tripFilter", selectedTripFilter);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.trips_reports_menu, menu);

        MenuItem item = menu.findItem(R.id.trips_filter);
        Spinner spinner = (Spinner) item.getActionView();

        ArrayList<Drawable> iconsList = new ArrayList<>();
        iconsList.add(ContextCompat.getDrawable(this, R.drawable.ic_car_white));
        iconsList.add(ContextCompat.getDrawable(this, R.drawable.ic_users));

        IconArrayAdapter adapter = new IconArrayAdapter(this, iconsList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                // Obtener filtro seleccionado por posición
                selectedTripFilter = TRIP_FILTERS[i];

                if (gotTrips)
                    onGotTrips();

                detailsLayout.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        else if (item.getItemId() == R.id.date_range)
        {
            DateRangePickerFragment dateRangePickerFragment = new DateRangePickerFragment();
            dateRangePickerFragment.setInitialDateRange(startDate, endDate);
            dateRangePickerFragment.setDateRangePickerInterface((startDate, endDate) ->
            {
                // Definir nuevo rango
                this.startDate = startDate;
                this.endDate = endDate;

                // Obtener textos
                String startDateStr = Constants.dateObjectToString(startDate);
                String endDateStr = Constants.dateObjectToString(endDate);
                String periodStr = String.format(Locale.getDefault(), "%s - %s", startDateStr,
                        endDateStr);
                periodTv.setText(periodStr);

                // Ejecutar query para obtener viajes
                getTrips();
            });
            dateRangePickerFragment.show(getSupportFragmentManager(), "dateRangePicker");
        }
        else if (item.getItemId() == R.id.chart_data_type)
        {
            if (chartDataType.equals(CHART_DATA_TYPES[0]))
            {
                // Cambiar de "Salidas" a "Kilometrajes"
                chartDataType = CHART_DATA_TYPES[1];
            }
            else
            {
                // Cambiar de "Kilometrajes" a "Salidas"
                chartDataType = CHART_DATA_TYPES[0];
            }

            onGotTrips();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE_PERMISSION)
        {
            // Obtener primer resultado
            if (grantResults.length > 0)
            {
                if (grantResults[0] == RESULT_OK)
                {
                    Toast.makeText(this, "Permiso concedido", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, "Permiso no concedido", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(this, "Permiso no concedido", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setChartData()
    {
        long maxVal = 0;

        List<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < xAxisEntries.size(); i++)
        {
            ArrayList<String> tripsList = getFilteredTripsList(i);

            long yValue;
            if (chartDataType.equals(CHART_DATA_TYPES[0]))
            {
                // Obtener suma de salidas
                yValue = tripsList == null ? 0 : tripsList.size();
            }
            else
            {
                // Obtener suma de kilometrajes
                yValue = tripsList == null ? 0 : tripsDistanceSum(tripsList);
            }

            entries.add(new BarEntry(i, yValue));
            if (yValue > maxVal) maxVal = yValue;
        }

        CustomBarDataSet barDataSet = new CustomBarDataSet(entries, getChartDesc());
        barDataSet.setColors(ContextCompat.getColor(this, getChartColor()));
        barDataSet.setFormSize(20);
        barDataSet.setValueTextSize(12);
        barDataSet.setValueFormatter(new ValueFormatter()
        {
            @Override
            public String getFormattedValue(float value)
            {
                return "" + (int) value;
            }
        });

        BarData data = new BarData(barDataSet);

        XAxis xAxis = horizontalChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisEntries));
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setTextSize(12f);

        YAxis yAxis = horizontalChart.getAxisLeft();
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum((float) (maxVal + (maxVal * 0.2)));

        Description description = new Description();
        description.setText("");
        horizontalChart.setDescription(description);
        horizontalChart.getAxisRight().setEnabled(false);
        horizontalChart.setData(data);
        horizontalChart.setScaleXEnabled(false);
        horizontalChart.animateY(500);
        horizontalChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener()
        {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {
                onEntrySelected((int) e.getX());
                detailsLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected()
            {
                detailsLayout.setVisibility(View.GONE);
            }
        });
    }

    private long tripsDistanceSum(ArrayList<String> trips)
    {
        long totalDistance = 0;

        for (String tripId : trips)
        {
            // Obtener objeto de salida
            Trip mTrip = tripsMap.get(tripId);

            // Evaluar viaje
            if (mTrip != null && mTrip.getTripDistance() != null)
            {
                // Sumar distancia de viaje
                totalDistance += mTrip.getTripDistance();
            }
        }

        return totalDistance;
    }

    private ArrayList<String> getFilteredTripsList(int idx)
    {
        if (selectedTripFilter.equals(TRIP_FILTERS[0]))
        {
            // Por vehículo
            return tripsByVehicle.get(xAxisEntries.get(idx));
        }
        else if (selectedTripFilter.equals(TRIP_FILTERS[1]))
        {
            // Por usuario
            return tripsByUser.get(xAxisEntries.get(idx));
        }

        return null;
    }

    private String getChartDesc()
    {
        String dataType;
        if (chartDataType.equals(CHART_DATA_TYPES[0]))
        {
            // Salidas
            dataType = "Salidas por ";
        }
        else
        {
            // Kilometrajes
            dataType = "Kilómetros por ";
        }

        if (selectedTripFilter.equals(TRIP_FILTERS[0]))
        {
            // Por vehículo
            return dataType + "vehículo";
        }
        else if (selectedTripFilter.equals(TRIP_FILTERS[1]))
        {
            // Por usuario
            return dataType + "usuario";
        }

        return null;
    }

    private int getChartColor()
    {
        int resource;
        if (chartDataType.equals(CHART_DATA_TYPES[0]))
        {
            // Salidas
            resource = android.R.color.holo_orange_dark;
        }
        else
        {
            // Kilometrajes
            resource = android.R.color.holo_blue_light;
        }

        return resource;
    }

    @SuppressWarnings({"null"})
    private void getTrips()
    {
        // Preparar views
        periodTv.setVisibility(View.GONE);
        horizontalChart.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.GONE);
        detailsList.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        // Limpiar listas
        tripsMap.clear();
        tripsByVehicle.clear();
        tripsByPlace.clear();
        tripsByUser.clear();

        // Crear query
        Query query = tripsCollection.orderBy("startDate")
                .whereGreaterThanOrEqualTo("startDate", startDate.getTime())
                .whereLessThan("startDate", endDate.getTime());

        // Ejecutar query
        query.get().addOnCompleteListener(task ->
        {
            if (task.isSuccessful())
            {
                if (task.getResult() != null)
                {
                    // Pasar documentos a mapa de Trips
                    for (DocumentSnapshot document : task.getResult().getDocuments())
                    {
                        // Convertir documento a objeto Trip
                        Trip mTrip = document.toObject(Trip.class);

                        // Obtener id de documento
                        String id = document.getId();

                        // Agregar a mapa
                        if (mTrip != null && mTrip.getCurrentState().equals(Trip.State.TERMINADA.name()))
                            tripsMap.put(id, mTrip);
                    }

                    Log.d(TAG, "Viajes en fecha obtenidos");
                    gotTrips = true;
                    onGotTrips();
                }
                else
                {
                    Log.e(TAG, "No hay viajes registrados");
                }
            }
            else
            {
                Log.e(TAG, "Error al obtener registro de viajes");
            }
        });
    }

    private void onGotTrips()
    {
        orderTripsByFilter();
        getXAxisList();
        setChartData();

        progressBar.setVisibility(View.GONE);
        horizontalChart.setVisibility(View.VISIBLE);
        periodTv.setVisibility(View.VISIBLE);
    }

    private void orderTripsByFilter()
    {
        if (selectedTripFilter.equals(TRIP_FILTERS[0]))
        {
            // Por vehículo
            orderTripsByVehicle();
        }
        else if (selectedTripFilter.equals(TRIP_FILTERS[1]))
        {
            // Por usuario
            orderTripsByUser();
        }
    }

    private void orderTripsByVehicle()
    {
        if (tripsByVehicle.isEmpty())
        {
            for (String key : tripsMap.keySet())
            {
                // Obtener viaje
                Trip trip = tripsMap.get(key);

                // Filtrar solo por viaje terminado
                if (trip != null && trip.getCurrentState().equals(Trip.State.TERMINADA.name()))
                {
                    // Obtener descripcion de vehículo
                    String vehicleDesc = trip.getVehicleDesc();

                    // Crear lista para obtener referencia
                    ArrayList<String> tripsRefs;

                    // Si aún no esta en el mapa...
                    if (!tripsByVehicle.containsKey(vehicleDesc))
                    {
                        // Crear nueva lista y agregar al mapa
                        tripsRefs = new ArrayList<>();
                        tripsByVehicle.put(vehicleDesc, tripsRefs);
                    }
                    else
                    {
                        // Obtener referencia de lista
                        tripsRefs = tripsByVehicle.get(vehicleDesc);
                    }

                    // Si no es nula la referencia, agregar
                    if (tripsRefs != null) tripsRefs.add(trip.getId());
                }
            }

            Log.d(TAG, "Viajes ordenados por vehículo");
        }
    }

    private void orderTripsByUser()
    {
        if (tripsByUser.isEmpty())
        {
            // Crear mapa para guardar relación de id's con nombre de usuario
            HashMap<String, String> usersNames = new HashMap<>();

            for (String key : tripsMap.keySet())
            {
                // Obtener viaje
                Trip trip = tripsMap.get(key);

                // Filtrar solo por viaje terminado
                if (trip != null && trip.getCurrentState().equals(Trip.State.TERMINADA.name()))
                {
                    // Obtener id del usuario
                    String userId = trip.getResponsibleUserId();

                    // Almacenar relación id - nombre de usuario
                    if (!usersNames.containsKey(userId))
                        usersNames.put(userId, trip.getUserName());

                    // Obtener nombre de usuario
                    String userKey = usersNames.get(userId);

                    // Crear lista para obtener referencia
                    ArrayList<String> tripsRefs;

                    // Si aún no esta en el mapa...
                    if (!tripsByUser.containsKey(userKey))
                    {
                        // Crear nueva lista y agregar al mapa
                        tripsRefs = new ArrayList<>();
                        tripsByUser.put(userKey, tripsRefs);
                    }
                    else
                    {
                        // Obtener referencia de lista
                        tripsRefs = tripsByUser.get(userKey);
                    }

                    // Si no es nula la referencia, agregar
                    if (tripsRefs != null) tripsRefs.add(trip.getId());
                }
            }

            Log.d(TAG, "Viajes ordenados por vehículo");
        }
    }

    private void getXAxisList()
    {
        xAxisEntries.clear();
        if (selectedTripFilter.equals(TRIP_FILTERS[0]))
        {
            // Por vehículo
            xAxisEntries.addAll(tripsByVehicle.keySet());
        }
        else if (selectedTripFilter.equals(TRIP_FILTERS[1]))
        {
            // Por usuario
            xAxisEntries.addAll(tripsByUser.keySet());
        }

        // Ordenar alfabéticamente
        Collections.sort(xAxisEntries, (s, t1) -> t1.compareTo(s));
    }

    private HashMap<String, ArrayList<String>> getFilteredMap()
    {
        if (selectedTripFilter.equals(TRIP_FILTERS[0]))
        {
            // Por vehículo
            return tripsByVehicle;
        }
        else if (selectedTripFilter.equals(TRIP_FILTERS[1]))
        {
            // Por usuario
            return tripsByUser;
        }

        return null;
    }

    private void onEntrySelected(int x)
    {
        LinearLayout tableHeader = findViewById(R.id.trips_detail_header_layout);
        detailsList.setVisibility(View.GONE);
        detailsListProgress.setVisibility(View.VISIBLE);
        tableHeader.setVisibility(View.GONE);

        // Obtener elemento seleccionado
        String desc = xAxisEntries.get(x);

        // Crear título de elemento seleccionado
        String title = String.format(Locale.getDefault(), "Salidas - %s", desc.toUpperCase());
        selectedChartElementTv.setText(title);

        // Obtener mapa de donde se obtendrá la lista de referencias
        HashMap<String, ArrayList<String>> currentMap = getFilteredMap();
        if (currentMap == null) return;

        // Crear lista con detalles del elemento seleccionado
        ArrayList<Trip> tripDetails = new ArrayList<>();
        for (String tripId : Objects.requireNonNull(currentMap.get(desc)))
        {
            tripDetails.add(tripsMap.get(tripId));
        }

        // Ordenar lista por fecha
        Collections.sort(tripDetails, (trip, t1) -> Long.compare(t1.getStartDate(),
                trip.getStartDate()));

        // Mostrar lista de proyectos
        TripOverviewAdapter tripOverviewAdapter = new TripOverviewAdapter(tripDetails);
        detailsList.setLayoutManager(new LinearLayoutManager(this));
        detailsList.setAdapter(tripOverviewAdapter);

        // Mostrar views
        detailsListProgress.setVisibility(View.GONE);
        detailsList.setVisibility(View.VISIBLE);
        tableHeader.setVisibility(View.VISIBLE);
    }

    private static class CustomBarDataSet extends BarDataSet
    {
        public CustomBarDataSet(List<BarEntry> barEntries, String label)
        {
            super(barEntries, label);
        }

        @Override
        public int getColor(int index)
        {
            return mColors.get(0);
        }
    }
}