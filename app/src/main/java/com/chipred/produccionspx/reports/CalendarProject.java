package com.chipred.produccionspx.reports;

public class CalendarProject {
    public String estacion;
    public String cliente;
    public Long fecha;
    public int tipo;

    public CalendarProject(String estacion, String cliente, Long fecha, int tipo) {
        this.estacion = estacion;
        this.cliente = cliente;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    public String getEstacion() {
        return estacion;
    }

    public String getCliente() {
        return cliente;
    }

    public Long getFecha() {
        return fecha;
    }

    public int getTipo() {
        return tipo;
    }
}
