package com.chipred.produccionspx.reports;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.chipred.produccionspx.BuildConfig;
import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.R;
import com.chipred.produccionspx.pdf.CalendarProjectsGenerator;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
import static com.chipred.produccionspx.reports.EventType.FINISH_DATE;
import static com.chipred.produccionspx.reports.EventType.REQUEST_DATE;
import static com.chipred.produccionspx.reports.EventType.SHIPMENT_DATE;
import static com.chipred.produccionspx.reports.EventType.START_DATE;

public class CalendarViewActivity extends AppCompatActivity {
    // Constantes
    private static final String TAG = CalendarViewActivity.class.getSimpleName();
    private static final int REQUEST_STORAGE_PERMISSION = 1;
    private static final String SOLICITUDES = "solicitudes";
    private static final String ENTREGAS = "entregas";
    private static final String EMBARQUES = "embarques";
    private static final String ARRANQUES = "arranques";

    // Variables
    private long startDate;
    private long endDate;

    // Objetos
    private ArrayList<EventType> currentDayEventsList;
    private final CollectionReference projectsCollection = FirebaseFirestore.getInstance().collection(
            "proyectos");
    private JSONObject datesByDay = new JSONObject();
    private JSONObject datesByMonth = new JSONObject();
    private final HashMap<String, Boolean> filtersValues = new HashMap<>();
    private final List<EventDay> events = new ArrayList<>();
    private final Set<String> requestedMonths = new HashSet<>();
    private final Set<Long> gotProjects = new HashSet<>();
    private SharedPreferences sharedPreferences;

    // Views
    private CalendarView calendarView;
    private EventTypeAdapter eventTypeAdapter;
    private ProgressDialog progressDialog;
    private RecyclerView eventsRecyclerView;
    private TextView noEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_view);

        //Agregar flecha para regresar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES,
                Context.MODE_PRIVATE);

        // Inicializar views
        initViews();
        initFiltersMap();

        // Inicializar datos de calendario
        setDateCalendarData();
    }

    private void initFiltersMap() {
        filtersValues.put(SOLICITUDES, getFilterPreference(SOLICITUDES));
        filtersValues.put(ENTREGAS, getFilterPreference(ENTREGAS));
        filtersValues.put(EMBARQUES, getFilterPreference(EMBARQUES));
        filtersValues.put(ARRANQUES, getFilterPreference(ARRANQUES));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calendar_view_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
        } else if (id == R.id.set_filter) {
            showCalendarFiltersDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(0);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void showCalendarFiltersDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View dialogContent = getLayoutInflater().inflate(R.layout.dialog_calendar_view_filters, null);
        builder.setView(dialogContent);

        final CheckBox solicitudesCb = dialogContent.findViewById(R.id.solicitudes_cb);
        final CheckBox entregasCb = dialogContent.findViewById(R.id.entregas_cb);
        final CheckBox embarquesCb = dialogContent.findViewById(R.id.embarques_cb);
        final CheckBox arranquesCb = dialogContent.findViewById(R.id.arranques_cb);

        solicitudesCb.setChecked(filtersValues.get(SOLICITUDES));
        entregasCb.setChecked(filtersValues.get(ENTREGAS));
        embarquesCb.setChecked(filtersValues.get(EMBARQUES));
        arranquesCb.setChecked(filtersValues.get(ARRANQUES));

        Button submitButton = dialogContent.findViewById(R.id.submit_button);
        Button cancelButton = dialogContent.findViewById(R.id.return_button);

        final Dialog dialog = builder.create();

        submitButton.setOnClickListener(view -> {
            HashMap<String, Boolean> filtersData = new HashMap<>();
            filtersData.put(SOLICITUDES, solicitudesCb.isChecked());
            filtersData.put(ENTREGAS, entregasCb.isChecked());
            filtersData.put(EMBARQUES, embarquesCb.isChecked());
            filtersData.put(ARRANQUES, arranquesCb.isChecked());
            saveFilterValues(filtersData);

            clearValues();
            setDateCalendarData();
            dialog.dismiss();
        });
        cancelButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    private void initViews() {
        // Obtener referencia de CalendarView
        calendarView = findViewById(R.id.calendar_view);
        calendarView.setOnDayClickListener(eventDay ->
        {
            Calendar calendar = eventDay.getCalendar();
            try {
                calendarView.setDate(calendar);
                getDayEvents(calendar.getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        OnCalendarPageChangeListener pageChangeListener = this::setDateCalendarData;
        calendarView.setOnPreviousPageChangeListener(pageChangeListener);
        calendarView.setOnForwardPageChangeListener(pageChangeListener);

        // Obtener views
        eventsRecyclerView = findViewById(R.id.recyclerView);
        noEvents = findViewById(R.id.calendar_no_events);
        FloatingActionButton pdfFab = findViewById(R.id.pdf_fab);

        pdfFab.setOnClickListener(view -> {
            checkPermissions();
        });
    }

    private void setDateCalendarData() {
        if (hasMonthYearRequested(getCurrentMonthYear())) {
            return;
        }

        // Mostrar progress
        progressDialog = ProgressDialog.show(this, "", "Obteniendo proyectos...");

        // Obtener mes actual
        Calendar currentMonthCalendar = calendarView.getCurrentPageDate();
        int currentMonth = currentMonthCalendar.get(Calendar.MONTH);
        int firstDay = currentMonthCalendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        int lastDay = currentMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int year = currentMonthCalendar.get(Calendar.YEAR);

        // Obtener fechas de inicio y final
        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.set(Calendar.MONTH, currentMonth);
        startDateCalendar.set(Calendar.DAY_OF_MONTH, firstDay);
        startDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startDateCalendar.set(Calendar.MINUTE, 0);
        startDateCalendar.set(Calendar.SECOND, 0);
        startDateCalendar.set(Calendar.YEAR, year);
        Date startDateObj = startDateCalendar.getTime();
        startDate = startDateObj.getTime();

        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.set(Calendar.MONTH, currentMonth);
        endDateCalendar.set(Calendar.DAY_OF_MONTH, lastDay);
        endDateCalendar.set(Calendar.HOUR_OF_DAY, 23);
        endDateCalendar.set(Calendar.MINUTE, 59);
        endDateCalendar.set(Calendar.SECOND, 59);
        endDateCalendar.set(Calendar.YEAR, year);
        Date endDateObj = endDateCalendar.getTime();
        endDate = endDateObj.getTime();

        getProjectByStatus();
    }

    private void getProjectByStatus() {
        Task<QuerySnapshot> requestedProjects = projectsCollection.whereGreaterThan(
                "fecha_solicitud", startDate)
                .whereLessThan("fecha_solicitud", endDate).get();

        Task<QuerySnapshot> finishedProjects = projectsCollection.whereGreaterThan(
                "fecha_terminado", startDate)
                .whereLessThan("fecha_terminado", endDate).get();

        Task<QuerySnapshot> shippedProjects = projectsCollection.whereGreaterThan(
                "fecha_embarque", startDate)
                .whereLessThan("fecha_embarque", endDate).get();

        Task<QuerySnapshot> startedProjects = projectsCollection.whereGreaterThan(
                "fecha_arranque", startDate)
                .whereLessThan("fecha_arranque", endDate).get();

        List<Task<QuerySnapshot>> tasks = new ArrayList<>();
        if (filtersValues.get(SOLICITUDES)) tasks.add(requestedProjects);
        if (filtersValues.get(ENTREGAS)) tasks.add(finishedProjects);
        if (filtersValues.get(EMBARQUES)) tasks.add(shippedProjects);
        if (filtersValues.get(ARRANQUES)) tasks.add(startedProjects);

        Task<List<QuerySnapshot>> allTasks = Tasks.whenAllSuccess(tasks);
        allTasks.addOnCompleteListener(task ->
        {
            if (task.getResult() != null) {
                // Obtener resultados de cada tarea
                for (QuerySnapshot querySnapshot : task.getResult()) {
                    // Obtener resultados de tarea en particular
                    for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                        addProjectToCalendar(document);
                    }
                }

                // Mostrar proyectos en calendario
                showCalendarProjects();
            } else {
                showErrorMessage();
                Log.e(TAG, "Error al obtener proyectos");
            }
        });
    }

    private void showErrorMessage() {
        if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
        Toast.makeText(CalendarViewActivity.this, "Error al obtener proyectos",
                Toast.LENGTH_SHORT).show();
    }

    private void addProjectToCalendar(DocumentSnapshot document) {
        Long orderNumber = document.getLong("numero_orden");
        if (!gotProjects.contains(orderNumber)) {
            // Obtener datos generales
            Map<String, Object> generalData =
                    (Map<String, Object>) document.get(
                            "datos_generales");

            // Obtener nombre de proyecto
            String projectStation = (String) generalData.get("estacion");
            String projectStationNumber = (String) generalData.get("numero_estacion");
            String projectClient = (String) generalData.get("cliente");

            projectStation += " ";
            projectStation += projectStationNumber;

            // Obtener fechas en su formato String
            String requestDateString = (String) generalData.get(
                    "fecha_solicitud_cliente");
            String finishDateString = (String) generalData.get(
                    "fecha_terminado");
            String shipmentString = (String) generalData.get(
                    "fecha_embarque");
            String startDateString = (String) generalData.get(
                    "fecha_arranque");

            // Agregar fechas al calendario
            if (filtersValues.get(SOLICITUDES))
                addDateToCalendar(requestDateString, projectStation,
                        projectClient, REQUEST_DATE);

            if (filtersValues.get(ENTREGAS))
                addDateToCalendar(finishDateString, projectStation, projectClient
                        , FINISH_DATE);

            if (filtersValues.get(EMBARQUES))
                addDateToCalendar(shipmentString, projectStation, projectClient,
                        SHIPMENT_DATE);

            if (filtersValues.get(ARRANQUES))
                addDateToCalendar(startDateString, projectStation, projectClient,
                        START_DATE);

            // Agregar a la lista de proyectos obtenidos
            gotProjects.add(orderNumber);
        }
    }

    private void showCalendarProjects() {
        Log.d(TAG, "Calendario JSON formado: ");
        Log.d(TAG, datesByDay.toString());
        Log.d(TAG, datesByMonth.toString());

        // Asignar eventos al calendario
        setCalendarEvents();
        calendarView.setEvents(events);

        // Cerrar progressDialog
        if (progressDialog != null) progressDialog.dismiss();

        // Obtener los eventos del día actual
        getDayEvents(new Date());

        // Agregar mes a la lista de meses guardados
        requestedMonths.add(getCurrentMonthYear());
    }

    private void addDateToCalendar(String day, String station, String client, int dateType) {
        // Regresar si la fecha es vacía
        if (day == null || day.isEmpty()) return;

        // Obtener solo el mes
        String yearMonth = day.substring(day.indexOf("/") + 1);

        try {
            // Obtener día
            JSONObject currentDay = datesByDay.optJSONObject(day);
            if (currentDay == null) currentDay = new JSONObject();

            // Obtener mes
            JSONObject currentMonth = datesByMonth.optJSONObject(yearMonth);
            if (currentMonth == null) currentMonth = new JSONObject();

            // Obtener arreglo de eventos
            JSONArray dayEvents;
            dayEvents = currentDay.optJSONArray("eventos");
            if (dayEvents == null) dayEvents = new JSONArray();

            JSONArray monthEvents;
            monthEvents = currentMonth.optJSONArray("eventos");
            if (monthEvents == null) monthEvents = new JSONArray();

            // Crear objeto evento
            JSONObject event = new JSONObject();
            event.put("estacion", station);
            event.put("cliente", client);
            event.put("tipo", dateType);
            event.put("fecha", Constants.dateStringToObject2(day).getTime());

            // Agregar evento
            dayEvents.put(event);
            monthEvents.put(event);

            // Actualizar arreglo
            currentDay.put("eventos", dayEvents);
            currentMonth.put("eventos", monthEvents);

            // Actualizar icono
            String icon = currentDay.optString("icono");
            String dateIcon = String.valueOf(dateType);
            if (!icon.contains(dateIcon)) {
                icon = appendNumber(icon, dateIcon);
            }
            currentDay.put("icono", icon);

            // Actualizar JSON principal
            datesByDay.put(day, currentDay);
            datesByMonth.put(yearMonth, currentMonth);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String appendNumber(String icon, String dateIcon) {
        // Crear StringBuilder para manejar el append de datos
        StringBuilder stringBuilder = new StringBuilder(icon);

        for (int i = 0; i < stringBuilder.length(); i++) {
            //Obtener valor decimal del caracter
            int currentIdxVal = stringBuilder.charAt(i) - 0x30;

            //Obtener valor decimal de número a agregar
            int numberToAdd = Integer.parseInt(dateIcon);

            //Agregar elemento
            if (currentIdxVal > numberToAdd) {
                stringBuilder.insert(i, dateIcon);
                return stringBuilder.toString();
            }

            //Si no cumple la condición, seguir
        }

        //Salió del ciclo, agregar al último índice
        stringBuilder.append(dateIcon);

        //Retornar valor
        return stringBuilder.toString();
    }

    private void setCalendarEvents() {
        // Obtener todos los keys
        Iterator<String> iter = datesByDay.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                // Obtener objeto JSON
                JSONObject currentDate = datesByDay.getJSONObject(key);

                // Convertir fecha a date
                Date date = Constants.dateStringToObject2(key);

                // Obtener drawable
                String iconName = "calendar_" + currentDate.getString("icono");
                int id = getResources().getIdentifier(iconName, "drawable", getPackageName());

                // Crear instancia de calendario
                Calendar c = Calendar.getInstance();
                c.setTime(date);

                // Agregar a la lista de eventos del calendario
                events.add(new EventDay(c, id));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getDayEvents(Date date) {
        // Convertir fecha a String
        String strDate = Constants.dateObjectToString(date);

        // Evaluar que existan eventos ese día
        if (!datesByDay.has(strDate)) {
            noEvents.setVisibility(View.VISIBLE);
            eventsRecyclerView.setVisibility(View.GONE);
            return;
        }

        // Obtener objeto JSON
        try {
            // Obtener objetos
            JSONObject currentDate = datesByDay.getJSONObject(strDate);
            JSONArray events = currentDate.getJSONArray("eventos");

            // Crear JSON organizado por tipos de eventos
            // Crear lista de eventos
            if (currentDayEventsList == null) currentDayEventsList = new ArrayList<>();
            currentDayEventsList.clear();

            for (int i = 0; i < events.length(); i++) {
                // Obtener evento
                JSONObject event = events.getJSONObject(i);

                // Obtener tipo
                String estacion = event.optString("estacion");
                String cliente = event.optString("cliente");
                String type = event.optString("tipo");
                Long projectDate = event.optLong("fecha");

                CalendarProject calendarProject = new CalendarProject(
                        estacion,
                        cliente,
                        projectDate,
                        Integer.parseInt(type));

                // Obtener indice de la lista correspondiente
                int idx = getEventTypeIdx(currentDayEventsList, type);
                if (idx == -1) {
                    // Crear nuevo objeto
                    EventType eventType = new EventType(type);
                    eventType.addEvent(calendarProject);

                    // Agregar a la lista
                    currentDayEventsList.add(eventType);
                } else {
                    // Obtener referencia
                    EventType eventType = currentDayEventsList.get(idx);
                    eventType.addEvent(calendarProject);
                }
            }

            if (currentDayEventsList.isEmpty()) {
                noEvents.setVisibility(View.VISIBLE);
                eventsRecyclerView.setVisibility(View.GONE);
            } else {
                noEvents.setVisibility(View.GONE);
                eventsRecyclerView.setVisibility(View.VISIBLE);

                if (eventTypeAdapter == null) {
                    // Crear adaptador
                    eventTypeAdapter = new EventTypeAdapter(this, currentDayEventsList);
                    eventsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                    eventsRecyclerView.setAdapter(eventTypeAdapter);
                } else {
                    eventTypeAdapter.notifyDataSetChanged();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int getEventTypeIdx(ArrayList<EventType> eventTypes, String type) {
        int idx = -1;
        for (int i = 0; i < eventTypes.size(); i++) {
            if (eventTypes.get(i).getType().equals(type)) {
                return i;
            }
        }

        return idx;
    }

    private boolean hasMonthYearRequested(String monthYear) {
        return requestedMonths.contains(monthYear);
    }

    private String getCurrentMonthYear() {
        Calendar currentMonth = calendarView.getCurrentPageDate();
        int month = currentMonth.get(Calendar.MONTH);
        int year = currentMonth.get(Calendar.YEAR);

        return String.format(Locale.getDefault(), "%02d%04d", month, year);
    }

    private void clearValues() {
        datesByDay = new JSONObject();
        datesByMonth = new JSONObject();
        events.clear();
        requestedMonths.clear();
        gotProjects.clear();
    }

    private void saveFilterPreference(String key, boolean val) {
        sharedPreferences.edit().putBoolean(key + "_filter", val).apply();
    }

    private boolean getFilterPreference(String key) {
        return sharedPreferences.getBoolean(key + "_filter", true);
    }

    private void saveFilterValues(HashMap<String, Boolean> filtersData) {
        filtersValues.put(SOLICITUDES, filtersData.get(SOLICITUDES));
        saveFilterPreference(SOLICITUDES, filtersData.get(SOLICITUDES));

        filtersValues.put(ENTREGAS, filtersData.get(ENTREGAS));
        saveFilterPreference(ENTREGAS, filtersData.get(ENTREGAS));

        filtersValues.put(EMBARQUES, filtersData.get(EMBARQUES));
        saveFilterPreference(EMBARQUES, filtersData.get(EMBARQUES));

        filtersValues.put(ARRANQUES, filtersData.get(ARRANQUES));
        saveFilterPreference(ARRANQUES, filtersData.get(ARRANQUES));
    }

    private void generatePdf() {
        // Obtener mes
        String monthString = Constants.dateObjectToYearMonthString(calendarView.getCurrentPageDate().getTime());

        // Buscar que tenga eventos
        if (datesByMonth.has(monthString)) {
            Toast.makeText(this, "Generando PDF para el mes " + monthString, Toast.LENGTH_SHORT).show();

            CalendarProjectsGenerator calendarProjectsGenerator = new CalendarProjectsGenerator(
                    new Handler(message ->
                    {
                        String filePath = (String) message.obj;

                        if (!filePath.isEmpty()) {
                            exportPdf(new File(filePath));
                        } else {
                            Toast.makeText(this, "Error al generar PDF", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "Error al obtener path de archivo");
                        }

                        return false;
                    })
            );

            try {
                JSONArray events = datesByMonth.getJSONObject(monthString).getJSONArray("eventos");
                calendarProjectsGenerator.generateMonthProjectsReport(this, monthString, events);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Error al procesar la información", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void exportPdf(File pdfFile) {
        try {
            Uri fileUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                fileUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID +
                        ".fileprovider", pdfFile);
            } else {
                fileUri = Uri.fromFile(pdfFile);
            }

            if (fileUri != null) {
                String mime = getContentResolver().getType(fileUri);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(fileUri, mime);
                intent.setFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);

                startActivity(intent);
            }
        } catch (NullPointerException | IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error al exportar a PDF", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                generatePdf();
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);
            }
        } else {
            Log.v(TAG, "Permission is granted");
            generatePdf();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            // Obtener primer resultado
            if (grantResults.length > 0) {
                if (grantResults[0] == RESULT_OK) {
                    Toast.makeText(this, "Permiso concedido", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permiso no concedido", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Permiso no concedido", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
