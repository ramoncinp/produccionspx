package com.chipred.produccionspx.reports;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class EventElementAdapter extends RecyclerView.Adapter<EventElementAdapter.EventViewHolder> {
    private final ArrayList<CalendarProject> projects;

    public EventElementAdapter(ArrayList<CalendarProject> projects) {
        this.projects = projects;
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.calendar_event_element,
                        null);

        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder viewHolder, int i) {
        // Obtener proyecto
        CalendarProject project = projects.get(i);
        viewHolder.client.setText(project.getCliente());
        viewHolder.station.setText(project.getEstacion());
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    static class EventViewHolder extends RecyclerView.ViewHolder {
        private final TextView client;
        private final TextView station;

        EventViewHolder(@NonNull View itemView) {
            super(itemView);
            client = itemView.findViewById(R.id.client);
            station = itemView.findViewById(R.id.station);
        }
    }
}
