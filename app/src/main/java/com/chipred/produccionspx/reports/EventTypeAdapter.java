package com.chipred.produccionspx.reports;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chipred.produccionspx.R;

import java.util.ArrayList;

public class EventTypeAdapter extends RecyclerView.Adapter<EventTypeAdapter.EventsViewHolder>
{
    private final ArrayList<EventType> events;
    private final Context context;

    public EventTypeAdapter(Context context, ArrayList<EventType> events)
    {
        this.context = context;
        this.events = events;
    }

    @NonNull
    @Override
    public EventsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.calendar_event_type,
                null);

        return new EventsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EventsViewHolder viewHolder, int i)
    {
        // Obtener datos
        EventType eventType = events.get(i);

        // Definir nombre
        switch (Integer.parseInt(eventType.getType()))
        {
            case EventType.REQUEST_DATE:
                viewHolder.name.setText(context.getResources().getString(R.string.event_type_request));
                viewHolder.icon.setImageResource(R.drawable.calendar_1);
                break;

            case EventType.FINISH_DATE:
                viewHolder.name.setText(context.getResources().getString(R.string.event_type_finish));
                viewHolder.icon.setImageResource(R.drawable.calendar_2);
                break;

            case EventType.SHIPMENT_DATE:
                viewHolder.name.setText(context.getResources().getString(R.string.event_type_shipment));
                viewHolder.icon.setImageResource(R.drawable.calendar_3);
                break;

            case EventType.START_DATE:
                viewHolder.name.setText(context.getResources().getString(R.string.event_type_start));
                viewHolder.icon.setImageResource(R.drawable.calendar_4);
                break;
        }

        EventElementAdapter elementAdapter = new EventElementAdapter(eventType.getEvents());

        // Obtener RecyclerView
        viewHolder.projectsList.setAdapter(elementAdapter);
        viewHolder.projectsList.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    public int getItemCount()
    {
        return events.size();
    }

    static class EventsViewHolder extends RecyclerView.ViewHolder
    {
        private final ImageView icon;
        private final RecyclerView projectsList;
        private final TextView name;

        EventsViewHolder(@NonNull View itemView)
        {
            super(itemView);
            icon = itemView.findViewById(R.id.event_type_icon);
            name = itemView.findViewById(R.id.event_type_name);
            projectsList = itemView.findViewById(R.id.events_list);
        }
    }
}
