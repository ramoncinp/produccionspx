package com.chipred.produccionspx.reports;

import java.util.ArrayList;

public class EventType {
    public static final int REQUEST_DATE = 1;
    public static final int FINISH_DATE = 2;
    public static final int SHIPMENT_DATE = 3;
    public static final int START_DATE = 4;
    private final String type;
    private final ArrayList<CalendarProject> events;

    public EventType(String type) {
        this.type = type;
        events = new ArrayList<>();
    }

    public ArrayList<CalendarProject> getEvents() {
        return events;
    }

    public void addEvent(CalendarProject event) {
        events.add(event);
    }

    public String getType() {
        return type;
    }

}
