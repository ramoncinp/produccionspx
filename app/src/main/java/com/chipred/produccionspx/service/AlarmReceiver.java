package com.chipred.produccionspx.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.chipred.produccionspx.Constants;
import com.chipred.produccionspx.notifications.NotificationsCreator;

public class AlarmReceiver extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {
        // Obtener id de proyecto a alertar
        String projectId = intent.hasExtra("projectId") ? intent.getStringExtra("projectId") : " " +
                "desconocido";

        // Obtener nombre de proyecto
        String projectName = intent.hasExtra("projectName") ?
                intent.getStringExtra("projectName") : " " +
                "desconocido";

        // Crear creador de Notificaciones
        NotificationsCreator notificationsCreator = new NotificationsCreator(context);
        notificationsCreator.setSharedPreferences(context.getSharedPreferences(Constants.SHARED_PREFERENCES,
                Context.MODE_PRIVATE));
        notificationsCreator.setUsername("");
        notificationsCreator.setElemetId(projectId);

        // Definir valores de la notificación
        String notificationTitle = "Proyecto inactivo";

        // Evaluar SDK
        if (Build.VERSION.SDK_INT >= 24)
        {
            notificationsCreator.showBundledNotification(notificationTitle,
                    projectName);
        }
        else
        {
            notificationsCreator.showSimpleNotification(notificationTitle,
                    projectName);
        }
    }
}
